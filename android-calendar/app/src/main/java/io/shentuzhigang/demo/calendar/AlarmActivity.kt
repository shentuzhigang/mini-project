package io.shentuzhigang.demo.calendar

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.calendar.util.DateUtil
import java.util.*

@SuppressLint("StaticFieldLeak")
class AlarmActivity : AppCompatActivity(), View.OnClickListener {
    private var mDelay = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)
        tv_alarm = findViewById(R.id.tv_alarm)
        findViewById<View>(R.id.btn_alarm).setOnClickListener(this)
        initDelaySpinner()
    }

    // 初始化闹钟延迟的下拉框
    private fun initDelaySpinner() {
        val delayAdapter = ArrayAdapter(
            this,
            R.layout.item_select, delayDescArray
        )
        val sp_delay = findViewById<Spinner>(R.id.sp_delay)
        sp_delay.prompt = "请选择闹钟延迟"
        sp_delay.adapter = delayAdapter
        sp_delay.onItemSelectedListener = DelaySelectedListener()
        sp_delay.setSelection(0)
    }

    private val delayArray = intArrayOf(5, 10, 15, 20, 25, 30)
    private val delayDescArray = arrayOf("5秒", "10秒", "15秒", "20秒", "25秒", "30秒")

    internal inner class DelaySelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            mDelay = delayArray[arg2]
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_alarm) {
            // 创建一个广播事件的意图
            val intent = Intent(ALARM_EVENT)
            // 创建一个用于广播的延迟意图
            val pIntent = PendingIntent.getBroadcast(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            // 从系统服务中获取闹钟管理器
            val alarmMgr = getSystemService(ALARM_SERVICE) as AlarmManager
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = System.currentTimeMillis()
            // 给当前时间加上若干秒
            calendar.add(Calendar.SECOND, mDelay)
            // 开始设定闹钟，延迟若干秒后，携带延迟意图发送闹钟广播
            alarmMgr[AlarmManager.RTC_WAKEUP, calendar.timeInMillis] = pIntent
            mDesc = DateUtil.nowTime + " 设置闹钟"
            tv_alarm!!.text = mDesc
        }
    }

    // 声明一个闹钟广播事件的标识串
    private val ALARM_EVENT = "io.shentuzhigang.demo.calendar.AlarmActivity.AlarmReceiver"

    // 定义一个闹钟广播的接收器
    class AlarmReceiver : BroadcastReceiver() {
        // 一旦接收到闹钟时间到达的广播，马上触发接收器的onReceive方法
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(TAG, "AlarmReceiver onReceive")
            if (tv_alarm != null && !isArrived) {
                isArrived = true
                mDesc = String.format("%s\n%s 闹钟时间到达", mDesc, DateUtil.nowTime)
                tv_alarm!!.text = mDesc
            }
        }
    }

    // 适配Android9.0开始
    public override fun onStart() {
        super.onStart()
        // 从Android9.0开始，系统不再支持静态广播，应用广播只能通过动态注册
        // 创建一个闹钟的广播接收器
        alarmReceiver = AlarmReceiver()
        // 创建一个意图过滤器，只处理指定事件来源的广播
        val filter = IntentFilter(ALARM_EVENT)
        // 注册广播接收器，注册之后才能正常接收广播
        registerReceiver(alarmReceiver, filter)
    }

    public override fun onStop() {
        super.onStop()
        // 注销广播接收器，注销之后就不再接收广播
        unregisterReceiver(alarmReceiver)
    }

    // 声明一个闹钟的广播接收器
    private var alarmReceiver: AlarmReceiver? = null // 适配Android9.0结束

    companion object {
        private const val TAG = "AlarmActivity"
        private var tv_alarm: TextView? = null
        private var mDesc = "" // 闹钟时间到达的描述
        private var isArrived = false // 闹钟时间是否到达
    }
}