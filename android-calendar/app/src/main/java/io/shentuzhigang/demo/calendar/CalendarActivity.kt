package io.shentuzhigang.demo.calendar

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.PagerTabStrip
import androidx.viewpager.widget.ViewPager
import io.shentuzhigang.demo.calendar.util.DateUtil
import io.shentuzhigang.demo.calendar.widget.MonthPicker
import io.shentuzhigang.demo.calendar.adapter.CalendarPagerAdapter
@SuppressLint("SetTextI18n")
class CalendarActivity : AppCompatActivity(), View.OnClickListener {
    private var ll_calendar_main // 声明一个万年历区域的线性布局对象
            : LinearLayout? = null
    private var ll_month_select // 声明一个月份选择区域的线性布局对象
            : LinearLayout? = null
    private var mp_month // 声明一个月份选择器对象
            : MonthPicker? = null
    private var vp_calendar // 声明一个翻页视图对象
            : ViewPager? = null
    private var tv_calendar // 声明一个选中年份的文本视图对象
            : TextView? = null
    private var isShowSelect = false // 是否显示月份选择器
    private var mSelectedYear = 2000 // 当前选中的年份
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)
        ll_calendar_main = findViewById(R.id.ll_calendar_main)
        ll_month_select = findViewById(R.id.ll_month_select)
        // 从布局文件中获取名叫mp_month的月份选择器
        mp_month = findViewById<MonthPicker>(R.id.mp_month)
        // 从布局文件中获取名叫pts_calendar的翻页标题栏
        findViewById<View>(R.id.btn_ok).setOnClickListener(this)
        val pts_calendar = findViewById<PagerTabStrip>(R.id.pts_calendar)
        pts_calendar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)
        pts_calendar.setTextColor(Color.BLACK)
        // 从布局文件中获取名叫vp_calendar的翻页视图
        vp_calendar = findViewById(R.id.vp_calendar)
        tv_calendar = findViewById(R.id.tv_calendar)
        with(tv_calendar){
            this?.setOnClickListener(this@CalendarActivity)
        }
        // 万年历默认显示当前年月的月历
        showCalendar(DateUtil.nowYear, DateUtil.nowMonth)
    }

    // 显示指定年月的万年历
    private fun showCalendar(year: Int, month: Int) {
        // 如果指定年份不是上次选中的年份，则需重新构建该年份的年历
        if (year != mSelectedYear) {
            tv_calendar!!.text = year.toString() + "年"
            // 构建一个指定年份的年历翻页适配器
            val adapter = CalendarPagerAdapter(supportFragmentManager, year)
            // 给vp_calendar设置年历翻页适配器
            vp_calendar!!.adapter = adapter
            mSelectedYear = year
        }
        // 设置vp_calendar默认显示指定月份的月历页
        vp_calendar!!.currentItem = month - 1
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tv_calendar) { // 点击了年份文本
            // 重新选择万年历的年月
            resetPage()
        } else if (v.id == R.id.btn_ok) { // 点击了确定按钮
            // 根据月份选择器上设定的年月，刷新万年历的显示界面
            showCalendar(mp_month!!.getYear(), mp_month!!.getMonth() + 1)
            resetPage()
        }
    }

    // 重置页面。在显示万年历主页面和显示月份选择器之间切换
    private fun resetPage() {
        isShowSelect = !isShowSelect
        ll_calendar_main!!.visibility = if (isShowSelect) View.GONE else View.VISIBLE
        ll_month_select!!.visibility = if (isShowSelect) View.VISIBLE else View.GONE
    }

    companion object {
        private const val TAG = "CalendarActivity"
    }
}