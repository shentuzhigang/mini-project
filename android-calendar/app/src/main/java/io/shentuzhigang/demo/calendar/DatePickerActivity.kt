package io.shentuzhigang.demo.calendar

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

@SuppressLint("DefaultLocale") // 该页面类实现了接口OnDateSetListener，意味着要重写日期监听器的onDateSet方法
class DatePickerActivity : AppCompatActivity(), View.OnClickListener, OnDateSetListener {
    private var tv_date: TextView? = null
    private var dp_date // 声明一个日期选择器对象
            : DatePicker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date_picker)
        tv_date = findViewById(R.id.tv_date)
        // 从布局文件中获取名叫dp_date的日期选择器
        dp_date = findViewById(R.id.dp_date)
        findViewById<View>(R.id.btn_date).setOnClickListener(this)
        findViewById<View>(R.id.btn_ok).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_date) {
            // 获取日历的一个实例，里面包含了当前的年月日
            val calendar = Calendar.getInstance()
            // 构建一个日期对话框，该对话框已经集成了日期选择器。
            // DatePickerDialog的第二个构造参数指定了日期监听器
            val dialog = DatePickerDialog(
                this, this,
                calendar[Calendar.YEAR],  // 年份
                calendar[Calendar.MONTH],  // 月份
                calendar[Calendar.DAY_OF_MONTH]
            ) // 日子
            // 把日期对话框显示在界面上
            dialog.show()
        } else if (v.id == R.id.btn_ok) {
            // 获取日期选择器dp_date设定的年月份
            val desc = String.format(
                "您选择的日期是%d年%d月%d日",
                dp_date!!.year, dp_date!!.month + 1, dp_date!!.dayOfMonth
            )
            tv_date!!.text = desc
        }
    }

    // 一旦点击日期对话框上的确定按钮，就会触发监听器的onDateSet方法
    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        // 获取日期对话框设定的年月份
        val desc = String.format(
            "您选择的日期是%d年%d月%d日",
            year, monthOfYear + 1, dayOfMonth
        )
        tv_date!!.text = desc
    }
}