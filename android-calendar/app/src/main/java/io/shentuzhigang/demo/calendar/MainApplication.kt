package io.shentuzhigang.demo.calendar

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import java.util.*

class MainApplication : Application() {
    // 声明一个公共的图标映射对象，可当作全局变量使用
    var mIconMap = HashMap<Long, Bitmap>()
    override fun onCreate() {
        super.onCreate()
        // 在打开应用时对静态的应用实例赋值
        mApp = this
        Log.d(TAG, "onCreate")
    }

    override fun onTerminate() {
        Log.d(TAG, "onTerminate")
        super.onTerminate()
    }

    companion object {
        private const val TAG = "MainApplication"

        // 声明一个当前应用的静态实例
        private var mApp: MainApplication? = null

        // 利用单例模式获取当前应用的唯一实例
        fun getInstance(): MainApplication? {
            return mApp
        }
    }
}