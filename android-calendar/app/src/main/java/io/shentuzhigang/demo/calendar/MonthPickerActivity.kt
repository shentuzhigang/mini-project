package io.shentuzhigang.demo.calendar

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.calendar.widget.MonthPicker

@SuppressLint("DefaultLocale")
class MonthPickerActivity : AppCompatActivity(), View.OnClickListener {
    private var tv_month: TextView? = null
    private var mp_month // 声明一个月份选择器对象
            : MonthPicker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_month_picker)
        tv_month = findViewById(R.id.tv_month)
        // 从布局文件中获取名叫mp_month的月份选择器
        mp_month = findViewById<MonthPicker>(R.id.mp_month)
        findViewById<View>(R.id.btn_ok).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_ok) {
            // 获取月份选择器mp_month设定的年月
            val desc = String.format(
                "您选择的月份是%d年%d月",
                mp_month!!.getYear(), mp_month!!.getMonth() + 1
            )
            tv_month!!.text = desc
        }
    }
}