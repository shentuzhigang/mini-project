package io.shentuzhigang.demo.calendar

import android.content.*
import android.graphics.Color
import android.os.*
import android.util.Log
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.PagerTabStrip
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import io.shentuzhigang.demo.calendar.adapter.SchedulePagerAdapter
import io.shentuzhigang.demo.calendar.calendar.SpecialCalendar
import io.shentuzhigang.demo.calendar.util.DateUtil

class ScheduleActivity : AppCompatActivity() {
    private var ll_schedule // 声明一个日程表区域的线性布局对象
            : LinearLayout? = null
    private var vp_schedule // 声明一个翻页视图对象
            : ViewPager? = null
    private var mSelectedWeek // 当前选中的星期
            = 0
    private var mFestivalResid = 0 // 节日图片的资源编号
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        // 从布局文件中获取名叫pts_schedule的翻页标题栏
        val pts_schedule = findViewById<PagerTabStrip>(R.id.pts_schedule)
        pts_schedule.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)
        pts_schedule.setTextColor(Color.BLACK)
        ll_schedule = findViewById(R.id.ll_schedule)
        // 从布局文件中获取名叫vp_schedule的翻页视图
        vp_schedule = findViewById(R.id.vp_schedule)
        val tv_schedule = findViewById<TextView>(R.id.tv_schedule)
        tv_schedule.setText(DateUtil.nowYearCN + " 日程安排")
        // 获取今天所处的星期在一年当中的序号
        mSelectedWeek = SpecialCalendar.todayWeek
        // 构建一个日程表的翻页适配器
        val adapter = SchedulePagerAdapter(supportFragmentManager)
        // 给vp_schedule设置日程表翻页适配器
        with(vp_schedule) {
            // 给vp_schedule设置日程表翻页适配器
            this?.setAdapter(adapter)
            // 设置vp_schedule默认显示当前周数的日程页
            this?.setCurrentItem(mSelectedWeek - 1)
            // 给vp_schedule添加页面变化监听器
            this?.addOnPageChangeListener(SheduleChangeListener())
        }
        // 延迟50毫秒再执行任务mFirst
        mHandler.postDelayed(mFirst, 50)
    }

    private val mHandler = Handler() // 声明一个处理器对象

    // 声明一个首次打开页面需要延迟执行的任务
    private val mFirst = Runnable {
        sendBroadcast(mSelectedWeek) // 发送广播，表示当前是在第几个星期
    }

    // 发送当前周数的广播
    private fun sendBroadcast(week: Int) {
        // 创建一个广播事件的意图
        val intent = Intent(ACTION_FRAGMENT_SELECTED)
        intent.putExtra(EXTRA_SELECTED_WEEK, week)
        // 通过本地的广播管理器来发送广播
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    public override fun onStart() {
        super.onStart()
        // 创建一个节日图片的广播接收器
        festivalReceiver = FestivalControlReceiver()
        // 注册广播接收器，注册之后才能正常接收广播
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(festivalReceiver!!, IntentFilter(ACTION_SHOW_FESTIVAL))
    }

    public override fun onStop() {
        super.onStop()
        // 注销广播接收器，注销之后就不再接收广播
        LocalBroadcastManager.getInstance(this).unregisterReceiver(festivalReceiver!!)
    }

    override fun onResume() {
        super.onResume()
        if (mFestivalResid != 0) { // 在横屏和竖屏之间翻转时，不会重新onCreate，只会onResume
            ll_schedule!!.setBackgroundResource(mFestivalResid)
        }
    }

    // 声明一个节日图片的广播接收器
    private var festivalReceiver: FestivalControlReceiver? = null

    // 定义一个广播接收器，用于处理节日图片事件
    private inner class FestivalControlReceiver : BroadcastReceiver() {
        // 一旦接收到节日图片的广播，马上触发接收器的onReceive方法
        override fun onReceive(context: Context, intent: Intent) {
            if (intent != null) {
                // 从广播消息中取出节日图片的资源编号
                mFestivalResid = intent.getIntExtra(EXTRA_FESTIVAL_RES, 1)
                // 把页面背景设置为广播发来的节日图片
                ll_schedule!!.setBackgroundResource(mFestivalResid)
            }
        }
    }

    // 定义一个页面变化监听器，用于处理翻页视图的翻页事件
    inner class SheduleChangeListener : OnPageChangeListener {
        // 在翻页结束后触发
        override fun onPageSelected(position: Int) {
            Log.d(TAG, "onPageSelected position=$position, mSelectedWeek=$mSelectedWeek")
            mSelectedWeek = position + 1
            sendBroadcast(mSelectedWeek)
        }

        // 在翻页过程中触发
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        // 翻页状态改变时触发
        override fun onPageScrollStateChanged(arg0: Int) {}
    }

    companion object {
        private const val TAG = "ScheduleActivity"

        // 声明一个碎片选中事件的标识串
        var ACTION_FRAGMENT_SELECTED = "io.shentuzhigang.demo.calendar.ACTION_FRAGMENT_SELECTED"

        // 声明一个选择星期参数的标识串
        var EXTRA_SELECTED_WEEK = "selected_week"

        // 声明一个显示节日事件的标识串
        var ACTION_SHOW_FESTIVAL = "io.shentuzhigang.demo.calendar.ACTION_SHOW_FESTIVAL"

        // 声明一个节日图片参数的标识串
        var EXTRA_FESTIVAL_RES = "festival_res"
    }
}