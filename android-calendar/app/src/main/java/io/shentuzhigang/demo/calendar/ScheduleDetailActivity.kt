package io.shentuzhigang.demo.calendar

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.calendar.bean.ScheduleArrange
import io.shentuzhigang.demo.calendar.database.DbHelper
import io.shentuzhigang.demo.calendar.database.ScheduleArrangeHelper
import java.util.*

@SuppressLint(value = ["SetTextI18n", "DefaultLocale"])
class ScheduleDetailActivity : AppCompatActivity(), View.OnClickListener, OnTimeSetListener {
    private var btn_back: Button? = null
    private var btn_edit: Button? = null
    private var btn_save: Button? = null
    private var schedule_date: TextView? = null
    private var schedule_time: TextView? = null
    private var schedule_alarm: Spinner? = null
    private var schedule_title: EditText? = null
    private var schedule_content: EditText? = null
    private var month: String? = null
    private var day: String? = null
    private var week: String? = null
    private var holiday: String? = null
    private var solar_date: String? = null
    private var lunar_date: String? = null
    private var detail_date: String? = null
    private var mArrange // 声明一个日程安排结构
            : ScheduleArrange? = null
    private var mScheduleHelper // 声明一个日程安排的数据库帮助器
            : ScheduleArrangeHelper? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_detail)
        btn_back = findViewById(R.id.btn_back)
        btn_edit = findViewById(R.id.btn_edit)
        btn_save = findViewById(R.id.btn_save)
        btn_back?.setOnClickListener(this)
        btn_edit?.setOnClickListener(this)
        btn_save?.setOnClickListener(this)
        schedule_date = findViewById(R.id.schedule_date)
        schedule_time = findViewById(R.id.schedule_time)
        schedule_title = findViewById(R.id.schedule_title)
        schedule_content = findViewById(R.id.schedule_content)
        schedule_time?.setText("00:00")
        schedule_time?.setOnClickListener(this)
        getBundleInfo()
        initAlarmSpinner()
    }

    // 获取前一个页面传来的包裹，并从中得到指定的参数信息
    private fun getBundleInfo() {
        val req = intent.extras
        day = req!!.getString("day")
        solar_date = req.getString("solar_date")
        lunar_date = req.getString("lunar_date")
        month = day!!.substring(0, 6)
        week = req.getString("week")
        holiday = req.getString("holiday")
        detail_date = String.format("%s %s\n%s", solar_date, lunar_date, week)
        if (!TextUtils.isEmpty(holiday)) {
            detail_date = String.format("%s，今天是 %s", detail_date, holiday)
        }
        schedule_date!!.text = detail_date
        Log.d(
            TAG, "month=" + month + ",day=" + day + ",solar_date=" + solar_date + ",lunar_date="
                    + lunar_date + ",week=" + week + ",holiday=" + holiday
        )
    }

    // 初始化提醒间隔的下拉框
    private fun initAlarmSpinner() {
        val adapter = ArrayAdapter(
            this,
            R.layout.item_select, alarmArray
        )
        schedule_alarm = findViewById(R.id.schedule_alarm)
        schedule_alarm?.setPrompt("请选择提醒间隔")
        schedule_alarm?.setAdapter(adapter)
        schedule_alarm?.setSelection(0)
        schedule_alarm?.setOnItemSelectedListener(AlarmSelectedListener())
    }

    private var alarmType = 0
    private val alarmArray = arrayOf(
        "不提醒", "提前5分钟", "提前10分钟",
        "提前15分钟", "提前半小时", "提前1小时", "当前时间后10秒"
    )
    private val advanceArray = intArrayOf(0, 5, 10, 15, 30, 60, 10)

    internal inner class AlarmSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            alarmType = arg2
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }

    override fun onResume() {
        super.onResume()
        mArrange = ScheduleArrange()
        // 获得数据库帮助器的实例
        mScheduleHelper = ScheduleArrangeHelper(this, DbHelper.Companion.db_name, null, 1)
        // 查询数据库获得当天的日程安排信息
        val mArrangeList: List<ScheduleArrange> =
            mScheduleHelper!!.queryInfoByDay(day) as List<ScheduleArrange>
        if (mArrangeList.size >= 1) { // 已有日程安排，则显示该日程信息
            enableEdit(false) // 关闭编辑模式
            mArrange = mArrangeList[0]
            schedule_time!!.setText(mArrange!!.hour + ":" + mArrange!!.minute)
            schedule_alarm!!.setSelection(mArrange!!.alarm_type)
            schedule_title!!.setText(mArrange!!.title)
            schedule_content!!.setText(mArrange!!.content)
        } else { // 没有日程安排，则提示用户输入新日程
            enableEdit(true) // 开启编辑模式
        }
    }

    override fun onPause() {
        super.onPause()
        // 关闭数据库连接
        mScheduleHelper!!.close()
    }

    // 在是否开启编辑模式之间切换
    private fun enableEdit(enabled: Boolean) {
        schedule_time!!.isEnabled = enabled
        schedule_alarm!!.isEnabled = enabled
        schedule_title!!.isEnabled = enabled
        schedule_content!!.isEnabled = enabled
        if (enabled) {
            schedule_time!!.setBackgroundResource(R.drawable.editext_selector)
            schedule_title!!.setBackgroundResource(R.drawable.editext_selector)
            schedule_content!!.setBackgroundResource(R.drawable.editext_selector)
        } else {
            schedule_time!!.background = null
            schedule_title!!.background = null
            schedule_content!!.background = null
        }
        btn_edit!!.visibility = if (enabled) View.GONE else View.VISIBLE
        btn_save!!.visibility = if (enabled) View.VISIBLE else View.GONE
    }

    override fun onClick(v: View) {
        if (v.id == R.id.schedule_time) { // 点击了日程时间的文本视图
            val calendar = Calendar.getInstance()
            // 弹出时间选择对话框，供用户选择日程事务发生的时间
            val dialog = TimePickerDialog(
                this, this,
                calendar[Calendar.HOUR_OF_DAY], calendar[Calendar.MINUTE], true
            )
            dialog.show()
        } else if (v.id == R.id.btn_back) { // 点击了返回按钮
            finish()
        } else if (v.id == R.id.btn_edit) { // 点击了编辑按钮
            enableEdit(true) // 开启编辑模式
        } else if (v.id == R.id.btn_save) { // 点击了保存按钮
            if (TextUtils.isEmpty(schedule_title!!.text)) {
                Toast.makeText(this, "请输入日程标题", Toast.LENGTH_SHORT).show()
                return
            }
            enableEdit(false) // 关闭编辑模式
            saveArrange()
        }
    }

    // 保存编辑好的日程安排数据
    private fun saveArrange() {
        val time_split = schedule_time!!.text.toString().split(":").toTypedArray()
        mArrange!!.hour = time_split[0]
        mArrange!!.minute = time_split[1]
        mArrange!!.alarm_type = alarmType
        mArrange!!.title = schedule_title!!.text.toString()
        mArrange!!.content = schedule_content!!.text.toString()
        if (mArrange!!.xuhao <= 0) { // 不存在日程记录
            mArrange!!.month = month
            mArrange!!.day = day
            mScheduleHelper!!.add(mArrange) // 添加新的日程记录
        } else { // 已存在日程记录
            mScheduleHelper!!.update(mArrange) // 更新旧的日程记录
        }
        Toast.makeText(this, "保存日程成功", Toast.LENGTH_SHORT).show()
        // 设置提醒闹钟
        if (alarmType > 0) { // 如果需要闹钟定时提醒
            // 创建一个广播事件的意图
            val intent = Intent(ALARM_EVENT)
            // 创建一个用于广播的延迟意图
            val pIntent = PendingIntent.getBroadcast(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            // 从系统服务中获取闹钟管理器
            val alarmMgr = getSystemService(ALARM_SERVICE) as AlarmManager
            val calendar = Calendar.getInstance()
            if (alarmType == 6) { // 以当前时间为基准
                calendar.timeInMillis = System.currentTimeMillis()
                calendar.add(Calendar.SECOND, advanceArray[alarmType])
            } else { // 指定一个明确的时间点
                val day_int = day!!.toInt()
                calendar[day_int / 10000, day_int % 10000 / 100 - 1, day_int % 100, mArrange!!.hour.toInt(), mArrange!!.minute.toInt()] =
                    0
                calendar.add(Calendar.SECOND, -advanceArray[alarmType] * 60)
            }
            // 开始设定闹钟，延迟若干秒后，携带延迟意图发送闹钟广播
            alarmMgr[AlarmManager.RTC_WAKEUP, calendar.timeInMillis] = pIntent
        }
    }

    // 一旦点击时间对话框上的确定按钮，就会触发监听器的onTimeSet方法
    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        // 获取时间对话框设定的小时和分钟
        val time = String.format("%02d:%02d", hourOfDay, minute)
        schedule_time!!.text = time
    }

    // 声明一个闹钟广播事件的标识串
    private val ALARM_EVENT = "io.shentuzhigang.demo.calendar.ScheduleDetailActivity.AlarmReceiver"

    // 定义一个闹钟广播的接收器
    class AlarmReceiver : BroadcastReceiver() {
        // 一旦接收到闹钟时间到达的广播，马上触发接收器的onReceive方法
        override fun onReceive(context: Context, intent: Intent) {
            if (intent != null) {
                Log.d(TAG, "AlarmReceiver onReceive")
                // 从系统服务中获取震动管理器
                val vibrator = context.getSystemService(VIBRATOR_SERVICE) as Vibrator
                vibrator.vibrate(3000) // 默认震动3秒
            }
        }
    }

    // 适配Android9.0开始
    public override fun onStart() {
        super.onStart()
        // 从Android9.0开始，系统不再支持静态广播，应用广播只能通过动态注册
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 创建一个闹钟的广播接收器
            alarmReceiver = AlarmReceiver()
            // 创建一个意图过滤器，只处理指定事件来源的广播
            val filter = IntentFilter(ALARM_EVENT)
            // 注册广播接收器，注册之后才能正常接收广播
            registerReceiver(alarmReceiver, filter)
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 注销广播接收器，注销之后就不再接收广播
            unregisterReceiver(alarmReceiver)
        }
    }

    // 声明一个闹钟的广播接收器
    private var alarmReceiver: AlarmReceiver? = null // 适配Android9.0结束

    companion object {
        private const val TAG = "ScheduleDetailActivity"
    }
}