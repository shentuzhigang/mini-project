package io.shentuzhigang.demo.calendar

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import java.util.*

@SuppressLint("DefaultLocale") // 该页面类实现了接口OnTimeSetListener，意味着要重写时间监听器的onTimeSet方法
class TimePickerActivity : AppCompatActivity(), View.OnClickListener, OnTimeSetListener {
    private var tv_time: TextView? = null
    private var tp_time // 声明一个时间选择器对象
            : TimePicker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time_picker)
        tv_time = findViewById(R.id.tv_time)
        // 从布局文件中获取名叫tp_time的时间选择器
        tp_time = findViewById(R.id.tp_time)
        findViewById<View>(R.id.btn_time).setOnClickListener(this)
        findViewById<View>(R.id.btn_ok).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_time) {
            // 获取日历的一个实例，里面包含了当前的时分秒
            val calendar = Calendar.getInstance()
            // 构建一个时间对话框，该对话框已经集成了时间选择器。
            // TimePickerDialog的第二个构造参数指定了时间监听器
            val dialog = TimePickerDialog(
                this, this,
                calendar[Calendar.HOUR_OF_DAY],  // 小时
                calendar[Calendar.MINUTE],  // 分钟
                true
            ) // true表示24小时制，false表示12小时制
            // 把时间对话框显示在界面上
            dialog.show()
        } else if (v.id == R.id.btn_ok) {
            // 获取时间选择器tp_time设定的小时和分钟
            val desc = String.format(
                "您选择的时间是%d时%d分",
                tp_time!!.currentHour, tp_time!!.currentMinute
            )
            tv_time!!.text = desc
        }
    }

    // 一旦点击时间对话框上的确定按钮，就会触发监听器的onTimeSet方法
    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        // 获取时间对话框设定的小时和分钟
        val desc = String.format("您选择的时间是%d时%d分", hourOfDay, minute)
        tv_time!!.text = desc
    }
}