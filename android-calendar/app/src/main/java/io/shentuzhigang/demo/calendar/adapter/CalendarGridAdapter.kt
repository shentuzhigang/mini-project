package io.shentuzhigang.demo.calendar.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import io.shentuzhigang.demo.calendar.bean.CalendarTransfer
import io.shentuzhigang.demo.calendar.calendar.LunarCalendar
import io.shentuzhigang.demo.calendar.calendar.SpecialCalendar
import io.shentuzhigang.demo.calendar.util.DateUtil

import android.view.LayoutInflater
import android.widget.AdapterView
import io.shentuzhigang.demo.calendar.CalendarActivity
import io.shentuzhigang.demo.calendar.R
import io.shentuzhigang.demo.calendar.ScheduleDetailActivity
import io.shentuzhigang.demo.calendar.calendar.Constant

class CalendarGridAdapter(// 声明一个上下文对象
    private val mContext: Context, year: Int, month: Int, day: Int
) : BaseAdapter(), AdapterView.OnItemClickListener {
    private var isLeapyear = false // 是否为闰年
    private var daysOfMonth = 0 // 某月的天数
    private var dayOfWeek = 0 // 具体某一天是星期几
    private var lastDaysOfMonth = 0 // 上一个月的总天数
    private val dayNumber = arrayOfNulls<String>(49) // 一个gridview中的日期存入此数组中
    private val transArray: ArrayList<CalendarTransfer> = ArrayList<CalendarTransfer>()
    private val lc: LunarCalendar
    private var currentDay = -1 // 用于标记当天
    override fun getCount(): Int {
        return dayNumber.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val holder: ViewHolder
        var convertView1:View? = convertView
        if (convertView1 == null) {
            holder = ViewHolder()
            // 根据布局文件item_calendar.xml生成转换视图对象
            convertView1 = LayoutInflater.from(mContext).inflate(R.layout.item_calendar, null)
            holder.tv_day = convertView1.findViewById(R.id.tv_day)
            convertView1.tag = holder
        } else {
            holder = convertView1.tag as ViewHolder
        }
        val split = dayNumber[position]!!.split(".")

        val day = split.toTypedArray()[0]
        var festival = split.toTypedArray()[0]

        if (split.size==2){
            festival = split.toTypedArray()[1]
        }
        var itemText = day
        if (position >= 7) {
            itemText = """
                $itemText
                $festival
                """.trimIndent()
        }
        holder.tv_day?.setText(itemText)
        holder.tv_day?.setTextColor(Color.GRAY)
        if (position < 7) {
            // 设置周一到周日的标题
            holder.tv_day?.setTextColor(Color.BLACK)
            holder.tv_day?.setBackgroundColor(Color.LTGRAY)
        } else if (currentDay == position) {
            holder.tv_day?.setBackgroundColor(Color.GREEN) // 设置当天的背景
        } else {
            holder.tv_day?.setBackgroundColor(Color.WHITE) // 设置其他日期的背景
        }
        if (position < daysOfMonth + dayOfWeek + 7 - 1 && position >= dayOfWeek + 7 - 1) {
            // 当前月信息显示
            if (DateUtil.isHoliday(festival)) {
                holder.tv_day?.setTextColor(Color.BLUE) // 节日字体标蓝
            } else if ((position + 1) % 7 == 6 || (position + 1) % 7 == 0) {
                holder.tv_day?.setTextColor(Color.RED) // 周末字体标红
            } else {
                holder.tv_day?.setTextColor(Color.BLACK) // 当月字体设黑
            }
        }
        return convertView1
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        Log.d(TAG, "onItemClick position=$position")
        val trans: CalendarTransfer = transArray[position]
        val day = String.format("%s%02d%02d", trans.solar_year, trans.solar_month, trans.solar_day)
        val solar_date =
            String.format("%s年%d月%d日", trans.solar_year, trans.solar_month, trans.solar_day)
        val lunar_date = String.format(
            "农历%s月%s",
            LunarCalendar.chineseNumber.get(trans.lunar_month - 1),
            LunarCalendar.getChinaDayString(trans.lunar_day)
        )
        var holiday = ""
        if (DateUtil.isHoliday(trans.day_name)) {
            holiday = trans.day_name
        }
        // 跳转到当天的日程详情页面
        val intent = Intent(mContext, ScheduleDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putString("day", day)
        bundle.putString("solar_date", solar_date)
        bundle.putString("lunar_date", lunar_date)
        bundle.putString(
            "week",
            Constant.weekArray.get(position%7)
        )
        bundle.putString("holiday", holiday)
        intent.putExtras(bundle)
        mContext.startActivity(intent)
    }

    // 将一个月中的每一天的值添加入数组dayNumber中
    private fun getWeekDays(year: Int, month: Int) {
        var nextMonthDay = 1
        var lunarDay = ""
        Log.d(TAG, "begin getWeekDays")
        for (i in dayNumber.indices) {
            var trans = CalendarTransfer()
            val weekday = (i - 7) % 7 + 1
            // 周一
            if (i < 7) {
                dayNumber[i] = weekTitle[i] + "." + " "
            } else if (i < dayOfWeek + 7 - 1) { // 前一个月
                val temp = lastDaysOfMonth - dayOfWeek + 1 - 7 + 1
                trans = lc.getSubDate(trans, year, month - 1, temp + i, weekday, false)
                lunarDay = trans.day_name
                dayNumber[i] = (temp + i).toString() + "." + lunarDay
            } else if (i < daysOfMonth + dayOfWeek + 7 - 1) { // 本月
                val day = i - dayOfWeek + 1 - 7 + 1
                trans = lc.getSubDate(trans, year, month, day, weekday, false)
                lunarDay = trans.day_name
                dayNumber[i] = "$day.$lunarDay"
                // 对于当前月才去标记当前日期
                if (year == DateUtil.nowYear && month == DateUtil.nowMonth && day == DateUtil.nowDay) {
                    currentDay = i
                }
            } else { // 下一个月
                var next_month = month + 1
                var next_year = year
                if (next_month >= 13) {
                    next_month = 1
                    next_year++
                }
                trans = lc.getSubDate(trans, next_year, next_month, nextMonthDay, weekday, false)
                lunarDay = trans.day_name
                dayNumber[i] = "$nextMonthDay.$lunarDay"
                nextMonthDay++
            }
            transArray.add(trans)
        }
        //        //把日期数据打印到日志中
//        String abc = "";
//        for (String aDay : dayNumber) {
//            abc = abc + aDay + ":";
//        }
//        Log.d(TAG, abc);
    }

    fun getCalendarList(pos: Int): CalendarTransfer {
        return transArray[pos]
    }

    inner class ViewHolder {
        var tv_day: TextView? = null
    }

    companion object {
        private const val TAG = "CalendarGridAdapter"
        private val weekTitle = arrayOf("周一", "周二", "周三", "周四", "周五", "周六", "周日")
    }

    init {
        lc = LunarCalendar()
        Log.d(TAG, "currentYear=$year, currentMonth=$month, currentDay=$day")
        // 得到某年的某月的天数且这月的第一天是星期几
        isLeapyear = SpecialCalendar.isLeapYear(year) // 是否为闰年
        daysOfMonth = SpecialCalendar.getDaysOfMonth(isLeapyear, month) // 某月的总天数
        dayOfWeek = SpecialCalendar.getWeekdayOfMonth(year, month) // 某月第一天为星期几
        lastDaysOfMonth = SpecialCalendar.getDaysOfMonth(isLeapyear, month - 1) //上一个月的总天数
        Log.d(
            TAG,
            "$isLeapyear ======  $daysOfMonth  ============  $dayOfWeek  =========   $lastDaysOfMonth"
        )
        getWeekDays(year, month)
    }
}