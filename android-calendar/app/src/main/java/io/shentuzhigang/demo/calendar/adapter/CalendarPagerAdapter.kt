package io.shentuzhigang.demo.calendar.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import io.shentuzhigang.demo.calendar.calendar.Constant
import io.shentuzhigang.demo.calendar.fragment.CalendarFragment

class CalendarPagerAdapter     // 碎片页适配器的构造函数，传入碎片管理器与年份
    (
    fm: FragmentManager?, // 声明当前日历所处的年份
    private val mYear: Int
) : FragmentStatePagerAdapter(fm!!,FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    // 获取碎片Fragment的个数，一年有12个月
    private val count: Int = 12
    override fun getCount(): Int {
        return count
    }

    // 获取指定月份的碎片Fragment
    override fun getItem(position: Int): Fragment {
        return CalendarFragment.newInstance(mYear, position + 1)
    }

    // 获得指定月份的标题文本
    override fun getPageTitle(position: Int): CharSequence {
        return Constant.xuhaoArray.get(position + 1) + "月"
    }
}