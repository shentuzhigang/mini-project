package io.shentuzhigang.demo.calendar.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import io.shentuzhigang.demo.calendar.R
import io.shentuzhigang.demo.calendar.ScheduleDetailActivity
import io.shentuzhigang.demo.calendar.bean.CalendarTransfer
import io.shentuzhigang.demo.calendar.bean.ScheduleArrange
import io.shentuzhigang.demo.calendar.calendar.LunarCalendar
import io.shentuzhigang.demo.calendar.calendar.SpecialCalendar
import io.shentuzhigang.demo.calendar.util.DateUtil


@SuppressLint("DefaultLocale")
class ScheduleListAdapter(// 声明一个上下文对象
    private val mContext: Context, tranArray: ArrayList<CalendarTransfer>,
    arrangeList: ArrayList<ScheduleArrange>
) : BaseAdapter(), AdapterView.OnItemClickListener {
    private var mTranArray: ArrayList<CalendarTransfer> = ArrayList<CalendarTransfer>()
    private var mArrangeList: ArrayList<ScheduleArrange> = ArrayList<ScheduleArrange>()
    override fun getCount(): Int {
        return mTranArray.size
    }

    override fun getItem(position: Int): Any {
        return mTranArray[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val holder: ViewHolder
        var convertView1:View? = convertView
        if (convertView1 == null) {
            holder = ViewHolder()
            // 根据布局文件item_calendar.xml生成转换视图对象
            convertView1 = LayoutInflater.from(mContext).inflate(R.layout.item_schedule, null)
            holder.week_number = convertView1.findViewById(R.id.week_number)
            holder.week_shedule = convertView1.findViewById(R.id.week_shedule)
            convertView1.tag = holder
        } else {
            holder = convertView1.tag as ViewHolder
        }
        holder.week_number?.setText(
            io.shentuzhigang.demo.calendar.calendar.Constant.weekArray.get(
                position
            )
        )
        if (position < 5) { // 周一到周五的星期几用黑色字体
            holder.week_number?.setTextColor(Color.BLACK)
        } else { // 周六周日的星期几用红色字体
            holder.week_number?.setTextColor(Color.RED)
        }
        val trans: CalendarTransfer = mTranArray[position]
        val day = String.format("%s%02d%02d", trans.solar_year, trans.solar_month, trans.solar_day)
        var arrangeTitle = ""
        var i = 0
        while (i < mArrangeList.size) {
            if (mArrangeList[i].day == day) {
                val item: ScheduleArrange = mArrangeList[i]
                // 拼接当前的日程安排标题
                arrangeTitle = String.format("%s时%s分：%s", item.hour, item.minute, item.title)
                break
            }
            i++
        }
        if (i >= mArrangeList.size) {
            arrangeTitle = "今日暂无日程安排"
        }
        // 拼接公历的日期文字
        val solar_date = String.format("%d月%d日", trans.solar_month, trans.solar_day)
        // 拼接农历的日期文字
        val lunar_date = String.format(
            "农历%s月%s",
            LunarCalendar.Companion.chineseNumber.get(trans.lunar_month - 1),
            LunarCalendar.Companion.getChinaDayString(trans.lunar_day)
        )
        var holiday = ""
        // 判断当前是否为特殊日子（含公历节日、农历节日、二十四节气等）
        if (DateUtil.isHoliday(trans.day_name)) {
            holiday = trans.day_name
        }
        // 拼接当天完整的日期描述（含公历日、农历日、各种节日、日程安排等）
        val content = String.format("%s %s %s\n%s", solar_date, lunar_date, holiday, arrangeTitle)
        holder.week_shedule?.setText(content)
        if (day == DateUtil.nowDate) { // 今天的日程描述用蓝色字体
            holder.week_shedule?.setTextColor(Color.BLUE)
        } else { // 其它日子的日程描述用黑色字体
            holder.week_shedule?.setTextColor(Color.BLACK)
        }
        return convertView1
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        Log.d(TAG, "onItemClick position=$position")
        val trans: CalendarTransfer = mTranArray[position]
        val day = String.format("%s%02d%02d", trans.solar_year, trans.solar_month, trans.solar_day)
        val solar_date =
            String.format("%s年%d月%d日", trans.solar_year, trans.solar_month, trans.solar_day)
        val lunar_date = String.format(
            "农历%s月%s",
            LunarCalendar.Companion.chineseNumber.get(trans.lunar_month - 1),
            LunarCalendar.Companion.getChinaDayString(trans.lunar_day)
        )
        var holiday = ""
        if (DateUtil.isHoliday(trans.day_name)) {
            holiday = trans.day_name
        }
        // 跳转到当天的日程详情页面
        val intent = Intent(mContext, ScheduleDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putString("day", day)
        bundle.putString("solar_date", solar_date)
        bundle.putString("lunar_date", lunar_date)
        bundle.putString(
            "week",
            io.shentuzhigang.demo.calendar.calendar.Constant.weekArray.get(position)
        )
        bundle.putString("holiday", holiday)
        intent.putExtras(bundle)
        mContext.startActivity(intent)
    }

    inner class ViewHolder {
        var week_number: TextView? = null
        var week_shedule: TextView? = null
    }

    companion object {
        private const val TAG = "ScheduleListAdapter"
    }

    init {
        mTranArray = tranArray
        mArrangeList = arrangeList
    }
}