package io.shentuzhigang.demo.calendar.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import io.shentuzhigang.demo.calendar.calendar.Constant;
import io.shentuzhigang.demo.calendar.fragment.ScheduleFragment;

public class SchedulePagerAdapter extends FragmentStatePagerAdapter {

    // 碎片页适配器的构造函数
    public SchedulePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    // 获取碎片Fragment的个数，一年有52个星期
    public int getCount() {
        return 52;
    }

    // 获取指定星期的碎片Fragment
    public Fragment getItem(int position) {
        return ScheduleFragment.Companion.newInstance(position + 1);
    }

    // 获得指定星期的标题文本
    public CharSequence getPageTitle(int position) {
        return "第" + Constant.INSTANCE.getXuhaoArray()[position + 1] + "周";
    }

}
