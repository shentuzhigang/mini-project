package io.shentuzhigang.demo.calendar.calendar

import android.util.Log
import java.util.*

object SpecialCalendar {
    val todayWeek: Int
        get() {
            var week = 0
            val cal = Calendar.getInstance()
            cal.time = Date()
            week = cal[Calendar.WEEK_OF_YEAR]
            return week
        }
    private const val TAG = "SpecialCalendar"

    // 判断是否为闰年
    fun isLeapYear(year: Int): Boolean {
        if (year % 100 == 0 && year % 400 == 0) {
            return true
        } else if (year % 100 != 0 && year % 4 == 0) {
            return true
        }
        return false
    }

    //得到某月有多少天数
    fun getDaysOfMonth(isLeapyear: Boolean, month: Int): Int {
        var daysOfMonth = 0
        when (month) {
            1, 3, 5, 7, 8, 10, 12 -> daysOfMonth = 31
            4, 6, 9, 11 -> daysOfMonth = 30
            2 -> daysOfMonth = if (isLeapyear) {
                29
            } else {
                28
            }
        }
        return daysOfMonth
    }

    //指定某年中的某月的第一天是星期几
    fun getWeekdayOfMonth(year: Int, month: Int): Int {
        val cal = Calendar.getInstance()
        cal[year, month - 1] = 1
        var dayOfWeek = cal[Calendar.DAY_OF_WEEK] - 1
        Log.d(TAG, " ===dayOfWeek===  $dayOfWeek")
        if (dayOfWeek == 0) {
            dayOfWeek = 7
        }
        Log.d(TAG, " ===dayOfWeek===  $dayOfWeek")
        return dayOfWeek
    }
}