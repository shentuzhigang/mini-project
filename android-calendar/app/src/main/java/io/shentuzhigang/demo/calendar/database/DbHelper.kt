package io.shentuzhigang.demo.calendar.database

import android.annotation.SuppressLint
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.util.*

@SuppressLint("DefaultLocale")
open class DbHelper(
    protected var mContext: Context,
    name: String?,
    factory: CursorFactory?,
    protected var mVersion: Int
) : SQLiteOpenHelper(
    mContext, name, factory, mVersion
) {
    protected var mReadDB: SQLiteDatabase
    protected var mWriteDB: SQLiteDatabase
    protected var mTableName: String? = null
    protected var mSelectSQL: String? = null
    protected var mCreateSQL: String? = null
    override fun onCreate(db: SQLiteDatabase) {
        mCreateSQL = "CREATE TABLE IF NOT EXISTS ScheduleArrange (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "month VARCHAR NOT NULL, day VARCHAR NOT NULL," +
                "hour VARCHAR NOT NULL, minute VARCHAR NOT NULL," +
                "title VARCHAR NOT NULL, content VARCHAR NOT NULL," +
                "update_time VARCHAR, alarm_type INTEGER NOT NULL" +
                ");"
        Log.d(TAG, "create_sql:$mCreateSQL")
        db.execSQL(mCreateSQL)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
    fun delete(xuhao: Int) {
        val delete_sql = String.format("delete from %s where _id=%d;", mTableName, xuhao)
        Log.d(TAG, "delete sql=$delete_sql")
        mWriteDB.execSQL(delete_sql)
    }

    protected open fun queryInfo(sql: String?): List<*> {
        return ArrayList<Any>()
    }

    fun queryInfoById(Id: Int): List<*> {
        val sql = "$mSelectSQL a._id=$Id;"
        return queryInfo(sql)
    }

    fun queryCount(sql: String): Int {
        var count = 0
        val cursor = mReadDB.rawQuery(sql, null)
        count = cursor.count
        cursor.close()
        Log.d(TAG, "count=$count,sql=$sql")
        return count
    }

    companion object {
        protected var TAG: String? = null
        var db_name = "schedule.sqlite"
    }

    init {
        mWriteDB = this.writableDatabase
        mReadDB = this.readableDatabase
    }
}