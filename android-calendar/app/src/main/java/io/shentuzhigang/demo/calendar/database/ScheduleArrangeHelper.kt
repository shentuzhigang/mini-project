package io.shentuzhigang.demo.calendar.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.util.Log
import io.shentuzhigang.demo.calendar.bean.ScheduleArrange
import java.util.*

@SuppressLint("DefaultLocale")
class ScheduleArrangeHelper(
    context: Context,
    name: String?,
    factory: CursorFactory?,
    version: Int
) : DbHelper(context, name, factory, version) {
    fun add(data: ScheduleArrange?): Boolean {
        val data_list: MutableList<ScheduleArrange?> = ArrayList<ScheduleArrange?>()
        data_list.add(data)
        return add(data_list)
    }
val TAG:String
    fun add(dataList: List<ScheduleArrange?>): Boolean {
        for (data in dataList) {
            // ContentValues对象
            val cv = ContentValues()
            cv.put("month", data!!.month)
            cv.put("day", data.day)
            cv.put("hour", data.hour)
            cv.put("minute", data.minute)
            cv.put("title", data.title)
            cv.put("content", data.content)
            cv.put("update_time", data.update_time)
            cv.put("alarm_type", data.alarm_type)
            Log.d(TAG, "cv.toString():$cv")
            if (getCount(data.day) <= 0) {
                val result: Long = mWriteDB.insert(mTableName, "", cv)
                // 添加成功后返回行号，失败后返回-1
                if (result == -1L) {
                    // 失败
                    return false
                }
            } else {
                update(data)
            }
        }
        return true
    }

    fun update(data: ScheduleArrange?) {
        var update_sql: String
        update_sql = java.lang.String.format(
            "update %s set month='%s', day='%s', hour='%s', minute='%s', " +
                    "title='%s', content='%s', update_time='%s', alarm_type='%d' where ",
            mTableName, data!!.month, data.day, data.hour, data.minute,
            data.title, data.content, data.update_time, data.alarm_type
        )
        update_sql = if (data.xuhao > 0) {
            String.format("%s _id=%d;", update_sql, data.xuhao)
        } else {
            String.format("%s day='%s';", update_sql, data.day)
        }
        Log.d(TAG, "update_sql:$update_sql")
        mWriteDB.execSQL(update_sql)
    }

    protected override fun queryInfo(sql: String?): List<*> {
        Log.d(TAG, "begin moveToFirst:$sql")
        val data_list: MutableList<ScheduleArrange> = ArrayList<ScheduleArrange>()
        val cursor: Cursor = mReadDB.rawQuery(sql, null)
        while (cursor.moveToNext()) {
            var data_info = ScheduleArrange()
            data_info.xuhao = cursor.getInt(0)
            data_info.month = cursor.getString(1)
            data_info.day = cursor.getString(2)
            data_info.hour = cursor.getString(3)
            data_info.minute = cursor.getString(4)
            data_info.title = cursor.getString(5)
            data_info.content = cursor.getString(6)
            data_info.update_time = cursor.getString(7)
            data_info.alarm_type = cursor.getInt(8)
            data_list.add(data_info)
            data_info = ScheduleArrange()
        }
        Log.d(TAG, "end query_info")
        cursor.close()
        return data_list
    }

    fun getCount(day: String?): Int {
        val sql = java.lang.String.format("%s day='%s';", mSelectSQL, day)
        return queryCount(sql)
    }

    fun queryInfoByDay(day: String?): List<*> {
        val sql = java.lang.String.format("%s day='%s';", mSelectSQL, day)
        return queryInfo(sql)
    }

    fun queryInfoByMonth(month: String?): List<*> {
        val sql = java.lang.String.format("%s month='%s';", mSelectSQL, month)
        return queryInfo(sql)
    }

    fun queryInfoByDayRange(begin_day: String?, end_day: String?): List<*> {
        val sql =
            java.lang.String.format("%s day>='%s' and day<='%s';", mSelectSQL, begin_day, end_day)
        return queryInfo(sql)
    }

    init {
        TAG = "ScheduleArrangeHelper"
        mTableName = "ScheduleArrange"
        mSelectSQL = java.lang.String.format(
            "select _id,month,day,hour,minute,title,content,update_time,alarm_type from %s where ",
            mTableName
        )
    }
}