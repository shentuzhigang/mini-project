package io.shentuzhigang.demo.calendar.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import io.shentuzhigang.demo.calendar.R
import io.shentuzhigang.demo.calendar.adapter.CalendarGridAdapter

class CalendarFragment : Fragment() {
    protected var mView // 声明一个视图对象
            : View? = null
    protected var mContext // 声明一个上下文对象
            : Context? = null
    private var mYear = 0
    private var mMonth // 当前碎片所要展示的年份和月份
            = 0
    private var gv_calendar // 声明一个网格视图对象
            : GridView? = null

    // 创建碎片视图
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = activity // 获取活动页面的上下文
        if (arguments != null) { // 如果碎片携带有包裹，则打开包裹获取参数信息
            mMonth = arguments!!.getInt("month", 1)
            mYear = arguments!!.getInt("year", 2000)
        }
        // 根据布局文件fragment_calendar.xml生成视图对象
        mView = inflater.inflate(R.layout.fragment_calendar, container, false)
        // 从布局视图中获取名叫gv_calendar的网格视图
        gv_calendar = mView?.findViewById<GridView>(R.id.gv_calendar)
        return mView // 返回该碎片的视图对象
    }

    override fun onResume() {
        super.onResume()
        // 构建一个月历的网格适配器
        val adapter = mContext?.let { CalendarGridAdapter(it, mYear, mMonth, 1) }
        // 给gv_calendar设置月历网格适配器
        gv_calendar?.setAdapter(adapter)
        gv_calendar?.setOnItemClickListener(adapter)
    }

    companion object {
        private const val TAG = "CalendarFragment"

        // 获取该碎片的一个实例
        fun newInstance(year: Int, month: Int): CalendarFragment {
            val fragment = CalendarFragment() // 创建该碎片的一个实例
            val bundle = Bundle() // 创建一个新包裹
            bundle.putInt("year", year) // 往包裹存入年份
            bundle.putInt("month", month) // 往包裹存入月份
            fragment.arguments = bundle // 把包裹塞给碎片
            return fragment // 返回碎片实例
        }
    }
}