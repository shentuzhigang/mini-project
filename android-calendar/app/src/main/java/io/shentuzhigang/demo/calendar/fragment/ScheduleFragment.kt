package io.shentuzhigang.demo.calendar.fragment

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.shentuzhigang.demo.calendar.R
import io.shentuzhigang.demo.calendar.ScheduleActivity
import io.shentuzhigang.demo.calendar.adapter.CalendarGridAdapter
import io.shentuzhigang.demo.calendar.adapter.ScheduleListAdapter
import io.shentuzhigang.demo.calendar.bean.CalendarTransfer
import io.shentuzhigang.demo.calendar.bean.ScheduleArrange
import io.shentuzhigang.demo.calendar.calendar.Constant
import io.shentuzhigang.demo.calendar.calendar.SpecialCalendar
import io.shentuzhigang.demo.calendar.database.DbHelper
import io.shentuzhigang.demo.calendar.database.ScheduleArrangeHelper
import io.shentuzhigang.demo.calendar.util.DateUtil

@SuppressLint(value = ["SimpleDateFormat", "DefaultLocale"])
class ScheduleFragment : Fragment() {
    protected var mView // 声明一个视图对象
            : View? = null
    protected var mContext // 声明一个上下文对象
            : Context? = null
    private var mSelectedWeek = 0
    private var mNowWeek // 当前选择的周数，以及今天所处的周数
            = 0
    private var lv_shedule // 声明一个列表视图对象
            : ListView? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay // 当前选择周数的星期一对应的年、月、日
            = 0
    private var first_pos = 0 // 当前选择周数的星期一在该月月历中的位置
    private var thisDate // 当前选择周数的星期一的具体日期
            : String? = null
    private val tranArray: ArrayList<CalendarTransfer> = ArrayList<CalendarTransfer>()
    private var mArrangeHelper // 声明一个日程安排的数据库帮助器
            : ScheduleArrangeHelper? = null

    // 创建碎片视图
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = activity // 获取活动页面的上下文
        if (arguments != null) { // 如果碎片携带有包裹，则打开包裹获取参数信息
            mSelectedWeek = arguments!!.getInt("week", 1)
        }
        // 获取今天所处的周数
        mNowWeek = SpecialCalendar.todayWeek
        initDate(mSelectedWeek - mNowWeek)
        Log.d(TAG, "thisDate=$thisDate,fisrt_pos=$first_pos")
        Log.d(TAG, "mYear=$mYear,mMonth=$mMonth,mDay=$mDay")
        // 根据年月日计算当周位于哪个日历网格适配器
        val calV = mContext?.let { CalendarGridAdapter(it, mYear, mMonth, mDay) }
        for (i in first_pos until first_pos + 7) { // 从月历中取出当周的七天
            val trans: CalendarTransfer? = calV?.getCalendarList(i)
            Log.d(
                TAG,
                "trans.solar_month=" + trans?.solar_month + ",trans.solar_day=" + trans?.solar_day
                        + ",trans.lunar_month=" + trans?.lunar_month + ",trans.lunar_day=" + trans?.lunar_day
            )
            if (trans != null) {
                tranArray.add(trans)
            } // 添加到日历转换队列中
        }
        // 根据布局文件fragment_schedule.xml生成视图对象
        mView = inflater.inflate(R.layout.fragment_schedule, container, false)
        // 从布局视图中获取名叫lv_shedule的列表视图
        lv_shedule = mView?.findViewById<ListView>(R.id.lv_shedule)
        return mView // 返回该碎片的视图对象
    }

    // 初始化当周的星期一对应的年、月、日
    private fun initDate(diff_weeks: Int) {
        val nowDate: String = DateUtil.nowDate
        thisDate = DateUtil.getAddDate(nowDate, diff_weeks * 7L)
        val thisDay = Integer.valueOf(thisDate!!.substring(6, 8))
        val weekIndex: Int = DateUtil.getWeekIndex(thisDate)
        var week_count = Math.ceil((thisDay - weekIndex + 0.5) / 7.0).toInt()
        if ((thisDay - weekIndex) % 7 > 0) {
            week_count++ // 需要计算当天所在周是当月的第几周
        }
        if (thisDay - weekIndex < 0) {
            week_count++
        }
        first_pos = week_count * 7
        mYear = thisDate!!.substring(0, 4).toInt()
        mMonth = thisDate!!.substring(4, 6).toInt()
        mDay = thisDate!!.substring(6, 8).toInt()
    }

    // 检查当周的七天是否存在特殊节日
    private fun checkFestival() {
        var i = 0
        while (i < tranArray.size) {
            val trans: CalendarTransfer = tranArray[i]
            var j = 0
            while (j < Constant.festivalArray.size) {
                if (trans.day_name.contains(Constant.festivalArray.get(j))) {
                    // 找到了特殊节日，则发送该节日图片广播
                    sendFestival(Constant.festivalResArray.get(j))
                    break
                }
                j++
            }
            if (j < Constant.festivalArray.size) {
                break
            }
            i++
        }
        // 未找到特殊节日，则发送日常图片的广播
        if (i >= tranArray.size) {
            sendFestival(R.drawable.normal_day)
        }
    }

    // 把图片编号通过广播发出去
    private fun sendFestival(resid: Int) {
        // 创建一个广播事件的意图
        val intent = Intent(ScheduleActivity.ACTION_SHOW_FESTIVAL)
        intent.putExtra(ScheduleActivity.EXTRA_FESTIVAL_RES, resid)
        // 通过本地的广播管理器来发送广播
        mContext?.let { LocalBroadcastManager.getInstance(it).sendBroadcast(intent) }
    }

    override fun onStart() {
        super.onStart()
        // 创建一个周数变更的广播接收器
        scrollControlReceiver = ScrollControlReceiver()
        // 注册广播接收器，注册之后才能正常接收广播
        mContext?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(
                scrollControlReceiver!!,
                IntentFilter(ScheduleActivity.ACTION_FRAGMENT_SELECTED)
            )
        }
        // 获得数据库帮助器的实例
        mArrangeHelper = mContext?.let { ScheduleArrangeHelper(it, DbHelper.db_name, null, 1) }
        val begin_trans: CalendarTransfer = tranArray[0]
        val begin_day = String.format(
            "%s%02d%02d",
            begin_trans.solar_year,
            begin_trans.solar_month,
            begin_trans.solar_day
        )
        val end_trans: CalendarTransfer = tranArray[tranArray.size - 1]
        val end_day = String.format(
            "%s%02d%02d",
            end_trans.solar_year,
            end_trans.solar_month,
            end_trans.solar_day
        )
        // 根据开始日期和结束日期，到数据库中查询这几天的日程安排信息
        // 构建一个当周日程的列表适配器
        val listAdapter = mContext?.let {
            ScheduleListAdapter(
                it,
                tranArray,
                mArrangeHelper!!.queryInfoByDayRange(begin_day, end_day) as ArrayList<ScheduleArrange>
            )
        }
        // 给lv_shedule设置日程列表适配器
        lv_shedule?.setAdapter(listAdapter)
        // 给lv_shedule设置列表项点击监听器
        lv_shedule?.setOnItemClickListener(listAdapter)
    }

    override fun onStop() {
        super.onStop()
        // 注销广播接收器，注销之后就不再接收广播
        mContext?.let { scrollControlReceiver?.let { it1 ->
            LocalBroadcastManager.getInstance(it).unregisterReceiver(
                it1
            )
        } }
        mArrangeHelper?.close() // 关闭数据库连接
    }

    // 声明一个周数变更的广播接收器
    private var scrollControlReceiver: ScrollControlReceiver? = null

    // 定义一个广播接收器，用于处理周数变更事件
    private inner class ScrollControlReceiver : BroadcastReceiver() {
        // 一旦接收到周数变更的广播，马上触发接收器的onReceive方法
        override fun onReceive(context: Context, intent: Intent) {
            // 从广播消息中取出最新的周数
            val selectedWeek: Int = intent.getIntExtra(ScheduleActivity.EXTRA_SELECTED_WEEK, 1)
            Log.d(TAG, "onReceive selectedWeek=$selectedWeek, mSelectedWeek=$mSelectedWeek")
            // 如果碎片对应的周数正好等于广播的周数，则检查当周是否存在节日
            if (mSelectedWeek == selectedWeek) {
                checkFestival()
            }
        }
    }

    companion object {
        private const val TAG = "ScheduleFragment"

        // 获取该碎片的一个实例
        fun newInstance(week: Int): ScheduleFragment {
            val fragment = ScheduleFragment() // 创建该碎片的一个实例
            val bundle = Bundle() // 创建一个新包裹
            bundle.putInt("week", week) // 往包裹存入周数
            fragment.arguments = bundle // 把包裹塞给碎片
            return fragment // 返回碎片实例
        }
    }
}