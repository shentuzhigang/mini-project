package io.shentuzhigang.demo.calendar.util

import android.annotation.SuppressLint
import android.text.TextUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ouyangshen on 2017/9/24.
 */
@SuppressLint("SimpleDateFormat")
object DateUtil {
    fun getNowDateTime(formatStr: String?): String {
        var format = formatStr
        if (TextUtils.isEmpty(format)) {
            format = "yyyyMMddHHmmss"
        }
        val sdf = SimpleDateFormat(format)
        return sdf.format(Date())
    }

    val nowTime: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss")
            return sdf.format(Date())
        }
    val nowTimeDetail: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss.SSS")
            return sdf.format(Date())
        }
    val nowDate: String
        get() {
            val sdf = SimpleDateFormat("yyyyMMdd")
            return sdf.format(Date())
        }
    val nowYearCN: String
        get() {
            val sdf = SimpleDateFormat("yyyy年")
            return sdf.format(Date())
        }
    val nowYear: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar[Calendar.YEAR]
        }
    val nowMonth: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar[Calendar.MONTH] + 1
        }
    val nowDay: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar[Calendar.DAY_OF_MONTH]
        }

    fun getAddDate(str: String?, day_num: Long): String {
        val sdf = SimpleDateFormat("yyyyMMdd")
        val old_date: Date
        old_date = try {
            sdf.parse(str)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
        var time = old_date.time
        val diff_time = day_num * 24 * 60 * 60 * 1000
        //		LogUtil.debug(TAG, "day_num="+day_num+", diff_time="+diff_time);
        time += diff_time
        val new_date = Date(time)
        return sdf.format(new_date)
    }

    fun getWeekIndex(s_date: String?): Int {
        val format = SimpleDateFormat("yyyyMMdd")
        val d_date: Date
        d_date = try {
            format.parse(s_date)
        } catch (e: Exception) {
            e.printStackTrace()
            return 1
        }
        val cal = Calendar.getInstance()
        cal.time = d_date
        var week_index = cal[Calendar.DAY_OF_WEEK] - 1
        if (week_index == 0) {
            week_index = 7
        }
        return week_index
    }

    fun isHoliday(text: String): Boolean {
        var result = true
        if (text.length == 2 && (text.indexOf("月") > 0 || text.contains("初") || text.contains("十")
                    || text.contains("廿") || text.contains("卅"))
            || text.length == 3 && text.indexOf("月") > 0
        ) {
            result = false
        }
        return result
    }
}