package io.shentuzhigang.demo.calendar.util

import android.content.Context
import android.content.SharedPreferences

// 这是共享参数的工具类，统一对共享参数的读写操作
class SharedUtil {
    // 把键名与字符串的配对信息写入共享参数
    fun writeString(key: String?, value: String?) {
        val editor = mShared!!.edit() // 获得编辑器的对象
        editor.putString(key, value) // 添加一个指定键名的字符串参数
        editor.commit() // 提交编辑器中的修改
    }

    // 根据键名到共享参数中查找对应的字符串对象
    fun readString(key: String?, defaultValue: String?): String? {
        return mShared!!.getString(key, defaultValue)
    }

    // 把键名与整型数的配对信息写入共享参数
    fun writeInt(key: String?, value: Int) {
        val editor = mShared!!.edit() // 获得编辑器的对象
        editor.putInt(key, value) // 添加一个指定键名的整型数参数
        editor.commit() // 提交编辑器中的修改
    }

    // 根据键名到共享参数中查找对应的整型数对象
    fun readInt(key: String?, defaultValue: Int): Int {
        return mShared!!.getInt(key, defaultValue)
    }

    companion object {
        private var mUtil // 声明一个共享参数工具类的实例
                : SharedUtil? = null
        private var mShared // 声明一个共享参数的实例
                : SharedPreferences? = null

        // 通过单例模式获取共享参数工具类的唯一实例
        fun getIntance(ctx: Context): SharedUtil? {
            if (mUtil == null) {
                mUtil = SharedUtil()
            }
            // 从share.xml中获取共享参数对象
            mShared = ctx.getSharedPreferences("share", Context.MODE_PRIVATE)
            return mUtil
        }
    }
}