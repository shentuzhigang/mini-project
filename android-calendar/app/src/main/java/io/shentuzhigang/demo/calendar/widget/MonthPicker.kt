package io.shentuzhigang.demo.calendar.widget

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.DatePicker

/**
 * Created by ouyangshen on 2018/1/2.
 */
// 由日期选择器派生出月份选择器
class MonthPicker(context: Context?, attrs: AttributeSet?) : DatePicker(context, attrs) {
    init {
        // 获取年月日的下拉列表项
        val vg = (getChildAt(0) as ViewGroup).getChildAt(0) as ViewGroup
        if (vg.childCount == 3) {
            // 有的机型显示格式为“年月日”，此时隐藏第三个控件
            vg.getChildAt(2).visibility = GONE
        } else if (vg.childCount == 5) {
            // 有的机型显示格式为“年|月|日”，此时隐藏第四个和第五个控件（即“|日”）
            vg.getChildAt(3).visibility = GONE
            vg.getChildAt(4).visibility = GONE
        }
    }
}