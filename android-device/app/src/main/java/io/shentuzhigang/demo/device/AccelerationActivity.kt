package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Vibrator
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.util.DateUtil



/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("SetTextI18n")
class AccelerationActivity : AppCompatActivity(), SensorEventListener {
    private var tv_shake: TextView? = null
    private var mSensorMgr // 声明一个传感管理器对象
            : SensorManager? = null
    private var mVibrator // 声明一个震动器对象
            : Vibrator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acceleration)
        tv_shake = findViewById(R.id.tv_shake)
        // 从系统服务中获取传感管理器对象
        mSensorMgr = getSystemService(SENSOR_SERVICE) as SensorManager
        // 从系统服务中获取震动器对象
        mVibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
    }

    override fun onPause() {
        super.onPause()
        // 注销当前活动的传感监听器
        mSensorMgr!!.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        // 给加速度传感器注册传感监听器
        mSensorMgr!!.registerListener(
            this,
            mSensorMgr!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) { // 加速度变更事件
            // values[0]:X轴，values[1]：Y轴，values[2]：Z轴
            val values = event.values
            if (Math.abs(values[0]) > 15 || Math.abs(values[1]) > 15 || Math.abs(
                    values[2]
                ) > 15
            ) {
                tv_shake?.setText(DateUtil.nowTime + " 恭喜您摇一摇啦")
                // 系统检测到摇一摇事件后，震动手机提示用户
                mVibrator!!.vibrate(500)
            }
        }
    }

    // 当传感器精度改变时回调该方法，一般无需处理
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
}