package io.shentuzhigang.demo.device

import android.os.*
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.widget.AudioPlayer
import io.shentuzhigang.demo.device.widget.AudioRecorder

/**
 * Created by ouyangshen on 2017/11/4.
 */
class AudioActivity : AppCompatActivity(), AudioRecorder.OnRecordFinishListener {
    private var ar_music // 声明一个音频录制器对象
            : AudioRecorder? = null
    private var ap_music // 声明一个音频播放器对象
            : AudioPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)
        // 从布局文件中获取名叫ar_music的音频录制器
        ar_music = findViewById(R.id.ar_music)
        // 给音频录制器设置录制完成监听器
        ar_music?.setOnRecordFinishListener(this)
        // 从布局文件中获取名叫ap_music的音频播放器
        ap_music = findViewById(R.id.ap_music)
    }

    // 音频录制一旦完成，就触发监听器的onRecordFinish方法
    override fun onRecordFinish() {
        // 延迟1秒后启动准备播放任务，好让系统有时间生成音频文件
        mHandler.postDelayed(mPreplay, 1000)
    }

    private val mHandler = Handler()

    // 定义一个准备播放任务
    private val mPreplay = Runnable { // 为音频播放器初始化待播放的音频文件
        ap_music!!.init(ar_music?.recordFilePath)
    }

    companion object {
        private const val TAG = "AudioActivity"
    }
}