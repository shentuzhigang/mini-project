package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.adapter.BlueListAdapter
import io.shentuzhigang.demo.device.bean.BlueDevice
import io.shentuzhigang.demo.device.util.BluetoothUtil
import java.util.*

/**
 * Created by ouyangshen on 2017/12/11.
 */
@SuppressLint("SetTextI18n")
class BluetoothActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener,
    OnItemClickListener {
    private var ck_bluetooth: CheckBox? = null
    private var tv_discovery: TextView? = null
    private var lv_bluetooth // 声明一个用于展示蓝牙设备的列表视图对象
            : ListView? = null
    private var mBluetooth // 声明一个蓝牙适配器对象
            : BluetoothAdapter? = null
    private var mListAdapter // 声明一个蓝牙设备的列表适配器对象
            : BlueListAdapter? = null
    private val mDeviceList = ArrayList<BlueDevice>() // 蓝牙设备队列
    private val mHandler = Handler() // 声明一个处理器对象
    private val mOpenCode = 1 // 是否允许扫描蓝牙设备的选择对话框返回结果代码
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth)
        initBluetooth() // 初始化蓝牙适配器
        ck_bluetooth = findViewById(R.id.ck_bluetooth)
        tv_discovery = findViewById(R.id.tv_discovery)
        lv_bluetooth = findViewById(R.id.lv_bluetooth)
        ck_bluetooth?.setOnCheckedChangeListener(this)
        if (BluetoothUtil.getBlueToothStatus(this)) {
            ck_bluetooth?.setChecked(true)
        }
        initBlueDevice() // 初始化蓝牙设备列表
    }

    // 初始化蓝牙适配器
    private fun initBluetooth() {
        // Android从4.3开始增加支持BLE技术（即蓝牙4.0及以上版本）
        mBluetooth = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            // 从系统服务中获取蓝牙管理器
            val bm = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
            bm.adapter
        } else {
            // 获取系统默认的蓝牙适配器
            BluetoothAdapter.getDefaultAdapter()
        }
        if (mBluetooth == null) {
            Toast.makeText(this, "本机未找到蓝牙功能", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    // 初始化蓝牙设备列表
    private fun initBlueDevice() {
        mDeviceList.clear()
        // 获取已经配对的蓝牙设备集合
        val bondedDevices = mBluetooth!!.bondedDevices
        for (device in bondedDevices) {
            mDeviceList.add(BlueDevice(device.name, device.address, device.bondState))
        }
        if (mListAdapter == null) { // 首次打开页面，则创建一个新的蓝牙设备列表
            mListAdapter = BlueListAdapter(this, mDeviceList)
            lv_bluetooth!!.adapter = mListAdapter
            lv_bluetooth!!.onItemClickListener = this
        } else { // 不是首次打开页面，则刷新蓝牙设备列表
            mListAdapter!!.notifyDataSetChanged()
        }
    }

    private val mDiscoverable: Runnable = object : Runnable {
        override fun run() {
            // Android8.0要在已打开蓝牙功能时才会弹出下面的选择窗
            if (BluetoothUtil.getBlueToothStatus(this@BluetoothActivity)) {
                // 弹出是否允许扫描蓝牙设备的选择对话框
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                startActivityForResult(intent, mOpenCode)
            } else {
                mHandler.postDelayed(this, 1000)
            }
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_bluetooth) {
            if (isChecked) { // 开启蓝牙功能
                ck_bluetooth!!.text = "蓝牙开"
                if (!BluetoothUtil.getBlueToothStatus(this)) {
                    BluetoothUtil.setBlueToothStatus(this, true) // 开启蓝牙功能
                }
                mHandler.post(mDiscoverable)
            } else { // 关闭蓝牙功能
                ck_bluetooth!!.text = "蓝牙关"
                cancelDiscovery() // 取消蓝牙设备的搜索
                BluetoothUtil.setBlueToothStatus(this, false) // 关闭蓝牙功能
                initBlueDevice() // 初始化蓝牙设备列表
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == mOpenCode) { // 来自允许蓝牙扫描的对话框
            // 延迟50毫秒后启动蓝牙设备的刷新任务
            mHandler.postDelayed(mRefresh, 50)
            if (resultCode == RESULT_OK) {
                Toast.makeText(
                    this, "允许本地蓝牙被附近的其它蓝牙设备发现",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(
                    this, "不允许蓝牙被附近的其它蓝牙设备发现",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    // 定义一个刷新任务，每隔两秒刷新扫描到的蓝牙设备
    private val mRefresh: Runnable = object : Runnable {
        override fun run() {
            beginDiscovery() // 开始扫描周围的蓝牙设备
            // 延迟2秒后再次启动蓝牙设备的刷新任务
            mHandler.postDelayed(this, 2000)
        }
    }

    // 开始扫描周围的蓝牙设备
    private fun beginDiscovery() {
        // 如果当前不是正在搜索，则开始新的搜索任务
        if (!mBluetooth!!.isDiscovering) {
            initBlueDevice() // 初始化蓝牙设备列表
            tv_discovery!!.text = "正在搜索蓝牙设备"
            mBluetooth!!.startDiscovery() // 开始扫描周围的蓝牙设备
        }
    }

    // 取消蓝牙设备的搜索
    private fun cancelDiscovery() {
        mHandler.removeCallbacks(mRefresh)
        tv_discovery!!.text = "取消搜索蓝牙设备"
        // 当前正在搜索，则取消搜索任务
        if (mBluetooth!!.isDiscovering) {
            mBluetooth!!.cancelDiscovery() // 取消扫描周围的蓝牙设备
        }
    }

    override fun onStart() {
        super.onStart()
        mHandler.postDelayed(mRefresh, 50)
        // 需要过滤多个动作，则调用IntentFilter对象的addAction添加新动作
        val discoveryFilter = IntentFilter()
        discoveryFilter.addAction(BluetoothDevice.ACTION_FOUND)
        discoveryFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        discoveryFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        // 注册蓝牙设备搜索的广播接收器
        registerReceiver(discoveryReceiver, discoveryFilter)
    }

    override fun onStop() {
        super.onStop()
        cancelDiscovery() // 取消蓝牙设备的搜索
        // 注销蓝牙设备搜索的广播接收器
        unregisterReceiver(discoveryReceiver)
    }

    // 蓝牙设备的搜索结果通过广播返回
    private val discoveryReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            Log.d(TAG, "onReceive action=$action")
            // 获得已经搜索到的蓝牙设备
            if (action == BluetoothDevice.ACTION_FOUND) { // 发现新的蓝牙设备
                val device =
                    intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                Log.d(
                    TAG,
                    "name=" + device!!.name + ", state=" + device.bondState
                )
                refreshDevice(device, device.bondState) // 将发现的蓝牙设备加入到设备列表
            } else if (action == BluetoothAdapter.ACTION_DISCOVERY_FINISHED) { // 搜索完毕
                //mHandler.removeCallbacks(mRefresh); // 需要持续搜索就要注释这行
                tv_discovery!!.text = "蓝牙设备搜索完成"
            } else if (action == BluetoothDevice.ACTION_BOND_STATE_CHANGED) { // 配对状态变更
                val device =
                    intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (device!!.bondState == BluetoothDevice.BOND_BONDING) {
                    tv_discovery!!.text = "正在配对" + device.name
                } else if (device.bondState == BluetoothDevice.BOND_BONDED) {
                    tv_discovery!!.text = "完成配对" + device.name
                    mHandler.postDelayed(mRefresh, 50)
                } else if (device.bondState == BluetoothDevice.BOND_NONE) {
                    tv_discovery!!.text = "取消配对" + device.name
                    refreshDevice(device, device.bondState)
                }
            }
        }
    }

    // 刷新蓝牙设备列表
    private fun refreshDevice(device: BluetoothDevice?, state: Int) {
        var i: Int
        i = 0
        while (i < mDeviceList.size) {
            val item = mDeviceList[i]
            if (item.address == device!!.address) {
                item.state = state
                mDeviceList[i] = item
                break
            }
            i++
        }
        if (i >= mDeviceList.size) {
            mDeviceList.add(BlueDevice(device!!.name, device.address, device.bondState))
        }
        mListAdapter!!.notifyDataSetChanged()
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        //cancelDiscovery();
        val item = mDeviceList[position]
        // 根据设备地址获得远端的蓝牙设备对象
        val device = mBluetooth!!.getRemoteDevice(item.address)
        Log.d(
            TAG,
            "getBondState=" + device.bondState + ", item.state=" + item.state
        )
        if (device.bondState == BluetoothDevice.BOND_NONE) { // 尚未配对
            BluetoothUtil.createBond(device) // 创建配对信息
        } else if (device.bondState == BluetoothDevice.BOND_BONDED) { // 已经配对
            val isSucc = BluetoothUtil.removeBond(device) // 移除配对信息
            if (!isSucc) {
                refreshDevice(device, BluetoothDevice.BOND_NONE)
            }
        }
    }

    companion object {
        private const val TAG = "BluetoothActivity"
    }
}