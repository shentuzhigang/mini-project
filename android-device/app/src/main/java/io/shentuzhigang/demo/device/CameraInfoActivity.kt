package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.hardware.*
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.adapter.CameraAdapter
import io.shentuzhigang.demo.device.bean.CameraInfo
import java.util.*

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("DefaultLocale")
class CameraInfoActivity : AppCompatActivity() {
    private var lv_camera: ListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_info)
        lv_camera = findViewById(R.id.lv_camera)
        checkCamera()
    }

    // 检查当前设备支持的摄像头信息
    private fun checkCamera() {
        val cameraList = ArrayList<CameraInfo>()
        // 获取摄像头的个数
        val cameraCount = Camera.getNumberOfCameras()
        Log.d(TAG, String.format("摄像头个数=%d", cameraCount))
        for (i in 0 until cameraCount) {
            val info = CameraInfo()
            val camera = Camera.open(i) // 打开指定摄像头
            val params = camera.parameters // 获取该摄像头的参数
            info.camera_type = if (i == 0) "前置" else "后置" // 获取摄像头的类型
            info.flash_mode = params.flashMode // 获取摄像头的闪光模式
            info.focus_mode = params.focusMode // 获取摄像头的对焦模式
            info.scene_mode = params.sceneMode // 获取摄像头的场景模式
            info.color_effect = params.colorEffect // 获取摄像头的颜色效果
            info.white_balance = params.whiteBalance // 获取摄像头的白平衡
            info.max_zoom = params.maxZoom // 获取摄像头的最大缩放比例
            info.zoom = params.zoom // 获取摄像头的当前缩放比例
            info.resolutionList = params.supportedPreviewSizes // 获取摄像头支持的预览分辨率
            camera.release() // 释放摄像头
            cameraList.add(info)
        }
        val adapter = CameraAdapter(this, cameraList)
        lv_camera!!.adapter = adapter
    }

    companion object {
        private const val TAG = "CameraInfoActivity"
    }
}