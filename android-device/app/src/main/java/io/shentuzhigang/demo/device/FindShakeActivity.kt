package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.widget.BettingView
import java.util.*


/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("SetTextI18n")
class FindShakeActivity : AppCompatActivity(), SensorEventListener {
    private var tv_cake: TextView? = null
    private var bv_cake // 声明一个博饼视图对象
            : BettingView? = null
    private var mSensorMgr // 声明一个传感管理器对象
            : SensorManager? = null
    private var mVibrator // 声明一个震动器对象
            : Vibrator? = null
    private var mDiceList = ArrayList<Int>() // 骰子队列
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_shake)
        tv_cake = findViewById(R.id.tv_cake)
        // 从布局文件中获取名叫bv_cake的博饼视图
        bv_cake = findViewById(R.id.bv_cake)
        // 从系统服务中获取传感管理器对象
        mSensorMgr = getSystemService(SENSOR_SERVICE) as SensorManager
        // 从系统服务中获取震动器对象
        mVibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
        for (i in 0..5) { // 一共六个骰子
            mDiceList.add(i)
        }
        // 设置博饼视图的骰子队列
        bv_cake?.setDiceList(mDiceList)
    }

    override fun onPause() {
        super.onPause()
        // 注销当前活动的传感监听器
        mSensorMgr!!.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        // 给加速度传感器注册传感监听器
        mSensorMgr!!.registerListener(
            this,
            mSensorMgr!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) { // 加速度变更事件
            // values[0]:X轴，values[1]：Y轴，values[2]：Z轴
            val values = event.values
            if (Math.abs(values[0]) > 15 || Math.abs(values[1]) > 15 || Math.abs(
                    values[2]
                ) > 15
            ) {
                if (!isShaking) {
                    isShaking = true
                    mCount = 0
                    mHandler.post(mShake)
                    // 系统检测到摇一摇事件后，震动手机提示用户
                    mVibrator!!.vibrate(500)
                }
            }
        }
    }

    // 当传感器精度改变时回调该方法，一般无需处理
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    private var isShaking = false // 是否正在摇骰子
    private var mCount = 0 // 骰子滚动的次数，暂定每次摇骰子滚动十次
    private val mHandler = Handler()

    // 定义一个摇骰子任务
    private val mShake: Runnable = object : Runnable {
        override fun run() {
            if (mCount < 10) { // 尚未结束摇骰子
                mCount++
                bv_cake!!.setRandom()
                mHandler.postDelayed(this, 150)
            } else { // 已经结束摇骰子
                mDiceList = ArrayList()
                for (i in 0..5) {
                    mDiceList.add((Math.random() * 30 % 6).toInt())
                }
                bv_cake!!.setDiceList(mDiceList)
                val desc = calculatePrize()
                tv_cake!!.text = "恭喜，您的博饼结果为：$desc"
                isShaking = false
            }
        }
    }

    // 计算中奖等级
    private fun calculatePrize(): String {
        val four_count = checkCount(4)
        return if (four_count == 6) { // 出现六个红四
            "状元(六杯红)"
        } else if (checkCount(1) == 6) { // 出现六个红一
            "状元(遍地锦)"
        } else if (four_count == 5) { // 出现五个红四
            "状元(五红)"
        } else if (four_count == 4) {
            if (checkCount(1) == 2) { // 出现四个红四加两个红一
                "状元插金花"
            } else {
                "状元(四点红)" // 出现四个红四，没有两个红一
            }
        } else if (four_count == 3) { // 出现三个红四
            "三红"
        } else if (checkCount(6) == 6) { // 出现六个黑六
            "黑六勃"
        } else if (checkCount(1) == 1 && checkCount(2) == 1 && checkCount(3) == 1 && checkCount(4) == 1 && checkCount(
                5
            ) == 1 && checkCount(6) == 1
        ) { // 123456的骰子各出现一个
            "对堂"
        } else if (checkCount(1) == 5 || checkCount(2) == 5 || checkCount(3) == 5 || checkCount(5) == 5 || checkCount(
                6
            ) == 5
        ) { // 出现五个相同的点数（五个红四除外）
            "状元(五子登科)"
        } else if (checkCount(1) == 4 || checkCount(2) == 4 || checkCount(3) == 4 || checkCount(5) == 4 || checkCount(
                6
            ) == 4
        ) { // 出现四个相同的点数（四个红四除外）
            "四进"
        } else if (four_count == 2) { // 出现两个红四
            "二举"
        } else if (four_count == 1) { // 出现一个红四
            "一秀"
        } else {
            "别着急，再来一次"
        }
    }

    // 检查某个点数的数目
    private fun checkCount(number: Int): Int {
        var count = 0
        for (i in mDiceList.indices) {
            if (mDiceList[i] + 1 == number) {
                count++
            }
        }
        return count
    }

    companion object {
        private const val TAG = "FindShakeActivity"
    }
}