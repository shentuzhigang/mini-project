package io.shentuzhigang.demo.device

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.*
import android.os.*
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.shentuzhigang.demo.device.bean.Satellite
import io.shentuzhigang.demo.device.util.DateUtil
import io.shentuzhigang.demo.device.util.SwitchUtil
import io.shentuzhigang.demo.device.widget.CompassView
import java.util.*


/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("DefaultLocale")
class FindSmellActivity : AppCompatActivity() {
    private var tv_satellite: TextView? = null
    private var cv_satellite // 声明一个罗盘视图对象
            : CompassView? = null
    private val mapSatellite: MutableMap<Int, Satellite> = HashMap()
    private var mLocationMgr // 声明一个定位管理器对象
            : LocationManager? = null
    private val mCriteria = Criteria() // 声明一个定位准则对象
    private val mHandler = Handler()
    private var isLocationEnable = false // 定位服务是否可用
    private var mLocationType: String? = "" // 定位类型。是卫星定位还是网络定位
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_smell)
        tv_satellite = findViewById(R.id.tv_satellite)
        // 从布局文件中获取名叫cv_satellite的罗盘视图
        cv_satellite = findViewById(R.id.cv_satellite)
        SwitchUtil.checkGpsIsOpen(this, "需要打开定位功能才能查看卫星导航信息")
    }

    override fun onResume() {
        super.onResume()
        mHandler.removeCallbacks(mRefresh) // 移除定位刷新任务
        initLocation()
        mHandler.postDelayed(mRefresh, 100) // 延迟100毫秒启动定位刷新任务
    }

    // 初始化定位服务
    private fun initLocation() {
        // 从系统服务中获取定位管理器
        mLocationMgr = getSystemService(LOCATION_SERVICE) as LocationManager
        // 设置定位精确度。Criteria.ACCURACY_COARSE表示粗略，Criteria.ACCURACY_FIN表示精细
        mCriteria.accuracy = Criteria.ACCURACY_FINE
        // 设置是否需要海拔信息
        mCriteria.isAltitudeRequired = true
        // 设置是否需要方位信息
        mCriteria.isBearingRequired = true
        // 设置是否允许运营商收费
        mCriteria.isCostAllowed = true
        // 设置对电源的需求
        mCriteria.powerRequirement = Criteria.POWER_LOW
        // 获取定位管理器的最佳定位提供者
        val bestProvider = mLocationMgr!!.getBestProvider(mCriteria, true)
        if (mLocationMgr!!.isProviderEnabled(bestProvider!!)) {  // 定位提供者当前可用
            mLocationType = bestProvider
            beginLocation(bestProvider)
            isLocationEnable = true
        } else { // 定位提供者暂不可用
            isLocationEnable = false
        }
    }

    // 设置定位结果文本
    private fun setLocationText(location: Location?) {
        if (location != null) {
            val desc = String.format(
                """
    当前定位类型：%s，定位时间：%s
    经度：%f，纬度：%f
    高度：%d米，精度：%d米
    """.trimIndent(),
                mLocationType, DateUtil.nowTime,
                location.longitude, location.latitude,
                Math.round(location.altitude), Math.round(location.accuracy)
            )
            tv_satellite!!.text = desc
        } else {
            Log.d(TAG, "暂未获取到定位对象")
        }
    }

    // 开始定位
    private fun beginLocation(method: String?) {
        // 检查当前设备是否已经开启了定位功能
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(this, "请授予定位权限并开启定位功能", Toast.LENGTH_SHORT).show()
            return
        }
        // 设置定位管理器的位置变更监听器
        mLocationMgr!!.requestLocationUpdates(method!!, 300, 0f, mLocationListener)
        // 获取最后一次成功定位的位置信息
        val location = mLocationMgr!!.getLastKnownLocation(method)
        setLocationText(location)
        // 给定位管理器添加导航状态监听器
        mLocationMgr!!.addGpsStatusListener(mStatusListener)
    }

    // 定义一个位置变更监听器
    private val mLocationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            setLocationText(location)
        }

        override fun onProviderDisabled(arg0: String) {}
        override fun onProviderEnabled(arg0: String) {}
        override fun onStatusChanged(arg0: String, arg1: Int, arg2: Bundle) {}
    }

    // 定义一个刷新任务，若无法定位则每隔一秒就尝试定位
    private val mRefresh: Runnable = object : Runnable {
        override fun run() {
            if (!isLocationEnable) {
                initLocation()
                mHandler.postDelayed(this, 1000)
            }
        }
    }

    override fun onDestroy() {
        if (mLocationMgr != null) {
            // 移除定位管理器的导航状态监听器
            mLocationMgr!!.removeGpsStatusListener(mStatusListener)
            // 移除定位管理器的位置变更监听器
            mLocationMgr!!.removeUpdates(mLocationListener)
        }
        super.onDestroy()
    }

    // 定义一个导航状态监听器
    @SuppressLint("MissingPermission")
    private val mStatusListener = GpsStatus.Listener { event ->

        // 在卫星导航系统的状态变更时触发
        // 获取卫星定位的状态信息

        val gpsStatus = mLocationMgr!!.getGpsStatus(null)
        when (event) {
            GpsStatus.GPS_EVENT_SATELLITE_STATUS -> {
                // 得到所有收到的卫星的信息，包括 卫星的高度角、方位角、信噪比、和伪随机号（及卫星编号）
                val satellites = gpsStatus!!.satellites
                for (satellite in satellites) {
                    /*
                          * satellite.getElevation(); //卫星的仰角 (卫星的高度)
                          * satellite.getAzimuth(); //卫星的方位角
                          * satellite.getSnr(); //卫星的信噪比
                          * satellite.getPrn(); //卫星的伪随机码，可以认为就是卫星的编号
                          * satellite.hasAlmanac(); //卫星是否有年历表
                          * satellite.hasEphemeris(); //卫星是否有星历表
                          * satellite.usedInFix(); //卫星是否被用于近期的GPS修正计算
                          */
                    val item = Satellite()
                    item.seq = satellite.prn
                    item.signal = Math.round(satellite.snr)
                    item.elevation = Math.round(satellite.elevation)
                    item.azimuth = Math.round(satellite.azimuth)
                    item.time = DateUtil.nowDateTime
                    if (item.seq <= 64 || item.seq >= 120 && item.seq <= 138) {
                        item.nation = "美国"
                        item.name = "GPS"
                    } else if (item.seq >= 201 && item.seq <= 237) {
                        item.nation = "中国"
                        item.name = "北斗"
                    } else if (item.seq >= 65 && item.seq <= 89) {
                        item.nation = "俄罗斯"
                        item.name = "格洛纳斯"
                    } else {
                        Log.d(
                            TAG,
                            "Other seq=" + item.seq + ", signal=" + item.signal + ", elevation=" + item.elevation + ", azimuth=" + item.azimuth
                        )
                        item.nation = "其他"
                        item.name = "未知"
                    }
                    mapSatellite[item.seq] = item
                }
                cv_satellite!!.setSatelliteMap(mapSatellite)
            }
            GpsStatus.GPS_EVENT_FIRST_FIX, GpsStatus.GPS_EVENT_STARTED, GpsStatus.GPS_EVENT_STOPPED -> {}
            else -> {}
        }
    }

    companion object {
        private const val TAG = "FindSmellActivity"
    }
}