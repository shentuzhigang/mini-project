package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("DefaultLocale")
class GyroscopeActivity : AppCompatActivity(), SensorEventListener {
    private var tv_gyroscope: TextView? = null
    private var mSensorMgr // 声明一个传感管理器对象
            : SensorManager? = null
    private var mTimestamp // 记录上次的时间戳
            = 0f
    private val mAngle = FloatArray(3) // 记录xyz三个方向上的旋转角度
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gyroscope)
        tv_gyroscope = findViewById(R.id.tv_gyroscope)
        // 从系统服务中获取传感管理器对象
        mSensorMgr = getSystemService(SENSOR_SERVICE) as SensorManager
    }

    override fun onPause() {
        super.onPause()
        // 注销当前活动的传感监听器
        mSensorMgr!!.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        // 获取当前设备支持的传感器列表
        val sensorList = mSensorMgr!!.getSensorList(Sensor.TYPE_ALL)
        var isSuitable = false
        for (sensor in sensorList) {
            if (sensor.type == Sensor.TYPE_GYROSCOPE) { // 找到陀螺仪
                isSuitable = true
                break
            }
        }
        if (isSuitable) {
            // 给陀螺仪传感器注册传感监听器
            mSensorMgr!!.registerListener(
                this,
                mSensorMgr!!.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                SensorManager.SENSOR_DELAY_FASTEST
            )
        } else {
            tv_gyroscope!!.text = "当前设备不支持陀螺仪，请检查是否存在陀螺仪传感器"
        }
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_GYROSCOPE) { // 陀螺仪角度变更事件
            if (mTimestamp != 0f) {
                val dT: Float = (event.timestamp - mTimestamp) * NS2S
                mAngle[0] += event.values[0] * dT
                mAngle[1] += event.values[1] * dT
                mAngle[2] += event.values[2] * dT
                // x轴的旋转角度，手机平放桌上，然后绕侧边转动
                val angleX = Math.toDegrees(mAngle[0].toDouble()).toFloat()
                // y轴的旋转角度，手机平放桌上，然后绕底边转动
                val angleY = Math.toDegrees(mAngle[1].toDouble()).toFloat()
                // z轴的旋转角度，手机平放桌上，然后水平旋转
                val angleZ = Math.toDegrees(mAngle[2].toDouble()).toFloat()
                val desc = String.format(
                    "陀螺仪检测到当前x轴方向的转动角度为%f，y轴方向的转动角度为%f，z轴方向的转动角度为%f",
                    angleX, angleY, angleZ
                )
                tv_gyroscope!!.text = desc
            }
            mTimestamp = event.timestamp.toFloat()
        }
    }

    //当传感器精度改变时回调该方法，一般无需处理
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}

    companion object {
        private const val NS2S = 1.0f / 1000000000.0f // 将纳秒转化为秒
    }
}