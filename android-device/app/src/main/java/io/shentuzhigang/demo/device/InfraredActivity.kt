package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.hardware.ConsumerIrManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.util.DateUtil

/**
 * Created by ouyangshen on 2018/1/29.
 */
@SuppressLint("DefaultLocale")
@TargetApi(Build.VERSION_CODES.KITKAT)
class InfraredActivity : AppCompatActivity(), View.OnClickListener {
    private var tv_infrared: TextView? = null
    private var cim // 声明一个红外遥控管理器对象
            : ConsumerIrManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_infrared)
        findViewById<View>(R.id.btn_send).setOnClickListener(this)
        findViewById<View>(R.id.btn_receive).setOnClickListener(this)
        tv_infrared = findViewById(R.id.tv_infrared)
        initInfrared()
    }

    // 初始化红外遥控管理器
    private fun initInfrared() {
        // 从系统服务中获取红外遥控管理器
        cim = getSystemService(CONSUMER_IR_SERVICE) as ConsumerIrManager
        if (!cim!!.hasIrEmitter()) { // 判断当前设备是否支持红外功能
            tv_infrared!!.text = "当前手机不支持红外遥控"
        } else {
            tv_infrared!!.text = "当前手机支持红外遥控"
        }
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_send) {
            // NEC协议的红外编码格式通常为：
            // 引导码+用户码+数据码+数据反码+结束码。
            // 下面是一个扫地机器人的开关按键编码，用户码为4055，数据码为44。
            val pattern = intArrayOf(
                9000,
                4500,  // 开头两个数字表示引导码
                // 下面两行表示用户码
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                1680,
                560,
                560,
                560,
                1680,
                560,
                560,
                560,
                1680,
                560,
                560,
                560,
                1680,
                560,
                560,
                560,
                1680,
                560,
                560,  // 下面一行表示数据码
                560,
                560,
                560,
                560,
                560,
                1680,
                560,
                560,
                560,
                560,
                560,
                560,
                560,
                1680,
                560,
                560,  // 下面一行表示数据反码
                560,
                1680,
                560,
                1680,
                560,
                560,
                560,
                1680,
                560,
                1680,
                560,
                1680,
                560,
                560,
                560,
                1680,
                560,
                20000
            ) // 末尾两个数字表示结束码
            // 发射指定编码格式的红外信号。普通家电的红外发射频率一般为38KHz
            cim!!.transmit(38000, pattern)
            val hint = DateUtil.nowTime + "：已发射红外信号，请观察扫地机器人是否有反应"
            tv_infrared!!.text = hint
        } else if (v.id == R.id.btn_receive) {
            // 获得可用的载波频率范围
            val freqs = cim!!.carrierFrequencies
            var result = "当前手机的红外载波频率范围为：\n"
            // 遍历获取所有的频率段
            for (range in freqs) {
                result = String.format(
                    "%s    %d - %d\n", result,
                    range.minFrequency, range.maxFrequency
                )
            }
            tv_infrared!!.text = result
        }
    }

    companion object {
        private const val TAG = "InfraredActivity"
    }
}