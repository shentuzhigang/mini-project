package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.util.DateUtil
import io.shentuzhigang.demo.device.util.SwitchUtil

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint(value = ["DefaultLocale", "SetTextI18n"])
class LightActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener,
    SensorEventListener {
    private var tv_light: TextView? = null
    private var mSensorMgr // 声明一个传感管理器对象
            : SensorManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_light)
        val ck_bright = findViewById<CheckBox>(R.id.ck_bright)
        // 检查屏幕亮度是否为自动调节
        if (SwitchUtil.getAutoBrightStatus(this)) {
            ck_bright.isChecked = true
        }
        // Android8.0之后普通应用不允许修改系统设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            ck_bright.setOnCheckedChangeListener(this)
        } else {
            ck_bright.isEnabled = false
        }
        tv_light = findViewById(R.id.tv_light)
        // 从系统服务中获取传感管理器对象
        mSensorMgr = getSystemService(SENSOR_SERVICE) as SensorManager
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_bright) {
            // 设置是否开启屏幕亮度的自动调节
            SwitchUtil.setAutoBrightStatus(this, isChecked)
        }
    }

    override fun onPause() {
        super.onPause()
        // 注销当前活动的传感监听器
        mSensorMgr!!.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        // 给光线传感器注册传感监听器
        mSensorMgr!!.registerListener(
            this, mSensorMgr!!.getDefaultSensor(Sensor.TYPE_LIGHT),
            SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_LIGHT) { // 光线强度变更事件
            val light_strength = event.values[0]
            tv_light?.setText(DateUtil.nowTime + " 当前光线强度为" + light_strength)
        }
    }

    //当传感器精度改变时回调该方法，一般无需处理
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
}