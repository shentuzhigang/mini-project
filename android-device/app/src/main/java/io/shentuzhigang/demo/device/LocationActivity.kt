package io.shentuzhigang.demo.device

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.shentuzhigang.demo.device.util.DateUtil
import io.shentuzhigang.demo.device.util.SwitchUtil

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint(value = ["DefaultLocale", "SetTextI18n"])
class LocationActivity : AppCompatActivity() {
    private var tv_location: TextView? = null
    private var mLocation = ""
    private var mLocationMgr // 声明一个定位管理器对象
            : LocationManager? = null
    private val mCriteria = Criteria() // 声明一个定位准则对象
    private val mHandler = Handler() // 声明一个处理器
    private var isLocationEnable = false // 定位服务是否可用
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        tv_location = findViewById(R.id.tv_location)
        SwitchUtil.checkGpsIsOpen(this, "需要打开定位功能才能查看定位结果信息")
    }

    override fun onResume() {
        super.onResume()
        mHandler.removeCallbacks(mRefresh) // 移除定位刷新任务
        initLocation()
        mHandler.postDelayed(mRefresh, 100) // 延迟100毫秒启动定位刷新任务
    }

    // 初始化定位服务
    private fun initLocation() {
        // 从系统服务中获取定位管理器
        mLocationMgr = getSystemService(LOCATION_SERVICE) as LocationManager
        // 设置定位精确度。Criteria.ACCURACY_COARSE表示粗略，Criteria.ACCURACY_FIN表示精细
        mCriteria.accuracy = Criteria.ACCURACY_FINE
        // 设置是否需要海拔信息
        mCriteria.isAltitudeRequired = true
        // 设置是否需要方位信息
        mCriteria.isBearingRequired = true
        // 设置是否允许运营商收费
        mCriteria.isCostAllowed = true
        // 设置对电源的需求
        mCriteria.powerRequirement = Criteria.POWER_LOW
        // 获取定位管理器的最佳定位提供者
        val bestProvider = mLocationMgr!!.getBestProvider(mCriteria, true)
        if (mLocationMgr!!.isProviderEnabled(bestProvider!!)) { // 定位提供者当前可用
            tv_location!!.text = "正在获取" + bestProvider + "定位对象"
            mLocation = String.format("定位类型=%s", bestProvider)
            beginLocation(bestProvider)
            isLocationEnable = true
        } else { // 定位提供者暂不可用
            tv_location!!.text = """
                
                ${bestProvider}定位不可用
                """.trimIndent()
            isLocationEnable = false
        }
    }

    // 设置定位结果文本
    private fun setLocationText(location: Location?) {
        if (location != null) {
            val desc = String.format(
                """%s
定位对象信息如下： 
	其中时间：%s
	其中经度：%f，纬度：%f
	其中高度：%d米，精度：%d米""",
                mLocation, DateUtil.nowDateTimeFormat,
                location.longitude, location.latitude,
                Math.round(location.altitude), Math.round(location.accuracy)
            )
            Log.d(TAG, desc)
            tv_location!!.text = desc
        } else {
            tv_location!!.text = "$mLocation\n暂未获取到定位对象"
        }
    }

    // 开始定位
    private fun beginLocation(method: String?) {
        // 检查当前设备是否已经开启了定位功能
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(this, "请授予定位权限并开启定位功能", Toast.LENGTH_SHORT).show()
            return
        }
        // 设置定位管理器的位置变更监听器
        mLocationMgr!!.requestLocationUpdates(method!!, 300, 0f, mLocationListener)
        // 获取最后一次成功定位的位置信息
        val location = mLocationMgr!!.getLastKnownLocation(method)
        setLocationText(location)
    }

    // 定义一个位置变更监听器
    private val mLocationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            setLocationText(location)
        }

        override fun onProviderDisabled(arg0: String) {}
        override fun onProviderEnabled(arg0: String) {}
        override fun onStatusChanged(arg0: String, arg1: Int, arg2: Bundle) {}
    }

    // 定义一个刷新任务，若无法定位则每隔一秒就尝试定位
    private val mRefresh: Runnable = object : Runnable {
        override fun run() {
            if (!isLocationEnable) {
                initLocation()
                mHandler.postDelayed(this, 1000)
            }
        }
    }

    override fun onDestroy() {
        if (mLocationMgr != null) {
            // 移除定位管理器的位置变更监听器
            mLocationMgr!!.removeUpdates(mLocationListener)
        }
        super.onDestroy()
    }

    companion object {
        private const val TAG = "LocationActivity"
    }
}