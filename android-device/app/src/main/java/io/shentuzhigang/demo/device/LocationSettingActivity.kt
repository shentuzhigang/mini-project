package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.util.SwitchUtil

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("SetTextI18n")
class LocationSettingActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {
    private var ck_gps // 声明一个定位功能的复选框对象
            : CheckBox? = null
    private var ck_wlan // 声明一个WLAN功能的复选框对象
            : CheckBox? = null
    private var ck_mobiledata // 声明一个数据连接功能的复选框对象
            : CheckBox? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_setting)
        ck_gps = findViewById(R.id.ck_gps)
        ck_wlan = findViewById(R.id.ck_wlan)
        ck_mobiledata = findViewById(R.id.ck_mobiledata)
    }

    override fun onResume() {
        super.onResume()
        ck_gps!!.setOnCheckedChangeListener(null)
        ck_wlan!!.setOnCheckedChangeListener(null)
        ck_mobiledata!!.setOnCheckedChangeListener(null)
        // 获取定位功能的开关状态
        val isGpsOpen = SwitchUtil.getGpsStatus(this)
        ck_gps!!.isChecked = isGpsOpen
        ck_gps!!.text = "定位功能" + if (isGpsOpen) "开启" else "关闭"
        // 获取WLAN功能的开关状态
        val isWlanOpen = SwitchUtil.getWlanStatus(this)
        ck_wlan!!.isChecked = isWlanOpen
        ck_wlan!!.text = "WLAN功能" + if (isWlanOpen) "开启" else "关闭"
        // 获取数据连接功能的开关状态
        val isMobileOpen = SwitchUtil.getMobileDataStatus(this)
        ck_mobiledata!!.isChecked = isMobileOpen
        ck_mobiledata!!.text = "数据连接" + if (isMobileOpen) "开启" else "关闭"
        ck_gps!!.setOnCheckedChangeListener(this)
        if (Build.VERSION.SDK_INT >= 29) { // Android10之后，普通应用不能开关WiFi
            ck_wlan!!.isEnabled = false
        } else {
            ck_wlan!!.setOnCheckedChangeListener(this)
        }
        ck_mobiledata!!.isEnabled = false // 数据连接只有系统应用才能开关
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        val id = buttonView.id
        if (id == R.id.ck_gps) {
            // 跳转到系统的定位设置页面
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        } else if (id == R.id.ck_wlan) {
            // 设置WLAN功能的开关状态
            SwitchUtil.setWlanStatus(this, isChecked)
            ck_wlan!!.text = "WLAN功能" + if (isChecked) "开启" else "关闭"
        }
    }
}