package io.shentuzhigang.demo.device

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.util.PermissionUtil

/**
 * Created by ouyangshen on 2017/11/4.
 */
class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.btn_turn_view).setOnClickListener(this)
        findViewById<View>(R.id.btn_turn_surface).setOnClickListener(this)
        findViewById<View>(R.id.btn_camera_info).setOnClickListener(this)
        findViewById<View>(R.id.btn_photograph).setOnClickListener(this)
        findViewById<View>(R.id.btn_turn_texture).setOnClickListener(this)
        findViewById<View>(R.id.btn_shooting).setOnClickListener(this)
        findViewById<View>(R.id.btn_seekbar).setOnClickListener(this)
        findViewById<View>(R.id.btn_volume).setOnClickListener(this)
        findViewById<View>(R.id.btn_audio).setOnClickListener(this)
        findViewById<View>(R.id.btn_video).setOnClickListener(this)
        findViewById<View>(R.id.btn_sensor).setOnClickListener(this)
        findViewById<View>(R.id.btn_acceleration).setOnClickListener(this)
        findViewById<View>(R.id.btn_light).setOnClickListener(this)
        findViewById<View>(R.id.btn_direction).setOnClickListener(this)
        findViewById<View>(R.id.btn_step).setOnClickListener(this)
        findViewById<View>(R.id.btn_gyroscope).setOnClickListener(this)
        findViewById<View>(R.id.btn_location_setting).setOnClickListener(this)
        findViewById<View>(R.id.btn_location_begin).setOnClickListener(this)
        findViewById<View>(R.id.btn_nfc).setOnClickListener(this)
        findViewById<View>(R.id.btn_infrared).setOnClickListener(this)
        findViewById<View>(R.id.btn_bluetooth).setOnClickListener(this)
        findViewById<View>(R.id.btn_navigation).setOnClickListener(this)
        findViewById<View>(R.id.btn_wechat).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_turn_view) {
            val intent = Intent(this, TurnViewActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_turn_surface) {
            val intent = Intent(this, TurnSurfaceActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_camera_info) {
            if (PermissionUtil.checkPermission(
                    this,
                    Manifest.permission.CAMERA,
                    R.id.btn_camera_info % 4096
                )
            ) {
                PermissionUtil.goActivity(this, CameraInfoActivity::class.java)
            }
        } else if (v.id == R.id.btn_photograph) {
            if (PermissionUtil.checkPermission(
                    this,
                    Manifest.permission.CAMERA,
                    R.id.btn_photograph % 4096
                )
            ) {
                PermissionUtil.goActivity(this, PhotographActivity::class.java)
            }
        } else if (v.id == R.id.btn_turn_texture) {
            val intent = Intent(this, TurnTextureActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_shooting) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                Toast.makeText(
                    this@MainActivity, "Andorid版本低于5.0无法使用camera2",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (PermissionUtil.checkPermission(
                    this,
                    Manifest.permission.CAMERA,
                    R.id.btn_shooting % 4096
                )
            ) {
                PermissionUtil.goActivity(this, ShootingActivity::class.java)
            }
        } else if (v.id == R.id.btn_seekbar) {
            val intent = Intent(this, SeekbarActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_volume) {
            val intent = Intent(this, VolumeActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_audio) {
            if (PermissionUtil.checkPermission(
                    this,
                    Manifest.permission.RECORD_AUDIO,
                    R.id.btn_audio % 4096
                )
            ) {
                PermissionUtil.goActivity(this, AudioActivity::class.java)
            }
        } else if (v.id == R.id.btn_video) {
            if (PermissionUtil.checkMultiPermission(
                    this, arrayOf<String?>(
                        Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA
                    ), R.id.btn_video % 4096
                )
            ) {
                PermissionUtil.goActivity(this, VideoActivity::class.java)
            }
        } else if (v.id == R.id.btn_sensor) {
            val intent = Intent(this, SensorActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_acceleration) {
            val intent = Intent(this, AccelerationActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_light) {
            if (PermissionUtil.checkWriteSettings(this, R.id.btn_light % 4096)) {
                PermissionUtil.goActivity(this, LightActivity::class.java)
            }
        } else if (v.id == R.id.btn_direction) {
            val intent = Intent(this, DirectionActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_step) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                Toast.makeText(
                    this@MainActivity, "计步器需要Android4.4或以上版本",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val intent = Intent(this, StepActivity::class.java)
                startActivity(intent)
            }
        } else if (v.id == R.id.btn_gyroscope) {
            val intent = Intent(this, GyroscopeActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_location_setting) {
            val intent = Intent(this, LocationSettingActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_location_begin) {
            if (PermissionUtil.checkPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    R.id.btn_location_begin % 4096
                )
            ) {
                PermissionUtil.goActivity(this, LocationActivity::class.java)
            }
        } else if (v.id == R.id.btn_infrared) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                Toast.makeText(
                    this@MainActivity, "红外遥控需要Android4.4或以上版本",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val intent = Intent(this, InfraredActivity::class.java)
                startActivity(intent)
            }
        } else if (v.id == R.id.btn_bluetooth) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Android6.0之后使用蓝牙需要定位权限
                if (PermissionUtil.checkPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        R.id.btn_bluetooth % 4096
                    )
                ) {
                    PermissionUtil.goActivity(this, BluetoothActivity::class.java)
                }
            } else {
                PermissionUtil.goActivity(this, BluetoothActivity::class.java)
            }
        } else if (v.id == R.id.btn_navigation) {
            if (PermissionUtil.checkPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    R.id.btn_navigation % 4096
                )
            ) {
                PermissionUtil.goActivity(this, NavigationActivity::class.java)
            }
        } else if (v.id == R.id.btn_wechat) {
            val intent = Intent(this, WeChatActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == R.id.btn_camera_info % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, CameraInfoActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许相机权限才能查看相机信息噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_photograph % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, PhotographActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许相机权限才能拍照噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_shooting % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, ShootingActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许相机权限才能拍照噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_audio % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, AudioActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许录音权限才能录音噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_video % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, VideoActivity::class.java)
            } else {
                Toast.makeText(this, "需要同时允许拍照和录音权限才能录像噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_location_begin % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, LocationActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许定位权限才能开始定位噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_light % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, LightActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许设置权限才能调节亮度噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_bluetooth % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, BluetoothActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许定位权限才能使用蓝牙噢", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == R.id.btn_navigation % 4096) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.goActivity(this, NavigationActivity::class.java)
            } else {
                Toast.makeText(this, "需要允许定位权限才能查看导航噢", Toast.LENGTH_SHORT).show()
            }
        }
    }
}