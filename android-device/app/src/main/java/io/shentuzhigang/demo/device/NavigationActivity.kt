package io.shentuzhigang.demo.device

import android.Manifest
import android.content.pm.PackageManager
import android.location.*
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.shentuzhigang.demo.device.bean.Satellite
import io.shentuzhigang.demo.device.util.DateUtil
import io.shentuzhigang.demo.device.util.SwitchUtil
import java.util.*


/**
 * Created by ouyangshen on 2018/1/27.
 */
class NavigationActivity : AppCompatActivity() {
    private var tv_navigation: TextView? = null
    private var mLocationMgr // 声明一个定位管理器对象
            : LocationManager? = null
    private val mapNavigation: MutableMap<String, Boolean> = HashMap()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        val tv_device = findViewById<TextView>(R.id.tv_device)
        tv_device.text = String.format("当前设备型号为%s", Build.MODEL)
        tv_navigation = findViewById(R.id.tv_navigation)
        SwitchUtil.checkGpsIsOpen(this, "需要打开定位功能才能查看卫星导航信息")
    }

    override fun onResume() {
        super.onResume()
        // 从系统服务中获取定位管理器
        mLocationMgr = getSystemService(LOCATION_SERVICE) as LocationManager
        // 检查当前设备是否已经开启了定位功能
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(this, "请授予定位权限并开启定位功能", Toast.LENGTH_SHORT).show()
            return
        }
        // 获取定位管理器的最佳定位提供者
        val bestProvider = mLocationMgr!!.getBestProvider(Criteria(), true)
        // 设置定位管理器的位置变更监听器
        mLocationMgr!!.requestLocationUpdates(bestProvider!!, 300, 0f, mLocationListener)
        // 给定位管理器添加导航状态监听器
        mLocationMgr!!.addGpsStatusListener(mStatusListener)
    }

    override fun onDestroy() {
        if (mLocationMgr != null) {
            // 移除定位管理器的导航状态监听器
            mLocationMgr!!.removeGpsStatusListener(mStatusListener)
            // 移除定位管理器的位置变更监听器
            mLocationMgr!!.removeUpdates(mLocationListener)
        }
        super.onDestroy()
    }

    // 定义一个位置变更监听器
    private val mLocationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {}
        override fun onProviderDisabled(arg0: String) {}
        override fun onProviderEnabled(arg0: String) {}
        override fun onStatusChanged(arg0: String, arg1: Int, arg2: Bundle) {}
    }

    // 定义一个导航状态监听器
    private val mStatusListener = GpsStatus.Listener { event ->

        // 在卫星导航系统的状态变更时触发
        Log.d(TAG, "onGpsStatusChanged event=$event")
        // 获取卫星定位的状态信息
        val gpsStatus = mLocationMgr!!.getGpsStatus(null)
        when (event) {
            GpsStatus.GPS_EVENT_SATELLITE_STATUS -> {
                // 得到所有收到的卫星信息，包括卫星的高度角、方位角、信噪比和卫星编号
                val satellites = gpsStatus!!.satellites
                for (satellite in satellites) {
                    val item = Satellite()
                    item.seq = satellite.prn // 卫星的伪随机码，可以认为就是卫星的编号
                    item.signal = Math.round(satellite.snr) // 卫星的信噪比
                    item.elevation = Math.round(satellite.elevation) // 卫星的仰角 (卫星的高度)
                    item.azimuth = Math.round(satellite.azimuth) // 卫星的方位角
                    item.time = DateUtil.nowDateTime
                    if (item.seq <= 64 || item.seq >= 120 && item.seq <= 138) { // 分给美国的
                        mapNavigation["GPS"] = true
                    } else if (item.seq >= 201 && item.seq <= 237) { // 分给中国的
                        mapNavigation["北斗"] = true
                    } else if (item.seq >= 65 && item.seq <= 89) { // 分给俄罗斯的
                        mapNavigation["格洛纳斯"] = true
                    } else if (item.seq != 193 && item.seq != 194) {
                        mapNavigation["未知"] = true
                    }
                }
                // 显示设备支持的卫星导航系统信息
                showNavigationInfo()
            }
            GpsStatus.GPS_EVENT_FIRST_FIX, GpsStatus.GPS_EVENT_STARTED, GpsStatus.GPS_EVENT_STOPPED -> {}
            else -> {}
        }
    }

    // 显示设备支持的卫星导航系统信息
    private fun showNavigationInfo() {
        var isFirst = true
        var desc = "支持的卫星导航系统包括："
        for ((key) in mapNavigation) {
            if (!isFirst) {
                desc = String.format("%s、%s", desc, key)
            } else {
                desc = String.format("%s%s", desc, key)
                isFirst = false
            }
        }
        tv_navigation!!.text = desc
    }

    companion object {
        private const val TAG = "NavigationActivity"
    }
}