package io.shentuzhigang.demo.device

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.Camera
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.GridView
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.adapter.ShootingAdapter
import io.shentuzhigang.demo.device.widget.CameraView

/**
 * Created by ouyangshen on 2017/11/4.
 */
class PhotographActivity : AppCompatActivity(), View.OnClickListener {
    private var fl_content // 声明一个框架布局对象
            : FrameLayout? = null
    private var iv_photo // 声明一个图像视图对象
            : ImageView? = null
    private var gv_shooting // 声明一个网格视图对象
            : GridView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photograph)
        // 从布局文件中获取名叫fl_content的框架布局
        fl_content = findViewById(R.id.fl_content)
        // 从布局文件中获取名叫iv_photo的图像视图
        iv_photo = findViewById(R.id.iv_photo)
        // 从布局文件中获取名叫gv_shooting的网格视图
        gv_shooting = findViewById(R.id.gv_shooting)
        findViewById<View>(R.id.btn_catch_behind).setOnClickListener(this)
        findViewById<View>(R.id.btn_catch_front).setOnClickListener(this)
    }

    // 处理Camera拍照页面的返回结果
    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        Log.d(
            TAG,
            "onActivityResult. requestCode=$requestCode, resultCode=$resultCode"
        )
        val resp = intent!!.extras // 获取返回的包裹
        val is_null = resp!!.getString("is_null")
        if (!TextUtils.isEmpty(is_null) && is_null != "yes") { // 有发生拍照动作
            val type = resp.getInt("type")
            Log.d(TAG, "type=$type")
            if (type == 0) { // 单拍。一次只拍一张
                iv_photo!!.visibility = View.VISIBLE
                gv_shooting!!.visibility = View.GONE
                val path = resp.getString("path")
                fillBitmap(BitmapFactory.decodeFile(path, null))
            } else if (type == 1) { // 连拍。一次连续拍了好几张
                iv_photo!!.visibility = View.GONE
                gv_shooting!!.visibility = View.VISIBLE
                val pathList = resp.getStringArrayList("path_list")
                Log.d(TAG, "pathList.size()=" + pathList!!.size)
                // 通过网格视图展示连拍的数张照片
                val adapter = ShootingAdapter(this, pathList)
                gv_shooting!!.adapter = adapter
            }
        }
    }

    // 以合适比例显示照片
    private fun fillBitmap(bitmap: Bitmap) {
        Log.d(
            TAG,
            "fillBitmap width=" + bitmap.width + ",height=" + bitmap.height
        )
        // 位图的高度大于框架布局的高度，则按比例调整图像视图的宽高
        if (bitmap.height > fl_content!!.measuredHeight) {
            val params = iv_photo!!.layoutParams
            params.height = fl_content!!.measuredHeight
            params.width = bitmap.width * fl_content!!.measuredHeight / bitmap.height
            // 设置iv_photo的布局参数
            iv_photo!!.layoutParams = params
        }
        // 设置iv_photo的拉伸类型为居中
        iv_photo!!.scaleType = ScaleType.FIT_CENTER
        // 设置iv_photo的位图对象
        iv_photo!!.setImageBitmap(bitmap)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_catch_behind) { // 点击了后置摄像头拍照按钮
            // 打开后置摄像头（未指定摄像头编号的话，默认就是打开后置摄像头）
            val mCamera = Camera.open()
            if (mCamera != null) {
                mCamera.release() // 释放摄像头
                // 前往Camera的拍照页面
                val intent = Intent(this, TakePictureActivity::class.java)
                // 类型为后置摄像头
                intent.putExtra("type", CameraView.Companion.CAMERA_BEHIND)
                // 需要处理拍照页面的返回结果
                startActivityForResult(intent, 1)
            } else {
                Toast.makeText(this, "当前设备不支持后置摄像头", Toast.LENGTH_SHORT).show()
            }
        } else if (v.id == R.id.btn_catch_front) { // 点击了前置摄像头拍照按钮
            Log.d(
                TAG,
                "getNumberOfCameras=" + Camera.getNumberOfCameras()
            )
            // 打开前置摄像头
            val mCamera = Camera.open(CameraView.Companion.CAMERA_FRONT)
            if (mCamera != null) {
                mCamera.release() // 释放摄像头
                // 前往Camera的拍照页面
                val intent = Intent(this, TakePictureActivity::class.java)
                // 类型为前置摄像头
                intent.putExtra("type", CameraView.Companion.CAMERA_FRONT)
                // 需要处理拍照页面的返回结果
                startActivityForResult(intent, 1)
            } else {
                Toast.makeText(this, "当前设备不支持前置摄像头", Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        private const val TAG = "PhotographActivity"
    }
}