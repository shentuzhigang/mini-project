package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("SetTextI18n")
class ScanResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_result)
        val tv_result = findViewById<TextView>(R.id.tv_result)
        // 获取扫码页面传来的结果字符串
        val result = intent.getStringExtra("result")
        tv_result.text = "扫码结果为：$result"
    }
}