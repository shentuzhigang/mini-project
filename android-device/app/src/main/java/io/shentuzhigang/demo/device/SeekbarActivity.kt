package io.shentuzhigang.demo.device

import android.os.Bundle
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by ouyangshen on 2017/11/4.
 */
class SeekbarActivity : AppCompatActivity(), OnSeekBarChangeListener {
    private var tv_progress: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seekbar)
        tv_progress = findViewById(R.id.tv_progress)
        // 从布局文件中获取名叫sb_progress的拖动条
        val sb_progress = findViewById<SeekBar>(R.id.sb_progress)
        // 给sb_progress设置拖动变更监听器
        sb_progress.setOnSeekBarChangeListener(this)
        // 设置拖动条的当前进度
        sb_progress.progress = 50
    }

    // 在进度变更时触发。第三个参数为true表示用户拖动，为false表示代码设置进度
    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        val desc = "当前进度为：" + seekBar.progress + ", 最大进度为" + seekBar.max
        tv_progress!!.text = desc
    }

    // 在开始拖动进度时触发
    override fun onStartTrackingTouch(seekBar: SeekBar) {}

    // 在停止拖动进度时触发
    override fun onStopTrackingTouch(seekBar: SeekBar) {}
}