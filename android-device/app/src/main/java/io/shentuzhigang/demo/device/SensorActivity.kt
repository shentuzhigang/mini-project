package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("DefaultLocale")
class SensorActivity : AppCompatActivity() {
    private var tv_sensor: TextView? = null
    private val mSensorType = arrayOf(
        "加速度", "磁场", "方向", "陀螺仪", "光线",
        "压力", "温度", "距离", "重力", "线性加速度",
        "旋转矢量", "湿度", "环境温度", "无标定磁场", "无标定旋转矢量",
        "未校准陀螺仪", "特殊动作", "步行检测", "计步器", "地磁旋转矢量",
        "心跳", "倾斜检测", "唤醒手势", "瞥一眼", "捡起来"
    )
    private val mapSensor: MutableMap<Int, String> = HashMap()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensor)
        tv_sensor = findViewById(R.id.tv_sensor)
        showSensorInfo() // 显示手机自带的传感器信息
    }

    private fun showSensorInfo() {
        // 从系统服务中获取传感管理器对象
        val mSensorMgr = getSystemService(SENSOR_SERVICE) as SensorManager
        // 获取当前设备支持的传感器列表
        val sensorList = mSensorMgr.getSensorList(Sensor.TYPE_ALL)
        var show_content: String? = "当前支持的传感器包括：\n"
        for (sensor in sensorList) {
            if (sensor.type >= mSensorType.size) {
                continue
            }
            mapSensor[sensor.type] = sensor.name
        }
        for ((type, name) in mapSensor) {
            val content = String.format("%d %s：%s\n", type, mSensorType[type - 1], name)
            show_content += content
        }
        tv_sensor!!.text = show_content
    }
}