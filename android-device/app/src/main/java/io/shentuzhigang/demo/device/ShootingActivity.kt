package io.shentuzhigang.demo.device

import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.GridView
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.adapter.ShootingAdapter

/**
 * Created by ouyangshen on 2017/11/4.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class ShootingActivity : AppCompatActivity(), View.OnClickListener {
    private var fl_content // 声明一个框架布局对象
            : FrameLayout? = null
    private var iv_photo // 声明一个图像视图对象
            : ImageView? = null
    private var gv_shooting // 声明一个网格视图对象
            : GridView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shooting)
        // 从布局文件中获取名叫fl_content的框架布局
        fl_content = findViewById(R.id.fl_content)
        // 从布局文件中获取名叫iv_photo的图像视图
        iv_photo = findViewById(R.id.iv_photo)
        // 从布局文件中获取名叫gv_shooting的网格视图
        gv_shooting = findViewById(R.id.gv_shooting)
        findViewById<View>(R.id.btn_catch_behind).setOnClickListener(this)
        findViewById<View>(R.id.btn_catch_front).setOnClickListener(this)
    }

    // 处理camera2拍照页面的返回结果
    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        Log.d(
            TAG,
            "onActivityResult. requestCode=$requestCode, resultCode=$resultCode"
        )
        val resp = intent!!.extras // 获取返回的包裹
        val is_null = resp!!.getString("is_null")
        if (!TextUtils.isEmpty(is_null) && is_null != "yes") { // 有发生拍照动作
            val type = resp.getInt("type")
            Log.d(TAG, "type=$type")
            if (type == 0) { // 单拍。一次只拍一张
                iv_photo!!.visibility = View.VISIBLE
                gv_shooting!!.visibility = View.GONE
                val path = resp.getString("path")
                fillBitmap(BitmapFactory.decodeFile(path, null))
            } else if (type == 1) { // 连拍。一次连续拍了好几张
                iv_photo!!.visibility = View.GONE
                gv_shooting!!.visibility = View.VISIBLE
                val pathList = resp.getStringArrayList("path_list")
                Log.d(TAG, "pathList.size()=" + pathList!!.size)
                // 通过网格视图展示连拍的数张照片
                val adapter = ShootingAdapter(this, pathList)
                gv_shooting!!.adapter = adapter
            }
        }
    }

    // 以合适比例显示照片
    private fun fillBitmap(bitmap: Bitmap) {
        Log.d(
            TAG,
            "fillBitmap width=" + bitmap.width + ",height=" + bitmap.height
        )
        // 位图的高度大于框架布局的高度，则按比例调整图像视图的宽高
        if (bitmap.height > fl_content!!.measuredHeight) {
            val params = iv_photo!!.layoutParams
            params.height = fl_content!!.measuredHeight
            params.width = bitmap.width * fl_content!!.measuredHeight / bitmap.height
            // 设置iv_photo的布局参数
            iv_photo!!.layoutParams = params
        }
        // 设置iv_photo的拉伸类型为居中
        iv_photo!!.scaleType = ScaleType.FIT_CENTER
        // 设置iv_photo的位图对象
        iv_photo!!.setImageBitmap(bitmap)
    }

    override fun onClick(v: View) {
        // 从系统服务中获取相机管理器
        val cm = getSystemService(CAMERA_SERVICE) as CameraManager
        val ids: Array<String>
        ids = try {
            // 获取摄像头的编号数组
            cm.cameraIdList
        } catch (e: CameraAccessException) {
            e.printStackTrace()
            return
        }
        if (v.id == R.id.btn_catch_behind) { // 点击了后置摄像头拍照按钮
            if (checkCamera(ids, CameraCharacteristics.LENS_FACING_FRONT.toString() + "")) {
                // 前往camera2的拍照页面
                val intent = Intent(this, TakeShootingActivity::class.java)
                // 类型为后置摄像头
                intent.putExtra("type", CameraCharacteristics.LENS_FACING_FRONT)
                // 需要处理拍照页面的返回结果
                startActivityForResult(intent, 1)
            } else {
                Toast.makeText(this, "当前设备不支持后置摄像头", Toast.LENGTH_SHORT).show()
            }
        } else if (v.id == R.id.btn_catch_front) { // 点击了前置摄像头拍照按钮
            if (checkCamera(ids, CameraCharacteristics.LENS_FACING_BACK.toString() + "")) {
                // 前往camera2的拍照页面
                val intent = Intent(this, TakeShootingActivity::class.java)
                // 类型为前置摄像头
                intent.putExtra("type", CameraCharacteristics.LENS_FACING_BACK)
                // 需要处理拍照页面的返回结果
                startActivityForResult(intent, 1)
            } else {
                Toast.makeText(this, "当前设备不支持前置摄像头", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // 检查是否存在指定类型的摄像头
    private fun checkCamera(ids: Array<String>, type: String): Boolean {
        var result = false
        for (id in ids) {
            if (id == type) {
                result = true
                break
            }
        }
        return result
    }

    companion object {
        private const val TAG = "ShootingActivity"
    }
}