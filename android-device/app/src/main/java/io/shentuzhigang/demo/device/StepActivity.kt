package io.shentuzhigang.demo.device

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by ouyangshen on 2017/11/4.
 */
@SuppressLint("DefaultLocale")
class StepActivity : AppCompatActivity(), SensorEventListener {
    private var tv_step: TextView? = null
    private var mSensorMgr // 声明一个传感管理器对象
            : SensorManager? = null
    private var mStepDetector = 0 // 累加的步行检测次数
    private var mStepCounter = 0 // 计步器统计的步伐数目
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step)
        tv_step = findViewById(R.id.tv_step)
        // 从系统服务中获取传感管理器对象
        mSensorMgr = getSystemService(SENSOR_SERVICE) as SensorManager
    }

    override fun onPause() {
        super.onPause()
        // 注销当前活动的传感监听器
        mSensorMgr!!.unregisterListener(this)
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    override fun onResume() {
        super.onResume()
        var suitable = 0
        // 获取当前设备支持的传感器列表
        val sensorList = mSensorMgr!!.getSensorList(Sensor.TYPE_ALL)
        for (sensor in sensorList) {
            if (sensor.type == Sensor.TYPE_STEP_DETECTOR) { // 找到步行检测传感器
                suitable += 1
            } else if (sensor.type == Sensor.TYPE_STEP_COUNTER) { // 找到计步器
                suitable += 10
            }
        }
        if (suitable / 10 > 0 && suitable % 10 > 0) {
            // 给步行检测传感器注册传感监听器
            mSensorMgr!!.registerListener(
                this,
                mSensorMgr!!.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),
                SensorManager.SENSOR_DELAY_NORMAL
            )
            // 给计步器注册传感监听器
            mSensorMgr!!.registerListener(
                this,
                mSensorMgr!!.getDefaultSensor(Sensor.TYPE_STEP_COUNTER),
                SensorManager.SENSOR_DELAY_NORMAL
            )
        } else {
            tv_step!!.text = "当前设备不支持计步器，请检查是否存在步行检测传感器和计步器"
        }
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_STEP_DETECTOR) { // 步行检测事件
            if (event.values[0] == 1.0f) {
                mStepDetector++
            }
        } else if (event.sensor.type == Sensor.TYPE_STEP_COUNTER) { // 计步器事件
            mStepCounter = event.values[0].toInt()
        }
        val desc = String.format(
            "设备检测到您当前走了%d步，总计数为%d步",
            mStepDetector, mStepCounter
        )
        tv_step!!.text = desc
    }

    //当传感器精度改变时回调该方法，一般无需处理
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
}