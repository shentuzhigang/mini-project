package io.shentuzhigang.demo.device

import android.content.Intent
import android.os.*
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.widget.CameraView


/**
 * Created by ouyangshen on 2017/11/4.
 */
class TakePictureActivity : AppCompatActivity(), View.OnClickListener {
    private var camera_view // 声明一个相机视图对象
            : CameraView? = null
    private var mTakeType = 0 // 拍照类型。0为单拍，1为连拍
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_take_picture)
        // 获取前一个页面传来的摄像头类型
        val camera_type = intent.getIntExtra("type", CameraView.Companion.CAMERA_BEHIND)
        // 从布局文件中获取名叫camera_view的相机视图
        camera_view = findViewById(R.id.camera_view)
        // 设置相机视图的摄像头类型
        camera_view?.cameraType = camera_type
        findViewById<View>(R.id.btn_shutter).setOnClickListener(this)
        findViewById<View>(R.id.btn_shooting).setOnClickListener(this)
    }

    override fun onBackPressed() {
        val intent = Intent() // 创建一个新意图
        val bundle = Bundle() // 创建一个新包裹
        val photo_path = camera_view?.photoPath // 获取照片的保存路径
        bundle.putInt("type", mTakeType)
        if (photo_path == null && mTakeType == 0) { // 未发生拍照动作
            bundle.putString("is_null", "yes")
        } else { // 有发生拍照动作
            bundle.putString("is_null", "no")
            if (mTakeType == 0) { // 单拍。一次只拍一张
                bundle.putString("path", photo_path)
            } else if (mTakeType == 1) { // 连拍。一次连续拍了好几张
                bundle.putStringArrayList("path_list", camera_view?.shootingList)
            }
        }
        intent.putExtras(bundle) // 往意图中存入包裹
        setResult(RESULT_OK, intent) // 携带意图返回前一个页面
        finish() // 关闭当前页面
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_shutter) { // 点击了单拍按钮
            mTakeType = 0
            // 命令相机视图执行单拍操作
            camera_view!!.doTakePicture()
            // 拍照需要完成对焦、图像捕获、图片保存等一系列动作，因而要留足时间给系统处理
            Handler().postDelayed({
                Toast.makeText(
                    this@TakePictureActivity,
                    "已完成拍照，按返回键回到上页查看照片。",
                    Toast.LENGTH_SHORT
                ).show()
            }, 1500)
        } else if (v.id == R.id.btn_shooting) { // 点击了连拍按钮
            mTakeType = 1
            // 命令相机视图执行连拍操作
            camera_view!!.doTakeShooting()
        }
    }

    companion object {
        private const val TAG = "TakePictureActivity"
    }
}