package io.shentuzhigang.demo.device

import android.os.Bundle
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.widget.TurnSurfaceView



/**
 * Created by ouyangshen on 2017/11/4.
 */
class TurnSurfaceActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {
    private var tfv_circle // 声明一个转动表面视图
            : TurnSurfaceView? = null
    private var ck_control: CheckBox? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_turn_surface)
        // 从布局文件中获取名叫tfv_circle的转动表面视图
        tfv_circle = findViewById(R.id.tfv_circle)
        ck_control = findViewById(R.id.ck_control)
        ck_control?.setOnCheckedChangeListener(this)
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_control) {
            if (isChecked) {
                ck_control!!.text = "停止"
                tfv_circle!!.start() // 转动表面视图开始转动
            } else {
                ck_control!!.text = "转动"
                tfv_circle!!.stop() // 转动表面视图停止转动
            }
        }
    }
}