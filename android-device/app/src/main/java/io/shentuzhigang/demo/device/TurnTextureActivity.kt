package io.shentuzhigang.demo.device

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.widget.TurnTextureView


/**
 * Created by ouyangshen on 2017/11/4.
 */
class TurnTextureActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {
    private var ttv_circle // 声明一个转动纹理视图对象
            : TurnTextureView? = null
    private var ck_control: CheckBox? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_turn_texture)
        // 从布局文件中获取名叫ttv_circle的转动纹理视图
        ttv_circle = findViewById(R.id.ttv_circle)
        // 给ttv_circle设置表面纹理监听器
        ttv_circle?.setSurfaceTextureListener(ttv_circle)
        ck_control = findViewById(R.id.ck_control)
        ck_control?.setOnCheckedChangeListener(this)
        initAlphaSpinner()
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_control) {
            if (isChecked) {
                ck_control!!.text = "停止"
                ttv_circle!!.start() // 转动纹理视图开始转动
            } else {
                ck_control!!.text = "转动"
                ttv_circle!!.stop() // 转动纹理视图停止转动
            }
        }
    }

    // 初始化透明度下拉框
    private fun initAlphaSpinner() {
        val starAdapter = ArrayAdapter(
            this,
            R.layout.item_select, alphaArray
        )
        starAdapter.setDropDownViewResource(R.layout.item_select)
        val sp_alpha = findViewById<Spinner>(R.id.sp_alpha)
        sp_alpha.prompt = "请选择透明度"
        sp_alpha.adapter = starAdapter
        sp_alpha.setSelection(1)
        sp_alpha.onItemSelectedListener = MySelectedListener()
    }

    private val alphaArray = arrayOf("0.0", "0.2", "0.4", "0.6", "0.8", "1.0")

    internal inner class MySelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            // 设置转动纹理视图的透明度
            ttv_circle!!.alpha = alphaArray[arg2].toFloat()
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }
}