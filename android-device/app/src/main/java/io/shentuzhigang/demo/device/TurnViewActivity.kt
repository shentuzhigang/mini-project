package io.shentuzhigang.demo.device

import android.os.Bundle
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.device.widget.TurnView


/**
 * Created by ouyangshen on 2017/11/4.
 */
class TurnViewActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {
    private var tv_circle // 声明一个转动视图对象
            : TurnView? = null
    private var ck_control: CheckBox? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_turn_view)
        // 从布局文件中获取名叫tv_circle的转动视图
        tv_circle = findViewById(R.id.tv_circle)
        ck_control = findViewById(R.id.ck_control)
        ck_control?.setOnCheckedChangeListener(this)
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_control) {
            if (isChecked) {
                ck_control!!.text = "停止"
                tv_circle!!.start() // 转动视图开始转动
            } else {
                ck_control!!.text = "转动"
                tv_circle!!.stop() // 转动视图停止转动
            }
        }
    }
}