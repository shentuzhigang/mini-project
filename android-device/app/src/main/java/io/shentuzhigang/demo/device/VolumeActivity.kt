package io.shentuzhigang.demo.device

import android.media.AudioManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by ouyangshen on 2017/11/4.
 */
class VolumeActivity : AppCompatActivity(), OnSeekBarChangeListener, View.OnClickListener {
    private val sb_voice: SeekBar? = null
    private val sb_system: SeekBar? = null
    private val sb_ring: SeekBar? = null
    private val sb_music: SeekBar? = null
    private val sb_alarm: SeekBar? = null
    private val sb_notify: SeekBar? = null
    private val iv_voice_add: ImageView? = null
    private val iv_system_add: ImageView? = null
    private val iv_ring_add: ImageView? = null
    private val iv_music_add: ImageView? = null
    private val iv_alarm_add: ImageView? = null
    private val iv_notify_add: ImageView? = null
    private val iv_voice_del: ImageView? = null
    private val iv_system_del: ImageView? = null
    private val iv_ring_del: ImageView? = null
    private val iv_music_del: ImageView? = null
    private val iv_alarm_del: ImageView? = null
    private val iv_notify_del: ImageView? = null
    private val mStreamType = intArrayOf(
        AudioManager.STREAM_VOICE_CALL, AudioManager.STREAM_SYSTEM,
        AudioManager.STREAM_RING, AudioManager.STREAM_MUSIC,
        AudioManager.STREAM_ALARM, AudioManager.STREAM_NOTIFICATION
    )
    private val mMaxVolume = intArrayOf(0, 0, 0, 0, 0, 0) // 最大音量数组
    private val mNowVolume = intArrayOf(0, 0, 0, 0, 0, 0) // 当前音量数组
    private val mSeekBar = arrayOf(
        sb_voice, sb_system, sb_ring,
        sb_music, sb_alarm, sb_notify
    )
    private val mStreamRes = intArrayOf(
        R.id.sb_voice, R.id.sb_system, R.id.sb_ring,
        R.id.sb_music, R.id.sb_alarm, R.id.sb_notify
    )
    private val mAddView = arrayOf(
        iv_voice_add, iv_system_add, iv_ring_add,
        iv_music_add, iv_alarm_add, iv_notify_add
    )
    private val mAddRes = intArrayOf(
        R.id.iv_voice_add, R.id.iv_system_add, R.id.iv_ring_add,
        R.id.iv_music_add, R.id.iv_alarm_add, R.id.iv_notify_add
    )
    private val mDelView = arrayOf(
        iv_voice_del, iv_system_del, iv_ring_del,
        iv_music_del, iv_alarm_del, iv_notify_del
    )
    private val mDelRes = intArrayOf(
        R.id.iv_voice_del, R.id.iv_system_del, R.id.iv_ring_del,
        R.id.iv_music_del, R.id.iv_alarm_del, R.id.iv_notify_del
    )
    private val SEEK_BAR = 1
    private val ADD_VIEW = 2
    private val DEL_VIEW = 3
    private var mAudioMgr // 声明一个音频管理器对象
            : AudioManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)
        // 从布局文件中依次获取各音量类型的拖动条、增大音量按钮、减小音量按钮
        for (i in mStreamType.indices) {
            mSeekBar[i] = findViewById(mStreamRes[i])
            mAddView[i] = findViewById(mAddRes[i])
            mDelView[i] = findViewById(mDelRes[i])
        }
        setStreamVolume()
        for (i in mStreamType.indices) {
            // 给各音量类型的拖动条设置拖动变更监听器
            mSeekBar[i]!!.setOnSeekBarChangeListener(this)
            // 给各音量类型的增大音量按钮设置点击监听器
            mAddView[i]!!.setOnClickListener(this)
            // 给各音量类型的减小音量按钮设置点击监听器
            mDelView[i]!!.setOnClickListener(this)
        }
    }

    // 设置各音量类型的拖动条进度
    fun setStreamVolume() {
        // 从系统服务中获取音频管理器
        mAudioMgr = getSystemService(AUDIO_SERVICE) as AudioManager
        for (i in mStreamType.indices) {
            val type = mStreamType[i]
            // 获取指定音频类型的最大音量
            mMaxVolume[i] = mAudioMgr!!.getStreamMaxVolume(type)
            // 获取指定音频类型的当前音量
            mNowVolume[i] = mAudioMgr!!.getStreamVolume(type)
            // 设置拖动条的音量大小进度
            mSeekBar[i]!!.progress = mSeekBar[i]!!.max * mNowVolume[i] / mMaxVolume[i]
        }
    }

    // 在进度变更时触发。第三个参数为true表示用户拖动，为false表示代码设置进度
    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}

    // 在开始拖动进度时触发
    override fun onStartTrackingTouch(seekBar: SeekBar) {}

    // 在停止拖动进度时触发
    override fun onStopTrackingTouch(seekBar: SeekBar) {
        Log.d(TAG, "当前进度为：" + seekBar.progress + ", 最大进度为" + seekBar.max)
        val index = getArrayIndex(seekBar.id, SEEK_BAR)
        val type = mStreamType[index]
        val volume = mMaxVolume[index] * seekBar.progress / seekBar.max
        Log.d(
            TAG,
            "volume=" + volume + ", last volume=" + mNowVolume[index] + ", max volume=" + mMaxVolume[index]
        )
        if (volume != mNowVolume[index]) {
            mNowVolume[index] = volume
            // 根据拖动位置，计算并设置拖动条的当前进度
            seekBar.progress = seekBar.max * mNowVolume[index] / mMaxVolume[index]
        }
        // 设置该音频类型的当前音量
        mAudioMgr!!.setStreamVolume(type, volume, AudioManager.FLAG_PLAY_SOUND)
    }

    override fun onClick(v: View) {
        val add_index = getArrayIndex(v.id, ADD_VIEW)
        val del_index = getArrayIndex(v.id, DEL_VIEW)
        if (add_index != -1) { // 点击了增大音量按钮
            val seekBar = mSeekBar[add_index]
            if (mNowVolume[add_index] < mMaxVolume[add_index]) {
                mNowVolume[add_index] = mNowVolume[add_index] + 1
                // 设置拖动条的音量大小进度
                seekBar!!.progress = seekBar.max * mNowVolume[add_index] / mMaxVolume[add_index]
                // 把该音频类型的当前音量调大一级
                mAudioMgr!!.adjustStreamVolume(
                    mStreamType[add_index],
                    AudioManager.ADJUST_RAISE,
                    AudioManager.FLAG_PLAY_SOUND
                )
            }
        } else if (del_index != -1) { // 点击了减小音量按钮
            val seekBar = mSeekBar[del_index]
            if (mNowVolume[del_index] > 0) {
                mNowVolume[del_index] = mNowVolume[del_index] - 1
                // 设置拖动条的音量大小进度
                seekBar!!.progress = seekBar.max * mNowVolume[del_index] / mMaxVolume[del_index]
                // 把该音频类型的当前音量调小一级
                mAudioMgr!!.adjustStreamVolume(
                    mStreamType[del_index],
                    AudioManager.ADJUST_LOWER,
                    AudioManager.FLAG_PLAY_SOUND
                )
            }
        }
    }

    private fun getArrayIndex(resid: Int, type: Int): Int {
        var index = -1
        if (type == SEEK_BAR) { // 这是拖动条
            for (i in mSeekBar.indices) {
                if (mSeekBar[i]!!.id == resid) {
                    index = i
                    break
                }
            }
        } else if (type == ADD_VIEW) { // 这是增大音量按钮
            for (i in mAddView.indices) {
                if (mAddView[i]!!.id == resid) {
                    index = i
                    break
                }
            }
        } else if (type == DEL_VIEW) { // 这是减小音量按钮
            for (i in mDelView.indices) {
                if (mDelView[i]!!.id == resid) {
                    index = i
                    break
                }
            }
        }
        return index
    }

    companion object {
        private const val TAG = "VolumeActivity"
    }
}