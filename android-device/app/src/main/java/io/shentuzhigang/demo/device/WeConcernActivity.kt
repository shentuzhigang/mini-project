package io.shentuzhigang.demo.device

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar



/**
 * Created by ouyangshen on 2017/11/4.
 */
class WeConcernActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_we_concern)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 设置工具栏的标题文本
        tl_head.title = resources.getString(R.string.menu_first)
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        // 给tl_head设置导航图标的点击监听器
        // setNavigationOnClickListener必须放到setSupportActionBar之后，不然不起作用
        tl_head.setNavigationOnClickListener { finish() }
    }

    companion object {
        private const val TAG = "WeConcernActivity"
    }
}