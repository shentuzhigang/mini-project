package io.shentuzhigang.demo.device

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.shentuzhigang.demo.device.util.PermissionUtil

/**
 * Created by ouyangshen on 2017/11/4.
 */
class WeFindActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_we_find)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 设置工具栏的标题文本
        tl_head.title = resources.getString(R.string.menu_third)
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        // 给tl_head设置导航图标的点击监听器
        // setNavigationOnClickListener必须放到setSupportActionBar之后，不然不起作用
        tl_head.setNavigationOnClickListener { finish() }
        findViewById<View>(R.id.tv_scan).setOnClickListener(this)
        findViewById<View>(R.id.tv_shake).setOnClickListener(this)
        findViewById<View>(R.id.tv_smell).setOnClickListener(this)
        findViewById<View>(R.id.tv_listen).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tv_scan) { // 点击了“扫一扫”
            // WeFindActivity内嵌到WeChatActivity中，造成不会在底部弹出权限选择对话框，所以要通过WeChatActivity弹窗
            // 并且权限选择结果onRequestPermissionsResult要在WeChatActivity里面重写
            if (PermissionUtil.checkPermission(
                    WeChatActivity.Companion.act,
                    Manifest.permission.CAMERA,
                    R.id.tv_scan % 4096
                )
            ) {
                // 若已获得相机权限，就跳到扫描二维码页面
                PermissionUtil.goActivity(this, FindScanActivity::class.java)
            }
        } else if (v.id == R.id.tv_shake) { // 点击了“摇一摇”
            // 跳到博饼中大奖页面
            val intent = Intent(this, FindShakeActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.tv_smell) { // 点击了“咻一咻”
            // WeFindActivity内嵌到WeChatActivity中，造成不会在底部弹出权限选择对话框，所以要通过WeChatActivity弹窗
            // 并且权限选择结果onRequestPermissionsResult要在WeChatActivity里面重写
            if (PermissionUtil.checkPermission(
                    WeChatActivity.Companion.act,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    R.id.tv_smell % 4096
                )
            ) {
                // 若已获得定位权限，就跳到卫星浑天仪页面
                PermissionUtil.goActivity(this, FindSmellActivity::class.java)
            }
        } else if (v.id == R.id.tv_listen) { // 点击了“听一听”
            // 跳到蓝牙播音乐页面
            val intent = Intent(this, FindListenActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        private const val TAG = "WeFindActivity"
    }
}