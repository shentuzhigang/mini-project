package io.shentuzhigang.demo.device.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import io.shentuzhigang.demo.device.R
import io.shentuzhigang.demo.device.bean.BlueDevice
import java.util.*

// 展示蓝牙设备列表的适配器
class BlueListAdapter(private val mContext: Context, blue_list: ArrayList<BlueDevice>) :
    BaseAdapter() {
    private var mBlueList = ArrayList<BlueDevice>()
    private val mStateArray = arrayOf("未绑定", "绑定中", "已绑定", "已连接")
    override fun getCount(): Int {
        return mBlueList.size
    }

    override fun getItem(position: Int): Any {
        return mBlueList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            holder = ViewHolder()
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_bluetooth, null)
            holder.tv_blue_name = convertView.findViewById(R.id.tv_blue_name)
            holder.tv_blue_address = convertView.findViewById(R.id.tv_blue_address)
            holder.tv_blue_state = convertView.findViewById(R.id.tv_blue_state)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }
        val device = mBlueList[position]
        holder.tv_blue_name!!.text = device.name // 显示蓝牙设备的名称
        holder.tv_blue_address!!.text = device.address // 显示蓝牙设备的地址
        holder.tv_blue_state!!.text = mStateArray[device.state - 10] // 显示蓝牙设备的状态
        return convertView
    }

    inner class ViewHolder {
        var tv_blue_name: TextView? = null
        var tv_blue_address: TextView? = null
        var tv_blue_state: TextView? = null
    }

    companion object {
        private const val TAG = "BlueListAdapter"
        var CONNECTED = 13
    }

    init {
        mBlueList = blue_list
    }
}