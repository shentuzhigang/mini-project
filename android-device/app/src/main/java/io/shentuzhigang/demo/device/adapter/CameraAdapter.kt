package io.shentuzhigang.demo.device.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import io.shentuzhigang.demo.device.R
import io.shentuzhigang.demo.device.bean.CameraInfo
import java.util.*

@SuppressLint(value = ["DefaultLocale", "SetTextI18n"]) // 展示相机信息列表的适配器
class CameraAdapter(private val mContext: Context, private val mCameraList: ArrayList<CameraInfo>) :
    BaseAdapter() {
    override fun getCount(): Int {
        return mCameraList.size
    }

    override fun getItem(arg0: Int): Any {
        return mCameraList[arg0]
    }

    override fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            holder = ViewHolder()
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_camera, null)
            holder.tv_camera_type = convertView.findViewById(R.id.tv_camera_type)
            holder.tv_flash_mode = convertView.findViewById(R.id.tv_flash_mode)
            holder.tv_focus_mode = convertView.findViewById(R.id.tv_focus_mode)
            holder.tv_scene_mode = convertView.findViewById(R.id.tv_scene_mode)
            holder.tv_color_effect = convertView.findViewById(R.id.tv_color_effect)
            holder.tv_scene_mode = convertView.findViewById(R.id.tv_scene_mode)
            holder.tv_white_balance = convertView.findViewById(R.id.tv_white_balance)
            holder.tv_max_zoom = convertView.findViewById(R.id.tv_max_zoom)
            holder.tv_zoom = convertView.findViewById(R.id.tv_zoom)
            holder.tv_resolution = convertView.findViewById(R.id.tv_resolution)
            holder.tv_resolution_list = convertView.findViewById(R.id.tv_resolution_list)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }
        val item = mCameraList[position]
        holder.tv_camera_type!!.text = "" + item.camera_type
        holder.tv_flash_mode!!.text = item.flash_mode
        holder.tv_focus_mode!!.text = item.focus_mode
        holder.tv_scene_mode!!.text = item.scene_mode
        holder.tv_color_effect!!.text = item.color_effect
        holder.tv_white_balance!!.text = item.white_balance
        holder.tv_max_zoom!!.text = item.max_zoom.toString() + ""
        holder.tv_zoom!!.text = item.zoom.toString() + ""
        var desc = ""
        for (j in item.resolutionList?.indices!!) {
            val size = item.resolutionList?.get(j)
            desc = String.format("%s分辨率%d为：宽%d*高%d\n", desc, j + 1, size?.width, size?.height)
        }
        holder.tv_resolution_list!!.text = desc
        listeneClick(holder)
        return convertView
    }

    private fun listeneClick(holder: ViewHolder) {
        holder.tv_resolution!!.setOnClickListener {
            val visibility = holder.tv_resolution_list!!.visibility
            if (visibility == View.GONE) {
                holder.tv_resolution!!.text = "收起"
                holder.tv_resolution_list!!.visibility = View.VISIBLE
            } else {
                holder.tv_resolution!!.text = "展示"
                holder.tv_resolution_list!!.visibility = View.GONE
            }
        }
    }

    inner class ViewHolder {
        var tv_camera_type: TextView? = null
        var tv_flash_mode: TextView? = null
        var tv_focus_mode: TextView? = null
        var tv_scene_mode: TextView? = null
        var tv_color_effect: TextView? = null
        var tv_white_balance: TextView? = null
        var tv_max_zoom: TextView? = null
        var tv_zoom: TextView? = null
        var tv_resolution: TextView? = null
        var tv_resolution_list: TextView? = null
    }
}