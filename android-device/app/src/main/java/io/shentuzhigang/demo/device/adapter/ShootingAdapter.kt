package io.shentuzhigang.demo.device.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import io.shentuzhigang.demo.device.R
import java.util.*

// 展示连拍缩略图的网格适配器
class ShootingAdapter(private val mContext: Context, private val mPathList: ArrayList<String>?) :
    BaseAdapter() {
    override fun getCount(): Int {
        return mPathList!!.size
    }

    override fun getItem(arg0: Int): Any {
        return mPathList!![arg0]
    }

    override fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            holder = ViewHolder()
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_shooting, null)
            holder.iv_shooting = convertView.findViewById(R.id.iv_shooting)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }
        val path = mPathList!![position]
        holder.iv_shooting!!.setImageBitmap(BitmapFactory.decodeFile(path))
        return convertView
    }

    inner class ViewHolder {
        var iv_shooting: ImageView? = null
    }
}