package io.shentuzhigang.demo.device.bean


class BlueDevice(// 蓝牙设备的名称
    var name: String, // 蓝牙设备的MAC地址
    var address: String, // 蓝牙设备的绑定状态
    var state: Int
)