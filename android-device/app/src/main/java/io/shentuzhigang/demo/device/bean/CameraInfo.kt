package io.shentuzhigang.demo.device.bean
import android.hardware.Camera

class CameraInfo {
    var camera_type: String? = null
    var flash_mode: String? = null
    var focus_mode: String? = null
    var scene_mode: String? = null
    var color_effect: String? = null
    var white_balance: String? = null
    var max_zoom = 0
    var zoom = 0
    var resolutionList: List<Camera.Size>? = null
}