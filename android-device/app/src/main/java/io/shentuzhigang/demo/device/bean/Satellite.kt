package io.shentuzhigang.demo.device.bean

class Satellite {
    var seq: Int
    var nation: String
    var name: String
    var signal: Int
    var elevation: Int
    var azimuth: Int
    var time: String

    init {
        seq = -1
        nation = ""
        name = ""
        signal = -1
        elevation = -1
        azimuth = -1
        time = ""
    }
}