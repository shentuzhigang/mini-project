package io.shentuzhigang.demo.device.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.Options
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Environment
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.ByteBuffer
import java.util.*

//import android.support.media.ExifInterface;
object BitmapUtil {
    // 把位图对象保存为指定路径的图片文件
    fun saveBitmap(path: String?, bitmap: Bitmap?, format: String, quality: Int) {
        var compressFormat = CompressFormat.JPEG
        if (format.toUpperCase(Locale.getDefault()) == "PNG") {
            compressFormat = CompressFormat.PNG
        }
        try {
            // 根据指定文件路径构建缓存输出流对象
            val bos = BufferedOutputStream(FileOutputStream(path))
            // 把位图数据压缩到缓存输出流中
            bitmap!!.compress(compressFormat, quality, bos)
            // 完成缓存输出流的写入动作
            bos.flush()
            // 关闭缓存输出流
            bos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 把位图数据保存到指定路径的图片文件
    fun saveBitmap(
        path: String?, buffer: ByteBuffer,
        sample_size: Int, format: String, quality: Int
    ) {
        try {
            val buff = ByteArray(buffer.remaining())
            buffer[buff]
            val ontain = Options()
            ontain.inSampleSize = sample_size
            val bitmap = BitmapFactory.decodeByteArray(buff, 0, buff.size, ontain)
            saveBitmap(path, bitmap, format, quality)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 从指定路径的图片文件中读取位图数据
    fun openBitmap(path: String?): Bitmap? {
        var bitmap: Bitmap? = null
        try {
            // 根据指定文件路径构建缓存输入流对象
            val bis = BufferedInputStream(FileInputStream(path))
            // 从缓存输入流中解码位图数据
            bitmap = BitmapFactory.decodeStream(bis)
            // 关闭缓存输入流
            bis.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // 返回图片文件中的位图数据
        return bitmap
    }

    // 获得旋转角度之后的位图对象
    fun getRotateBitmap(b: Bitmap?, rotateDegree: Float): Bitmap {
        // 创建操作图片用的矩阵对象
        val matrix = Matrix()
        // 执行图片的旋转动作
        matrix.postRotate(rotateDegree)
        // 创建并返回旋转后的位图对象
        return Bitmap.createBitmap(
            b!!, 0, 0, b.width,
            b.height, matrix, false
        )
    }

    // 获得图片的缓存路径
    fun getCachePath(context: Context): String {
        return context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
    }

    // 按照新的宽高缩放位图对象
    fun zoomImage(origImage: Bitmap, newWidth: Double, newHeight: Double): Bitmap {
        // 获取原始位图的宽度
        val width = origImage.width.toFloat()
        // 获取原始位图的高度
        val height = origImage.height.toFloat()
        // 创建操作图片用的矩阵对象
        val matrix = Matrix()
        // 计算宽度的缩放率
        val scaleWidth = newWidth.toFloat() / width
        // 计算高度的缩放率
        val scaleHeight = newHeight.toFloat() / height
        // 执行图片的缩放动作
        matrix.postScale(scaleWidth, scaleHeight)
        // 创建并返回缩放后的位图对象
        return Bitmap.createBitmap(origImage, 0, 0, width.toInt(), height.toInt(), matrix, true)
    }

    // 将图片的旋转角度置为0，此方法可以解决某些机型拍照后，图像出现了旋转情况
    fun setPictureDegreeZero(path: String?) {
        try {
            val exifInterface = ExifInterface(path!!)
            // 修正图片的旋转角度，设置其不旋转。这里也可以设置其旋转的角度，可以传值过去，
            // 例如旋转90度，传值ExifInterface.ORIENTATION_ROTATE_90，需要将这个值转换为String类型的
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "no")
            exifInterface.saveAttributes()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}