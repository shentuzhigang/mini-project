package io.shentuzhigang.demo.device.util

import android.bluetooth.BluetoothA2dp
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.util.Log

object BluetoothUtil {
    private const val TAG = "BluetoothUtil"

    // 获取蓝牙的开关状态
    fun getBlueToothStatus(context: Context?): Boolean {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val enabled: Boolean
        enabled = when (bluetoothAdapter.state) {
            BluetoothAdapter.STATE_ON, BluetoothAdapter.STATE_TURNING_ON -> true
            BluetoothAdapter.STATE_OFF, BluetoothAdapter.STATE_TURNING_OFF -> false
            else -> false
        }
        return enabled
    }

    // 打开或关闭蓝牙
    fun setBlueToothStatus(context: Context?, enabled: Boolean) {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (enabled) {
            bluetoothAdapter.enable()
        } else {
            bluetoothAdapter.disable()
        }
    }

    // 建立蓝牙配对
    fun createBond(device: BluetoothDevice?): Boolean {
        return try {
            val createBondMethod = BluetoothDevice::class.java.getMethod("createBond")
            Log.d(TAG, "开始配对")
            val result = createBondMethod.invoke(device) as Boolean
            Log.d(TAG, "配对结果=$result")
            result
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    // 取消蓝牙配对
    fun removeBond(device: BluetoothDevice?): Boolean {
        return try {
            val createBondMethod = BluetoothDevice::class.java.getMethod("removeBond")
            Log.d(TAG, "取消配对")
            val result = createBondMethod.invoke(device) as Boolean
            Log.d(TAG, "取消结果=$result")
            result
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    // 建立A2DP连接
    fun connectA2dp(a2dp: BluetoothA2dp?, device: BluetoothDevice?): Boolean {
        return try {
            val setMethod = BluetoothA2dp::class.java.getMethod(
                "setPriority",
                BluetoothDevice::class.java,
                Int::class.javaPrimitiveType
            )
            val setResult = setMethod.invoke(a2dp, device, 100) as Boolean
            Log.d(TAG, "设置优先级结果=$setResult")
            val connectMethod =
                BluetoothA2dp::class.java.getMethod("connect", BluetoothDevice::class.java)
            val connectResult = connectMethod.invoke(a2dp, device) as Boolean
            Log.d(TAG, "A2DP连接结果=$connectResult")
            connectResult
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    // 取消A2DP连接
    fun disconnectA2dp(a2dp: BluetoothA2dp?, device: BluetoothDevice?): Boolean {
        return try {
            val method =
                BluetoothA2dp::class.java.getMethod("disconnect", BluetoothDevice::class.java)
            val result = method.invoke(a2dp, device) as Boolean
            Log.d(TAG, "A2DP取消结果=$result")
            result
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }
}