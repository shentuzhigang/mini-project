package io.shentuzhigang.demo.device.util

import android.content.Context
import android.graphics.Point
import android.hardware.Camera
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import java.util.regex.Pattern

object CameraUtil {
    private const val TAG = "CameraUtil"
    @kotlin.jvm.JvmStatic
    fun getSize(ctx: Context): Point {
        val wm = ctx.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val dm = DisplayMetrics()
        wm.defaultDisplay.getMetrics(dm)
        val size = Point()
        size.x = dm.widthPixels
        size.y = dm.heightPixels
        return size
    }

    private val COMMA_PATTERN = Pattern.compile(",")
    @kotlin.jvm.JvmStatic
    fun getCameraSize(params: Camera.Parameters, screenSize: Point?): Point {
        var previewSizeValueString = params["preview-size-values"]
        if (previewSizeValueString == null) {
            previewSizeValueString = params["preview-size-value"]
        }
        var cameraSize: Point? = null
        if (previewSizeValueString != null) {
            Log.d(TAG, "preview-size-values parameter: $previewSizeValueString")
            cameraSize = findBestPreviewSizeValue(previewSizeValueString, screenSize)
        }
        if (cameraSize == null) {
            cameraSize = Point(screenSize!!.x shr 3 shl 3, screenSize.y shr 3 shl 3)
        }
        return cameraSize
    }

    private fun findBestPreviewSizeValue(
        previewSizeValueString: CharSequence,
        screenSize: Point?
    ): Point? {
        var bestX = 0
        var bestY = 0
        var diff = Int.MAX_VALUE
        for (previewSize in COMMA_PATTERN.split(previewSizeValueString)) {
            val previewSize = previewSize.trim { it <= ' ' }
            val dimPosition = previewSize.indexOf('x')
            if (dimPosition < 0) {
                Log.d(TAG, "Bad preview-size: $previewSize")
                continue
            }
            var newX: Int
            var newY: Int
            try {
                newX = previewSize.substring(0, dimPosition).toInt()
                newY = previewSize.substring(dimPosition + 1).toInt()
            } catch (nfe: NumberFormatException) {
                Log.d(TAG, "Bad preview-size: $previewSize")
                continue
            }
            val newDiff = Math.abs(newX - screenSize!!.x + (newY - screenSize.y))
            if (newDiff == 0) {
                bestX = newX
                bestY = newY
                break
            } else if (newDiff < diff) {
                bestX = newX
                bestY = newY
                diff = newDiff
            }
        }
        return if (bestX > 0 && bestY > 0) {
            Point(bestX, bestY)
        } else null
    }

    fun getMaxPictureSize(params: Camera.Parameters): Camera.Size {
        val supportedSizes = params.supportedPictureSizes
        var maxSize = supportedSizes[0]
        for (size in supportedSizes) {
            if (size.width > maxSize.width) {
                maxSize = size
            }
        }
        return maxSize
    }
}