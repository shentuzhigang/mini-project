package io.shentuzhigang.demo.device.util

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
object DateUtil {
    val nowDateTime: String
        get() {
            val sdf = SimpleDateFormat("yyyyMMddHHmmss")
            return sdf.format(Date())
        }
    val nowDateTimeFull: String
        get() {
            val sdf = SimpleDateFormat("yyyyMMddHHmmssSSS")
            return sdf.format(Date())
        }
    val nowDateTimeFormat: String
        get() {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            return sdf.format(Date())
        }
    val nowTime: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss")
            return sdf.format(Date())
        }
}