package io.shentuzhigang.demo.device.util

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.File

object MediaUtil {
    private const val TAG = "MediaUtil"

    // 获得音视频文件的缓存路径
    fun getRecordFilePath(context: Context, dir_name: String, extend_name: String): String {
        var path = ""
        val recordDir = File(
            context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                .toString() + "/" + dir_name + "/"
        )
        if (!recordDir.exists()) {
            recordDir.mkdirs()
        }
        try {
            val recordFile = File.createTempFile(DateUtil.nowDateTime, extend_name, recordDir)
            path = recordFile.absolutePath
            Log.d(TAG, "dir_name=$dir_name, extend_name=$extend_name, path=$path")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return path
    }
}