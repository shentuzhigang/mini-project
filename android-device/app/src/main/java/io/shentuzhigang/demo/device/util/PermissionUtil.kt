package io.shentuzhigang.demo.device.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

/**
 * Created by ouyangshen on 2018/1/28.
 */
object PermissionUtil {
    private const val TAG = "PermissionUtil"

    // 检查某个权限。返回true表示已启用该权限，返回false表示未启用该权限
    fun checkPermission(act: Activity?, permission: String, requestCode: Int): Boolean {
        Log.d(TAG, "checkPermission: $permission")
        var result = true
        // 只对Android6.0及以上系统进行校验
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // 检查当前App是否开启了名称为permission的权限
            val check = ContextCompat.checkSelfPermission(act!!, permission)
            if (check != PackageManager.PERMISSION_GRANTED) {
                // 未开启该权限，则请求系统弹窗，好让用户选择是否立即开启权限
                ActivityCompat.requestPermissions(act, arrayOf(permission), requestCode)
                result = false
            }
        }
        return result
    }

    // 检查多个权限。返回true表示已完全启用权限，返回false表示未完全启用权限
    fun checkMultiPermission(
        act: Activity?,
        permissions: Array<String?>,
        requestCode: Int
    ): Boolean {
        var result = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var check = PackageManager.PERMISSION_GRANTED
            // 通过权限数组检查是否都开启了这些权限
            for (permission in permissions) {
                check = ContextCompat.checkSelfPermission(act!!, permission!!)
                if (check != PackageManager.PERMISSION_GRANTED) {
                    break
                }
            }
            if (check != PackageManager.PERMISSION_GRANTED) {
                // 未开启该权限，则请求系统弹窗，好让用户选择是否立即开启权限
                ActivityCompat.requestPermissions(act!!, permissions, requestCode)
                result = false
            }
        }
        return result
    }

    // 检查是否允许修改系统设置
    fun checkWriteSettings(act: Activity, requestCode: Int): Boolean {
        Log.d(TAG, "checkWriteSettings:")
        var result = true
        // 只对Android6.0及Android7.0系统进行校验
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            && Build.VERSION.SDK_INT < Build.VERSION_CODES.O
        ) {
            // 检查当前App是否允许修改系统设置
            if (!Settings.System.canWrite(act)) {
                val intent = Intent(
                    Settings.ACTION_MANAGE_WRITE_SETTINGS,
                    Uri.parse("package:" + act.packageName)
                )
                act.startActivityForResult(intent, requestCode)
                Toast.makeText(act, "需要允许设置权限才能调节亮度噢", Toast.LENGTH_SHORT).show()
                result = false
            }
        }
        return result
    }

    fun goActivity(ctx: Context, cls: Class<*>?) {
        val intent = Intent(ctx, cls)
        ctx.startActivity(intent)
    }
}