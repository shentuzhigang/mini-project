package io.shentuzhigang.demo.device.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.net.wifi.WifiManager
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast

@SuppressLint("WifiManagerPotentialLeak")
object SwitchUtil {
    private const val TAG = "SwitchUtil"

    // 获取定位功能的开关状态
    fun getGpsStatus(ctx: Context): Boolean {
        // 从系统服务中获取定位管理器
        val lm = ctx.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    // 检查定位功能是否打开，若未打开则跳到系统的定位功能设置页面
    fun checkGpsIsOpen(ctx: Context, hint: String?) {
        if (!getGpsStatus(ctx)) {
            Toast.makeText(ctx, hint, Toast.LENGTH_SHORT).show()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            ctx.startActivity(intent)
        }
    }

    // 获取无线网络的开关状态
    fun getWlanStatus(ctx: Context): Boolean {
        // 从系统服务中获取无线网络管理器
        val wm = ctx.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return wm.isWifiEnabled
    }

    // 打开或关闭无线网络
    fun setWlanStatus(ctx: Context, enabled: Boolean) {
        // 从系统服务中获取无线网络管理器
        val wm = ctx.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wm.isWifiEnabled = enabled
    }

    // 获取数据连接的开关状态
    fun getMobileDataStatus(ctx: Context): Boolean {
        // 从系统服务中获取电话管理器
        val tm = ctx.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        var isOpen = false
        try {
            val methodName = "getDataEnabled" // 这是隐藏方法，需要通过反射调用
            val method = tm.javaClass.getMethod(methodName)
            isOpen = method.invoke(tm) as Boolean
            Log.d(TAG, "getMobileDataStatus isOpen=$isOpen")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return isOpen
    }

    // 设置亮度自动调节的开关
    fun setAutoBrightStatus(ctx: Context, enabled: Boolean) {
        val screenMode =
            if (enabled) Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC else Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
        Settings.System.putInt(
            ctx.contentResolver,
            Settings.System.SCREEN_BRIGHTNESS_MODE, screenMode
        )
    }

    // 获取亮度自动调节的状态
    fun getAutoBrightStatus(ctx: Context): Boolean {
        var screenMode = Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
        try {
            screenMode = Settings.System.getInt(
                ctx.contentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return screenMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
    }
}