package io.shentuzhigang.demo.device.widget

import android.content.Context
import android.media.MediaRecorder
import android.media.MediaRecorder.AudioEncoder
import android.media.MediaRecorder.AudioSource
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.ProgressBar
import io.shentuzhigang.demo.device.R
import io.shentuzhigang.demo.device.util.MediaUtil
import java.util.*

class AudioRecorder @JvmOverloads constructor(// 声明一个上下文对象
    private val mContext: Context, attrs: AttributeSet? = null, defStyle: Int = 0
) : LinearLayout(
    mContext, attrs, defStyle
), MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener,
    CompoundButton.OnCheckedChangeListener {
    private var mMediaRecorder // 声明一个媒体录制器对象
            : MediaRecorder? = null
    private val pb_record // 声明一个进度条对象
            : ProgressBar
    private val ck_record: CheckBox
    private var mTimer // 计时器
            : Timer? = null
    private val mRecordMaxTime = 10 // 一次录制的最长时间
    private var mTimeCount // 时间计数
            = 0

    // 获取录制好的媒体文件路径
    var recordFilePath // 录制文件的保存路径
            : String? = null
        private set

    // 开始录制
    fun start() {
        // 获取本次录制的媒体文件路径
        recordFilePath = MediaUtil.getRecordFilePath(mContext, "RecordAudio", ".amr")
        try {
            initRecord() // 初始化录制操作
            mTimeCount = 0 // 时间计数清零
            mTimer = Timer() // 创建一个计时器
            // 计时器每隔一秒就更新进度条上的录制进度
            mTimer!!.schedule(object : TimerTask() {
                override fun run() {
                    pb_record.progress = mTimeCount++
                }
            }, 0, 1000)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 停止录制
    fun stop() {
        if (mOnRecordFinishListener != null) {
            mOnRecordFinishListener!!.onRecordFinish()
        }
        pb_record.progress = 0 // 进度条归零
        if (mTimer != null) {
            mTimer!!.cancel() // 取消计时器
        }
        cancelRecord() // 取消录制操作
    }

    // 初始化录制操作
    private fun initRecord() {
        mMediaRecorder = MediaRecorder() // 创建一个媒体录制器
        mMediaRecorder!!.setOnErrorListener(this) // 设置媒体录制器的错误监听器
        mMediaRecorder!!.setOnInfoListener(this) // 设置媒体录制器的信息监听器
        mMediaRecorder!!.setAudioSource(AudioSource.MIC) // 设置音频源为麦克风
        mMediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB) // 设置媒体的输出格式
        mMediaRecorder!!.setAudioEncoder(AudioEncoder.AMR_NB) // 设置媒体的音频编码器
        // mMediaRecorder.setAudioSamplingRate(8); // 设置媒体的音频采样率。可选
        // mMediaRecorder.setAudioChannels(2); // 设置媒体的音频声道数。可选
        // mMediaRecorder.setAudioEncodingBitRate(1024); // 设置音频每秒录制的字节数。可选
        mMediaRecorder!!.setMaxDuration(10 * 1000) // 设置媒体的最大录制时长
        // mMediaRecorder.setMaxFileSize(1024*1024*10); // 设置媒体的最大文件大小
        // setMaxFileSize与setMaxDuration设置其一即可
        mMediaRecorder!!.setOutputFile(recordFilePath) // 设置媒体文件的保存路径
        try {
            mMediaRecorder!!.prepare() // 媒体录制器准备就绪
            mMediaRecorder!!.start() // 媒体录制器开始录制
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 取消录制操作
    private fun cancelRecord() {
        if (mMediaRecorder != null) {
            mMediaRecorder!!.setOnErrorListener(null) // 错误监听器置空
            mMediaRecorder!!.setPreviewDisplay(null) // 预览界面置空
            try {
                mMediaRecorder!!.stop() // 媒体录制器停止录制
            } catch (e: Exception) {
                e.printStackTrace()
            }
            mMediaRecorder!!.release() // 媒体录制器释放资源
            mMediaRecorder = null
        }
    }

    private var mOnRecordFinishListener // 声明一个录制完成监听器对象
            : OnRecordFinishListener? = null

    // 定义一个录制完成监听器接口
    interface OnRecordFinishListener {
        fun onRecordFinish()
    }

    // 设置录制完成监听器
    fun setOnRecordFinishListener(listener: OnRecordFinishListener?) {
        mOnRecordFinishListener = listener
    }

    // 在录制发生错误时触发
    override fun onError(mr: MediaRecorder, what: Int, extra: Int) {
        if (mr != null) {
            mr.reset() // 重置媒体录制器
        }
    }

    // 在录制遇到状况时触发
    override fun onInfo(mr: MediaRecorder, what: Int, extra: Int) {
        // 录制达到最大时长，或者达到文件大小限制，都停止录制
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED
            || what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED
        ) {
            ck_record.isChecked = false
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_record) {
            if (isChecked) { // 开始录制
                ck_record.text = "停止录制"
                start()
            } else { // 停止录制
                ck_record.text = "开始录制"
                stop()
            }
        }
    }

    companion object {
        private const val TAG = "AudioRecorder"
    }

    init {
        // 从布局文件audio_recorder.xml生成当前的布局视图
        LayoutInflater.from(mContext).inflate(R.layout.audio_recorder, this)
        // 从布局文件中获取名叫pb_record的进度条
        pb_record = findViewById(R.id.pb_record)
        // 设置进度条的最大值
        pb_record.max = mRecordMaxTime
        ck_record = findViewById(R.id.ck_record)
        ck_record.setOnCheckedChangeListener(this)
    }
}