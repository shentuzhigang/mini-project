package io.shentuzhigang.demo.device.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import io.shentuzhigang.demo.device.R
import java.util.*

class BettingView @JvmOverloads constructor(context: Context?, attr: AttributeSet? = null) :
    View(context, attr) {
    private var mWidth // 区域的宽度
            = 0
    private var mHeight // 区域的高度
            = 0
    private val mBowlBg // 摇骰子的碗背景位图
            : Bitmap
    private val mRectSrc // 位图的原始边界
            : Rect
    private var mRectDest // 位图的目标边界
            : Rect? = null
    private var mDiceList: ArrayList<Int>? = ArrayList() // 骰子队列
    private val mDiceOne: Bitmap
    private val mDiceTwo: Bitmap
    private val mDiceThree: Bitmap
    private val mDiceFour: Bitmap
    private val mDiceFive: Bitmap
    private val mDiceSix // 六个骰子的位图
            : Bitmap
    private val mShake01: Bitmap
    private val mShake02: Bitmap
    private val mShake03: Bitmap
    private val mShake04: Bitmap
    private val mShake05 // 摇晃时候的位图
            : Bitmap

    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        mWidth = measuredWidth
        mHeight = mWidth * mBowlBg.height / mBowlBg.width
        if (width < height) { // 宽度小于高度
            // 缩小高度到宽度一样尺寸
            super.onMeasure(widthMeasureSpec, widthMeasureSpec)
        } else { // 宽度不小于高度
            // 缩小宽度到高度一样尺寸
            super.onMeasure(heightMeasureSpec, heightMeasureSpec)
        }
        // 根据视图的宽高创建位图的目标边界
        mRectDest = Rect(0, 0, mWidth, mHeight)
        Log.d(TAG, "mWidth=$mWidth")
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        val item_width = mWidth / 5
        val item_height = mHeight / 5
        // 在画布上绘制摇骰子的碗背景
        canvas.drawBitmap(mBowlBg, mRectSrc, mRectDest!!, Paint())
        // 摇晃结束，逐个绘制六个骰子的位图
        for (i in mDiceList!!.indices) {
            var bitmap = mDiceSix // 默认点数6
            if (mDiceList!![i] == 0) { // 点数1
                bitmap = mDiceOne
            } else if (mDiceList!![i] == 1) { // 点数2
                bitmap = mDiceTwo
            } else if (mDiceList!![i] == 2) { // 点数3
                bitmap = mDiceThree
            } else if (mDiceList!![i] == 3) { // 点数4
                bitmap = mDiceFour
            } else if (mDiceList!![i] == 4) { // 点数5
                bitmap = mDiceFive
            }
            // 计算该骰子的左侧坐标
            val left = item_width + item_width * (i % 3)
            // 计算该骰子的上方坐标
            val top = item_height * 2 + item_height * (i / 3)
            if (item_width > bitmap.width * 2.5) { // 碗够大，则放大骰子
                Log.d(
                    TAG,
                    "left=" + left + ", top=" + top + ", right=" + (left + bitmap.width) + ", bottom=" + (top + bitmap.height)
                )
                val src = Rect(0, 0, bitmap.width, bitmap.height)
                // 骰子图案放大至原来的1.5倍
                val dst = Rect(
                    left, top,
                    (left + bitmap.width * 1.5).toInt(), (top + bitmap.height * 1.5).toInt()
                )
                // 在画布上绘制放大后的骰子
                canvas.drawBitmap(bitmap, src, dst, Paint())
            } else { // 碗不够大，则不放大骰子
                // 在画布上绘制保持原状的骰子
                canvas.drawBitmap(bitmap, left.toFloat(), top.toFloat(), Paint())
            }
        }
        // 还在摇晃，于是绘制随机位置的几个骰子
        if (mDiceList == null || mDiceList!!.size <= 0) {
            for (j in 0..5) {
                val seq = (Math.random() * 100 % 5).toInt()
                var bitmap = mShake05
                if (seq == 0) {
                    bitmap = mShake01
                } else if (seq == 1) {
                    bitmap = mShake02
                } else if (seq == 2) {
                    bitmap = mShake03
                } else if (seq == 3) {
                    bitmap = mShake04
                }
                val left = item_width + (item_width * (Math.random() * 10 % 2.0)).toInt()
                val top = item_height + (item_height * (Math.random() * 10 % 2.0)).toInt()
                if (item_width > bitmap.width * 2) {
                    val src = Rect(0, 0, bitmap.width, bitmap.height)
                    val dst = Rect(
                        left, top,
                        (left + bitmap.width * 1.5).toInt(), (top + bitmap.height * 1.5).toInt()
                    )
                    canvas.drawBitmap(bitmap, src, dst, Paint())
                } else {
                    canvas.drawBitmap(bitmap, left.toFloat(), top.toFloat(), Paint())
                }
            }
        }
    }

    // 结束摇骰子，展示点数确定的骰子队列
    fun setDiceList(diceList: ArrayList<Int>?) {
        mDiceList = diceList
        // 立即刷新视图，也就是调用视图的onDraw和dispatchDraw方法
        invalidate()
    }

    // 正在摇骰子，随机展示摇晃着的骰子
    fun setRandom() {
        mDiceList = ArrayList()
        // 立即刷新视图，也就是调用视图的onDraw和dispatchDraw方法
        invalidate()
    }

    companion object {
        private const val TAG = "BettingView"
    }

    init {
        // 从资源图片中获取碗背景的位图
        mBowlBg = BitmapFactory.decodeResource(resources, R.drawable.bobing_bg)
        // 根据位图的宽高创建位图的原始边界
        mRectSrc = Rect(0, 0, mBowlBg.width, mBowlBg.height)
        // 以下分别从资源图片中获取六个骰子的位图
        mDiceOne = BitmapFactory.decodeResource(resources, R.drawable.dice01)
        mDiceTwo = BitmapFactory.decodeResource(resources, R.drawable.dice02)
        mDiceThree = BitmapFactory.decodeResource(resources, R.drawable.dice03)
        mDiceFour = BitmapFactory.decodeResource(resources, R.drawable.dice04)
        mDiceFive = BitmapFactory.decodeResource(resources, R.drawable.dice05)
        mDiceSix = BitmapFactory.decodeResource(resources, R.drawable.dice06)
        // 以下分别从资源图片中获取摇晃时候的位图
        mShake01 = BitmapFactory.decodeResource(resources, R.drawable.shake01)
        mShake02 = BitmapFactory.decodeResource(resources, R.drawable.shake02)
        mShake03 = BitmapFactory.decodeResource(resources, R.drawable.shake03)
        mShake04 = BitmapFactory.decodeResource(resources, R.drawable.shake04)
        mShake05 = BitmapFactory.decodeResource(resources, R.drawable.shake05)
    }
}