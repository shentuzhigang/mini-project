package io.shentuzhigang.demo.device.widget

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.Image
import android.media.ImageReader
import android.media.ImageReader.OnImageAvailableListener
import android.os.*
import android.util.AttributeSet
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import io.shentuzhigang.demo.device.util.BitmapUtil
import io.shentuzhigang.demo.device.util.DateUtil
import java.util.*

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class Camera2View @JvmOverloads constructor(// 声明一个上下文对象
    private val mContext: Context, attrs: AttributeSet? = null
) : TextureView(
    mContext, attrs
) {
    private var mHandler: Handler? = null
    private val mThreadHandler: HandlerThread
    private var mPreviewBuilder // 声明一个拍照请求构建器对象
            : CaptureRequest.Builder? = null
    private var mCameraSession // 声明一个相机拍照会话对象
            : CameraCaptureSession? = null
    private var mCameraDevice // 声明一个相机设备对象
            : CameraDevice? = null
    private var mImageReader // 声明一个图像读取器对象
            : ImageReader? = null
    private var mPreViewSize // 预览画面的尺寸
            : Size? = null
    private var mCameraType = CameraCharacteristics.LENS_FACING_FRONT // 摄像头类型
    private var mTakeType = 0 // 拍摄类型。0为单拍，1为连拍

    // 打开指定摄像头的相机视图
    fun open(camera_type: Int) {
        mCameraType = camera_type
        // 设置表面纹理变更监听器
        surfaceTextureListener = mSurfacetextlistener
    }

    // 获取照片的保存路径
    var photoPath // 照片的保存路径
            : String? = null
        private set

    // 执行拍照动作
    fun takePicture() {
        Log.d(TAG, "正在拍照")
        mTakeType = 0
        try {
            val builder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            // 把图像读取器添加到预览目标
            builder.addTarget(mImageReader!!.surface)
            // 设置自动对焦模式
            builder.set(
                CaptureRequest.CONTROL_AF_MODE,
                CaptureRequest.CONTROL_AF_MODE_AUTO
            )
            // 设置自动曝光模式
            builder.set(
                CaptureRequest.CONTROL_AF_MODE,
                CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH
            )
            // 开始对焦
            builder.set(
                CaptureRequest.CONTROL_AF_TRIGGER,
                CameraMetadata.CONTROL_AF_TRIGGER_START
            )
            // 设置照片的方向
            builder.set(
                CaptureRequest.JPEG_ORIENTATION,
                if (mCameraType == CameraCharacteristics.LENS_FACING_FRONT) 90 else 270
            )
            // 拍照会话开始捕获相片
            mCameraSession!!.capture(builder.build(), null, mHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private var mShootingArray // 连拍的相片保存路径队列
            : ArrayList<String>? = null

    // 获取连拍的相片保存路径队列
    val shootingList: ArrayList<String>?
        get() {
            Log.d(TAG, "mShootingArray.size()=" + mShootingArray!!.size)
            return mShootingArray
        }

    // 开始连拍
    fun startShooting(duration: Int) {
        Log.d(TAG, "正在连拍")
        mTakeType = 1
        mShootingArray = ArrayList()
        try {
            // 停止连拍
            mCameraSession!!.stopRepeating()
            // 把图像读取器添加到预览目标
            mPreviewBuilder!!.addTarget(mImageReader!!.surface)
            // 设置连拍请求。此时预览画面会同时发给手机屏幕和图像读取器
            mCameraSession!!.setRepeatingRequest(mPreviewBuilder!!.build(), null, mHandler)
            // duration小等于0时，表示持续连拍，此时外部要调用stopShooting方法来结束连拍
            if (duration > 0) {
                // 延迟若干秒后启动拍摄停止任务
                mHandler?.postDelayed(mStop, duration.toLong())
            }
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    // 停止连拍
    fun stopShooting() {
        try {
            // 停止连拍
            mCameraSession!!.stopRepeating()
            // 移除图像读取器的预览目标
            mPreviewBuilder!!.removeTarget(mImageReader!!.surface)
            // 设置连拍请求。此时预览画面只会发给手机屏幕
            mCameraSession!!.setRepeatingRequest(mPreviewBuilder!!.build(), null, mHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
        Toast.makeText(mContext, "已完成连拍，按返回键回到上页查看照片。", Toast.LENGTH_SHORT).show()
    }

    // 定义一个拍摄停止任务
    private val mStop = Runnable { stopShooting() }

    // 打开相机
    private fun openCamera() {
        // 从系统服务中获取相机管理器
        val cm = mContext.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraid = mCameraType.toString() + ""
        try {
            // 获取可用相机设备列表
            val cc = cm.getCameraCharacteristics(cameraid)
            // 检查相机硬件的支持级别
            // CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL表示完全支持
            // CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED表示有限支持
            // CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY表示遗留的
            val level = cc.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL)!!
            val map = cc.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            val largest = Collections.max(
                Arrays.asList(*map!!.getOutputSizes(ImageFormat.JPEG)),
                CompareSizeByArea()
            )
            // 获取预览画面的尺寸
            mPreViewSize = map.getOutputSizes(SurfaceTexture::class.java)[0]
            // 创建一个JPEG格式的图像读取器
            mImageReader =
                ImageReader.newInstance(largest.width, largest.height, ImageFormat.JPEG, 10)
            // 设置图像读取器的图像可用监听器，一旦捕捉到图像数据就会触发监听器的onImageAvailable方法
            mImageReader!!.setOnImageAvailableListener(onImageAvaiableListener, mHandler)
            // 开启摄像头
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            cm.openCamera(cameraid, mDeviceStateCallback, mHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    // 关闭相机
    private fun closeCamera() {
        if (null != mCameraSession) {
            mCameraSession!!.close() // 关闭相机拍摄会话
            mCameraSession = null
        }
        if (null != mCameraDevice) {
            mCameraDevice!!.close() // 关闭相机设备
            mCameraDevice = null
        }
        if (null != mImageReader) {
            mImageReader!!.close() // 关闭图像读取器
            mImageReader = null
        }
    }

    // 定义一个表面纹理变更监听器。TextureView准备就绪后，立即开启相机
    private val mSurfacetextlistener: SurfaceTextureListener = object : SurfaceTextureListener {
        // 在纹理表面可用时触发
        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
            openCamera() // 打开相机
        }

        // 在纹理表面的尺寸发生改变时触发
        override fun onSurfaceTextureSizeChanged(
            surface: SurfaceTexture,
            width: Int,
            height: Int
        ) {
        }

        // 在纹理表面销毁时触发
        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
            closeCamera() // 关闭相机
            return true
        }

        // 在纹理表面更新时触发
        override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}
    }

    // 创建相机预览会话
    private fun createCameraPreviewSession() {
        // 获取纹理视图的表面纹理
        val texture = surfaceTexture
        // 设置表面纹理的默认缓存尺寸
        texture!!.setDefaultBufferSize(mPreViewSize!!.width, mPreViewSize!!.height)
        // 创建一个该表面纹理的表面对象
        val surface = Surface(texture)
        try {
            mPreviewBuilder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            // 把纹理视图添加到预览目标
            mPreviewBuilder!!.addTarget(surface)
            // 设置自动对焦模式
            mPreviewBuilder!!.set(
                CaptureRequest.CONTROL_AF_MODE,
                CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
            )
            // 设置自动曝光模式
            mPreviewBuilder!!.set(
                CaptureRequest.CONTROL_AF_MODE,
                CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH
            )
            // 开始对焦
            mPreviewBuilder!!.set(
                CaptureRequest.CONTROL_AF_TRIGGER,
                CameraMetadata.CONTROL_AF_TRIGGER_START
            )
            // 设置照片的方向
            mPreviewBuilder!!.set(
                CaptureRequest.JPEG_ORIENTATION,
                if (mCameraType == CameraCharacteristics.LENS_FACING_FRONT) 90 else 270
            )
            // 创建一个相片捕获会话。此时预览画面显示在纹理视图上
            mCameraDevice!!.createCaptureSession(
                Arrays.asList(surface, mImageReader!!.surface),
                mSessionStateCallback, mHandler
            )
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    // 相机准备就绪后，开启捕捉影像的会话
    private val mDeviceStateCallback: CameraDevice.StateCallback =
        object : CameraDevice.StateCallback() {
            override fun onOpened(cameraDevice: CameraDevice) {
                mCameraDevice = cameraDevice
                createCameraPreviewSession()
            }

            override fun onDisconnected(cameraDevice: CameraDevice) {
                cameraDevice.close()
                mCameraDevice = null
            }

            override fun onError(cameraDevice: CameraDevice, error: Int) {
                cameraDevice.close()
                mCameraDevice = null
            }
        }

    // 影像配置就绪后，将预览画面呈现到手机屏幕上
    private val mSessionStateCallback: CameraCaptureSession.StateCallback =
        object : CameraCaptureSession.StateCallback() {
            override fun onConfigured(session: CameraCaptureSession) {
                try {
                    Log.d(TAG, "onConfigured")
                    mCameraSession = session
                    // 设置连拍请求。此时预览画面只会发给手机屏幕
                    mCameraSession!!.setRepeatingRequest(mPreviewBuilder!!.build(), null, mHandler)
                } catch (e: CameraAccessException) {
                    e.printStackTrace()
                }
            }

            override fun onConfigureFailed(session: CameraCaptureSession) {}
        }

    // 一旦有图像数据生成，立刻触发onImageAvailable事件
    private val onImageAvaiableListener = OnImageAvailableListener { imageReader ->
        Log.d(TAG, "onImageAvailable")
        mHandler?.post(ImageSaver(imageReader.acquireNextImage()))
    }

    // 定义一个图像保存任务
    private inner class ImageSaver(private val mImage: Image?) : Runnable {
        override fun run() {
            // 获取本次拍摄的照片保存路径
            val path = String.format(
                "%s%s.jpg",
                BitmapUtil.getCachePath(mContext),
                DateUtil.nowDateTime
            )
            Log.d(TAG, "正在保存图片 path=$path")
            // 保存图片文件
            BitmapUtil.saveBitmap(path, mImage!!.planes[0].buffer, 4, "JPEG", 80)
            //BitmapUtil.setPictureDegreeZero(path);
            if (mImage != null) {
                mImage.close()
            }
            if (mTakeType == 0) { // 单拍
                photoPath = path
            } else { // 连拍
                mShootingArray!!.add(path)
            }
            Log.d(TAG, "完成保存图片 path=$path")
        }
    }

    private inner class CompareSizeByArea : Comparator<Size> {
        override fun compare(lhs: Size, rhs: Size): Int {
            return java.lang.Long.signum(
                lhs.width.toLong() * lhs.height
                        - rhs.width.toLong() * rhs.height
            )
        }
    }

    companion object {
        private const val TAG = "Camera2View"
    }

    init {
        mThreadHandler = HandlerThread("camera2")
        mThreadHandler.start()
        mHandler = Handler(mThreadHandler.looper)
    }
}