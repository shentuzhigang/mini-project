package io.shentuzhigang.demo.device.widget

import android.content.Context
import android.graphics.*
import android.hardware.Camera
import android.hardware.Camera.PictureCallback
import android.hardware.Camera.ShutterCallback
import android.util.AttributeSet
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.Toast
import io.shentuzhigang.demo.device.util.BitmapUtil
import io.shentuzhigang.demo.device.util.CameraUtil
import io.shentuzhigang.demo.device.util.DateUtil
import java.io.ByteArrayOutputStream
import java.util.*

class CameraView @JvmOverloads constructor(// 声明一个上下文对象
    private val mContext: Context, attrs: AttributeSet? = null
) : SurfaceView(
    mContext, attrs
) {
    private var mCamera // 声明一个相机对象
            : Camera? = null
    private var isPreviewing = false // 是否正在预览
    private var mCameraSize // 相机画面的尺寸
            : Point? = null

    // 设置摄像头的类型
    // 获取摄像头的类型
    var cameraType = CAMERA_BEHIND // 摄像头类型

    // 下面是单拍的代码
    // 执行拍照动作。外部调用该方法完成拍照
    fun doTakePicture() {
        if (isPreviewing && mCamera != null) {
            // 命令相机拍摄一张照片
            mCamera!!.takePicture(mShutterCallback, null, mPictureCallback)
        }
    }

    // 获取照片的保存路径。外部调用该方法获得相片文件的路径
    var photoPath // 照片的保存路径
            : String? = null
        private set

    // 定义一个快门按下的回调监听器。可在此设置类似播放“咔嚓”声之类的操作，默认就是咔嚓。
    private val mShutterCallback = ShutterCallback { Log.d(TAG, "onShutter...") }

    // 定义一个获得拍照结果的回调监听器。可在此保存图片
    private val mPictureCallback: PictureCallback = object : PictureCallback {
        override fun onPictureTaken(data: ByteArray, camera: Camera) {
            Log.d(TAG, "onPictureTaken...")
            var raw: Bitmap? = null
            // 原始图像数据data是字节数组，需要将其解析成位图
            raw = BitmapFactory.decodeByteArray(data, 0, data.size)
            // 停止预览画面
            mCamera!!.stopPreview()
            isPreviewing = false
            // 旋转位图
            val bitmap = BitmapUtil.getRotateBitmap(
                raw,
                (if (cameraType == CAMERA_BEHIND) 90 else -90.toFloat()) as Float
            )
            // 获取本次拍摄的照片保存路径
            photoPath = String.format(
                "%s%s.jpg", BitmapUtil.getCachePath(mContext),
                DateUtil.nowDateTime
            )
            // 保存照片文件
            BitmapUtil.saveBitmap(photoPath, bitmap, "jpg", 80)
            Log.d(TAG, "bitmap.size=" + bitmap.byteCount / 1024 + "K" + ", path=" + photoPath)
            try {
                Thread.sleep(1000) // 保存文件需要时间
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            // 再次进入预览画面
            mCamera!!.startPreview()
            isPreviewing = true
        }
    }

    // 预览画面状态变更时的回调监听器
    private val mSurfaceCallback: SurfaceHolder.Callback = object : SurfaceHolder.Callback {
        // 在表面视图创建时触发
        override fun surfaceCreated(holder: SurfaceHolder) {
            // 打开摄像头
            mCamera = Camera.open(cameraType)
            try {
                // 设置相机的预览界面
                mCamera?.setPreviewDisplay(holder)
                // 获得相机画面的尺寸
                mCameraSize = mCamera?.getParameters()?.let {
                    CameraUtil.getCameraSize(
                        it,
                        CameraUtil.getSize(mContext)
                    )
                }
                Log.d(TAG, "width=" + mCameraSize!!.x + ", height=" + mCameraSize!!.y)
                // 获取相机的参数信息
                val parameters = mCamera?.getParameters()
                // 设置预览界面的尺寸
                parameters?.setPreviewSize(mCameraSize!!.x, mCameraSize!!.y)
                // 设置图片的分辨率
                parameters?.setPictureSize(mCameraSize!!.x, mCameraSize!!.y)
                // 如果想得到最大分辨率的图片，可使用下面两行代码设置最大的图片尺寸
                //Camera.Size maxSize = CameraUtil.getMaxPictureSize(mCamera.getParameters());
                //parameters.setPictureSize(maxSize.width, maxSize.height);
                // 设置图片的格式
                parameters?.pictureFormat = ImageFormat.JPEG
                // 设置对焦模式为自动对焦。前置摄像头似乎无法自动对焦
                if (cameraType == CAMERA_BEHIND) {
                    //parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    // FOCUS_MODE_AUTO只会自动对焦一次，若想连续对焦则需用下面的FOCUS_MODE_CONTINUOUS_PICTURE
                    parameters?.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
                }
                // 设置相机的参数信息
                mCamera?.setParameters(parameters)
            } catch (e: Exception) {
                e.printStackTrace()
                mCamera?.release() // 遇到异常要释放相机资源
                mCamera = null
            }
        }

        // 在表面视图变更时触发
        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            // 设置相机的展示角度
            mCamera!!.setDisplayOrientation(90)
            // 开始预览画面
            mCamera!!.startPreview()
            isPreviewing = true
            // 开始自动对焦
            mCamera!!.autoFocus(null)
            // 设置相机的预览监听器。注意这里的setPreviewCallback给连拍功能使用
            mCamera!!.setPreviewCallback(mPreviewCallback)
        }

        // 在表面视图销毁时触发
        override fun surfaceDestroyed(holder: SurfaceHolder) {
            // 将预览监听器置空
            mCamera!!.setPreviewCallback(null)
            // 停止预览画面
            mCamera!!.stopPreview()
            // 释放相机资源
            mCamera!!.release()
            mCamera = null
        }
    }

    // 下面是连拍的代码
    private var isShooting = false // 是否正在连拍
    private var shooting_num = 0 // 已经拍摄的相片数量

    // 执行连拍动作。外部调用该方法完成连拍
    fun doTakeShooting() {
        shootingList = ArrayList()
        isShooting = true
        shooting_num = 0
    }

    // 获取连拍的相片保存路径队列。外部调用该方法获得连拍结果相片的路径队列
    var shootingList // 连拍的相片保存路径队列
            : ArrayList<String>? = null
        private set

    // 定义一个画面预览的回调监听器。在此可捕获动态的连续图片
    private val mPreviewCallback: Camera.PreviewCallback = object : Camera.PreviewCallback {
        override fun onPreviewFrame(data: ByteArray, camera: Camera) {
            Log.d(TAG, "onPreviewFrame isShooting=$isShooting, shooting_num=$shooting_num")
            if (!isShooting) {
                return
            }
            // 获取相机的参数信息
            val parameters = camera.parameters
            // 获得预览数据的格式
            val imageFormat = parameters.previewFormat
            val width = parameters.previewSize.width
            val height = parameters.previewSize.height
            val rect = Rect(0, 0, width, height)
            // 创建一个YUV格式的图像对象
            val yuvImg = YuvImage(data, imageFormat, width, height, null)
            try {
                val bos = ByteArrayOutputStream()
                yuvImg.compressToJpeg(rect, 80, bos)
                // 从字节数组中解析出位图数据
                val raw = BitmapFactory.decodeByteArray(
                    bos.toByteArray(), 0, bos.size()
                )
                // 旋转位图
                val bitmap = BitmapUtil.getRotateBitmap(
                    raw,
                    (if (cameraType == CAMERA_BEHIND) 90 else -90.toFloat()) as Float
                )
                // 获取本次拍摄的照片保存路径
                val path = String.format(
                    "%s%s.jpg", BitmapUtil.getCachePath(mContext),
                    DateUtil.nowDateTimeFull
                )
                // 把位图保存为图片文件
                BitmapUtil.saveBitmap(path, bitmap, "jpg", 80)
                Log.d(TAG, "bitmap.size=" + bitmap.byteCount / 1024 + "K" + ", path=" + path)
                // 再次进入预览画面
                camera.startPreview()
                shooting_num++
                shootingList!!.add(path)
                if (shooting_num > 8) {  // 每次连拍9张
                    isShooting = false
                    Toast.makeText(mContext, "已完成连拍，按返回键回到上页查看照片。", Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        private const val TAG = "CameraView"
        var CAMERA_BEHIND = 0 // 后置摄像头
        var CAMERA_FRONT = 1 // 前置摄像头
    }

    init {
        // 获取表面视图的表面持有者
        val holder = holder
        // 给表面持有者添加表面变更监听器
        holder.addCallback(mSurfaceCallback)
        // 去除黑色背景。TRANSLUCENT半透明；TRANSPARENT透明
        holder.setFormat(PixelFormat.TRANSPARENT)
    }
}