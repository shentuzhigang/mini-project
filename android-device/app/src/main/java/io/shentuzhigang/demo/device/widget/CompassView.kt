package io.shentuzhigang.demo.device.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import io.shentuzhigang.demo.device.R
import io.shentuzhigang.demo.device.bean.Satellite
import io.shentuzhigang.demo.device.util.Utils
import java.util.*

class CompassView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null) :
    View(context, attr) {
    private var mWidth = 0
    private val mPaintLine // 弧线的画笔
            : Paint
    private val mPaintText // 文字的画笔
            : Paint
    private val mPaintAngle // 刻度的画笔
            : Paint
    private val mCompassBg // 背景罗盘的位图
            : Bitmap
    private val mRectSrc // 位图的原始边界
            : Rect
    private var mRectDest // 位图的目标边界
            : Rect? = null
    private var mRectAngle // 刻度的矩形边界
            : RectF? = null
    private val mSatelliteChina // 中国北斗卫星的图标
            : Bitmap
    private val mSatelliteAmerica // 美国GPS卫星的图标
            : Bitmap
    private val mSatelliteRussia // 俄罗斯格洛纳斯卫星的图标
            : Bitmap
    private val mSatelliteOther // 其它国家卫星的图标
            : Bitmap
    private var mapSatellite: Map<Int, Satellite> = HashMap() // 卫星分布映射
    private val mScaleLength = 25 // 刻度线的长度
    private val mBorder = 0.9f // 边界的倍率，比如只在整个区域的90%内部绘图
    private var mRectSourth // 指南针的矩形边界
            : RectF? = null
    private var mDirection = -1024 // 指南针的方向
    private val mPaintSourth // 指南针的画笔
            : Paint

    // 重写onMeasure方法，使得该视图无论竖屏还是横屏都保持正方形状
    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        mWidth = measuredWidth
        if (width < height) { // 宽度比高度小，则缩短高度使之与宽度一样长
            super.onMeasure(widthMeasureSpec, widthMeasureSpec)
        } else { // 宽度比高度大，则缩短宽度使之与高度一样长
            super.onMeasure(heightMeasureSpec, heightMeasureSpec)
        }
        // 根据视图的宽高创建位图的目标边界
        mRectDest = Rect(0, 0, mWidth, mWidth)
        // 创建刻度的矩形边界
        mRectAngle = RectF(
            (mWidth / 10).toFloat(),
            (mWidth / 10).toFloat(),
            (mWidth * 9 / 10).toFloat(),
            (mWidth * 9 / 10).toFloat()
        )
        // 创建指南针的矩形边界
        mRectSourth =
            RectF(mWidth * 0.3f / 10, mWidth * 0.3f / 10, mWidth * 9.7f / 10, mWidth * 9.7f / 10)
        Log.d(TAG, "mWidth=$mWidth")
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        val radius = mWidth / 2
        val margin = radius / 10
        // 在画布上绘制罗盘背景
        canvas.drawBitmap(mCompassBg, mRectSrc, mRectDest!!, Paint())
        // 以下在画布上绘制各种半径的圆圈
        canvas.drawCircle(
            radius.toFloat(),
            radius.toFloat(),
            (radius * 3 / 10).toFloat(),
            mPaintLine
        )
        canvas.drawCircle(
            radius.toFloat(),
            radius.toFloat(),
            (radius * 5 / 10).toFloat(),
            mPaintLine
        )
        canvas.drawCircle(
            radius.toFloat(),
            radius.toFloat(),
            (radius * 7 / 10).toFloat(),
            mPaintLine
        )
        canvas.drawCircle(
            radius.toFloat(),
            radius.toFloat(),
            (radius * 9 / 10).toFloat(),
            mPaintLine
        )
        // 在画布上绘制罗盘的中央垂直线
        canvas.drawLine(
            radius.toFloat(),
            margin.toFloat(),
            radius.toFloat(),
            (mWidth - margin).toFloat(),
            mPaintLine
        )
        // 在画布上绘制罗盘的中央水平线
        canvas.drawLine(
            margin.toFloat(),
            radius.toFloat(),
            (mWidth - margin).toFloat(),
            radius.toFloat(),
            mPaintLine
        )
        // 画罗盘的刻度
        var i = 0
        while (i < 360) {
            val path = Path()
            path.addArc(mRectAngle!!, (i - 3).toFloat(), (i + 3).toFloat())
            val angle = (i + 90) % 360
            // 在画布上绘制刻度文字
            canvas.drawTextOnPath("" + angle, path, 0f, 0f, mPaintAngle)
            // 在画布上绘制刻度线条
            canvas.drawLine(
                getXpos(radius, angle, (radius * mBorder).toDouble()),
                getYpos(radius, angle, (radius * mBorder).toDouble()),
                getXpos(radius, angle, ((radius - mScaleLength) * mBorder).toDouble()),
                getYpos(radius, angle, ((radius - mScaleLength) * mBorder).toDouble()),
                mPaintAngle
            )
            i += 30
        }
        // 画卫星分布图
        for ((_, item) in mapSatellite) {
            var bitmap: Bitmap
            if (item.nation == "中国") { // 北斗卫星
                bitmap = mSatelliteChina
            } else if (item.nation == "美国") { // GPS卫星
                bitmap = mSatelliteAmerica
            } else if (item.nation == "俄罗斯") { // 格洛纳斯卫星
                bitmap = mSatelliteRussia
            } else if (item.nation != "") { // 其它卫星
                bitmap = mSatelliteOther
                Log.d(TAG, "Other seq=" + item.seq)
            } else {
                continue
            }
            val left = getXpos(radius, item.azimuth, radius * mBorder * getCos(item.elevation))
            val top = getYpos(radius, item.azimuth, radius * mBorder * getCos(item.elevation))
            // 在画布上绘制卫星图标
            canvas.drawBitmap(
                bitmap, left - bitmap.width / 2,
                top - bitmap.height / 2, Paint()
            )
        }
        // 画指南针
        if (mDirection > -1024) {
            val angle = (-mDirection + 450) % 360
            // 在画布上绘制组成指南针的四个线条，包括一个三角形加上一根杆
            canvas.drawLine(
                getXpos(radius, angle, (radius * mBorder).toDouble()),
                getYpos(radius, angle, (radius * mBorder).toDouble()),
                getXpos(radius, angle, 0.0),
                getYpos(radius, angle, 0.0),
                mPaintSourth
            )
            canvas.drawLine(
                getXpos(radius, angle, (radius * mBorder).toDouble()),
                getYpos(radius, angle, (radius * mBorder).toDouble()),
                getXpos(radius, angle - 10, (radius * 7 / 10).toDouble()),
                getYpos(radius, angle - 10, (radius * 7 / 10).toDouble()),
                mPaintSourth
            )
            canvas.drawLine(
                getXpos(radius, angle, (radius * mBorder).toDouble()),
                getYpos(radius, angle, (radius * mBorder).toDouble()),
                getXpos(radius, angle + 10, (radius * 7 / 10).toDouble()),
                getYpos(radius, angle + 10, (radius * 7 / 10).toDouble()),
                mPaintSourth
            )
            canvas.drawLine(
                getXpos(radius, angle - 10, (radius * 7 / 10).toDouble()),
                getYpos(radius, angle - 10, (radius * 7 / 10).toDouble()),
                getXpos(radius, angle + 10, (radius * 7 / 10).toDouble()),
                getYpos(radius, angle + 10, (radius * 7 / 10).toDouble()),
                mPaintSourth
            )
            val path = Path()
            path.addArc(mRectSourth!!, (angle - 2).toFloat(), (angle + 2).toFloat())
            // 在画布上绘制指南的“南”
            canvas.drawTextOnPath("南", path, 0f, 0f, mPaintText)
        } else {
            // 在画布上绘制指北的“北”
            canvas.drawText("北", (radius - 15).toFloat(), (margin - 15).toFloat(), mPaintText)
        }
    }

    // 根据半径、角度、线长，计算该点的横坐标
    private fun getXpos(radius: Int, angle: Int, length: Double): Float {
        return (radius + getCos(angle) * length).toFloat()
    }

    // 根据半径、角度、线长，计算该点的纵坐标
    private fun getYpos(radius: Int, angle: Int, length: Double): Float {
        return (radius + getSin(angle) * length).toFloat()
    }

    // 获得指定角度的正弦值
    private fun getSin(angle: Int): Double {
        return Math.sin(Math.PI * angle / 180.0)
    }

    // 获得指定角度的余弦值
    private fun getCos(angle: Int): Double {
        return Math.cos(Math.PI * angle / 180.0)
    }

    // 设置卫星分布映射，用于卫星浑天仪
    fun setSatelliteMap(map: Map<Int, Satellite>) {
        mapSatellite = map
        // 立即刷新视图，也就是调用视图的onDraw和dispatchDraw方法
        invalidate()
    }

    // 设置正南方的方向，用于指南针
    fun setDirection(direction: Int) {
        mDirection = direction
        // 立即刷新视图，也就是调用视图的onDraw和dispatchDraw方法
        invalidate()
    }

    companion object {
        private const val TAG = "CompassView"
    }

    init {
        // 以下初始化弧线的画笔
        mPaintLine = Paint()
        mPaintLine.isAntiAlias = true
        mPaintLine.color = Color.GREEN
        mPaintLine.strokeWidth = 2f
        mPaintLine.style = Paint.Style.STROKE
        // 以下初始化文字的画笔
        mPaintText = Paint()
        mPaintText.isAntiAlias = true
        mPaintText.color = Color.RED
        mPaintText.strokeWidth = 1f
        mPaintText.style = Paint.Style.FILL
        mPaintText.textSize = Utils.dip2px(context, 14f).toFloat()
        // 以下初始化刻度的画笔
        mPaintAngle = Paint()
        mPaintAngle.isAntiAlias = true
        mPaintAngle.color = Color.BLACK
        mPaintAngle.strokeWidth = 1f
        mPaintAngle.style = Paint.Style.FILL
        mPaintAngle.textSize = Utils.dip2px(context, 12f).toFloat()
        // 以下初始化指南针的画笔
        mPaintSourth = Paint()
        mPaintSourth.isAntiAlias = true
        mPaintSourth.color = Color.RED
        mPaintSourth.strokeWidth = 4f
        mPaintSourth.style = Paint.Style.STROKE
        // 从资源图片中获取罗盘背景的位图
        mCompassBg = BitmapFactory.decodeResource(resources, R.drawable.compass_bg)
        // 根据位图的宽高创建位图的原始边界
        mRectSrc = Rect(0, 0, mCompassBg.width, mCompassBg.height)
        // 从资源图片中获取中国北斗卫星的图标
        mSatelliteChina = BitmapFactory.decodeResource(resources, R.drawable.satellite_china)
        // 从资源图片中获取美国GPS卫星的图标
        mSatelliteAmerica = BitmapFactory.decodeResource(resources, R.drawable.satellite_america)
        // 从资源图片中获取俄罗斯格洛纳斯卫星的图标
        mSatelliteRussia = BitmapFactory.decodeResource(resources, R.drawable.satellite_russia)
        // 从资源图片中获取其它国家卫星的图标
        mSatelliteOther = BitmapFactory.decodeResource(resources, R.drawable.satellite_other)
    }
}