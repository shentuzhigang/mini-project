package io.shentuzhigang.demo.device.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.RectF
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView

class TurnSurfaceView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null) :
    SurfaceView(context, attrs), SurfaceHolder.Callback {
    private var mPaint1: Paint? = null
    private var mPaint2 // 声明两个画笔对象
            : Paint? = null
    private var mRectF // 矩形边界
            : RectF? = null
    private var mBeginAngle1 = 0
    private var mBeginAngle2 = 180 // 两个扇形的起始角度
    private val mInterval = 70 // 绘制间隔
    private var isRunning = false // 是否正在转动
    private val mHolder // 声明一个表面持有者对象
            : SurfaceHolder

    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // 计算转动圆圈的直径
        val diameter = measuredWidth - paddingLeft - paddingRight
        // 根据圆圈直径创建转动区域的矩形边界
        mRectF = RectF(
            paddingLeft.toFloat(), paddingTop.toFloat(),
            (paddingLeft + diameter).toFloat(), (paddingTop + diameter).toFloat()
        )
    }

    // 开始转动
    fun start() {
        isRunning = true
        // 绘制第一个扇形的线程
        object : Thread() {
            override fun run() {
                while (isRunning) {
                    draw(mPaint1, mBeginAngle1)
                    try {
                        sleep(mInterval.toLong())
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    mBeginAngle1 += 3
                }
            }
        }.start()
        // 绘制第二个扇形的线程，第二个扇形在第一个扇形的对面
        object : Thread() {
            override fun run() {
                while (isRunning) {
                    draw(mPaint2, mBeginAngle2)
                    try {
                        sleep(mInterval.toLong())
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    mBeginAngle2 += 3
                }
            }
        }.start()
    }

    // 停止转动
    fun stop() {
        isRunning = false
    }

    // 绘制图形
    private fun draw(paint: Paint?, beginAngle: Int) {
        // 因为两个线程都在绘制，所以这里利用同步机制，防止资源被锁住
        synchronized(mHolder) {

            // 锁定表面持有者的画布对象
            val canvas = mHolder.lockCanvas()
            if (canvas != null) {
                // SurfaceView上次的绘图结果仍然保留，如果不想保留上次的绘图，则需将整个画布清空
                // canvas.drawColor(Color.WHITE);
                // 在画布上绘制扇形。第四个参数为true表示绘制扇形，为false表示绘制圆弧
                canvas.drawArc(mRectF!!, beginAngle.toFloat(), 10f, true, paint!!)
                // 解锁表面持有者的画布对象
                mHolder.unlockCanvasAndPost(canvas)
            }
        }
    }

    // 获取指定颜色的画笔
    private fun getPaint(color: Int): Paint {
        val paint = Paint() // 创建新画笔
        paint.isAntiAlias = true // 设置画笔为无锯齿
        paint.color = color // 设置画笔的颜色
        paint.strokeWidth = 10f // 设置画笔的线宽
        paint.style = Paint.Style.FILL // 设置画笔的类型。STROK表示空心，FILL表示实心
        return paint
    }

    // 在表面视图创建时触发
    override fun surfaceCreated(holder: SurfaceHolder) {
        mPaint1 = getPaint(Color.RED)
        mPaint2 = getPaint(Color.CYAN)
    }

    // 在表面视图变更时触发
    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}

    // 在表面视图销毁时触发
    override fun surfaceDestroyed(holder: SurfaceHolder) {}

    init {
        // 获取表面视图的表面持有者
        mHolder = holder
        // 给表面持有者添加表面变更监听器
        mHolder.addCallback(this)
        // 下面两行设置背景为透明，因为SurfaceView默认背景是黑色
        setZOrderOnTop(true)
        mHolder.setFormat(PixelFormat.TRANSLUCENT)
    }
}