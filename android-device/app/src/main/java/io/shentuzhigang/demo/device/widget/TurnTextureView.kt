package io.shentuzhigang.demo.device.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.SurfaceTexture
import android.util.AttributeSet
import android.view.TextureView
import android.view.TextureView.SurfaceTextureListener

class TurnTextureView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null) :
    TextureView(
        context!!, attrs
    ), SurfaceTextureListener, Runnable {
    private val mPaint // 声明一个画笔对象
            : Paint
    private var mRectF // 矩形边界
            : RectF? = null
    private var mBeginAngle = 0 // 起始角度
    private var isRunning = false // 是否正在转动
    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // 计算转动圆圈的直径
        val diameter = measuredWidth - paddingLeft - paddingRight
        // 根据圆圈直径创建转动区域的矩形边界
        mRectF = RectF(
            paddingLeft.toFloat(), paddingTop.toFloat(),
            (paddingLeft + diameter).toFloat(), (paddingTop + diameter).toFloat()
        )
    }

    // 开始转动
    fun start() {
        isRunning = true
        // 启动绘制线程
        Thread(this).start()
    }

    fun stop() {
        isRunning = false
    }

    // 停止转动
    override fun run() {
        while (isRunning) {
            draw(false)
            try {
                Thread.sleep(70)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            mBeginAngle += 3
        }
    }

    // 绘制图形
    private fun draw(isFirst: Boolean) {
        // 锁定纹理视图的画布对象
        val canvas = lockCanvas()
        if (canvas != null) {
            // TextureView上次的绘图结果仍然保留，如果不想保留上次的绘图，则需将整个画布清空
            // canvas.drawColor(Color.WHITE);
            if (!isFirst) {
                // 在画布上绘制扇形。第四个参数为true表示绘制扇形，为false表示绘制圆弧
                canvas.drawArc(mRectF!!, mBeginAngle.toFloat(), 30f, true, mPaint)
            }
            // 解锁纹理视图的画布对象
            unlockCanvasAndPost(canvas)
        }
    }

    // 在纹理表面可用时触发
    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
        draw(true)
    }

    // 在纹理表面销毁时触发
    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
        return true
    }

    // 在纹理表面的尺寸发生改变时触发
    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {}

    // 在纹理表面更新时触发
    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}

    init {
        mPaint = Paint() // 创建新画笔
        mPaint.isAntiAlias = true // 设置画笔为无锯齿
        mPaint.color = Color.RED // 设置画笔的颜色
        mPaint.strokeWidth = 10f // 设置画笔的线宽
        mPaint.style = Paint.Style.FILL // 设置画笔的类型。STROK表示空心，FILL表示实心
    }
}