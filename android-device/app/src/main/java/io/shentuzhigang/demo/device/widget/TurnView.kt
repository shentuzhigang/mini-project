package io.shentuzhigang.demo.device.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Handler
import android.util.AttributeSet
import android.view.View

class TurnView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null) :
    View(context, attrs) {
    private val mPaint // 声明一个画笔对象
            : Paint
    private var mRectF // 矩形边界
            : RectF? = null
    private var mBeginAngle = 0 // 起始角度
    private var isRunning = false // 是否正在转动
    private val mHandler = Handler() // 声明一个处理器对象
    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // 计算转动圆圈的直径
        val diameter = measuredWidth - paddingLeft - paddingRight
        // 根据圆圈直径创建转动区域的矩形边界
        mRectF = RectF(
            paddingLeft.toFloat(), paddingTop.toFloat(),
            (paddingLeft + diameter).toFloat(), (paddingTop + diameter).toFloat()
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // 在画布上绘制扇形。第四个参数为true表示绘制扇形，为false表示绘制圆弧
        canvas.drawArc(mRectF!!, mBeginAngle.toFloat(), 30f, true, mPaint)
    }

    // 开始转动
    fun start() {
        isRunning = true
        // 立即启动绘制任务
        mHandler.post(drawRunnable)
    }

    // 停止转动
    fun stop() {
        isRunning = false
    }

    // 定义一个绘制任务，通过持续绘制实现转动效果
    private val drawRunnable: Runnable = object : Runnable {
        override fun run() {
            if (isRunning) { // 正在转动
                // 延迟70毫秒后再次启动绘制任务
                mHandler.postDelayed(this, 70)
                mBeginAngle += 3
                // 立即刷新视图，也就是调用视图的onDraw和dispatchDraw方法
                invalidate()
            } else { // 不在转动
                // 移除绘制任务
                mHandler.removeCallbacks(this)
            }
        }
    }

    init {
        mPaint = Paint() // 创建新画笔
        mPaint.isAntiAlias = true // 设置画笔为无锯齿
        mPaint.color = Color.RED // 设置画笔的颜色
        mPaint.strokeWidth = 10f // 设置画笔的线宽
        mPaint.style = Paint.Style.FILL // 设置画笔的类型。STROK表示空心，FILL表示实心
    }
}