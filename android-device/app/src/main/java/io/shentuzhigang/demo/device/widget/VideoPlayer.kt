package io.shentuzhigang.demo.device.widget

import android.content.Context
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.SurfaceView
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.ProgressBar
import io.shentuzhigang.demo.device.R
import java.util.*

class VideoPlayer @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle), OnCompletionListener,
    CompoundButton.OnCheckedChangeListener {
    private var mMediaPlayer // 声明一个媒体播放器对象
            : MediaPlayer? = null
    private val sv_play // 声明一个表面视图对象
            : SurfaceView
    private val pb_play // 声明一个进度条对象
            : ProgressBar
    private val ck_play: CheckBox
    private var mTimer // 计时器
            : Timer? = null
    private var mVideoPath // 视频文件的路径
            : String? = null
    private var isFinished = true // 是否播放结束

    // 根据SD卡的文件路径，初始化媒体播放器
    fun init(path: String?) {
        mVideoPath = path
        ck_play.isEnabled = true
        ck_play.setTextColor(Color.BLACK)
        // 创建一个媒体播放器
        mMediaPlayer = MediaPlayer()
        // 设置媒体播放器的播放完成监听器
        mMediaPlayer!!.setOnCompletionListener(this)
    }

    // 从头开始播放
    private fun play() {
        try {
            mMediaPlayer!!.reset() // 重置媒体播放器
            // 设置视频流的类型为音乐
            mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            Log.d(TAG, "video path = $mVideoPath")
            // 录制完毕要等一秒钟再setDataSource，否则会报异常“java.io.IOException: setDataSourceFD failed”
            mMediaPlayer!!.setDataSource(mVideoPath)
            // 把视频画面输出到表面视图SurfaceView
            mMediaPlayer!!.setDisplay(sv_play.holder)
            mMediaPlayer!!.prepare() // 媒体播放器准备就绪
            mMediaPlayer!!.start() // 媒体播放器开始播放
            // 设置进度条的最大值，也就是媒体的播放时长
            pb_play.max = mMediaPlayer!!.duration
            mTimer = Timer() // 创建一个计时器
            // 计时器每隔一秒就更新进度条上的播放进度
            mTimer!!.schedule(object : TimerTask() {
                override fun run() {
                    pb_play.progress = mMediaPlayer!!.currentPosition
                }
            }, 0, 1000)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 一旦发现媒体播放完毕，就触发播放完成监听器的onCompletion方法
    override fun onCompletion(mp: MediaPlayer) {
        isFinished = true
        pb_play.progress = 100
        ck_play.isChecked = false
        if (mTimer != null) {
            mTimer!!.cancel() // 取消计时器
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_play) {
            if (isChecked) { // 开始播放
                ck_play.text = "暂停播放"
                if (isFinished) {
                    play() // 重新播放
                } else {
                    mMediaPlayer!!.start() // 媒体播放器恢复播放
                }
                isFinished = false
            } else { // 暂停播放
                ck_play.text = "开始播放"
                mMediaPlayer!!.pause() // 媒体播放器暂停播放
            }
        }
    }

    companion object {
        private const val TAG = "VideoPlayer"
    }

    init {
        // 从布局文件video_player.xml生成当前的布局视图
        LayoutInflater.from(context).inflate(R.layout.video_player, this)
        // 从布局文件中获取名叫sv_play的表面视图
        sv_play = findViewById(R.id.sv_play)
        // 从布局文件中获取名叫pb_play的进度条
        pb_play = findViewById(R.id.pb_play)
        ck_play = findViewById(R.id.ck_play)
        ck_play.setOnCheckedChangeListener(this)
    }
}