package io.shentuzhigang.demo.fangdaijisuanqi

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import io.shentuzhigang.demo.fangdaijisuanqi.databinding.ActivityMainBinding
import io.shentuzhigang.demo.fangdaijisuanqi.util.TextUtil


class MainActivity : AppCompatActivity() {
    var binding: ActivityMainBinding? = null

    //声明用到的所有控件
    var spinner1: Spinner? = null
    var spinner2: Spinner? = null
    var spinner3: Spinner? = null
    var spinner4: Spinner? = null
    var row1edit: EditText? = null
    var row2edit: EditText? = null
    var radioGroup: RadioGroup? = null
    var checkBox1: CheckBox? = null
    var checkBox2: CheckBox? = null
    var row4edit: EditText? = null
    var row5edit: EditText? = null
    var detail: Button? = null
    var alldetail: TextView? = null
    private fun initSpinner() {
        //建立数据源
        val years: Array<String> = getResources().getStringArray(R.array.years)
        //声明一个下拉列表的数组适配器并绑定数据源
        val yearAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, years)
        //绑定Adapter到控件
        spinner1!!.adapter = yearAdapter
        //设置默认选择第一项
        spinner1!!.setSelection(0)
        //设置标题
        spinner1!!.prompt = "请选择商贷贷款年限"

        val oldbaseRates: Array<String> = getResources().getStringArray(R.array.old_base_rate)
        val oldbaseRatesAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, oldbaseRates)
        spinner2!!.adapter = oldbaseRatesAdapter
        spinner2!!.setSelection(0)
        spinner2!!.prompt = "请选择商贷基准利率"

        spinner3!!.adapter = yearAdapter
        spinner3!!.setSelection(0)
        spinner3!!.prompt = "请选择公积金贷款年限"

        val baserates: Array<String> = getResources().getStringArray(R.array.base_rate)
        val baserateAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, baserates)
        spinner4!!.adapter = baserateAdapter
        spinner4!!.setSelection(0)
        spinner4!!.prompt = "请选择公积金基准利率"
    }

    //声明下列函数中要用到的变量
    var buyTotal //购房总额
            : String? = null
    var percent //贷款百分比
            : String? = null
    var inTotal = 0.0 // 贷款总额
    var outTotal = 0.0
    var backTotal = 0.0 // 还款总额
    var month //月份
            = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //初始化控件
        spinner1 = binding?.sp1
        spinner2 = binding?.sp2
        spinner3 = binding?.sp3
        spinner4 = binding?.sp4
        row1edit = binding?.row1edit
        row2edit = binding?.row2edit
        radioGroup = binding?.radiogroup
        checkBox1 = binding?.check1
        checkBox2 = binding?.check2
        detail = binding?.detail
        alldetail = binding?.alldetail
        row4edit = binding?.row4label
        row5edit = binding?.row5label
        initSpinner()

        row1edit!!.addTextChangedListener(NumberTextWatcher(row1edit!!))
        row2edit!!.addTextChangedListener(PercentageTextWatcher(row2edit!!))
        row4edit!!.addTextChangedListener(NumberTextWatcher(row4edit!!))
        row5edit!!.addTextChangedListener(NumberTextWatcher(row5edit!!))

        //点击软键盘外部，收起软键盘
        row1edit?.setOnFocusChangeListener { view, hasFocus ->
            if (!hasFocus) {
                val manager =
                    this@MainActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                manager.hideSoftInputFromWindow(
                    view.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        }

        detail!!.setOnClickListener {
            val manager =
                this@MainActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(
                it.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            buyTotal = row1edit!!.text.toString()
            percent = row2edit!!.text.toString()
            //判断前两个输入框是否非空
            if (TextUtils.isEmpty(buyTotal) || TextUtils.isEmpty(percent)) {
                Toast.makeText(getApplicationContext(), "购房总价和按揭部分信息填写完整", Toast.LENGTH_LONG).show()
            } else if (!TextUtil.isNum(buyTotal!!) || !TextUtil.isNum(percent!!)) { //判断输入的是否是数字
                Toast.makeText(
                    this@MainActivity.applicationContext,
                    "包含不合法的输入信息",
                    Toast.LENGTH_LONG
                ).show()
            } else if (percent!!.toDouble() > 100) { //判断百分比部分输入是否大于100%
                Toast.makeText(
                    this@MainActivity.applicationContext,
                    "按揭部分不能超过100%",
                    Toast.LENGTH_LONG
                ).show()
            } else if (TextUtil.isNum(buyTotal!!) && TextUtil.isNum(percent!!)) {
                inTotal = buyTotal!!.toDouble() * percent!!.toDouble() * 0.01
            }

            if (checkBox1!!.isChecked == false && checkBox2!!.isChecked == false) {
                //监听勾选的多选框
                Toast.makeText(this@MainActivity, "请勾选贷款种类", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            val first = row4edit!!.text.toString()
            val second = row5edit!!.text.toString()

            outTotal = 0.0
            backTotal = 0.0
            val array = ArrayList<Double>()
            if (radioGroup!!.checkedRadioButtonId == R.id.btn1) {
                // 等额本息贷款算法
                if (checkBox1!!.isChecked) {
                    // 商业贷款
                    if (first == "请输入商业贷款总额（单位万）") {
                        Toast.makeText(this@MainActivity, "请将空信息填写完整", Toast.LENGTH_LONG).show()
                    }
                    if (!TextUtil.isNum(first)) {
                        Toast.makeText(this@MainActivity, "包含不合法的输入信息", Toast.LENGTH_LONG).show()
                    }
                    val firstTotal = first.toDouble()
                    val firstYear = spinner1!!.selectedItem.toString()
                    val firstMonth = firstYear.substring(0, firstYear.length - 1).toInt() * 12
                    val firstRate =
                        spinner2!!.selectedItem.toString().substring(20, 24).toDouble() * 0.01
                    val firstMonthRate = firstRate / 12
                    outTotal += firstTotal
                    val perTime = firstTotal * firstMonthRate * Math.pow(
                        1 + firstMonthRate,
                        firstMonth.toDouble()
                    ) /
                            (Math.pow(1 + firstMonthRate, firstMonth.toDouble()) - 1)
                    for (i in 0 until firstMonth) {
                        if (array.size < i + 1) {
                            array.add(perTime)
                        } else {
                            array.set(i, array.get(i) + perTime)
                        }
                    }
                    backTotal += perTime * firstMonth
                }

                if (checkBox2!!.isChecked) {
                    // 公积金贷款
                    if (second == "请输入公积金贷款总额（单位万）") {
                        Toast.makeText(this@MainActivity, "请将空信息填写完整", Toast.LENGTH_LONG).show()
                    }
                    if (!TextUtil.isNum(second)) {
                        Toast.makeText(this@MainActivity, "包含不合法的输入信息", Toast.LENGTH_LONG).show()
                    }
                    val secondTotal = second.toDouble()
                    val secondYear = spinner1!!.selectedItem.toString()
                    val secondMonth = secondYear.substring(0, secondYear.length - 1).toInt() * 12
                    val secondRate =
                        spinner4!!.selectedItem.toString().substring(17, 21).toDouble() * 0.01
                    val secondMonthRate = secondRate / 12
                    outTotal += secondTotal
                    val perTime = secondTotal * secondMonthRate * Math.pow(
                        1 + secondMonthRate,
                        secondMonth.toDouble()
                    ) /
                            (Math.pow(1 + secondMonthRate, secondMonth.toDouble()) - 1)
                    for (i in 0 until secondMonth) {
                        if (array.size < i + 1) {
                            array.add(perTime)
                        } else {
                            array.set(i, array.get(i) + perTime)
                        }
                    }
                    backTotal += perTime * secondMonth
                }
            } else if (radioGroup!!.checkedRadioButtonId == R.id.btn2) {
                // 等额本金贷款算法
                if (checkBox1!!.isChecked) {
                    // 商业贷款
                    if (first == "请输入商业贷款总额（单位万）") {
                        Toast.makeText(this@MainActivity, "请将空信息填写完整", Toast.LENGTH_LONG).show()
                    }
                    if (!TextUtil.isNum(first)) {
                        Toast.makeText(this@MainActivity, "包含不合法的输入信息", Toast.LENGTH_LONG).show()
                    }
                    val firstTotal = first.toDouble()
                    val firstYear = spinner1!!.selectedItem.toString()
                    val firstMonth = firstYear.substring(0, firstYear.length - 1).toInt() * 12
                    val firstRate =
                        spinner2!!.selectedItem.toString().substring(20, 24).toDouble() * 0.01
                    val firstMonthRate = firstRate / 12
                    outTotal += firstTotal
                    var sum = 0.0
                    for (i in 0 until firstMonth) {
                        val money = firstTotal / firstMonth + (firstTotal - sum) * firstMonthRate
                        if (array.size < i + 1) {
                            array.add(money)
                        } else {
                            array.set(i, array.get(i) + money)
                        }
                        backTotal += array[i]
                        sum += array[i]
                    }
                }

                if (checkBox2!!.isChecked) {
                    // 公积金贷款
                    if (second == "请输入公积金贷款总额（单位万）") {
                        Toast.makeText(this@MainActivity, "请将空信息填写完整", Toast.LENGTH_LONG).show()
                    }
                    if (!TextUtil.isNum(second)) {
                        Toast.makeText(this@MainActivity, "包含不合法的输入信息", Toast.LENGTH_LONG).show()
                    }
                    val secondTotal = second.toDouble()
                    val secondYear = spinner1!!.selectedItem.toString()
                    val secondMonth = secondYear.substring(0, secondYear.length - 1).toInt() * 12
                    val secondRate =
                        spinner4!!.selectedItem.toString().substring(17, 21).toDouble() * 0.01
                    val secondMonthRate = secondRate / 12
                    outTotal += secondTotal
                    var sum = 0.0
                    for (i in 0 until secondMonth) {
                        val money =
                            secondTotal / secondMonth + (secondTotal - sum) * secondMonthRate
                        if (array.size < i + 1) {
                            array.add(money)
                        } else {
                            array.set(i, array.get(i) + money)
                        }
                        backTotal += array[i]
                        sum += array[i]
                    }
                }
            }
            if (String.format("%.2f", outTotal).toDouble() != String.format("%.2f", inTotal)
                    .toDouble()
            ) {
                Toast.makeText(this@MainActivity, "填写的两项贷款总额不等于初始贷款额度，请重新填写", Toast.LENGTH_LONG)
                    .show()
            }
            var perMonth = "每月还款金额如下：\n"
            for (i in 0 until array.size) {
                perMonth += String.format("第%d个月应还金额为：%.2f\n", i + 1, array[i] * 10000)
            }
            alldetail!!.text = String.format(
                "您的贷款总额为%.2f万元\n还款总额为%.2f万元\n其中利息总额为%.2f万元\n还款总时间为%d月\n%s",
                inTotal,
                backTotal,
                backTotal - inTotal,
                array.size,
                perMonth
            )
        }
    }
}

class PercentageTextWatcher(editText: EditText) : TextWatcher {
    private val editText: EditText

    init {
        this.editText = editText
    }

    private var oldValue = ""
    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        val length = charSequence.length
        var text: String = charSequence.toString()
        if (length > oldValue.length) {
            val newchar = charSequence[i]
            if (newchar < '0' && newchar > '9' && newchar != '.') {
                if (i != length - 1) {
                    text = oldValue
                }
            }
        }

        try {
            val value = text.toDouble()
            if (value >= 100) {
                text = oldValue
            } else if (text.indexOf(".") == -1 || text.indexOf(".") == 0) {
                text = String.format("%.0f", value)
            }
        } catch (e: NumberFormatException) {
            text = oldValue
        }
        if (!charSequence.toString().equals(text)) {
            this.editText.setText(text)
        }
        oldValue = text
    }

    override fun afterTextChanged(editable: Editable) {}
}

class NumberTextWatcher(editText: EditText) : TextWatcher {
    private val editText: EditText

    init {
        this.editText = editText
    }

    var oldlength = 0
    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        val length = charSequence.length
        if (length > oldlength) {
            val newchar = charSequence[i]
            if (newchar < '0' && newchar > '9' && newchar != '.') {
                if (i != length - 1) {
                    val text = charSequence.subSequence(0, i).toString() +
                            charSequence.subSequence(i + 1, length).toString()
                    this.editText.setText(text)
                } else editText.setText(charSequence.subSequence(0, length - 1))
            }
        }
        oldlength = length
    }

    override fun afterTextChanged(editable: Editable) {}
}
