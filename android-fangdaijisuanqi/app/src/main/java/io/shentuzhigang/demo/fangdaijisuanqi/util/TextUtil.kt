package io.shentuzhigang.demo.fangdaijisuanqi.util

class TextUtil {
    companion object {
        fun isNum(string: String): Boolean {
            var flag = 0
            if (string[0] == '0' && string[1] != '.') return false
            if (string[0] == '.') return false
            for (i in 0 until string.length) {
                if ((string[i] < '0' || string[i] > '9') && string[i] != '.') return false else if (string[i] == '.') {
                    flag++
                    if (flag > 1) return false
                }
            }
            return true
        }
    }
}
