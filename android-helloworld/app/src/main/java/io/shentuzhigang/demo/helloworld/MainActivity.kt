package io.shentuzhigang.demo.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.e("ActivityLifeLog","onCreate")
    }

    override fun onStart(){
        super.onStart()
        Log.e("ActivityLifeLog","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("ActivityLifeLog","onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("ActivityLifeLog","onPause")
    }

    override fun onStop(){
        super.onStop()
        Log.e("ActivityLifeLog","onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("ActivityLifeLog","onDestroy")
    }

    override fun onRestart(){
        super.onRestart()
        Log.e("ActivityLifeLog","onRestart")
    }
}