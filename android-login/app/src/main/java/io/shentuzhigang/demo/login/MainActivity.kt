package io.shentuzhigang.demo.login

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import io.shentuzhigang.demo.login.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    var binding: ActivityMainBinding? = null

    //声明用到的所有控件
    var spinner1: Spinner? = null

    private fun initSpinner() {
        //建立数据源
        val userTypes: Array<String> = getResources().getStringArray(R.array.user_type)
        //声明一个下拉列表的数组适配器并绑定数据源
        val userTypeAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, userTypes)
        //绑定Adapter到控件
        spinner1!!.adapter = userTypeAdapter
        //设置默认选择第一项
        spinner1!!.setSelection(0)
        //设置标题
        spinner1!!.prompt = "请选择用户类型"

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //初始化控件
        spinner1 = binding?.sp1

        initSpinner()

        binding?.btnForgotPassword?.setOnClickListener {
            startActivity(Intent(this@MainActivity, ForgotPasswordActivity::class.java))
        }

        binding?.btnLogin?.setOnClickListener {
            val normalDialog = AlertDialog.Builder(this@MainActivity);
            normalDialog.setTitle("登录成功")
            normalDialog.setMessage(binding!!.editPhone.text.toString() + "登录成功");
            normalDialog.setPositiveButton("确定返回", { dialog, which->
                //...Todo
            })
            normalDialog.setNegativeButton("我再看看",{ dialog, which->
                //...Todo
            });
            // 显示
            normalDialog.show();
        }
    }
}