package io.shentuzhigang.demo.network

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.widget.TextView
import com.alibaba.fastjson.JSON
import okhttp3.OkHttpClient
import okhttp3.Request

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        if (android.os.Build.VERSION.SDK_INT > 9) {
           val policy = StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        val str = run("http://www.weather.com.cn/data/sk/101210101.html")

        val json = JSON.parseObject(str).getJSONObject("weatherinfo")

        val textView = findViewById<TextView>(R.id.textView)

        textView.text = String.format("城市：%s\n风向：%s\n风力：%s\n湿度：%s\nAP：%s\n",
            json.getString("city"),
            json.getString("WD"),
            json.getString("WS"),
            json.getString("SD"),
            json.getString("AP"),
        )
    }

    var client = OkHttpClient()

    fun run(url:String): String? {
        val request = Request.Builder()
            .url(url)
            .build();
        val response = client.newCall(request).execute()
        return response.body?.string();
    }
}