package io.shentuzhigang.demo.shopping

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import java.util.*

/**
 * Created by ouyangshen on 2017/10/1.
 */
class MainApplication : Application() {
    // 声明一个公共的信息映射对象，可当作全局变量使用
    var mInfoMap = HashMap<String, String>()
    override fun onCreate() {
        super.onCreate()
        // 在打开应用时对静态的应用实例赋值
        instance = this
        Log.d(TAG, "onCreate")
    }

    override fun onTerminate() {
        Log.d(TAG, "onTerminate")
        super.onTerminate()
    }

    // 声明一个公共的图标映射对象，
    var mIconMap = HashMap<Long, Bitmap>()

    companion object {
        private const val TAG = "MainApplication"

        // 利用单例模式获取当前应用的唯一实例
        // 声明一个当前应用的静态实例
        var instance: MainApplication? = null
            private set
    }
}