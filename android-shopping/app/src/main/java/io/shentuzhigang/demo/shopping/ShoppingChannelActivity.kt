package io.shentuzhigang.demo.shopping

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.*
import android.widget.ImageView.ScaleType
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.shopping.MainApplication
import io.shentuzhigang.demo.shopping.bean.CartInfo
import io.shentuzhigang.demo.shopping.bean.GoodsInfo
import io.shentuzhigang.demo.shopping.database.CartDBHelper
import io.shentuzhigang.demo.shopping.database.GoodsDBHelper
import io.shentuzhigang.demo.shopping.util.DateUtil
import io.shentuzhigang.demo.shopping.util.SharedUtil
import io.shentuzhigang.demo.shopping.util.Utils
import java.util.*

/**
 * Created by ouyangshen on 2017/10/1.
 */
@SuppressLint("SetTextI18n")
class ShoppingChannelActivity : AppCompatActivity(), View.OnClickListener {
    private var tv_count: TextView? = null
    private var ll_channel: LinearLayout? = null
    private var mCount // 购物车中的商品数量
            = 0
    private var mGoodsHelper // 声明一个商品数据库的帮助器对象
            : GoodsDBHelper? = null
    private var mCartHelper // 声明一个购物车数据库的帮助器对象
            : CartDBHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_channel)
        val tv_title: TextView = findViewById(R.id.tv_title)
        tv_count = findViewById(R.id.tv_count)
        ll_channel = findViewById(R.id.ll_channel)
        val ivCart:ImageView= findViewById(R.id.iv_cart)
        ivCart.setOnClickListener(this)
        tv_title.setText("手机商场")
    }

    override fun onClick(v: View) {
        if (v.getId() == R.id.iv_cart) { // 点击了购物车图标
            // 跳转到购物车页面
            val intent = Intent(this, ShoppingCartActivity::class.java)
            startActivity(intent)
        }
    }

    // 把指定编号的商品添加到购物车
    private fun addToCart(goods_id: Long) {
        mCount++
        tv_count?.setText("" + mCount)
        // 把购物车中的商品数量写入共享参数
        SharedUtil.getIntance(this)?.writeShared("count", "" + mCount)
        // 根据商品编号查询购物车数据库中的商品记录
        var info: CartInfo? = mCartHelper?.queryByGoodsId(goods_id)
        if (info != null) { // 购物车已存在该商品记录
            info.count++ // 该商品的数量加一
            info.update_time = DateUtil.getNowDateTime("")
            // 更新购物车数据库中的商品记录信息
            mCartHelper?.update(info)
        } else { // 购物车不存在该商品记录
            info = CartInfo()
            info.goods_id = goods_id
            info.count = 1
            info.update_time = DateUtil.getNowDateTime("")
            // 往购物车数据库中添加一条新的商品记录
            mCartHelper?.insert(info)
        }
    }

    protected override fun onResume() {
        super.onResume()
        // 获取共享参数保存的购物车中的商品数量
        mCount = (SharedUtil.getIntance(this)?.readShared("count", "0")?.toInt() ?: tv_count?.setText("" + mCount)) as Int
        // 获取商品数据库的帮助器对象
        mGoodsHelper = GoodsDBHelper.Companion.getInstance(this, 1)
        // 打开商品数据库的读连接
        mGoodsHelper?.openReadLink()
        // 获取购物车数据库的帮助器对象
        mCartHelper = CartDBHelper.Companion.getInstance(this, 1)
        // 打开购物车数据库的写连接
        mCartHelper?.openWriteLink()
        // 展示商品列表
        showGoods()
    }

    protected override fun onPause() {
        super.onPause()
        // 关闭商品数据库的数据库连接
        mGoodsHelper?.closeLink()
        // 关闭购物车数据库的数据库连接
        mCartHelper?.closeLink()
    }

    private var mFullParams: LinearLayout.LayoutParams? = null
    private var mHalfParams: LinearLayout.LayoutParams? = null
    private fun showGoods() {
        // 移除线性布局ll_channel下面的所有子视图
        ll_channel?.removeAllViews()
        // mFullParams这个布局参数的宽度占了一整行
        mFullParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        // mHalfParams这个布局参数的宽度与其它布局平均分
        mHalfParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1F)
        // 给mHalfParams设置四周的空白距离
        mHalfParams!!.setMargins(
            Utils.dip2px(this, 2f),
            Utils.dip2px(this, 2f),
            Utils.dip2px(this, 2f),
            Utils.dip2px(this, 2f)
        )
        // 创建一行的线性布局
        var ll_row: LinearLayout = newLinearLayout(LinearLayout.HORIZONTAL, 0)
        // 查询商品数据库中的所有商品记录
        val goodsArray: ArrayList<GoodsInfo>? = mGoodsHelper?.query("1=1")
        var i = 0
        if (goodsArray != null) {
            while (i < goodsArray.size) {
                val info: GoodsInfo = goodsArray[i]
                // 创建一个商品项的垂直线性布局，从上到下依次列出商品标题、商品图片、商品价格
                val ll_goods: LinearLayout = newLinearLayout(LinearLayout.VERTICAL, 1)
                ll_goods.setBackgroundColor(Color.WHITE)
                // 添加商品标题
                val tv_name = TextView(this)
                tv_name.setLayoutParams(mFullParams)
                tv_name.setGravity(Gravity.CENTER)
                tv_name.setText(info.name)
                tv_name.setTextColor(Color.BLACK)
                tv_name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)
                ll_goods.addView(tv_name)
                // 添加商品小图
                val iv_thumb = ImageView(this)
                iv_thumb.setLayoutParams(
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, Utils.dip2px(this, 150f)
                    )
                )
                iv_thumb.setScaleType(ScaleType.FIT_CENTER)
                iv_thumb.setImageBitmap(MainApplication.instance?.mIconMap?.get(info.rowid))
                iv_thumb.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        val intent =
                            Intent(this@ShoppingChannelActivity, ShoppingDetailActivity::class.java)
                        intent.putExtra("goods_id", info.rowid)
                        startActivity(intent)
                    }
                })
                ll_goods.addView(iv_thumb)
                // 添加商品价格
                val ll_bottom: LinearLayout = newLinearLayout(LinearLayout.HORIZONTAL, 0)
                val tv_price = TextView(this)
                tv_price.setLayoutParams(
                    LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        2F
                    )
                )
                tv_price.setGravity(Gravity.CENTER)
                tv_price.setText("" + info.price.toInt())
                tv_price.setTextColor(Color.RED)
                tv_price.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                ll_bottom.addView(tv_price)
                // 添加购物车按钮
                val btn_add = Button(this)
                btn_add.setLayoutParams(
                    LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        3F
                    )
                )
                btn_add.setGravity(Gravity.CENTER)
                btn_add.setText("加入购物车")
                btn_add.setTextColor(Color.BLACK)
                btn_add.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                btn_add.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        addToCart(info.rowid)
                        Toast.makeText(
                            this@ShoppingChannelActivity,
                            "已添加一部" + info.name + "到购物车", Toast.LENGTH_SHORT
                        ).show()
                    }
                })
                ll_bottom.addView(btn_add)
                ll_goods.addView(ll_bottom)
                // 把商品项添加到该行上
                ll_row.addView(ll_goods)
                // 每行放两个商品项，所以放满两个商品后，就要重新创建下一行的线性视图
                if (i % 2 == 1) {
                    ll_channel?.addView(ll_row)
                    ll_row = newLinearLayout(LinearLayout.HORIZONTAL, 0)
                }
                i++
            }
        }
        // 最后一行只有一个商品项，则补上一个空白格，然后把最后一行添加到ll_channel
        if (i % 2 == 0) {
            ll_row.addView(newLinearLayout(LinearLayout.VERTICAL, 1))
            ll_channel?.addView(ll_row)
        }
    }

    // 创建一个线性视图的框架
    private fun newLinearLayout(orientation: Int, weight: Int): LinearLayout {
        val ll_new = LinearLayout(this)
        ll_new.setLayoutParams(if (weight == 0) mFullParams else mHalfParams)
        ll_new.setOrientation(orientation)
        return ll_new
    }
}