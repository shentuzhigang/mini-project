package io.shentuzhigang.demo.shopping.bean

class CallRecord {
    var name = ""
    var phone = ""
    var type = 0
    var date = ""
    var duration: Long = 0
    var _new = 0
}