package io.shentuzhigang.demo.shopping.bean

class UserInfo {
    var rowid = 0L
    var xuhao = 0
    var name: String? = ""
    var age = 0
    var height = 0L
    var weight = 0.0f
    var married = false
    var update_time = ""
    var phone: String? = ""
    var password = ""
}