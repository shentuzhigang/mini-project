package io.shentuzhigang.demo.shopping.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import io.shentuzhigang.demo.shopping.bean.UserInfo
import java.util.*

@SuppressLint("DefaultLocale")
class UserDBHelper : SQLiteOpenHelper {
    private var mDB: SQLiteDatabase? = null // 数据库的实例

    private constructor(context: Context?) : super(context, DB_NAME, null, DB_VERSION) {}
    private constructor(context: Context?, version: Int) : super(context, DB_NAME, null, version) {}

    // 打开数据库的读连接
    fun openReadLink(): SQLiteDatabase? {
        if (mDB == null || !mDB!!.isOpen) {
            mDB = mHelper!!.readableDatabase
        }
        return mDB
    }

    // 打开数据库的写连接
    fun openWriteLink(): SQLiteDatabase? {
        if (mDB == null || !mDB!!.isOpen) {
            mDB = mHelper!!.writableDatabase
        }
        return mDB
    }

    // 关闭数据库连接
    fun closeLink() {
        if (mDB != null && mDB!!.isOpen) {
            mDB!!.close()
            mDB = null
        }
    }

    // 创建数据库，执行建表语句
    override fun onCreate(db: SQLiteDatabase) {
        Log.d(TAG, "onCreate")
        val drop_sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";"
        Log.d(TAG, "drop_sql:$drop_sql")
        db.execSQL(drop_sql)
        val create_sql = ("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + "_id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,"
                + "name VARCHAR NOT NULL," + "age INTEGER NOT NULL,"
                + "height LONG NOT NULL," + "weight FLOAT NOT NULL,"
                + "married INTEGER NOT NULL," + "update_time VARCHAR NOT NULL" //演示数据库升级时要先把下面这行注释
                + ",phone VARCHAR" + ",password VARCHAR"
                + ");")
        Log.d(TAG, "create_sql:$create_sql")
        db.execSQL(create_sql)
    }

    // 修改数据库，执行表结构变更语句
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        Log.d(TAG, "onUpgrade oldVersion=$oldVersion, newVersion=$newVersion")
        if (newVersion > 1) {
            //Android的ALTER命令不支持一次添加多列，只能分多次添加
            var alter_sql = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + "phone VARCHAR;"
            Log.d(TAG, "alter_sql:$alter_sql")
            db.execSQL(alter_sql)
            alter_sql = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + "password VARCHAR;"
            Log.d(TAG, "alter_sql:$alter_sql")
            db.execSQL(alter_sql)
        }
    }

    // 根据指定条件删除表记录
    fun delete(condition: String?): Int {
        // 执行删除记录动作，该语句返回删除记录的数目
        return mDB!!.delete(TABLE_NAME, condition, null)
    }

    // 删除该表的所有记录
    fun deleteAll(): Int {
        // 执行删除记录动作，该语句返回删除记录的数目
        return mDB!!.delete(TABLE_NAME, "1=1", null)
    }

    // 往该表添加一条记录
    fun insert(info: UserInfo): Long {
        val infoArray: ArrayList<UserInfo> = ArrayList<UserInfo>()
        infoArray.add(info)
        return insert(infoArray)
    }

    // 往该表添加多条记录
    fun insert(infoArray: ArrayList<UserInfo>): Long {
        var result: Long = -1
        for (i in infoArray.indices) {
            val info: UserInfo = infoArray[i]
            var tempArray: ArrayList<UserInfo> = ArrayList<UserInfo>()
            // 如果存在同名记录，则更新记录
            // 注意条件语句的等号后面要用单引号括起来
            if (info.name != null && info.name!!.length > 0) {
                val condition = String.format("name='%s'", info.name)
                tempArray = query(condition)
                if (tempArray.size > 0) {
                    update(info, condition)
                    result = tempArray[0].rowid
                    continue
                }
            }
            // 如果存在同样的手机号码，则更新记录
            if (info.phone != null && info.phone!!.length > 0) {
                val condition = String.format("phone='%s'", info.phone)
                tempArray = query(condition)
                if (tempArray.size > 0) {
                    update(info, condition)
                    result = tempArray[0].rowid
                    continue
                }
            }
            // 不存在唯一性重复的记录，则插入新记录
            val cv = ContentValues()
            cv.put("name", info.name)
            cv.put("age", info.age)
            cv.put("height", info.height)
            cv.put("weight", info.weight)
            cv.put("married", info.married)
            cv.put("update_time", info.update_time)
            cv.put("phone", info.phone)
            cv.put("password", info.password)
            // 执行插入记录动作，该语句返回插入记录的行号
            result = mDB!!.insert(TABLE_NAME, "", cv)
            // 添加成功后返回行号，失败后返回-1
            if (result == -1L) {
                return result
            }
        }
        return result
    }

    // 根据条件更新指定的表记录
    @JvmOverloads
    fun update(info: UserInfo, condition: String? = "rowid=" + info.rowid): Int {
        val cv = ContentValues()
        cv.put("name", info.name)
        cv.put("age", info.age)
        cv.put("height", info.height)
        cv.put("weight", info.weight)
        cv.put("married", info.married)
        cv.put("update_time", info.update_time)
        cv.put("phone", info.phone)
        cv.put("password", info.password)
        // 执行更新记录动作，该语句返回记录更新的数目
        return mDB!!.update(TABLE_NAME, cv, condition, null)
    }

    // 根据指定条件查询记录，并返回结果数据队列
    fun query(condition: String?): ArrayList<UserInfo> {
        val sql = String.format(
            "select rowid,_id,name,age,height,weight,married,update_time," +
                    "phone,password from %s where %s;", TABLE_NAME, condition
        )
        Log.d(TAG, "query sql: $sql")
        val infoArray: ArrayList<UserInfo> = ArrayList<UserInfo>()
        // 执行记录查询动作，该语句返回结果集的游标
        val cursor = mDB!!.rawQuery(sql, null)
        // 循环取出游标指向的每条记录
        while (cursor.moveToNext()) {
            val info = UserInfo()
            info.rowid = cursor.getLong(0) // 取出长整型数
            info.xuhao = cursor.getInt(1) // 取出整型数
            info.name = cursor.getString(2) // 取出字符串
            info.age = cursor.getInt(3)
            info.height = cursor.getLong(4)
            info.weight = cursor.getFloat(5) // 取出浮点数
            //SQLite没有布尔型，用0表示false，用1表示true
            info.married = if (cursor.getInt(6) == 0) false else true
            info.update_time = cursor.getString(7)
            info.phone = cursor.getString(8)
            info.password = cursor.getString(9)
            infoArray.add(info)
        }
        cursor.close() // 查询完毕，关闭游标
        return infoArray
    }

    // 根据手机号码查询指定记录
    fun queryByPhone(phone: String?): UserInfo? {
        var info: UserInfo? = null
        val infoArray: ArrayList<UserInfo> = query(String.format("phone='%s'", phone))
        if (infoArray.size > 0) {
            info = infoArray[0]
        }
        return info
    }

    companion object {
        private const val TAG = "UserDBHelper"
        private const val DB_NAME = "user.db" // 数据库的名称
        private const val DB_VERSION = 1 // 数据库的版本号
        private var mHelper: UserDBHelper? = null // 数据库帮助器的实例
        const val TABLE_NAME = "user_info" // 表的名称

        // 利用单例模式获取数据库帮助器的唯一实例
        fun getInstance(context: Context?, version: Int): UserDBHelper? {
            if (version > 0 && mHelper == null) {
                mHelper = UserDBHelper(context, version)
            } else if (mHelper == null) {
                mHelper = UserDBHelper(context)
            }
            return mHelper
        }
    }
}