package io.shentuzhigang.demo.shopping.provider

import android.net.Uri
import android.provider.BaseColumns
import io.shentuzhigang.demo.shopping.database.UserDBHelper

object UserInfoContent : BaseColumns {
    // 这里的名称必须与AndroidManifest.xml里的android:authorities保持一致
    const val AUTHORITIES = "io.shentuzhigang.demo.shopping.provider.UserInfoProvider"

    // 表名
    val TABLE_NAME: String = UserDBHelper.Companion.TABLE_NAME

    // 访问该内容提供器的URI
    val CONTENT_URI = Uri.parse("content://" + AUTHORITIES + "/user")

    //	// 该内容提供器返回的数据类型定义
    //	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.myprovider.user";
    //	public static final String CONTENT_TYPE_ITEM = "vnd.android.cursor.item/vnd.myprovider.user";
    // 下面是该表的各个字段名称
    const val USER_NAME = "name"
    const val USER_AGE = "age"
    const val USER_HEIGHT = "height"
    const val USER_WEIGHT = "weight"
    const val USER_MARRIED = "married"

    // 默认的排序方法
    const val DEFAULT_SORT_ORDER = "_id desc"
}