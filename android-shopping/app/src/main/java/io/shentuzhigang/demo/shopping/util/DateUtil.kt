package io.shentuzhigang.demo.shopping.util

import android.annotation.SuppressLint
import android.text.TextUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ouyangshen on 2017/9/24.
 */
@SuppressLint("SimpleDateFormat")
object DateUtil {
    fun getNowDateTime(formatStr: String?): String {
        var format = formatStr
        if (TextUtils.isEmpty(format)) {
            format = "yyyyMMddHHmmss"
        }
        val sdf = SimpleDateFormat(format)
        return sdf.format(Date())
    }

    val nowTime: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss")
            return sdf.format(Date())
        }
    val nowTimeDetail: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss.SSS")
            return sdf.format(Date())
        }

    fun formatDate(time: Long): String {
        val date = Date(time)
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return sdf.format(date)
    }
}