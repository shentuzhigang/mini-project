package io.shentuzhigang.demo.shopping.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.*
import java.util.*

object FileUtil {
    // 把字符串保存到指定路径的文本文件
    fun saveText(path: String?, txt: String) {
        try {
            // 根据指定文件路径构建文件输出流对象
            val fos = FileOutputStream(path)
            // 把字符串写入文件输出流
            fos.write(txt.toByteArray())
            // 关闭文件输出流
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 从指定路径的文本文件中读取内容字符串
    fun openText(path: String?): String {
        var readStr = ""
        try {
            // 根据指定文件路径构建文件输入流对象
            val fis = FileInputStream(path)
            val b = ByteArray(fis.available())
            // 从文件输入流读取字节数组
            fis.read(b)
            // 把字节数组转换为字符串
            readStr = String(b)
            // 关闭文件输入流
            fis.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // 返回文本文件中的文本字符串
        return readStr
    }

    // 把位图数据保存到指定路径的图片文件
    fun saveImage(path: String?, bitmap: Bitmap) {
        try {
            // 根据指定文件路径构建缓存输出流对象
            val bos = BufferedOutputStream(FileOutputStream(path))
            // 把位图数据压缩到缓存输出流中
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos)
            // 完成缓存输出流的写入动作
            bos.flush()
            // 关闭缓存输出流
            bos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 从指定路径的图片文件中读取位图数据
    fun openImage(path: String?): Bitmap? {
        var bitmap: Bitmap? = null
        try {
            // 根据指定文件路径构建缓存输入流对象
            val bis = BufferedInputStream(FileInputStream(path))
            // 从缓存输入流中解码位图数据
            bitmap = BitmapFactory.decodeStream(bis)
            bis.close() // 关闭缓存输入流
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // 返回图片文件中的位图数据
        return bitmap
    }

    fun getFileList(path: String?, extendArray: Array<String>?): ArrayList<File> {
        val displayedContent = ArrayList<File>()
        var files: Array<File>? = null
        val directory = File(path)
        files = if (extendArray != null && extendArray.size > 0) {
            val fileFilter = getTypeFilter(extendArray)
            directory.listFiles(fileFilter)
        } else {
            directory.listFiles()
        }
        if (files != null) {
            for (f in files) {
                if (!f.isDirectory && !f.isHidden) {
                    displayedContent.add(f)
                }
            }
        }
        return displayedContent
    }

    fun getTypeFilter(extendArray: Array<String>): FilenameFilter {
        val fileExtensions = ArrayList<String>()
        for (i in extendArray.indices) {
            fileExtensions.add(extendArray[i])
        }
        return FilenameFilter { directory, fileName ->
            var fileName = fileName
            var matched = false
            val f = File(
                String.format(
                    "%s/%s",
                    directory.absolutePath, fileName
                )
            )
            matched = f.isDirectory
            if (!matched) {
                for (s:String in fileExtensions) {
                    var s0 = String.format(".{0,}\\%s$", s)
                    s0 = s0.uppercase(Locale.getDefault())
                    fileName = fileName.uppercase(Locale.getDefault())
                    matched = fileName.matches(Regex(s0))
                    if (matched) {
                        break
                    }
                }
            }
            matched
        }
    }
}