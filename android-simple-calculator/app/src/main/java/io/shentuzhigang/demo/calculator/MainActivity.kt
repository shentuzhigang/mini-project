package io.shentuzhigang.demo.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var textView: TextView

    private lateinit var bt_num0: Button
    private lateinit var bt_num1: Button
    private lateinit var bt_num2: Button
    private lateinit var bt_num3: Button
    private lateinit var bt_num4: Button
    private lateinit var bt_num5: Button
    private lateinit var bt_num6: Button
    private lateinit var bt_num7: Button
    private lateinit var bt_num8: Button
    private lateinit var bt_num9: Button
    private lateinit var bt_ce: Button
    private lateinit var bt_div: Button
    private lateinit var bt_mult: Button
    private lateinit var bt_minus: Button
    private lateinit var bt_plus: Button
    private lateinit var bt_equal: Button
    private lateinit var bt_dot: Button
    private lateinit var bt_clear: Button
    private lateinit var bt_sqrt: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.textView = findViewById(R.id.textView)
        this.bt_num0 = findViewById(R.id.button0)
        this.bt_num0.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num1 = findViewById(R.id.button1)
        this.bt_num1.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num2 = findViewById(R.id.button2)
        this.bt_num2.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num3 = findViewById(R.id.button3)
        this.bt_num3.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num4 = findViewById(R.id.button4)
        this.bt_num4.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num5 = findViewById(R.id.button5)
        this.bt_num5.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num6 = findViewById(R.id.button6)
        this.bt_num6.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num7 = findViewById(R.id.button7)
        this.bt_num7.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num8 = findViewById(R.id.button8)
        this.bt_num8.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_num9 = findViewById(R.id.button9)
        this.bt_num9.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_ce = findViewById(R.id.button10)
        this.bt_ce.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

            }
        })
        this.bt_div = findViewById(R.id.button11)
        this.bt_div.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_mult = findViewById(R.id.button12)
        this.bt_mult.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_clear = findViewById(R.id.button13)
        this.bt_clear.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = ""
            }
        })
        this.bt_plus = findViewById(R.id.button14)
        this.bt_plus.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_minus = findViewById(R.id.button15)
        this.bt_minus.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_sqrt = findViewById(R.id.button16)
        this.bt_sqrt.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_dot = findViewById(R.id.button17)
        this.bt_dot.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                textView.text = textView.text.toString() + (v as Button).text
            }
        })
        this.bt_equal = findViewById(R.id.button18)
        this.bt_equal.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

            }
        })
    }
}