package io.shentuzhigang.demo.taobao

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.shentuzhigang.demo.taobao.R

class AppbarNestedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appbar_nested)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_title = findViewById<Toolbar>(R.id.tl_title)
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_title)
    }
}