package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.constant.ImageList
import io.shentuzhigang.demo.taobao.util.Utils
import io.shentuzhigang.demo.taobao.widget.BannerIndicator

@SuppressLint("DefaultLocale")
class BannerIndicatorActivity : AppCompatActivity(), BannerIndicator.BannerClickListener {
    private var tv_pager: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banner_indicator)
        tv_pager = findViewById(R.id.tv_pager)
        // 从布局文件中获取名叫banner_indicator的横幅指示器
        val banner = findViewById<BannerIndicator>(R.id.banner_indicator)
        val params = banner.layoutParams as LinearLayout.LayoutParams
        params.height = (Utils.getScreenWidth(this) * 250f / 640f).toInt()
        // 设置横幅指示器的布局参数
        banner.layoutParams = params
        // 设置横幅指示器的广告图片队列
        banner.setImage(ImageList.default)
        // 设置横幅指示器的广告点击监听器
        banner.setOnBannerListener(this)
    }

    // 一旦点击了广告图，就回调监听器的onBannerClick方法
    override fun onBannerClick(position: Int) {
        val desc = String.format("您点击了第%d张图片", position + 1)
        tv_pager!!.text = desc
    }

    companion object {
        private const val TAG = "BannerIndicatorActivity"
    }
}