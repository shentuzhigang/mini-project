package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.constant.ImageList
import io.shentuzhigang.demo.taobao.util.Utils
import io.shentuzhigang.demo.taobao.widget.BannerPager

@SuppressLint("DefaultLocale")
class BannerPagerActivity : AppCompatActivity(), BannerPager.BannerClickListener {
    private var tv_pager: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banner_pager)
        tv_pager = findViewById(R.id.tv_pager)
        // 从布局文件中获取名叫banner_pager的横幅轮播条
        val banner = findViewById<BannerPager>(R.id.banner_pager)
        // 获取横幅轮播条的布局参数
        val params = banner.layoutParams as LinearLayout.LayoutParams
        params.height = (Utils.getScreenWidth(this) * 250f / 640f).toInt()
        // 设置横幅轮播条的布局参数
        banner.layoutParams = params
        // 设置横幅轮播条的广告图片队列
        banner.setImage(ImageList.default)
        // 设置横幅轮播条的广告点击监听器
        banner.setOnBannerListener(this)
        // 开始广告图片的轮播滚动
        banner.start()
    }

    // 一旦点击了广告图，就回调监听器的onBannerClick方法
    override fun onBannerClick(position: Int) {
        val desc = String.format("您点击了第%d张图片", position + 1)
        tv_pager!!.text = desc
    }

    companion object {
        private const val TAG = "BannerPagerActivity"
    }
}