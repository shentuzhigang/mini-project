package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.constant.ImageList
import io.shentuzhigang.demo.taobao.util.StatusBarUtil
import io.shentuzhigang.demo.taobao.util.Utils
import io.shentuzhigang.demo.taobao.widget.BannerPager

@SuppressLint("DefaultLocale")
class BannerTopActivity : AppCompatActivity(), View.OnClickListener,
    BannerPager.BannerClickListener {
    private var btn_top: Button? = null
    private var isOccupy = true // 是否占据了状态栏
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banner_top)
        btn_top = findViewById(R.id.btn_top)
        btn_top?.setOnClickListener(this)
        // 让当前页面全屏展示，也就是向上顶到状态栏
        StatusBarUtil.fullScreen(this)
        // 从布局文件中获取名叫banner_top的横幅轮播条
        val banner = findViewById<BannerPager>(R.id.banner_top)
        val params = banner.layoutParams as LinearLayout.LayoutParams
        params.height = (Utils.getScreenWidth(this) * 250f / 640f).toInt()
        // 设置横幅轮播条的布局参数
        banner.layoutParams = params
        // 设置横幅轮播条的广告图片队列
        banner.setImage(ImageList.default)
        // 设置横幅轮播条的广告点击监听器
        banner.setOnBannerListener(this)
        // 开始广告图片的轮播滚动
        banner.start()
    }

    // 一旦点击了广告图，就回调监听器的onBannerClick方法
    override fun onBannerClick(position: Int) {
        val desc = String.format("您点击了第%d张图片", position + 1)
        Toast.makeText(this, desc, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_top) {
            if (isOccupy) { // 已占据状态栏
                // 下挪页面内容，从而恢复状态栏
                StatusBarUtil.reset(this)
            } else { // 未占据状态栏
                // 上挪页面内容，使之占据状态栏
                StatusBarUtil.fullScreen(this)
            }
            isOccupy = !isOccupy
            btn_top!!.text = if (isOccupy) "腾出状态栏" else "霸占状态栏"
        }
    }

    companion object {
        private const val TAG = "BannerTopActivity"
    }
}