package io.shentuzhigang.demo.taobao

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import com.google.android.material.appbar.CollapsingToolbarLayout
import io.shentuzhigang.demo.taobao.adapter.RecyclerCollapseAdapter

class CollapseParallaxActivity : AppCompatActivity() {
    private val yearArray = arrayOf(
        "鼠年", "牛年", "虎年", "兔年", "龙年", "蛇年",
        "马年", "羊年", "猴年", "鸡年", "狗年", "猪年"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collapse_parallax)
        // 从布局文件中获取名叫tl_title的工具栏
        val tl_title = findViewById<Toolbar>(R.id.tl_title)
        // 设置工具栏的背景
        tl_title.setBackgroundColor(Color.RED)
        // 使用tl_title替换系统自带的ActionBar
        setSupportActionBar(tl_title)
        // 从布局文件中获取名叫ctl_title的可折叠布局
        val ctl_title = findViewById<CollapsingToolbarLayout>(R.id.ctl_title)
        // 设置可折叠布局的标题文字
        ctl_title.title = getString(R.string.toolbar_name)
        // 从布局文件中获取名叫rv_main的循环视图
        val rv_main = findViewById<RecyclerView>(R.id.rv_main)
        // 创建一个垂直方向的线性布局管理器
        val llm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        // 设置循环视图的布局管理器
        rv_main.layoutManager = llm
        // 构建一个十二生肖的线性适配器
        val adapter = RecyclerCollapseAdapter(this, yearArray)
        // 给rv_main设置十二生肖线性适配器
        rv_main.adapter = adapter
    }
}