package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.SearchViewActivity
import io.shentuzhigang.demo.taobao.adapter.RecyclerCombineAdapter
import io.shentuzhigang.demo.taobao.adapter.RecyclerGridAdapter
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.constant.ImageList
import io.shentuzhigang.demo.taobao.util.DateUtil
import io.shentuzhigang.demo.taobao.util.MenuUtil
import io.shentuzhigang.demo.taobao.util.Utils
import io.shentuzhigang.demo.taobao.widget.BannerPager
import io.shentuzhigang.demo.taobao.widget.SpacesItemDecoration

@SuppressLint("DefaultLocale")
class DepartmentHomeActivity : AppCompatActivity(), BannerPager.BannerClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_department_home)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 设置工具栏的标题文字
        tl_head.title = "商城首页"
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        initBanner() // 初始化广告轮播条
        initGrid() // 初始化市场网格列表
        initCombine() // 初始化猜你喜欢的商品展示网格
    }

    private fun initBanner() {
        // 从布局文件中获取名叫banner_pager的横幅轮播条
        val banner = findViewById<BannerPager>(R.id.banner_pager)
        // 获取横幅轮播条的布局参数
        val params = banner.layoutParams as LinearLayout.LayoutParams
        params.height = (Utils.getScreenWidth(this) * 250f / 640f).toInt()
        // 设置横幅轮播条的布局参数
        banner.layoutParams = params
        // 设置横幅轮播条的广告图片队列
        banner.setImage(ImageList.default)
        // 设置横幅轮播条的广告点击监听器
        banner.setOnBannerListener(this)
        // 开始广告图片的轮播滚动
        banner.start()
    }

    // 一旦点击了广告图，就回调监听器的onBannerClick方法
    override fun onBannerClick(position: Int) {
        val desc = String.format("您点击了第%d张图片", position + 1)
        Toast.makeText(this, desc, Toast.LENGTH_LONG).show()
    }

    private fun initGrid() {
        // 从布局文件中获取名叫rv_grid的循环视图
        val rv_grid = findViewById<RecyclerView>(R.id.rv_grid)
        // 创建一个垂直方向的网格布局管理器
        val manager = GridLayoutManager(this, 5)
        // 设置循环视图的布局管理器
        rv_grid.layoutManager = manager
        // 构建一个市场列表的网格适配器
        val adapter = RecyclerGridAdapter(this, GoodsInfo.defaultGrid)
        // 设置网格列表的点击监听器
        adapter.setOnItemClickListener(adapter)
        // 设置网格列表的长按监听器
        adapter.setOnItemLongClickListener(adapter)
        // 给rv_grid设置市场网格适配器
        rv_grid.adapter = adapter
        // 设置rv_grid的默认动画效果
        rv_grid.itemAnimator = DefaultItemAnimator()
        // 给rv_grid添加列表项之间的空白装饰
        rv_grid.addItemDecoration(SpacesItemDecoration(1))
    }

    private fun initCombine() {
        // 从布局文件中获取名叫rv_combine的循环视图
        val rv_combine = findViewById<RecyclerView>(R.id.rv_combine)
        // 创建一个四列的网格布局管理器
        val manager = GridLayoutManager(this, 4)
        // 设置网格布局管理器的占位规则
        // 以下占位规则的意思是：第一项和第二项占两列，其它项占一列；
        // 如果网格的列数为四，那么第一项和第二项平分第一行，第二行开始每行有四项。
        manager.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == 0 || position == 1) { // 为第一项或者第二项
                    2 // 占据两列
                } else { // 为其它项
                    1 // 占据一列
                }
            }
        }
        // 设置循环视图的布局管理器
        rv_combine.layoutManager = manager
        // 构建一个猜你喜欢的网格适配器
        val adapter = RecyclerCombineAdapter(this, GoodsInfo.Companion.defaultCombine)
        // 设置网格列表的点击监听器
        adapter.setOnItemClickListener(adapter)
        // 设置网格列表的长按监听器
        adapter.setOnItemLongClickListener(adapter)
        // 给rv_combine设置猜你喜欢网格适配器
        rv_combine.adapter = adapter
        // 设置rv_combine的默认动画效果
        rv_combine.itemAnimator = DefaultItemAnimator()
        // 给rv_combine添加列表项之间的空白装饰
        rv_combine.addItemDecoration(SpacesItemDecoration(1))
    }

    override fun onMenuOpened(featureId: Int, menu: Menu): Boolean {
        // 显示菜单项左侧的图标
        MenuUtil.setOverflowIconVisible(featureId, menu)
        return super.onMenuOpened(featureId, menu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // 从menu_home.xml中构建菜单界面布局
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) { // 点击了工具栏左边的返回箭头
            finish()
        } else if (id == R.id.menu_search) { // 点击了搜索图标
            // 跳转到搜索页面
            val intent = Intent(this, SearchViewActivity::class.java)
            intent.putExtra("collapse", false)
            startActivity(intent)
        } else if (id == R.id.menu_refresh) { // 点击了刷新图标
            Toast.makeText(
                this, "当前刷新时间: " +
                        DateUtil.getNowDateTime("yyyy-MM-dd HH:mm:ss"), Toast.LENGTH_LONG
            ).show()
            return true
        } else if (id == R.id.menu_about) { // 点击了关于菜单项
            Toast.makeText(this, "这个是商城首页", Toast.LENGTH_LONG).show()
            return true
        } else if (id == R.id.menu_quit) { // 点击了退出菜单项
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "DepartmentHomeActivity"
    }
}