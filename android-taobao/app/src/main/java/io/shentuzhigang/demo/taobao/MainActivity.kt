package io.shentuzhigang.demo.taobao

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.BannerIndicatorActivity
import io.shentuzhigang.demo.taobao.BannerPagerActivity
import io.shentuzhigang.demo.taobao.BannerTopActivity
import io.shentuzhigang.demo.taobao.DepartmentStoreActivity
import io.shentuzhigang.demo.taobao.ScrollAlipayActivity
import io.shentuzhigang.demo.taobao.SearchViewActivity
import io.shentuzhigang.demo.taobao.TabCustomActivity
import io.shentuzhigang.demo.taobao.TabFragmentActivity
import io.shentuzhigang.demo.taobao.TabGroupActivity
import io.shentuzhigang.demo.taobao.TabHostActivity
import io.shentuzhigang.demo.taobao.TabLayoutActivity
import io.shentuzhigang.demo.taobao.ToolbarActivity
import io.shentuzhigang.demo.taobao.ToolbarCustomActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.btn_tab_button).setOnClickListener(this)
        findViewById<View>(R.id.btn_tab_host).setOnClickListener(this)
        findViewById<View>(R.id.btn_tab_group).setOnClickListener(this)
        findViewById<View>(R.id.btn_tab_fragment).setOnClickListener(this)
        findViewById<View>(R.id.btn_toolbar).setOnClickListener(this)
        findViewById<View>(R.id.btn_toolbar_custom).setOnClickListener(this)
        findViewById<View>(R.id.btn_overflow_menu).setOnClickListener(this)
        findViewById<View>(R.id.btn_search_view).setOnClickListener(this)
        findViewById<View>(R.id.btn_tab_layout).setOnClickListener(this)
        findViewById<View>(R.id.btn_tab_custom).setOnClickListener(this)
        findViewById<View>(R.id.btn_banner_indicator).setOnClickListener(this)
        findViewById<View>(R.id.btn_banner_pager).setOnClickListener(this)
        findViewById<View>(R.id.btn_banner_top).setOnClickListener(this)
        findViewById<View>(R.id.btn_recycler_linear).setOnClickListener(this)
        findViewById<View>(R.id.btn_recycler_grid).setOnClickListener(this)
        findViewById<View>(R.id.btn_recycler_combine).setOnClickListener(this)
        findViewById<View>(R.id.btn_recycler_staggered).setOnClickListener(this)
        findViewById<View>(R.id.btn_recycler_dynamic).setOnClickListener(this)
        findViewById<View>(R.id.btn_coordinator).setOnClickListener(this)
        findViewById<View>(R.id.btn_appbar_recycler).setOnClickListener(this)
        findViewById<View>(R.id.btn_appbar_nested).setOnClickListener(this)
        findViewById<View>(R.id.btn_collapse_pin).setOnClickListener(this)
        findViewById<View>(R.id.btn_collapse_parallax).setOnClickListener(this)
        findViewById<View>(R.id.btn_image_fade).setOnClickListener(this)
        findViewById<View>(R.id.btn_scroll_flag).setOnClickListener(this)
        findViewById<View>(R.id.btn_scroll_alipay).setOnClickListener(this)
        findViewById<View>(R.id.btn_swipe_refresh).setOnClickListener(this)
        findViewById<View>(R.id.btn_swipe_recycler).setOnClickListener(this)
        findViewById<View>(R.id.btn_department_store).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_tab_button) {
            val intent = Intent(this, TabButtonActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_tab_host) {
            val intent = Intent(this, TabHostActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_tab_group) {
            val intent = Intent(this, TabGroupActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_tab_fragment) {
            val intent = Intent(this, TabFragmentActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_toolbar) {
            val intent = Intent(this, ToolbarActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_overflow_menu) {
            val intent = Intent(this, OverflowMenuActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_search_view) {
            val intent = Intent(this, SearchViewActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_toolbar_custom) {
            val intent = Intent(this, ToolbarCustomActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_tab_layout) {
            val intent = Intent(this, TabLayoutActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_tab_custom) {
            val intent = Intent(this, TabCustomActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_banner_indicator) {
            val intent = Intent(this, BannerIndicatorActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_banner_pager) {
            val intent = Intent(this, BannerPagerActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_banner_top) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                val intent = Intent(this, BannerTopActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "悬浮状态栏需要Android4.4或以上版本", Toast.LENGTH_SHORT).show()
            }
        } else if (v.id == R.id.btn_recycler_linear) {
            val intent = Intent(this, RecyclerLinearActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_recycler_grid) {
            val intent = Intent(this, RecyclerGridActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_recycler_combine) {
            val intent = Intent(this, RecyclerCombineActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_recycler_staggered) {
            val intent = Intent(this, RecyclerStaggeredActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_recycler_dynamic) {
            val intent = Intent(this, RecyclerDynamicActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_coordinator) {
            val intent = Intent(this, CoordinatorActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_appbar_recycler) {
            val intent = Intent(this, AppbarRecyclerActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_appbar_nested) {
            val intent = Intent(this, AppbarNestedActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_collapse_pin) {
            val intent = Intent(this, CollapsePinActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_collapse_parallax) {
            val intent = Intent(this, CollapseParallaxActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_image_fade) {
            val intent = Intent(this, ImageFadeActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_scroll_flag) {
            val intent = Intent(this, ScrollFlagActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_scroll_alipay) {
            val intent = Intent(this, ScrollAlipayActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_swipe_refresh) {
            val intent = Intent(this, SwipeRefreshActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_swipe_recycler) {
            val intent = Intent(this, SwipeRecyclerActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_department_store) {
            val intent = Intent(this, DepartmentStoreActivity::class.java)
            startActivity(intent)
        }
    }
}