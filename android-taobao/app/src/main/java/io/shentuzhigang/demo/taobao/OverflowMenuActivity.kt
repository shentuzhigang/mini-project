package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.util.DateUtil
import io.shentuzhigang.demo.taobao.util.MenuUtil

@SuppressLint("SetTextI18n")
class OverflowMenuActivity : AppCompatActivity() {
    private var tv_desc: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_overflow_menu)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 设置工具栏的标题文字
        tl_head.title = "溢出菜单页面"
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        tv_desc = findViewById(R.id.tv_desc)
    }

    override fun onMenuOpened(featureId: Int, menu: Menu): Boolean {
        // 显示菜单项左侧的图标
        MenuUtil.setOverflowIconVisible(featureId, menu)
        return super.onMenuOpened(featureId, menu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // 从menu_overflow.xml中构建菜单界面布局
        menuInflater.inflate(R.menu.menu_overflow, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) { // 点击了工具栏左边的返回箭头
            finish()
        } else if (id == R.id.menu_refresh) { // 点击了刷新图标
            tv_desc!!.text = "当前刷新时间: " + DateUtil.getNowDateTime("yyyy-MM-dd HH:mm:ss")
            return true
        } else if (id == R.id.menu_about) { // 点击了关于菜单项
            Toast.makeText(this, "这个是工具栏的演示demo", Toast.LENGTH_LONG).show()
            return true
        } else if (id == R.id.menu_quit) { // 点击了退出菜单项
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}