package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.adapter.LinearDynamicAdapter
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.widget.*
import io.shentuzhigang.demo.taobao.widget.RecyclerExtras.OnItemDeleteClickListener
import java.util.*

@SuppressLint("DefaultLocale")
class RecyclerDynamicActivity : AppCompatActivity(), View.OnClickListener,
    RecyclerExtras.OnItemClickListener, RecyclerExtras.OnItemLongClickListener,
    OnItemDeleteClickListener {
    private var rv_dynamic // 声明一个循环视图对象
            : RecyclerView? = null
    private var mAdapter // 声明一个线性适配器对象
            : LinearDynamicAdapter? = null
    private var mPublicArray // 当前公众号信息队列
            : ArrayList<GoodsInfo>? = null
    private var mAllArray // 所有公众号信息队列
            : ArrayList<GoodsInfo>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_dynamic)
        findViewById<View>(R.id.btn_recycler_add).setOnClickListener(this)
        initRecyclerDynamic() // 初始化动态线性布局的循环视图
    }

    // 初始化动态线性布局的循环视图
    private fun initRecyclerDynamic() {
        // 从布局文件中获取名叫rv_dynamic的循环视图
        rv_dynamic = findViewById(R.id.rv_dynamic)
        // 创建一个垂直方向的线性布局管理器
        val manager = LinearLayoutManager(
            this, RecyclerView.VERTICAL, false
        )
        // 设置循环视图的布局管理器
        rv_dynamic?.setLayoutManager(manager)
        // 获取默认的所有公众号信息队列
        mAllArray = GoodsInfo.defaultList
        // 获取默认的当前公众号信息队列
        mPublicArray = GoodsInfo.defaultList
        // 构建一个公众号列表的线性适配器
        mAdapter = LinearDynamicAdapter(this, mPublicArray)
        // 设置线性列表的点击监听器
        mAdapter!!.setOnItemClickListener(this)
        // 设置线性列表的长按监听器
        mAdapter!!.setOnItemLongClickListener(this)
        // 设置线性列表的删除按钮监听器
        mAdapter!!.setOnItemDeleteClickListener(this)
        // 给rv_dynamic设置公众号线性适配器
        rv_dynamic?.setAdapter(mAdapter)
        // 设置rv_dynamic的默认动画效果
        rv_dynamic?.setItemAnimator(DefaultItemAnimator())
        // 给rv_dynamic添加列表项之间的空白装饰
        rv_dynamic?.addItemDecoration(SpacesItemDecoration(1))
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_recycler_add) {
            val position = (Math.random() * 100 % mAllArray!!.size).toInt()
            val old_item = mAllArray!![position]
            val new_item = GoodsInfo(old_item.pic_id, old_item.title, old_item.desc)
            mPublicArray!!.add(0, new_item)
            // 通知适配器列表在第一项插入数据
            mAdapter!!.notifyItemInserted(0)
            // 让循环视图滚动到第一项所在的位置
            rv_dynamic!!.scrollToPosition(0)
        }
    }

    // 一旦点击循环适配器的列表项，就触发点击监听器的onItemClick方法
    override fun onItemClick(view: View?, position: Int) {
        val desc = String.format(
            "您点击了第%d项，标题是%s", position + 1,
            mPublicArray!![position].title
        )
        Toast.makeText(this, desc, Toast.LENGTH_SHORT).show()
    }

    // 一旦长按循环适配器的列表项，就触发长按监听器的onItemLongClick方法
    override fun onItemLongClick(view: View?, position: Int) {
        val item = mPublicArray!![position]
        item.bPressed = !item.bPressed
        mPublicArray!![position] = item
        // 通知适配器列表在第几项发生变更
        mAdapter!!.notifyItemChanged(position)
    }

    // 一旦点击循环适配器列表项的删除按钮，就触发删除监听器的onItemDeleteClick方法
    override fun onItemDeleteClick(view: View?, position: Int) {
        mPublicArray!!.removeAt(position)
        // 通知适配器列表在第几项删除数据
        mAdapter!!.notifyItemRemoved(position)
    }
}