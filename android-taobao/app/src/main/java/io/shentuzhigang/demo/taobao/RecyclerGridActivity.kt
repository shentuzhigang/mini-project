package io.shentuzhigang.demo.taobao

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.adapter.RecyclerGridAdapter
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.widget.SpacesItemDecoration

class RecyclerGridActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_grid)
        initRecyclerGrid() // 初始化网格布局的循环视图
    }

    // 初始化网格布局的循环视图
    private fun initRecyclerGrid() {
        // 从布局文件中获取名叫rv_grid的循环视图
        val rv_grid = findViewById<RecyclerView>(R.id.rv_grid)
        // 创建一个垂直方向的网格布局管理器
        val manager = GridLayoutManager(this, 5)
        // 设置循环视图的布局管理器
        rv_grid.layoutManager = manager
        // 构建一个市场列表的网格适配器
        val adapter = RecyclerGridAdapter(this, GoodsInfo.defaultGrid)
        // 设置网格列表的点击监听器
        adapter.setOnItemClickListener(adapter)
        // 设置网格列表的长按监听器
        adapter.setOnItemLongClickListener(adapter)
        // 给rv_grid设置市场网格适配器
        rv_grid.adapter = adapter
        // 设置rv_grid的默认动画效果
        rv_grid.itemAnimator = DefaultItemAnimator()
        // 给rv_grid添加列表项之间的空白装饰
        rv_grid.addItemDecoration(SpacesItemDecoration(1))
    }
}