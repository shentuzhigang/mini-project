package io.shentuzhigang.demo.taobao

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.adapter.RecyclerLinearAdapter
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.widget.SpacesItemDecoration

class RecyclerLinearActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_linear)
        initRecyclerLinear() // 初始化线性布局的循环视图
    }

    // 初始化线性布局的循环视图
    private fun initRecyclerLinear() {
        // 从布局文件中获取名叫rv_linear的循环视图
        val rv_linear = findViewById<RecyclerView>(R.id.rv_linear)
        // 创建一个垂直方向的线性布局管理器
        val manager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        // 设置循环视图的布局管理器
        rv_linear.layoutManager = manager
        // 构建一个公众号列表的线性适配器
        val adapter = RecyclerLinearAdapter(this, GoodsInfo.defaultList)
        // 设置线性列表的点击监听器
        adapter.setOnItemClickListener(adapter)
        // 设置线性列表的长按监听器
        adapter.setOnItemLongClickListener(adapter)
        // 给rv_linear设置公众号线性适配器
        rv_linear.adapter = adapter
        // 设置rv_linear的默认动画效果
        rv_linear.itemAnimator = DefaultItemAnimator()
        // 给rv_linear添加列表项之间的空白装饰
        rv_linear.addItemDecoration(SpacesItemDecoration(1))
    }
}