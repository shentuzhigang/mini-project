package io.shentuzhigang.demo.taobao

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.adapter.RecyclerStaggeredAdapter
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.widget.SpacesItemDecoration

class RecyclerStaggeredActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_staggered)
        initRecyclerStaggered() // 初始化瀑布流布局的循环视图
    }

    // 初始化瀑布流布局的循环视图
    private fun initRecyclerStaggered() {
        // 从布局文件中获取名叫rv_staggered的循环视图
        val rv_staggered = findViewById<RecyclerView>(R.id.rv_staggered)
        // 创建一个垂直方向的瀑布流布局管理器
        val manager = StaggeredGridLayoutManager(
            3, RecyclerView.VERTICAL
        )
        // 设置循环视图的布局管理器
        rv_staggered.layoutManager = manager
        // 构建一个服装列表的瀑布流适配器
        val adapter = RecyclerStaggeredAdapter(this, GoodsInfo.defaultStag)
        // 设置瀑布流列表的点击监听器
        adapter.setOnItemClickListener(adapter)
        // 设置瀑布流列表的长按监听器
        adapter.setOnItemLongClickListener(adapter)
        // 给rv_staggered设置服装瀑布流适配器
        rv_staggered.adapter = adapter
        // 设置rv_staggered的默认动画效果
        rv_staggered.itemAnimator = DefaultItemAnimator()
        // 给rv_staggered添加列表项之间的空白装饰
        rv_staggered.addItemDecoration(SpacesItemDecoration(3))
    }
}