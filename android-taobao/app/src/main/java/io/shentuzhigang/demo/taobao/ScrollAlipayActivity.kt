package io.shentuzhigang.demo.taobao

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import io.shentuzhigang.demo.taobao.adapter.LifeRecyclerAdapter
import io.shentuzhigang.demo.taobao.bean.LifeItem
import io.shentuzhigang.demo.taobao.util.Utils

class ScrollAlipayActivity : AppCompatActivity(), OnOffsetChangedListener {
    private var tl_expand: View? = null
    private var tl_collapse // 分别声明伸展时候与收缩时候的工具栏视图
            : View? = null
    private var v_expand_mask: View? = null
    private var v_collapse_mask: View? = null
    private var v_pay_mask // 分别声明三个遮罩视图
            : View? = null
    private var mMaskColor // 遮罩颜色
            = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scroll_alipay)
        // 获取默认的蓝色遮罩颜色
        mMaskColor = resources.getColor(R.color.blue_dark)
        // 从布局文件中获取名叫rv_content的循环视图
        val rv_content = findViewById<RecyclerView>(R.id.rv_content)
        // 设置循环视图的布局管理器（四列的网格布局管理器）
        rv_content.layoutManager = GridLayoutManager(this, 4)
        // 给rv_content设置生活频道网格适配器
        rv_content.adapter = LifeRecyclerAdapter(this, LifeItem.default)
        // 从布局文件中获取名叫abl_bar的应用栏布局
        val abl_bar = findViewById<AppBarLayout>(R.id.abl_bar)
        // 从布局文件中获取伸展之后的工具栏视图
        tl_expand = findViewById(R.id.tl_expand)
        // 从布局文件中获取收缩之后的工具栏视图
        tl_collapse = findViewById(R.id.tl_collapse)
        // 从布局文件中获取伸展之后的工具栏遮罩视图
        v_expand_mask = findViewById(R.id.v_expand_mask)
        // 从布局文件中获取收缩之后的工具栏遮罩视图
        v_collapse_mask = findViewById(R.id.v_collapse_mask)
        // 从布局文件中获取生活频道的遮罩视图
        v_pay_mask = findViewById(R.id.v_pay_mask)
        // 给abl_bar注册一个位置偏移的监听器
        abl_bar.addOnOffsetChangedListener(this)
    }

    // 每当应用栏向上滚动或者向下滚动，就会触发位置偏移监听器的onOffsetChanged方法
    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
        val offset = Math.abs(verticalOffset)
        // 获取应用栏的整个滑动范围，以此计算当前的位移比例
        val total = appBarLayout.totalScrollRange
        val alphaIn = Utils.px2dip(this, offset.toFloat()) * 2
        val alphaOut = if (200 - alphaIn < 0) 0 else 200 - alphaIn
        // 计算淡入时候的遮罩透明度
        val maskColorIn = Color.argb(
            alphaIn, Color.red(mMaskColor),
            Color.green(mMaskColor), Color.blue(mMaskColor)
        )
        // 工具栏下方的生活频道布局要加速淡入或者淡出
        val maskColorInDouble = Color.argb(
            alphaIn * 2, Color.red(mMaskColor),
            Color.green(mMaskColor), Color.blue(mMaskColor)
        )
        // 计算淡出时候的遮罩透明度
        val maskColorOut = Color.argb(
            alphaOut * 3, Color.red(mMaskColor),
            Color.green(mMaskColor), Color.blue(mMaskColor)
        )
        if (offset <= total * 0.45) { // 偏移量小于一半，则显示伸展时候的工具栏
            tl_expand!!.visibility = View.VISIBLE
            tl_collapse!!.visibility = View.GONE
            v_expand_mask!!.setBackgroundColor(maskColorInDouble)
        } else { // 偏移量大于一半，则显示收缩时候的工具栏
            tl_expand!!.visibility = View.GONE
            tl_collapse!!.visibility = View.VISIBLE
            v_collapse_mask!!.setBackgroundColor(maskColorOut)
        }
        // 设置life_pay.xml即生活频道视图的遮罩颜色
        v_pay_mask!!.setBackgroundColor(maskColorIn)
    }

    companion object {
        private const val TAG = "ScrollAlipayActivity"
    }
}