package io.shentuzhigang.demo.taobao

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import io.shentuzhigang.demo.taobao.adapter.RecyclerCollapseAdapter

class ScrollFlagActivity : AppCompatActivity() {
    private var ctl_title // 声明一个可折叠布局对象
            : CollapsingToolbarLayout? = null
    private val yearArray = arrayOf(
        "鼠年", "牛年", "虎年", "兔年", "龙年", "蛇年",
        "马年", "羊年", "猴年", "鸡年", "狗年", "猪年"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scroll_flag)
        // 从布局文件中获取名叫tl_title的工具栏
        val tl_title = findViewById<Toolbar>(R.id.tl_title)
        // 设置工具栏的背景
        tl_title.setBackgroundColor(Color.YELLOW)
        // 使用tl_title替换系统自带的ActionBar
        setSupportActionBar(tl_title)
        // 从布局文件中获取名叫ctl_title的可折叠布局
        ctl_title = findViewById(R.id.ctl_title)
        // 设置可折叠布局的标题文字
        ctl_title?.setTitle("滚动标志")
        initFlagSpinner()
        // 从布局文件中获取名叫rv_main的循环视图
        val rv_main = findViewById<RecyclerView>(R.id.rv_main)
        // 创建一个垂直方向的线性布局管理器
        val llm = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        // 设置循环视图的布局管理器
        rv_main.layoutManager = llm
        // 构建一个十二生肖的线性适配器
        val adapter = RecyclerCollapseAdapter(this, yearArray)
        // 给rv_main设置十二生肖线性适配器
        rv_main.adapter = adapter
    }

    // 定义一个滚动标志说明的字符串数组
    private val descArray = arrayOf(
        "scroll",
        "scroll|enterAlways",
        "scroll|exitUntilCollapsed",
        "scroll|enterAlways|enterAlwaysCollapsed",
        "scroll|snap"
    )

    // 定义一个滚动标志位的整型数组
    private val flagArray = intArrayOf(
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL,
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS,
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED,
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED,
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
    )

    // 初始化滚动标志的下拉框
    private fun initFlagSpinner() {
        val flagAdapter = ArrayAdapter(
            this,
            R.layout.item_select, descArray
        )
        flagAdapter.setDropDownViewResource(R.layout.item_dropdown)
        val sp_style = findViewById<Spinner>(R.id.sp_flag)
        sp_style.prompt = "请选择滚动标志"
        sp_style.adapter = flagAdapter
        // 设置下拉框列表的选择监听器
        sp_style.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
                // 获取可折叠布局的布局参数
                val params = ctl_title!!.layoutParams as AppBarLayout.LayoutParams
                // 设置布局参数中的滚动标志位
                params.scrollFlags = flagArray[arg2]
                // 设置可折叠布局的布局参数。注意：第三种滚动标志一定要调用setLayoutParams
                ctl_title!!.layoutParams = params
            }

            override fun onNothingSelected(arg0: AdapterView<*>?) {}
        }
        sp_style.setSelection(0)
    }
}