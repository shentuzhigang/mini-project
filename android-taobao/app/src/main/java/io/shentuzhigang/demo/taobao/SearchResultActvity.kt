package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.shentuzhigang.demo.taobao.R

@SuppressLint("SetTextI18n")
class SearchResultActvity : AppCompatActivity() {
    private var tv_search_result: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result)
        // 从布局文件中获取名叫tl_result的工具栏
        val tl_result = findViewById<Toolbar>(R.id.tl_result)
        // 设置工具栏的背景
        tl_result.setBackgroundResource(R.color.blue_light)
        // 设置工具栏的标志图片
        tl_result.setLogo(R.drawable.ic_app)
        // 设置工具栏的标题文字
        tl_result.title = "搜索结果页"
        // 设置工具栏的导航图标
        tl_result.setNavigationIcon(R.drawable.ic_back)
        // 使用tl_result替换系统自带的ActionBar
        setSupportActionBar(tl_result)
        tv_search_result = findViewById(R.id.tv_search_result)
        // 执行搜索查询操作
        doSearchQuery(intent)
    }

    // 解析搜索请求页面传来的搜索信息，并据此执行搜索查询操作
    private fun doSearchQuery(intent: Intent?) {
        if (intent != null) {
            // 如果是通过ACTION_SEARCH来调用，即为搜索框来源
            if (Intent.ACTION_SEARCH == intent.action) {
                // 获取额外的搜索数据
                val bundle = intent.getBundleExtra(SearchManager.APP_DATA)
                val value = bundle!!.getString("hi")
                // 获取实际的搜索文本
                val queryString = intent.getStringExtra(SearchManager.QUERY)
                tv_search_result!!.text = "您输入的搜索文字是：$queryString, 额外信息：$value"
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // 从menu_null.xml中构建菜单界面布局
        menuInflater.inflate(R.menu.menu_null, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) { // 点击了工具栏左边的返回箭头
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "SearchResultActvity"
    }
}