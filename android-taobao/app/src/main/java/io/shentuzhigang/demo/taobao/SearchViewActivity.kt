package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.ComponentName
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SearchView.SearchAutoComplete
import androidx.appcompat.widget.Toolbar
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.SearchResultActvity
import io.shentuzhigang.demo.taobao.util.DateUtil
import io.shentuzhigang.demo.taobao.util.MenuUtil

@SuppressLint("SetTextI18n")
class SearchViewActivity : AppCompatActivity() {
    private var tv_desc: TextView? = null
    private var sac_key // 声明一个搜索自动完成的编辑框对象
            : SearchAutoComplete? = null
    private val hintArray = arrayOf("iphone", "iphone8", "iphone8 plus", "iphone7", "iphone7 plus")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_view)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 设置工具栏的标题文字
        tl_head.title = "搜索框页面"
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        tv_desc = findViewById(R.id.tv_desc)
    }

    // 根据菜单项初始化搜索框
    @SuppressLint("RestrictedApi")
    private fun initSearchView(menu: Menu) {
        val menuItem = menu.findItem(R.id.menu_search)
        // 从菜单项中获取搜索框对象
        val searchView = menuItem.actionView as SearchView
        // 设置搜索框默认自动缩小为图标
        searchView.setIconifiedByDefault(intent.getBooleanExtra("collapse", true))
        // 设置是否显示搜索按钮。搜索按钮只显示一个箭头图标，Android暂不支持显示文本。
        // 查看Android源码，搜索按钮用的控件是ImageView，所以只能显示图标不能显示文字。
        searchView.isSubmitButtonEnabled = true
        // 从系统服务中获取搜索管理器
        val sm = getSystemService(SEARCH_SERVICE) as SearchManager
        // 创建搜索结果页面的组件名称对象
        val cn = ComponentName(this, SearchResultActvity::class.java)
        // 从结果页面注册的activity节点获取相关搜索信息，即searchable.xml定义的搜索控件
        val info = sm.getSearchableInfo(cn)
        if (info == null) {
            Log.d(TAG, "Fail to get SearchResultActvity.")
            return
        }
        // 设置搜索框的可搜索信息
        searchView.setSearchableInfo(info)
        // 从搜索框中获取名叫search_src_text的自动完成编辑框
        sac_key = searchView.findViewById(R.id.search_src_text)
        // 设置自动完成编辑框的文本颜色
        sac_key?.setTextColor(Color.WHITE)
        // 设置自动完成编辑框的提示文本颜色
        sac_key?.setHintTextColor(Color.WHITE)
        // 给搜索框设置文本变化监听器
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            // 搜索关键词完成输入
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            // 搜索关键词发生变化
            override fun onQueryTextChange(newText: String): Boolean {
                doSearch(newText)
                return true
            }
        })
        val bundle = Bundle() // 创建一个新包裹
        bundle.putString("hi", "hello") // 往包裹中存放名叫hi的字符串
        // 设置搜索框的额外搜索数据
        searchView.setAppSearchData(bundle)
    }

    // 自动匹配相关的关键词列表
    private fun doSearch(text: String) {
        if (text.indexOf("i") == 0) {
            // 根据提示词数组构建一个数组适配器
            val adapter = ArrayAdapter(
                this,
                R.layout.search_list_auto, hintArray
            )
            // 设置自动完成编辑框的数组适配器
            sac_key!!.setAdapter(adapter)
            // 给自动完成编辑框设置列表项的点击监听器
            sac_key!!.onItemClickListener =
                AdapterView.OnItemClickListener { parent, view, position, id ->
                    // 一旦点击关键词匹配列表中的某一项，就触发点击监听器的onItemClick方法
                    sac_key!!.setText((view as TextView).text)
                }
        }
    }

    override fun onMenuOpened(featureId: Int, menu: Menu): Boolean {
        // 显示菜单项左侧的图标
        MenuUtil.setOverflowIconVisible(featureId, menu)
        return super.onMenuOpened(featureId, menu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // 从menu_search.xml中构建菜单界面布局
        menuInflater.inflate(R.menu.menu_search, menu)
        // 初始化搜索框
        initSearchView(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) { // 点击了工具栏左边的返回箭头
            finish()
        } else if (id == R.id.menu_refresh) { // 点击了刷新图标
            tv_desc!!.text = "当前刷新时间: " + DateUtil.getNowDateTime("yyyy-MM-dd HH:mm:ss")
            return true
        } else if (id == R.id.menu_about) { // 点击了关于菜单项
            Toast.makeText(this, "这个是工具栏的演示demo", Toast.LENGTH_LONG).show()
            return true
        } else if (id == R.id.menu_quit) { // 点击了退出菜单项
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "SearchViewActivity"
    }
}