package io.shentuzhigang.demo.taobao

import android.os.Bundle
import android.os.Handler
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class SwipeRefreshActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    private var tv_simple: TextView? = null
    private var srl_simple // 声明一个下拉刷新布局对象
            : SwipeRefreshLayout? = null

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swipe_refresh)
        tv_simple = findViewById<TextView>(R.id.tv_simple)
        // 从布局文件中获取名叫srl_simple的下拉刷新布局
        srl_simple = findViewById(R.id.srl_simple)
        // 给srl_simple设置下拉刷新监听器
        srl_simple?.setOnRefreshListener(this)
        // 设置下拉刷新布局的进度圆圈颜色
        srl_simple?.setColorSchemeResources(
            R.color.red, R.color.orange, R.color.green, R.color.blue
        )
    }

    // 一旦在下拉刷新布局内部往下拉动页面，就触发下拉监听器的onRefresh方法
    override fun onRefresh() {
        tv_simple?.setText("正在刷新")
        // 延迟若干秒后启动刷新任务
        mHandler.postDelayed(mRefresh, 2000)
    }

    private val mHandler = Handler() // 声明一个处理器对象

    // 定义一个刷新任务
    private val mRefresh = Runnable {
        tv_simple?.setText("刷新完成")
        // 结束下拉刷新布局的刷新动作
        srl_simple?.setRefreshing(false)
    }
}