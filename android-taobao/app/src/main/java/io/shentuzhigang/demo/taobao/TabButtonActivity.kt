package io.shentuzhigang.demo.taobao

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.taobao.R

class TabButtonActivity : AppCompatActivity(), View.OnClickListener,
    CompoundButton.OnCheckedChangeListener {
    private var tv_tab_button // 声明一个标签按钮对象
            : TextView? = null
    private var ck_select // 声明一个复选框对象
            : CheckBox? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_button)
        // 从布局文件中获取名叫tv_tab_button的标签按钮
        tv_tab_button = findViewById(R.id.tv_tab_button)
        tv_tab_button?.setOnClickListener(this)
        // 从布局文件中获取名叫ck_select的复选框
        ck_select = findViewById(R.id.ck_select)
        // 给复选框ck_select设置勾选监听器
        ck_select?.setOnCheckedChangeListener(this)
    }

    // 一旦勾选或者取消勾选复选框，就触发勾选监听器的onCheckedChanged方法
    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (buttonView.id == R.id.ck_select) {
            // 设置标签按钮的选中状态
            tv_tab_button!!.isSelected = isChecked
        }
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tv_tab_button) {
            // 将复选框的状态置反
            ck_select!!.isChecked = !ck_select!!.isChecked
        }
    }
}