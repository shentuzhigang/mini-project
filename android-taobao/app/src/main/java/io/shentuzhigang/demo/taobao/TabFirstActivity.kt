package io.shentuzhigang.demo.taobao

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import io.shentuzhigang.demo.taobao.R

class TabFirstActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_tab_first)
        // 根据标签栏传来的参数拼接文本字符串
        val desc = String.format(
            "我是%s页面，来自%s",
            "首页", intent.extras!!.getString("tag")
        )
        val tv_first = findViewById<TextView>(R.id.tv_first)
        Log.d("TabFirstActivity", "getId=" + tv_first.id)
        tv_first.text = desc
    }
}