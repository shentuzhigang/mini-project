package io.shentuzhigang.demo.taobao

import android.app.ActivityGroup
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import io.shentuzhigang.demo.taobao.R

class TabGroupActivity : ActivityGroup(), View.OnClickListener {
    private val mBundle = Bundle() // 声明一个包裹对象
    private var ll_container: LinearLayout? = null
    private var ll_first: LinearLayout? = null
    private var ll_second: LinearLayout? = null
    private var ll_third: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_group)
        // 从布局文件中获取名叫ll_container的线性布局，用于存放内容视图
        ll_container = findViewById(R.id.ll_container)
        ll_first = findViewById(R.id.ll_first) // 获取第一个标签的线性布局
        ll_second = findViewById(R.id.ll_second) // 获取第二个标签的线性布局
        ll_third = findViewById(R.id.ll_third) // 获取第三个标签的线性布局
        ll_first?.setOnClickListener(this) // 给第一个标签注册点击监听器
        ll_second?.setOnClickListener(this) // 给第二个标签注册点击监听器
        ll_third?.setOnClickListener(this) // 给第三个标签注册点击监听器
        mBundle.putString("tag", TAG) // 往包裹中存入名叫tag的标记串
        changeContainerView(ll_first) // 默认显示第一个标签的内容视图
    }

    override fun onClick(v: View) {
        if (v.id == R.id.ll_first || v.id == R.id.ll_second || v.id == R.id.ll_third) {
            changeContainerView(v) // 点击了哪个标签，就切换到该标签对应的内容视图
        }
    }

    // 内容视图改为展示指定的视图
    private fun changeContainerView(v: View?) {
        ll_first!!.isSelected = false // 取消选中第一个标签
        ll_second!!.isSelected = false // 取消选中第二个标签
        ll_third!!.isSelected = false // 取消选中第三个标签
        v!!.isSelected = true // 选中指定标签
        if (v === ll_first) {
            // 切换到第一个活动页面TabFirstActivity
            toActivity("first", TabFirstActivity::class.java)
        } else if (v === ll_second) {
            // 切换到第二个活动页面TabSecondActivity
            toActivity("second", TabSecondActivity::class.java)
        } else if (v === ll_third) {
            // 切换到第三个活动页面TabThirdActivity
            toActivity("third", TabThirdActivity::class.java)
        }
    }

    // 把内容视图切换到对应的Activity活动页面
    private fun toActivity(label: String, cls: Class<*>) {
        // 创建一个意图，并存入指定包裹
        val intent = Intent(this, cls).putExtras(mBundle)
        // 移除内容框架下面的所有下级视图
        ll_container!!.removeAllViews()
        // 启动意图指向的活动，并获取该活动页面的顶层视图
        val v = localActivityManager.startActivity(label, intent).decorView
        // 设置内容视图的布局参数
        v.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        )
        // 把活动页面的顶层视图（即内容视图）添加到内容框架上
        ll_container!!.addView(v)
    }

    companion object {
        private const val TAG = "TabGroupActivity"
    }
}