package io.shentuzhigang.demo.taobao

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener
import io.shentuzhigang.demo.taobao.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import io.shentuzhigang.demo.taobao.adapter.GoodsPagerAdapter
import io.shentuzhigang.demo.taobao.util.DateUtil
import io.shentuzhigang.demo.taobao.util.MenuUtil
import java.util.*

class TabLayoutActivity : AppCompatActivity(), OnTabSelectedListener {
    private var vp_content // 定义一个翻页视图对象
            : ViewPager? = null
    private var tab_title // 定义一个标签布局对象
            : TabLayout? = null
    private val mTitleArray = ArrayList<String>() // 标题文字队列
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_layout)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        mTitleArray.add("商品")
        mTitleArray.add("详情")
        initTabLayout() // 初始化标签布局
        initTabViewPager() // 初始化标签翻页
    }

    // 初始化标签布局
    private fun initTabLayout() {
        // 从布局文件中获取名叫tab_title的标签布局
        tab_title = findViewById(R.id.tab_title)
        // 给tab_title添加一个指定文字的标签
        tab_title?.addTab(tab_title!!.newTab().setText(mTitleArray[0]))
        // 给tab_title添加一个指定文字的标签
        tab_title?.addTab(tab_title!!.newTab().setText(mTitleArray[1]))
        // 给tab_title添加标签选中监听器
        tab_title?.addOnTabSelectedListener(this)
    }

    // 初始化标签翻页
    private fun initTabViewPager() {
        // 从布局文件中获取名叫vp_content的翻页视图
        vp_content = findViewById(R.id.vp_content)
        // 构建一个商品信息的翻页适配器
        val adapter = GoodsPagerAdapter(
            supportFragmentManager, mTitleArray
        )
        // 给vp_content设置商品翻页适配器
        vp_content?.setAdapter(adapter)
        // 给vp_content添加页面变更监听器
        vp_content?.addOnPageChangeListener(object : SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                // 选中tab_title指定位置的标签
                tab_title!!.getTabAt(position)!!.select()
            }
        })
    }

    // 在标签被重复选中时触发
    override fun onTabReselected(tab: TabLayout.Tab) {}

    // 在标签选中时触发
    override fun onTabSelected(tab: TabLayout.Tab) {
        // 让vp_content显示指定位置的页面
        vp_content!!.currentItem = tab.position
    }

    // 在标签取消选中时触发
    override fun onTabUnselected(tab: TabLayout.Tab) {}
    override fun onMenuOpened(featureId: Int, menu: Menu): Boolean {
        // 显示菜单项左侧的图标
        MenuUtil.setOverflowIconVisible(featureId, menu)
        return super.onMenuOpened(featureId, menu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // 从menu_overflow.xml中构建菜单界面布局
        menuInflater.inflate(R.menu.menu_overflow, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) { // 点击了工具栏左边的返回箭头
            finish()
        } else if (id == R.id.menu_refresh) { // 点击了刷新图标
            Toast.makeText(
                this, "当前刷新时间: " + DateUtil.getNowDateTime("yyyy-MM-dd HH:mm:ss"), Toast.LENGTH_LONG
            ).show()
            return true
        } else if (id == R.id.menu_about) { // 点击了关于菜单项
            Toast.makeText(this, "这个是标签布局的演示demo", Toast.LENGTH_LONG).show()
            return true
        } else if (id == R.id.menu_quit) { // 点击了退出菜单项
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "TabLayoutActivity"
    }
}