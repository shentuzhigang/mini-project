package io.shentuzhigang.demo.taobao

import android.app.Activity
import android.os.Bundle
import android.widget.TextView
import io.shentuzhigang.demo.taobao.R

class TabSecondActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_tab_second)
        // 根据标签栏传来的参数拼接文本字符串
        val desc = String.format(
            "我是%s页面，来自%s",
            "分类", intent.extras!!.getString("tag")
        )
        val tv_second = findViewById<TextView>(R.id.tv_second)
        tv_second.text = desc
    }
}