package io.shentuzhigang.demo.taobao

import android.app.Activity
import android.os.Bundle
import android.widget.TextView
import io.shentuzhigang.demo.taobao.R

class TabThirdActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_tab_third)
        // 根据标签栏传来的参数拼接文本字符串
        val desc = String.format(
            "我是%s页面，来自%s",
            "购物车", intent.extras!!.getString("tag")
        )
        val tv_third = findViewById<TextView>(R.id.tv_third)
        tv_third.text = desc
    }
}