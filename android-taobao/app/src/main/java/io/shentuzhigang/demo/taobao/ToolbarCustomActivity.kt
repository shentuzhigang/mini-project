package io.shentuzhigang.demo.taobao

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.util.DateUtil
import io.shentuzhigang.demo.taobao.util.MenuUtil
import io.shentuzhigang.demo.taobao.widget.CustomDateDialog
import java.util.*

@SuppressLint(value = ["DefaultLocale", "SetTextI18n"])
class ToolbarCustomActivity : AppCompatActivity(), View.OnClickListener,
    CustomDateDialog.OnDateSetListener {
    private var tv_day: TextView? = null
    private var tv_desc: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toolbar_custom)
        // 从布局文件中获取名叫tl_head的工具栏
        val tl_head = findViewById<Toolbar>(R.id.tl_head)
        // 使用tl_head替换系统自带的ActionBar
        setSupportActionBar(tl_head)
        tv_day = findViewById(R.id.tv_day)
        tv_desc = findViewById(R.id.tv_desc)
        tv_day?.setText(DateUtil.getNowDateTime("yyyy年MM月dd日"))
        tv_day?.setOnClickListener(this)
    }

    override fun onMenuOpened(featureId: Int, menu: Menu): Boolean {
        // 显示菜单项左侧的图标
        MenuUtil.setOverflowIconVisible(featureId, menu)
        return super.onMenuOpened(featureId, menu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // 从menu_overflow.xml中构建菜单界面布局
        menuInflater.inflate(R.menu.menu_overflow, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) { // 点击了工具栏左边的返回箭头
            finish()
        } else if (id == R.id.menu_refresh) { // 点击了刷新图标
            tv_desc!!.text = "当前刷新时间: " + DateUtil.getNowDateTime("yyyy-MM-dd HH:mm:ss")
            return true
        } else if (id == R.id.menu_about) { // 点击了关于菜单项
            Toast.makeText(this, "这个是工具栏的演示demo", Toast.LENGTH_LONG).show()
            return true
        } else if (id == R.id.menu_quit) { // 点击了退出菜单项
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDateSet(year: Int, month: Int, day: Int) {
        val date = String.format("%d年%d月%d日", year, month, day)
        tv_day!!.text = date
        tv_desc!!.text = "您选择的日期是$date"
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tv_day) {
            val calendar = Calendar.getInstance()
            // 弹出自定义的日期选择对话框
            val dialog = CustomDateDialog(this)
            dialog.setDate(
                calendar[Calendar.YEAR], calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH], this
            )
            dialog.show()
        }
    }

    companion object {
        private const val TAG = "ToolbarCustomActivity"
    }
}