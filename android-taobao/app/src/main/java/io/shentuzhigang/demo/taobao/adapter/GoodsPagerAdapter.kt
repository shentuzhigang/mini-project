package io.shentuzhigang.demo.taobao.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import io.shentuzhigang.demo.taobao.fragment.BookCoverFragment
import io.shentuzhigang.demo.taobao.fragment.BookDetailFragment
import java.util.*

class GoodsPagerAdapter     // 碎片页适配器的构造函数，传入碎片管理器与标题队列
    (
    fm: FragmentManager?, // 声明一个标题文字队列
    private val mTitleArray: ArrayList<String>
) : FragmentPagerAdapter(fm!!) {
    // 获取指定位置的碎片Fragment
    override fun getItem(position: Int): Fragment {
        if (position == 0) { // 第一页展示书籍封面
            return BookCoverFragment()
        } else if (position == 1) { // 第二页展示书籍详情
            return BookDetailFragment()
        }
        return BookCoverFragment()
    }

    // 获取碎片Fragment的个数
    override fun getCount(): Int {
        return mTitleArray.size
    }

    // 获得指定碎片页的标题文本
    override fun getPageTitle(position: Int): CharSequence? {
        return mTitleArray[position]
    }
}