package io.shentuzhigang.demo.taobao.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.widget.RecyclerExtras
import io.shentuzhigang.demo.taobao.widget.RecyclerExtras.OnItemDeleteClickListener
import java.util.*

class LinearDynamicAdapter(// 声明一个上下文对象
    private val mContext: Context, private val mPublicArray: ArrayList<GoodsInfo>?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), View.OnClickListener, OnLongClickListener {
    // 获取列表项的个数
    override fun getItemCount(): Int {
        return mPublicArray!!.size
    }

    // 创建列表项的视图持有者
    override fun onCreateViewHolder(vg: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // 根据布局文件item_linear.xml生成视图对象
        val v = LayoutInflater.from(mContext).inflate(R.layout.item_linear, vg, false)
        return ItemHolder(v)
    }

    // 根据列表项编号获取当前的位置序号
    private fun getPosition(item_id: Int): Int {
        var pos = 0
        for (i in mPublicArray!!.indices) {
            if (mPublicArray[i].id == item_id) {
                pos = i
                break
            }
        }
        return pos
    }

    private val CLICK = 0 // 正常点击
    private val DELETE = 1 // 点击了删除按钮
    override fun onClick(v: View) {
        val position = getPosition(v.id / 10)
        val type = v.id % 10
        if (type == CLICK) { // 正常点击，则触发点击监听器的onItemClick方法
            if (mOnItemClickListener != null) {
                mOnItemClickListener!!.onItemClick(v, position)
            }
        } else if (type == DELETE) { // 点击了删除按钮，则触发删除监听器的onItemDeleteClick方法
            if (mOnItemDeleteClickListener != null) {
                mOnItemDeleteClickListener!!.onItemDeleteClick(v, position)
            }
        }
    }

    override fun onLongClick(v: View): Boolean { // 长按了列表项，则调用长按监听器的onItemLongClick方法
        val position = getPosition(v.id / 10)
        if (mOnItemLongClickListener != null) {
            mOnItemLongClickListener!!.onItemLongClick(v, position)
        }
        return true
    }

    // 绑定列表项的视图持有者
    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        val item = mPublicArray!![position]
        val holder = vh as ItemHolder
        holder.iv_pic.setImageResource(item.pic_id)
        holder.tv_title.text = item.title
        holder.tv_desc.text = item.desc
        holder.tv_delete.visibility = if (item.bPressed) View.VISIBLE else View.GONE
        holder.tv_delete.id = item.id * 10 + DELETE
        holder.tv_delete.setOnClickListener(this)
        holder.ll_item.id = item.id * 10 + CLICK
        // 列表项的点击事件需要自己实现
        holder.ll_item.setOnClickListener(this)
        // 列表项的长按事件需要自己实现
        holder.ll_item.setOnLongClickListener(this)
    }

    // 获取列表项的类型
    override fun getItemViewType(position: Int): Int {
        return 0
    }

    // 获取列表项的编号
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    // 定义列表项的视图持有者
    inner class ItemHolder(v: View) : RecyclerView.ViewHolder(v) {
        var ll_item // 声明列表项的线性布局
                : LinearLayout
        var iv_pic // 声明列表项图标的图像视图
                : ImageView
        var tv_title // 声明列表项标题的文本视图
                : TextView
        var tv_desc // 声明列表项描述的文本视图
                : TextView
        var tv_delete // 声明列表项删除按钮的文本视图
                : TextView

        init {
            ll_item = v.findViewById(R.id.ll_item)
            iv_pic = v.findViewById(R.id.iv_pic)
            tv_title = v.findViewById(R.id.tv_title)
            tv_desc = v.findViewById(R.id.tv_desc)
            tv_delete = v.findViewById(R.id.tv_delete)
        }
    }

    // 声明列表项的点击监听器对象
    private var mOnItemClickListener: RecyclerExtras.OnItemClickListener? = null
    fun setOnItemClickListener(listener: RecyclerExtras.OnItemClickListener?) {
        mOnItemClickListener = listener
    }

    // 声明列表项的长按监听器对象
    private var mOnItemLongClickListener: RecyclerExtras.OnItemLongClickListener? = null
    fun setOnItemLongClickListener(listener: RecyclerExtras.OnItemLongClickListener?) {
        mOnItemLongClickListener = listener
    }

    // 声明列表项的删除监听器对象
    private var mOnItemDeleteClickListener: OnItemDeleteClickListener? = null
    fun setOnItemDeleteClickListener(listener: OnItemDeleteClickListener?) {
        mOnItemDeleteClickListener = listener
    }

    companion object {
        private const val TAG = "LinearDynamicAdapter"
    }
}