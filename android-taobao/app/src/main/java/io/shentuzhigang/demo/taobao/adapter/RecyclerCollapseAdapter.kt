package io.shentuzhigang.demo.taobao.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R

class RecyclerCollapseAdapter(// 声明一个上下文对象
    private val mContext: Context, // 标题文字数组
    private val mTitleArray: Array<String>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    // 获取列表项的个数
    override fun getItemCount(): Int {
        return mTitleArray.size
    }

    // 创建列表项的视图持有者
    override fun onCreateViewHolder(vg: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // 根据布局文件item_collapse.xml生成视图对象
        val v = LayoutInflater.from(mContext).inflate(R.layout.item_collapse, vg, false)
        return TitleHolder(v)
    }

    // 绑定列表项的视图持有者
    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        val holder = vh as TitleHolder
        holder.tv_seq.text = "" + (position + 1)
        holder.tv_title.text = mTitleArray[position]
    }

    // 获取列表项的类型
    override fun getItemViewType(position: Int): Int {
        return 0
    }

    // 获取列表项的编号
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    // 定义列表项的视图持有者
    inner class TitleHolder(v: View) : RecyclerView.ViewHolder(v) {
        var ll_item // 声明列表项的线性布局
                : LinearLayout
        var tv_seq // 声明列表项序号的文本视图
                : TextView
        var tv_title // 声明列表项标题的文本视图
                : TextView

        init {
            ll_item = v.findViewById(R.id.ll_item)
            tv_seq = v.findViewById(R.id.tv_seq)
            tv_title = v.findViewById(R.id.tv_title)
        }
    }

    companion object {
        private const val TAG = "RecyclerCollapseAdapter"
    }
}