package io.shentuzhigang.demo.taobao.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.bean.GoodsInfo
import io.shentuzhigang.demo.taobao.widget.RecyclerExtras
import java.util.*

@SuppressLint("DefaultLocale")
class RecyclerStaggeredAdapter(// 声明一个上下文对象
    private val mContext: Context, private val mGoodsArray: ArrayList<GoodsInfo>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), RecyclerExtras.OnItemClickListener,
    RecyclerExtras.OnItemLongClickListener {
    // 获取列表项的个数
    override fun getItemCount(): Int {
        return mGoodsArray.size
    }

    // 创建列表项的视图持有者
    override fun onCreateViewHolder(vg: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // 根据布局文件item_staggered.xml生成视图对象
        val v = LayoutInflater.from(mContext).inflate(R.layout.item_staggered, vg, false)
        return ItemHolder(v)
    }

    // 绑定列表项的视图持有者
    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        val holder = vh as ItemHolder
        holder.iv_pic.setImageResource(mGoodsArray[position].pic_id)
        holder.tv_title.text = mGoodsArray[position].title
        //        LayoutParams params = holder.ll_item.getLayoutParams();
//        params.height = (int) Math.round(300 * Math.random());
//        if (params.height < 60) {
//            params.height = 60;
//        }
//        // 很奇怪，setLayoutParams对瀑布流网格不起作用，只能用setHeight
//        holder.tv_title.setHeight(params.height);

        // 列表项的点击事件需要自己实现
        holder.ll_item.setOnClickListener { v ->
            if (mOnItemClickListener != null) {
                mOnItemClickListener!!.onItemClick(v, position)
            }
        }
        // 列表项的长按事件需要自己实现
        holder.ll_item.setOnLongClickListener { v ->
            if (mOnItemLongClickListener != null) {
                mOnItemLongClickListener!!.onItemLongClick(v, position)
            }
            true
        }
    }

    // 获取列表项的类型
    override fun getItemViewType(position: Int): Int {
        return 0
    }

    // 获取列表项的编号
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    // 定义列表项的视图持有者
    inner class ItemHolder(v: View) : RecyclerView.ViewHolder(v) {
        var ll_item // 声明列表项的线性布局
                : LinearLayout
        var iv_pic // 声明列表项图标的图像视图
                : ImageView
        var tv_title // 声明列表项标题的文本视图
                : TextView

        init {
            ll_item = v.findViewById(R.id.ll_item)
            iv_pic = v.findViewById(R.id.iv_pic)
            tv_title = v.findViewById(R.id.tv_title)
        }
    }

    // 声明列表项的点击监听器对象
    private var mOnItemClickListener: RecyclerExtras.OnItemClickListener? = null
    fun setOnItemClickListener(listener: RecyclerExtras.OnItemClickListener?) {
        mOnItemClickListener = listener
    }

    // 声明列表项的长按监听器对象
    private var mOnItemLongClickListener: RecyclerExtras.OnItemLongClickListener? = null
    fun setOnItemLongClickListener(listener: RecyclerExtras.OnItemLongClickListener?) {
        mOnItemLongClickListener = listener
    }

    // 处理列表项的点击事件
    override fun onItemClick(view: View?, position: Int) {
        val desc = String.format(
            "您点击了第%d项，商品名称是%s", position + 1,
            mGoodsArray[position].title
        )
        Toast.makeText(mContext, desc, Toast.LENGTH_SHORT).show()
    }

    // 处理列表项的长按事件
    override fun onItemLongClick(view: View?, position: Int) {
        val desc = String.format(
            "您长按了第%d项，商品名称是%s", position + 1,
            mGoodsArray[position].title
        )
        Toast.makeText(mContext, desc, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val TAG = "RecyclerStaggeredAdapter"
    }
}