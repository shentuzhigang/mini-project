package io.shentuzhigang.demo.taobao.bean

import io.shentuzhigang.demo.taobao.R
import java.util.*

class GoodsInfo(var pic_id: Int, var title: String?, var desc: String?) {
    var bPressed = false
    var id: Int

    companion object {
        private var seq = 0
        private val listImageArray = intArrayOf(
            R.drawable.public_01,
            R.drawable.public_02,
            R.drawable.public_03,
            R.drawable.public_04,
            R.drawable.public_05
        )
        private val listTitleArray = arrayOf(
            "首都日报", "海峡时报", "北方周末", "参照消息", "挨踢杂志"
        )
        private val listDescArray = arrayOf(
            "金秋时节香山染红，市民纷纷登山赏叶",
            "台风接踵而来，出门小心暴雨",
            "俄罗斯老人在东北，生活滋润乐不思蜀",
            "蒙内铁路建成通车，中国标准再下一城",
            "米6大战Mate10，千元智能机谁领风骚"
        )
        val defaultList: ArrayList<GoodsInfo>
            get() {
                val listArray = ArrayList<GoodsInfo>()
                for (i in listImageArray.indices) {
                    listArray.add(GoodsInfo(listImageArray[i], listTitleArray[i], listDescArray[i]))
                }
                return listArray
            }
        private val gridImageArray = intArrayOf(
            R.drawable.pic_01,
            R.drawable.pic_02,
            R.drawable.pic_03,
            R.drawable.pic_04,
            R.drawable.pic_05,
            R.drawable.pic_06,
            R.drawable.pic_07,
            R.drawable.pic_08,
            R.drawable.pic_09,
            R.drawable.pic_10
        )
        private val gridTitleArray = arrayOf(
            "商场", "超市", "百货", "便利店",
            "地摊", "食杂店", "饭店", "餐厅", "会所", "菜市场"
        )
        val defaultGrid: ArrayList<GoodsInfo>
            get() {
                val gridArray = ArrayList<GoodsInfo>()
                for (i in gridImageArray.indices) {
                    gridArray.add(GoodsInfo(gridImageArray[i], gridTitleArray[i], null))
                }
                return gridArray
            }
        private val stagImageArray = intArrayOf(
            R.drawable.skirt01,
            R.drawable.skirt02,
            R.drawable.skirt03,
            R.drawable.skirt04,
            R.drawable.skirt05,
            R.drawable.skirt06,
            R.drawable.skirt07,
            R.drawable.skirt08,
            R.drawable.skirt09,
            R.drawable.skirt10,
            R.drawable.skirt11,
            R.drawable.skirt12,
            R.drawable.skirt13,
            R.drawable.skirt14,
            R.drawable.skirt15,
            R.drawable.skirt16,
            R.drawable.skirt17,
            R.drawable.skirt18,
            R.drawable.skirt19,
            R.drawable.skirt20,
            R.drawable.skirt21,
            R.drawable.skirt22,
            R.drawable.skirt23
        )
        private val stagTitleArray = arrayOf(
            "促销价", "惊爆价", "跳楼价", "白菜价", "清仓价", "割肉价",
            "实惠价", "一口价", "满意价", "打折价", "腰斩价", "无人问津", "算了吧", "大声点",
            "嘘嘘", "嗯嗯", "呼呼", "呵呵", "哈哈", "嘿嘿", "嘻嘻", "嗷嗷", "喔喔"
        )
        val defaultStag: ArrayList<GoodsInfo>
            get() {
                val stagArray = ArrayList<GoodsInfo>()
                for (i in stagImageArray.indices) {
                    stagArray.add(GoodsInfo(stagImageArray[i], stagTitleArray[i], null))
                }
                return stagArray
            }
        private val combineImageArray = intArrayOf(
            R.drawable.cainixihuan,
            R.drawable.dapaijiadao,
            R.drawable.trip_01,
            R.drawable.trip_02,
            R.drawable.trip_03,
            R.drawable.trip_04
        )
        private val combineTitleArray = arrayOf(
            "猜你喜欢", "大牌驾到", "买哪个", "别想了", "先下单", "包你满意"
        )
        val defaultCombine: ArrayList<GoodsInfo>
            get() {
                val combineArray = ArrayList<GoodsInfo>()
                for (i in combineImageArray.indices) {
                    combineArray.add(GoodsInfo(combineImageArray[i], combineTitleArray[i], null))
                }
                return combineArray
            }
        private val appiImageArray = intArrayOf(
            R.drawable.dian01,
            R.drawable.dian02,
            R.drawable.dian03,
            R.drawable.dian04,
            R.drawable.dian05,
            R.drawable.dian06,
            R.drawable.dian07,
            R.drawable.dian08,
            R.drawable.dian09,
            R.drawable.dian10,
            R.drawable.dian11,
            R.drawable.dian12,
            R.drawable.dian13,
            R.drawable.dian14,
            R.drawable.dian15
        )
        private val appiTitleArray = arrayOf(
            "双十一", "大聚惠", "爆款价",
            "就一次", "手慢无", "快点击", "付定金", "享特权", "包安装", "再返券",
            "白送你", "想得美", "干活去", "好好学", "才有钱"
        )
        val defaultAppi: ArrayList<GoodsInfo>
            get() {
                val appiArray = ArrayList<GoodsInfo>()
                for (i in appiImageArray.indices) {
                    appiArray.add(GoodsInfo(appiImageArray[i], appiTitleArray[i], null))
                }
                return appiArray
            }
    }

    init {
        id = seq
        seq++
    }
}