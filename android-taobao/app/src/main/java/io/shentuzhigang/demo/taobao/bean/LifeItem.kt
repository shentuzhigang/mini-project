package io.shentuzhigang.demo.taobao.bean

import io.shentuzhigang.demo.taobao.R
import java.util.*

class LifeItem(var pic: Int, var title: String) {
    companion object {
        val default: ArrayList<LifeItem>
            get() {
                val itemArray = ArrayList<LifeItem>()
                for (i in 0..39) {
                    itemArray.add(LifeItem(R.drawable.icon_transfer, "转账"))
                }
                return itemArray
            }
    }
}