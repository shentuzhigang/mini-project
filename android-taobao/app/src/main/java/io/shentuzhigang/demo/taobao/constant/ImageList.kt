package io.shentuzhigang.demo.taobao.constant

import io.shentuzhigang.demo.taobao.R
import java.util.*

/**
 * Created by ouyangshen on 2018/1/4.
 */
object ImageList {
    val default: ArrayList<Int?>?
        get() {
            val imageList = ArrayList<Int?>()
            imageList.add(R.drawable.banner_1)
            imageList.add(R.drawable.banner_2)
            imageList.add(R.drawable.banner_3)
            imageList.add(R.drawable.banner_4)
            imageList.add(R.drawable.banner_5)
            return imageList
        }
}