package io.shentuzhigang.demo.taobao.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.shentuzhigang.demo.taobao.R

class BookCoverFragment : Fragment() {
    protected var mView // 声明一个视图对象
            : View? = null
    protected var mContext // 声明一个上下文对象
            : Context? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = activity // 获取活动页面的上下文
        // 根据布局文件fragment_book_cover.xml生成视图对象
        mView = inflater.inflate(R.layout.fragment_book_cover, container, false)
        return mView
    }
}