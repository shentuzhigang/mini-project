package io.shentuzhigang.demo.taobao.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.shentuzhigang.demo.taobao.R

class TabFirstFragment : Fragment() {
    protected var mView // 声明一个视图对象
            : View? = null
    protected var mContext // 声明一个上下文对象
            : Context? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = activity // 获取活动页面的上下文
        // 根据布局文件fragment_tab_first.xml生成视图对象
        mView = inflater.inflate(R.layout.fragment_tab_first, container, false)
        // 根据碎片标签栏传来的参数拼接文本字符串
        val desc = String.format(
            "我是%s页面，来自%s",
            "首页", arguments!!.getString("tag")
        )
        val tv_first = mView?.findViewById<TextView>(R.id.tv_first)
        tv_first?.text = desc
        return mView
    }

    companion object {
        private const val TAG = "TabFirstFragment"
    }
}