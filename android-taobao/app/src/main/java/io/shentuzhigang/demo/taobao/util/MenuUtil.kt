package io.shentuzhigang.demo.taobao.util

import android.content.Context
import android.view.Menu
import android.view.ViewConfiguration
import android.view.Window
import java.lang.Boolean

/**
 * Created by ouyangshen on 2018/1/21.
 */
object MenuUtil {
    // 如果设备有物理菜单按键，需要将其屏蔽才能显示OverflowMenu
    // API18以下需要该函数在右上角强制显示选项菜单
    fun forceShowOverflowMenu(context: Context?) {
        try {
            val config = ViewConfiguration.get(context)
            val menuKeyField =
                ViewConfiguration::class.java.getDeclaredField("sHasPermanentMenuKey")
            if (menuKeyField != null) {
                menuKeyField.isAccessible = true
                menuKeyField.setBoolean(config, false)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // 显示OverflowMenu的Icon
    fun setOverflowIconVisible(featureId: Int, menu: Menu?) {
        // ActionBar的featureId是8，Toolbar的featureId是108
        if (featureId % 100 == Window.FEATURE_ACTION_BAR && menu != null) {
            if (menu.javaClass.simpleName == "MenuBuilder") {
                try {
                    // setOptionalIconsVisible是个隐藏方法，需要通过反射机制调用
                    val m = menu.javaClass.getDeclaredMethod(
                        "setOptionalIconsVisible", Boolean.TYPE
                    )
                    m.isAccessible = true
                    m.invoke(menu, true)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }
}