package io.shentuzhigang.demo.taobao.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.*
import android.widget.FrameLayout

object StatusBarUtil {
    private const val TAG_FAKE_STATUS_BAR_VIEW = "statusBarView"
    private const val TAG_MARGIN_ADDED = "marginAdded"

    // 获取顶部状态栏的高度
    fun getStatusBarHeight(context: Context): Int {
        var statusBarHeight = -1
        val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            // 根据资源ID获取响应的尺寸值
            statusBarHeight = context.resources.getDimensionPixelSize(resourceId)
        }
        return statusBarHeight
    }

    // 把页面内容顶到状态栏内部，看起来状态栏就像是悬浮在页面之上
    fun fullScreen(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                val decorView = window.decorView
                // 两个标志位要结合使用，表示让应用的主体内容占用系统状态栏的空间
                // 第三个标志位可让底部导航栏变透明View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                val option = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                decorView.systemUiVisibility = option
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            } else {
                val window = activity.window
                val attributes = window.attributes
                val flagTranslucentStatus = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                // 底部导航栏也可以弄成透明的
                //int flagTranslucentNavigation = WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
                attributes.flags = attributes.flags or flagTranslucentStatus
                //attributes.flags |= flagTranslucentNavigation;
                window.attributes = attributes
            }
            // 需要把状态栏颜色设置透明，这样才有悬浮的效果
            setStatusBarColor(activity, Color.TRANSPARENT)
        }
    }

    // 重置状态栏。即把状态栏颜色恢复为系统默认的黑色
    fun reset(activity: Activity) {
        setStatusBarColor(activity, Color.BLACK)
    }

    // 设置状态栏的背景色。对于Android4.4和Android5.0以上版本要区分处理
    fun setStatusBarColor(activity: Activity, color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.window.statusBarColor = color
                // 底部导航栏颜色也可以由系统设置
                //activity.getWindow().setNavigationBarColor(color);
            } else {
                setKitKatStatusBarColor(activity, color)
            }
            if (color == Color.TRANSPARENT) { // 透明背景表示要悬浮状态栏
                removeMarginTop(activity)
            } else { // 其它背景表示要恢复状态栏
                addMarginTop(activity)
            }
        }
    }

    // 添加顶部间隔，留出状态栏的位置
    private fun addMarginTop(activity: Activity) {
        val window = activity.window
        val contentView = window.findViewById<ViewGroup>(Window.ID_ANDROID_CONTENT)
        val child = contentView.getChildAt(0)
        if (TAG_MARGIN_ADDED != child.tag) {
            val params = child.layoutParams as FrameLayout.LayoutParams
            // 添加的间隔大小就是状态栏的高度
            params.topMargin += getStatusBarHeight(activity)
            child.layoutParams = params
            child.tag = TAG_MARGIN_ADDED
        }
    }

    // 移除顶部间隔，霸占状态栏的位置
    private fun removeMarginTop(activity: Activity) {
        val window = activity.window
        val contentView = window.findViewById<ViewGroup>(Window.ID_ANDROID_CONTENT)
        val child = contentView.getChildAt(0)
        if (TAG_MARGIN_ADDED == child.tag) {
            val params = child.layoutParams as FrameLayout.LayoutParams
            // 移除的间隔大小就是状态栏的高度
            params.topMargin -= getStatusBarHeight(activity)
            child.layoutParams = params
            child.tag = null
        }
    }

    // 对于Android4.4，系统没有提供设置状态栏颜色的方法，只能手工搞个假冒的状态栏来占坑
    private fun setKitKatStatusBarColor(activity: Activity, statusBarColor: Int) {
        val window = activity.window
        val decorView = window.decorView as ViewGroup
        // 先移除已有的冒牌状态栏
        val fakeView = decorView.findViewWithTag<View>(TAG_FAKE_STATUS_BAR_VIEW)
        if (fakeView != null) {
            decorView.removeView(fakeView) // 从根视图移除旧状态栏
        }
        // 再添加新来的冒牌状态栏
        val statusBarView = View(activity)
        val params = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, getStatusBarHeight(activity)
        )
        params.gravity = Gravity.TOP
        statusBarView.layoutParams = params
        statusBarView.setBackgroundColor(statusBarColor)
        statusBarView.tag = TAG_FAKE_STATUS_BAR_VIEW
        decorView.addView(statusBarView) // 往根视图添加新状态栏
    }
}