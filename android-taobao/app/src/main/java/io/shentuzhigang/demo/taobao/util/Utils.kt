package io.shentuzhigang.demo.taobao.util

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

/**
 * Created by ouyangshen on 2017/9/11.
 */
object Utils {
    // 根据手机的分辨率从 dp 的单位 转成为 px(像素)
    fun dip2px(context: Context, dpValue: Float): Int {
        // 获取当前手机的像素密度
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt() // 四舍五入取整
    }

    // 根据手机的分辨率从 px(像素) 的单位 转成为 dp
    fun px2dip(context: Context, pxValue: Float): Int {
        // 获取当前手机的像素密度
        val scale = context.resources.displayMetrics.density
        return (pxValue / scale + 0.5f).toInt() // 四舍五入取整
    }

    // 获得屏幕的宽度
    fun getScreenWidth(ctx: Context): Int {
        // 从系统服务中获取窗口管理器
        val wm = ctx.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val dm = DisplayMetrics()
        // 从默认显示器中获取显示参数保存到dm对象中
        wm.defaultDisplay.getMetrics(dm)
        return dm.widthPixels // 返回屏幕的宽度数值
    }

    // 获得屏幕的高度
    fun getScreenHeight(ctx: Context): Int {
        // 从系统服务中获取窗口管理器
        val wm = ctx.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val dm = DisplayMetrics()
        // 从默认显示器中获取显示参数保存到dm对象中
        wm.defaultDisplay.getMetrics(dm)
        return dm.heightPixels // 返回屏幕的高度数值
    }

    // 获得屏幕的像素密度
    fun getScreenDensity(ctx: Context): Float {
        // 从系统服务中获取窗口管理器
        val wm = ctx.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val dm = DisplayMetrics()
        // 从默认显示器中获取显示参数保存到dm对象中
        wm.defaultDisplay.getMetrics(dm)
        return dm.density // 返回屏幕的像素密度数值
    }
}