package io.shentuzhigang.demo.taobao.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import io.shentuzhigang.demo.taobao.R
import java.util.*

class BannerIndicator @JvmOverloads constructor(// 声明一个上下文对象
    private val mContext: Context, attrs: AttributeSet? = null
) : RelativeLayout(
    mContext, attrs
), View.OnClickListener {
    private var vp_banner // 声明一个翻页视图对象
            : ViewPager? = null
    private var pi_banner // 声明一个翻页指示器对象
            : PagerIndicator? = null
    private val mViewList: MutableList<ImageView> = ArrayList() // 声明一个图像视图队列

    // 设置广告图片队列
    fun setImage(imageList: ArrayList<Int?>?) {
        // 根据图片队列生成图像视图队列
        for (i in imageList!!.indices) {
            val imageID = imageList[i]
            val iv = ImageView(mContext)
            iv.layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT
            )
            iv.scaleType = ImageView.ScaleType.FIT_XY
            iv.setImageResource(imageID!!)
            iv.setOnClickListener(this)
            mViewList.add(iv)
        }
        // 设置翻页视图的图像翻页适配器
        vp_banner!!.adapter = ImageAdapater()
        // 给翻页视图添加页面变更监听器
        vp_banner!!.addOnPageChangeListener(BannerChangeListener())
        // 设置翻页视图默认显示第一页
        vp_banner!!.currentItem = 0
        // 设置翻页指示器的个数与间隔
        pi_banner!!.setCount(imageList.size, 15)
    }

    // 初始化视图
    private fun initView() {
        // 根据布局文件banner_indicator.xml生成视图对象
        val view = LayoutInflater.from(mContext).inflate(R.layout.banner_indicator, null)
        // 从布局文件中获取名叫vp_banner的翻页视图
        vp_banner = view.findViewById(R.id.vp_banner)
        // 从布局文件中获取名叫pi_banner的翻页指示器
        pi_banner = view.findViewById(R.id.pi_banner)
        addView(view) // 将该布局视图添加到横幅指示器中
    }

    // 定义一个图像翻页适配器
    private inner class ImageAdapater : PagerAdapter() {
        // 获取页面项的个数
        override fun getCount(): Int {
            return mViewList.size
        }

        override fun isViewFromObject(arg0: View, arg1: Any): Boolean {
            return arg0 === arg1
        }

        // 从容器中销毁指定位置的页面
        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(mViewList[position])
        }

        // 实例化指定位置的页面，并将其添加到容器中
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            container.addView(mViewList[position])
            return mViewList[position]
        }
    }

    override fun onClick(v: View) {
        // 获取翻页视图当前页面项的序号
        val position = vp_banner!!.currentItem
        // 触发点击监听器的onBannerClick方法
        mListener!!.onBannerClick(position)
    }

    // 设置广告图的点击监听器
    fun setOnBannerListener(listener: BannerClickListener?) {
        mListener = listener
    }

    // 声明一个广告图点击的监听器对象
    private var mListener: BannerClickListener? = null

    // 定义一个广告图片的点击监听器接口
    interface BannerClickListener {
        fun onBannerClick(position: Int)
    }

    // 定义一个广告轮播监听器
    private inner class BannerChangeListener : OnPageChangeListener {
        // 翻页状态改变时触发
        override fun onPageScrollStateChanged(arg0: Int) {}

        // 在翻页过程中触发
        override fun onPageScrolled(seq: Int, ratio: Float, offset: Int) {
            // 设置指示器高亮圆点的位置
            pi_banner!!.setCurrent(seq, ratio)
        }

        // 在翻页结束后触发
        override fun onPageSelected(seq: Int) {
            // 设置指示器高亮圆点的位置
            pi_banner!!.setCurrent(seq, 0f)
        }
    }

    companion object {
        private const val TAG = "BannerIndicator"
    }

    init {
        initView()
    }
}