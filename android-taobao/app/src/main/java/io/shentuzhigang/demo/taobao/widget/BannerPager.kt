package io.shentuzhigang.demo.taobao.widget

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.RelativeLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.util.Utils
import java.util.*

class BannerPager @JvmOverloads constructor(// 声明一个上下文对象
    private val mContext: Context, attrs: AttributeSet? = null
) : RelativeLayout(
    mContext, attrs
), View.OnClickListener {
    private var vp_banner // 声明一个翻页视图对象
            : ViewPager? = null
    private var rg_indicator // 声明一个单选组对象
            : RadioGroup? = null
    private val mViewList: MutableList<ImageView> = ArrayList() // 声明一个图像视图队列
    private var mInterval = 2000 // 轮播的时间间隔，单位毫秒

    // 开始广告轮播
    fun start() {
        // 延迟若干秒后启动滚动任务
        mHandler.postDelayed(mScroll, mInterval.toLong())
    }

    // 停止广告轮播
    fun stop() {
        // 移除滚动任务
        mHandler.removeCallbacks(mScroll)
    }

    // 设置广告轮播的时间间隔
    fun setInterval(interval: Int) {
        mInterval = interval
    }

    // 设置广告图片队列
    fun setImage(imageList: ArrayList<Int?>?) {
        val dip_15 = Utils.dip2px(mContext, 15f)
        // 根据图片队列生成图像视图队列
        for (i in imageList!!.indices) {
            val imageID = imageList[i]
            val iv = ImageView(mContext)
            iv.layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT
            )
            iv.scaleType = ImageView.ScaleType.FIT_XY
            iv.setImageResource(imageID!!)
            iv.setOnClickListener(this)
            mViewList.add(iv)
        }
        // 设置翻页视图的图像翻页适配器
        vp_banner!!.adapter = ImageAdapater()
        // 给翻页视图添加简单的页面变更监听器，此时只需重写onPageSelected方法
        vp_banner!!.addOnPageChangeListener(object : SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                // 高亮显示该位置的指示按钮
                setButton(position)
            }
        })
        // 根据图片队列生成指示按钮队列
        for (i in imageList.indices) {
            val radio = RadioButton(mContext)
            radio.layoutParams = RadioGroup.LayoutParams(dip_15, dip_15)
            radio.gravity = Gravity.CENTER
            radio.setButtonDrawable(R.drawable.indicator_selector)
            rg_indicator!!.addView(radio)
        }
        // 设置翻页视图默认显示第一页
        vp_banner!!.currentItem = 0
        // 默认高亮显示第一个指示按钮
        setButton(0)
    }

    // 设置选中单选组内部的哪个单选按钮
    private fun setButton(position: Int) {
        (rg_indicator!!.getChildAt(position) as RadioButton).isChecked = true
    }

    // 初始化视图
    private fun initView() {
        // 根据布局文件banner_pager.xml生成视图对象
        val view = LayoutInflater.from(mContext).inflate(R.layout.banner_pager, null)
        // 从布局文件中获取名叫vp_banner的翻页视图
        vp_banner = view.findViewById(R.id.vp_banner)
        // 从布局文件中获取名叫rg_indicator的单选组
        rg_indicator = view.findViewById(R.id.rg_indicator)
        addView(view) // 将该布局视图添加到横幅轮播条
    }

    private val mHandler = Handler() // 声明一个处理器对象

    // 定义一个滚动任务
    private val mScroll: Runnable = object : Runnable {
        override fun run() {
            scrollToNext() // 滚动广告图片
            // 延迟若干秒后继续启动滚动任务
            mHandler.postDelayed(this, mInterval.toLong())
        }
    }

    // 滚动到下一张广告图
    fun scrollToNext() {
        // 获得下一张广告图的位置
        var index = vp_banner!!.currentItem + 1
        if (index >= mViewList.size) {
            index = 0
        }
        // 设置翻页视图显示指定位置的页面
        vp_banner!!.currentItem = index
    }

    // 定义一个图像翻页适配器
    private inner class ImageAdapater : PagerAdapter() {
        // 获取页面项的个数
        override fun getCount(): Int {
            return mViewList.size
        }

        override fun isViewFromObject(arg0: View, arg1: Any): Boolean {
            return arg0 === arg1
        }

        // 从容器中销毁指定位置的页面
        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(mViewList[position])
        }

        // 实例化指定位置的页面，并将其添加到容器中
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            container.addView(mViewList[position])
            return mViewList[position]
        }
    }

    override fun onClick(v: View) {
        // 获取翻页视图当前页面项的序号
        val position = vp_banner!!.currentItem
        // 触发点击监听器的onBannerClick方法
        mListener!!.onBannerClick(position)
    }

    // 设置广告图的点击监听器
    fun setOnBannerListener(listener: BannerClickListener?) {
        mListener = listener
    }

    // 声明一个广告图点击的监听器对象
    private var mListener: BannerClickListener? = null

    // 定义一个广告图片的点击监听器接口
    interface BannerClickListener {
        fun onBannerClick(position: Int)
    }

    companion object {
        private const val TAG = "BannerPager"
    }

    init {
        initView()
    }
}