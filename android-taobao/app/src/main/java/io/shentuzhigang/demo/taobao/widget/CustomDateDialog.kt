package io.shentuzhigang.demo.taobao.widget

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TextView
import io.shentuzhigang.demo.taobao.R

class CustomDateDialog(context: Context?) : View.OnClickListener {
    private val dialog // 声明一个对话框对象
            : Dialog?
    private val view // 声明一个视图对象
            : View
    private val tv_title: TextView
    private val dp_date // 声明一个日期选择器对象
            : DatePicker

    // 设置日期对话框的标题文本
    fun setTitle(title: String?) {
        tv_title.text = title
    }

    // 设置日期对话框内部的年、月、日，以及日期变更监听器
    fun setDate(year: Int, month: Int, day: Int, listener: OnDateSetListener?) {
        dp_date.init(year, month, day, null)
        mDateSetListener = listener
    }

    // 显示对话框
    fun show() {
        // 设置对话框窗口的内容视图
        dialog!!.window!!.setContentView(view)
        // 设置对话框窗口的布局参数
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.show() // 显示对话框
    }

    // 关闭对话框
    fun dismiss() {
        // 如果对话框显示出来了，就关闭它
        if (dialog != null && dialog.isShowing) {
            dialog.dismiss()
        }
    }

    // 判断对话框是否显示
    val isShowing: Boolean
        get() = dialog?.isShowing ?: false

    override fun onClick(v: View) {
        if (v.id == R.id.btn_ok) { // 点击了确定按钮
            dismiss() // 关闭对话框
            if (mDateSetListener != null) { // 如果存在月份变更监听器
                dp_date.clearFocus() // 清除日期选择器的焦点
                // 回调监听器的onDateSet方法
                mDateSetListener!!.onDateSet(
                    dp_date.year,
                    dp_date.month + 1, dp_date.dayOfMonth
                )
            }
        }
    }

    // 声明一个日期变更的监听器对象
    private var mDateSetListener: OnDateSetListener? = null

    // 定义一个日期变更的监听器接口
    interface OnDateSetListener {
        fun onDateSet(year: Int, monthOfYear: Int, dayOfMonth: Int)
    }

    init {
        // 根据布局文件dialog_date.xml生成视图对象
        view = LayoutInflater.from(context).inflate(R.layout.dialog_date, null)
        // 创建一个指定风格的对话框对象
        dialog = Dialog(context!!, R.style.CustomDateDialog)
        tv_title = view.findViewById(R.id.tv_title)
        // 从布局文件中获取名叫dp_date的日期选择器
        dp_date = view.findViewById(R.id.dp_date)
        view.findViewById<View>(R.id.btn_ok).setOnClickListener(this)
    }
}