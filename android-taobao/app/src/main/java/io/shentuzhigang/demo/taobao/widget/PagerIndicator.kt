package io.shentuzhigang.demo.taobao.widget

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.LinearLayout
import io.shentuzhigang.demo.taobao.R
import io.shentuzhigang.demo.taobao.util.Utils

class PagerIndicator @JvmOverloads constructor(// 声明一个上下文对象
    private val mContext: Context, attrs: AttributeSet? = null
) : LinearLayout(
    mContext, attrs
) {
    private var mCount = 5 // 指示器的个数
    private var mPad // 两个圆点之间的间隔
            = 0
    private var mSeq = 0 // 当前指示器的序号
    private var mRatio = 0.0f // 已经移动的距离百分比
    private var mPaint // 声明一个画笔对象
            : Paint? = null
    private var mBackImage // 背景位图，通常是灰色圆点
            : Bitmap? = null
    private var mForeImage // 前景位图，通常是高亮的红色圆点
            : Bitmap? = null

    private fun init() {
        // 创建一个新的画笔
        mPaint = Paint()
        mPad = Utils.dip2px(mContext, 15f)
        // 从资源图片icon_point_n.png中得到背景位图对象
        mBackImage = BitmapFactory.decodeResource(resources, R.drawable.icon_point_n)
        // 从资源图片icon_point_c.png中得到前景位图对象
        mForeImage = BitmapFactory.decodeResource(resources, R.drawable.icon_point_c)
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        val left = (measuredWidth - mCount * mPad) / 2
        // 先绘制作为背景的几个灰色圆点
        for (i in 0 until mCount) {
            canvas.drawBitmap(mBackImage!!, (left + i * mPad).toFloat(), 0f, mPaint)
        }
        // 再绘制作为前景的高亮红点，该红点随着翻页滑动而左右滚动
        canvas.drawBitmap(mForeImage!!, left + (mSeq + mRatio) * mPad, 0f, mPaint)
    }

    // 设置指示器的个数，以及指示器之间的距离
    fun setCount(count: Int, pad: Int) {
        mCount = count
        mPad = Utils.dip2px(mContext, pad.toFloat())
        invalidate() // 立刻刷新视图
    }

    // 设置指示器当前移动到的位置，及其位移比率
    fun setCurrent(seq: Int, ratio: Float) {
        mSeq = seq
        mRatio = ratio
        invalidate() // 立刻刷新视图
    }

    companion object {
        private const val TAG = "PagerIndicator"
    }

    init {
        init()
    }
}