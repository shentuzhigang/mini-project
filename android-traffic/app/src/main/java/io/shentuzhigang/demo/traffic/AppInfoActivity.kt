package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.AppInfoAdapter
import io.shentuzhigang.demo.traffic.util.AppUtil

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class AppInfoActivity : AppCompatActivity() {
    private var lv_appinfo // 声明一个列表视图对象
            : ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_info)
        // 从布局文件中获取名叫lv_appinfo的列表视图
        lv_appinfo = findViewById(R.id.lv_appinfo)
        initTypeSpinner()
    }

    // 初始化应用类型的下拉框
    private fun initTypeSpinner() {
        val typeAdapter = ArrayAdapter(
            this,
            R.layout.item_select, typeArray
        )
        val sp_list = findViewById<Spinner>(R.id.sp_type)
        sp_list.prompt = "请选择应用类型"
        sp_list.adapter = typeAdapter
        sp_list.onItemSelectedListener = TypeSelectedListener()
        sp_list.setSelection(0)
    }

    private val typeArray = arrayOf("所有应用", "联网应用")

    internal inner class TypeSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            // 获取已安装的应用信息队列
            val appinfoList = AppUtil.getAppInfo(this@AppInfoActivity, arg2)
            // 构建一个应用信息的列表适配器
            val adapter = AppInfoAdapter(this@AppInfoActivity, appinfoList)
            // 给lv_appinfo设置应用信息列表适配器
            lv_appinfo!!.adapter = adapter
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }

    companion object {
        private const val TAG = "AppInfoActivity"
    }
}