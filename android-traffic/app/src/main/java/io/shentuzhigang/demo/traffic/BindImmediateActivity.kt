package io.shentuzhigang.demo.traffic

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.service.BindImmediateService
import io.shentuzhigang.demo.traffic.util.DateUtil

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class BindImmediateActivity : AppCompatActivity(), View.OnClickListener {
    private var mIntent // 声明一个意图对象
            : Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bind_immediate)
        tv_immediate = findViewById(R.id.tv_immediate)
        findViewById<View>(R.id.btn_start_bind).setOnClickListener(this)
        findViewById<View>(R.id.btn_unbind).setOnClickListener(this)
        // 创建一个通往立即绑定服务的意图
        mIntent = Intent(this, BindImmediateService::class.java)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_start_bind) { // 点击了绑定服务按钮
            // 绑定服务。如果服务未启动，则系统先启动该服务再进行绑定
            val bindFlag = bindService(mIntent, mFirstConn, BIND_AUTO_CREATE)
            Log.d(TAG, "bindFlag=$bindFlag")
        } else if (v.id == R.id.btn_unbind) { // 点击了解绑服务按钮
            if (mBindService != null) {
                // 解绑服务。如果先前服务立即绑定，则此时解绑之后自动停止服务
                unbindService(mFirstConn)
                mBindService = null
            }
        }
    }

    private var mBindService // 声明一个服务对象
            : BindImmediateService? = null
    private val mFirstConn: ServiceConnection = object : ServiceConnection {
        // 获取服务对象时的操作
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            // 如果服务运行于另外一个进程，则不能直接强制转换类型，
            // 否则会报错“java.lang.ClassCastException: android.os.BinderProxy cannot be cast to...”
            mBindService = (service as BindImmediateService.LocalBinder).service
            Log.d(TAG, "onServiceConnected")
        }

        // 无法获取到服务对象时的操作
        override fun onServiceDisconnected(name: ComponentName) {
            mBindService = null
            Log.d(TAG, "onServiceDisconnected")
        }
    }

    companion object {
        private const val TAG = "BindImmediateActivity"
        private var tv_immediate: TextView? = null
        private var mDesc = ""
        fun showText(desc: String?) {
            if (tv_immediate != null) {
                mDesc = String.format("%s%s %s\n", mDesc, DateUtil.getNowDateTime("HH:mm:ss"), desc)
                tv_immediate!!.text = mDesc
            }
        }
    }
}