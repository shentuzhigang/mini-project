package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.widget.CircleAnimation

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class CircleAnimationActivity : AppCompatActivity(), View.OnClickListener {
    private var mAnimation // 定义一个圆弧动画对象
            : CircleAnimation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_circle_animation)
        findViewById<View>(R.id.btn_play).setOnClickListener(this)
        val ll_layout = findViewById<LinearLayout>(R.id.ll_layout)
        // 创建一个新的圆弧动画
        mAnimation = CircleAnimation(this)
        // 把圆弧动画添加到线性布局ll_layout之中
        ll_layout.addView(mAnimation)
        // 渲染圆弧动画。渲染操作包括初始化与播放两个动作
        mAnimation!!.render()
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_play) {
            // 开始播放圆弧动画
            mAnimation!!.play()
        }
    }
}