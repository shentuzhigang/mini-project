package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.ImagePagerAdapater
import io.shentuzhigang.demo.traffic.bean.GoodsInfo
import java.util.*

//import android.support.v4.view.ViewPager;
//import android.support.v4.view.ViewPager.OnPageChangeListener;
//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class CustomPropertyActivity : AppCompatActivity(), OnPageChangeListener {
    private var goodsList: ArrayList<GoodsInfo>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_property)
        goodsList = GoodsInfo.defaultList
        // 构建一个商品图片的翻页适配器
        val adapter = ImagePagerAdapater(this, goodsList)
        // 从布局视图中获取名叫vp_content的翻页视图
        val vp_content = findViewById<ViewPager>(R.id.vp_content)
        // 给vp_content设置图片翻页适配器
        vp_content.adapter = adapter
        // 设置vp_content默认显示第一个页面
        vp_content.currentItem = 0
        // 给vp_content添加页面变化监听器
        vp_content.addOnPageChangeListener(this)
    }

    // 翻页状态改变时触发。arg0取值说明为：0表示静止，1表示正在滑动，2表示滑动完毕
    // 在翻页过程中，状态值变化依次为：正在滑动→滑动完毕→静止
    override fun onPageScrollStateChanged(arg0: Int) {}

    // 在翻页过程中触发。该方法的三个参数取值说明为 ：第一个参数表示当前页面的序号
    // 第二个参数表示当前页面偏移的百分比，取值为0到1；第三个参数表示当前页面的偏移距离
    override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

    // 在翻页结束后触发。arg0表示当前滑到了哪一个页面
    override fun onPageSelected(arg0: Int) {
        Toast.makeText(this, "您翻到的手机品牌是：" + goodsList!![arg0].name, Toast.LENGTH_SHORT).show()
    }
}