package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.widget.CustomDateDialog
import io.shentuzhigang.demo.traffic.widget.CustomMonthDialog
import io.shentuzhigang.demo.traffic.widget.CustomMonthDialog.OnMonthSetListener
import java.util.*

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint("DefaultLocale")
class DialogDateActivity : AppCompatActivity(), View.OnClickListener {
    private var tv_date: TextView? = null
    private var tv_month: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog_date)
        tv_date = findViewById(R.id.tv_date)
        findViewById<View>(R.id.btn_date).setOnClickListener(this)
        tv_month = findViewById(R.id.tv_month)
        findViewById<View>(R.id.btn_month).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_date) { // 点击了“选择日期”按钮
            showDateDialog() // 显示自定义的日期对话框
        } else if (v.id == R.id.btn_month) { // 点击了“选择月份”按钮
            showMonthDialog() // 显示自定义的月份对话框
        }
    }

    // 显示自定义的日期对话框
    private fun showDateDialog() {
        val calendar = Calendar.getInstance()
        // 创建一个自定义的日期对话框实例
        val dialog = CustomDateDialog(this)
        // 设置日期对话框上面的年、月、日，并指定日期变更监听器
        dialog.setDate(
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH], DateListener()
        )
        dialog.show() // 显示日期对话框
    }

    // 定义一个日期变更监听器，一旦点击对话框的确定按钮，就触发监听器的onDateSet方法
    private inner class DateListener : CustomDateDialog.OnDateSetListener {
        override fun onDateSet(year: Int, month: Int, day: Int) {
            val desc = String.format("您选择的日期是%d年%d月%d日", year, month, day)
            tv_date!!.text = desc
        }
    }

    // 显示自定义的月份对话框
    private fun showMonthDialog() {
        val calendar = Calendar.getInstance()
        // 创建一个自定义的月份对话框实例
        val dialog = CustomMonthDialog(this)
        // 设置月份对话框上面的年、月，并指定月份变更监听器
        dialog.setDate(
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH], MonthListener()
        )
        dialog.show() // 显示月份对话框
    }

    // 定义一个月份变更监听器，一旦点击对话框的确定按钮，就触发监听器的onMonthSet方法
    private inner class MonthListener : OnMonthSetListener {
        override fun onMonthSet(year: Int, month: Int) {
            val desc = String.format("您选择的月份是%d年%d月", year, month)
            tv_month!!.text = desc
        }
    }
}