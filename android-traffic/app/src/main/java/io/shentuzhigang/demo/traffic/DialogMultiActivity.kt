package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.bean.Friend
import io.shentuzhigang.demo.traffic.widget.DialogFriend
import io.shentuzhigang.demo.traffic.widget.DialogFriend.onAddFriendListener
import java.util.*

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class DialogMultiActivity : AppCompatActivity(), View.OnClickListener, onAddFriendListener {
    private var tv_result: TextView? = null
    private val phoneArray = arrayOf("15960238696", "15805910591", "18905710571")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog_multi)
        val tv_origin = findViewById<TextView>(R.id.tv_origin)
        tv_result = findViewById(R.id.tv_result)
        findViewById<View>(R.id.btn_friend).setOnClickListener(this)
        var origin = ""
        for (phone in phoneArray) {
            origin = String.format("%s  %s", origin, phone)
        }
        tv_origin.text = origin.trim { it <= ' ' }
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_friend) {
            val friendList = ArrayList<Friend>()
            for (phone in phoneArray) {
                friendList.add(Friend(phone))
            }
            // 在页面底部弹出一个添加好友对话框，其中第三个参数为确认监听器
            val dialog = DialogFriend(this, friendList, this)
            dialog.show()
        }
    }

    // 一旦用户点击对话框上的确定按钮，就触发监听器的addFriend方法
    override fun addFriend(friendList: List<Friend?>?) {
        var result = "添加的好友信息如下："
        // 循环取出好友的关系信息，并拼接成描述文字
        for (item in friendList!!) {
            result = String.format(
                "%s\n号码%s，关系是%s，%s访问朋友圈", result,
                item!!.phone, item.relation, if (item.admit_circle) "允许" else "禁止"
            )
        }
        tv_result!!.text = result
    }
}