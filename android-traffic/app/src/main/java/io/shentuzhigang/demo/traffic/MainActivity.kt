package io.shentuzhigang.demo.traffic

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.AppInfoActivity
import io.shentuzhigang.demo.traffic.BindDelayActivity
import io.shentuzhigang.demo.traffic.BindImmediateActivity
import io.shentuzhigang.demo.traffic.MobileAssistantActivity
import io.shentuzhigang.demo.traffic.PullRefreshActivity
import io.shentuzhigang.demo.traffic.RunnableActivity
import io.shentuzhigang.demo.traffic.ServiceNormalActivity
import io.shentuzhigang.demo.traffic.TrafficInfoActivity

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.btn_app_info).setOnClickListener(this)
        findViewById<View>(R.id.btn_traffic_info).setOnClickListener(this)
        findViewById<View>(R.id.btn_mobile_assistant).setOnClickListener(this)
    }

    override fun onClick(v: View) {
      if (v.id == R.id.btn_app_info) {
            val intent = Intent(this, AppInfoActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_traffic_info) {
            val intent = Intent(this, TrafficInfoActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.btn_mobile_assistant) {
            val intent = Intent(this, MobileAssistantActivity::class.java)
            startActivity(intent)
        }
    }
}