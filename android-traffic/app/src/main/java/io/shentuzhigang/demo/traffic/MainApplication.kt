package io.shentuzhigang.demo.traffic

import android.app.Application
import android.os.Build
import android.util.Log
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.database.TrafficDBHelper
import io.shentuzhigang.demo.traffic.util.NotifyUtil

/**
 * Created by ouyangshen on 2017/10/14.
 */
class MainApplication : Application() {
    // 声明一个公共的流量数据库帮助器
    var mTrafficHelper: TrafficDBHelper? = null
    override fun onCreate() {
        super.onCreate()
        // 在打开应用时对静态的应用实例赋值
        instance = this
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Android 8.0开始必须给每个通知分配对应的渠道
            NotifyUtil.createNotifyChannel(this, getString(R.string.app_name))
        }
        // 获得流量数据库帮助器的实例
        mTrafficHelper = TrafficDBHelper.Companion.getInstance(this, 1)
        // 打开流量数据库帮助器的写连接
        mTrafficHelper!!.openWriteLink()
        Log.d(TAG, "onCreate")
    }

    companion object {
        private const val TAG = "MainApplication"

        // 利用单例模式获取当前应用的唯一实例
        // 声明一个当前应用的静态实例
        var instance: MainApplication? = null
            private set
    }
}