package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.util.MeasureUtil

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint("DefaultLocale")
class MeasureLayoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_measure_layout)
        val ll_header = findViewById<LinearLayout>(R.id.ll_header)
        val tv_desc = findViewById<TextView>(R.id.tv_desc)
        // 计算获取线性布局的实际高度
        val height = MeasureUtil.getRealHeight(ll_header)
        val desc = String.format("上面下拉刷新头部的高度是%f", height)
        tv_desc.text = desc
    }
}