package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.util.MeasureUtil

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint("DefaultLocale")
class MeasureTextActivity : AppCompatActivity() {
    private var tv_desc: TextView? = null
    private var tv_text: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_measure_text)
        tv_desc = findViewById(R.id.tv_desc)
        tv_text = findViewById(R.id.tv_text)
        initSizeSpinner()
    }

    // 初始化文字大小的下拉框
    private fun initSizeSpinner() {
        val sizeAdapter = ArrayAdapter(
            this,
            R.layout.item_select, descArray
        )
        val sp_size = findViewById<Spinner>(R.id.sp_size)
        sp_size.prompt = "请选择文字大小"
        sp_size.adapter = sizeAdapter
        sp_size.onItemSelectedListener = SizeSelectedListener()
        sp_size.setSelection(0)
    }

    private val descArray = arrayOf("12sp", "15sp", "17sp", "20sp", "22sp", "25sp", "27sp", "30sp")
    private val sizeArray = intArrayOf(12, 15, 17, 20, 22, 25, 27, 30)

    internal inner class SizeSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            val text = tv_text!!.text.toString()
            val textSize = sizeArray[arg2]
            tv_text!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize.toFloat())
            // 计算获取指定文本的宽度（其实就是长度）
            val width = MeasureUtil.getTextWidth(text, textSize.toFloat()).toInt()
            // 计算获取指定文本的高度
            val height = MeasureUtil.getTextHeight(text, textSize.toFloat()).toInt()
            val desc = String.format("下面文字的宽度是%d ,高度是%d", width, height)
            tv_desc!!.text = desc
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }
}