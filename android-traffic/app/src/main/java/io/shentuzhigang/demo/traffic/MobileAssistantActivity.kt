package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.*
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.MainApplication
import io.shentuzhigang.demo.traffic.MobileConfigActivity
import io.shentuzhigang.demo.traffic.adapter.TrafficInfoAdapter
import io.shentuzhigang.demo.traffic.bean.AppInfo
import io.shentuzhigang.demo.traffic.service.TrafficService
import io.shentuzhigang.demo.traffic.util.AppUtil
import io.shentuzhigang.demo.traffic.util.DateUtil
import io.shentuzhigang.demo.traffic.util.SharedUtil
import io.shentuzhigang.demo.traffic.util.StringUtil
import io.shentuzhigang.demo.traffic.widget.CircleAnimation
import io.shentuzhigang.demo.traffic.widget.CustomDateDialog
import io.shentuzhigang.demo.traffic.widget.NoScrollListView
import java.util.*

/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint("DefaultLocale")
class MobileAssistantActivity : Activity(), View.OnClickListener,
    CustomDateDialog.OnDateSetListener {
    private var tv_day: TextView? = null
    private var rl_month: RelativeLayout? = null
    private var tv_month_traffic: TextView? = null
    private var rl_day: RelativeLayout? = null
    private var tv_day_traffic: TextView? = null
    private var nslv_traffic // 声明一个不滚动列表视图
            : NoScrollListView? = null
    private var mDay // 选择的日期
            = 0
    private var mNowDay // 今天的日期
            = 0
    private val traffic_month: Long = 0 // 月流量数据
    private var traffic_day: Long = 0 // 日流量数据
    private var limit_month // 月流量限额
            = 0
    private var limit_day // 日流量限额
            = 0
    private val line_width = 10
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_assistant)
        // 创建一个通往流量服务的意图
        val intent = Intent(this, TrafficService::class.java)
        // 启动指定意图的服务
        startService(intent)
        initView()
    }

    // 初始化各视图对象
    private fun initView() {
        tv_day = findViewById(R.id.tv_day)
        rl_month = findViewById(R.id.rl_month)
        tv_month_traffic = findViewById(R.id.tv_month_traffic)
        rl_day = findViewById(R.id.rl_day)
        tv_day_traffic = findViewById(R.id.tv_day_traffic)
        // 从布局文件中获取名叫nslv_traffic的不滚动列表视图
        nslv_traffic = findViewById(R.id.nslv_traffic)
        findViewById<View>(R.id.iv_menu).setOnClickListener(this)
        findViewById<View>(R.id.iv_refresh).setOnClickListener(this)
        // 从共享参数中读取月流量限额
        limit_month = SharedUtil.Companion.getIntance(this)!!.readInt("limit_month", 1024)
        // 从共享参数中读取日流量限额
        limit_day = SharedUtil.Companion.getIntance(this)!!.readInt("limit_day", 30)
        mNowDay = DateUtil.getNowDateTime("yyyyMMdd").toInt()
        mDay = mNowDay
        val day = DateUtil.getNowDateTime("yyyy年MM月dd日")
        tv_day?.setText(day)
        tv_day?.setOnClickListener(this)
        // 延迟500毫秒后开始刷新日流量数据
        mHandler.postDelayed(mDayRefresh, 500)
    }

    private val mHandler = Handler() // 声明一个处理器对象

    // 定义一个日流量的刷新任务
    private val mDayRefresh = Runnable { refreshTraffic(mDay) }
    override fun onClick(v: View) {
        if (v.id == R.id.tv_day) { // 点击了日期文本
            val calendar = Calendar.getInstance()
            // 弹出自定义的日期选择对话框
            val dialog = CustomDateDialog(this)
            dialog.setDate(
                calendar[Calendar.YEAR], calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH], this
            )
            dialog.show()
        } else if (v.id == R.id.iv_menu) { // 点击了三点菜单图标
            // 跳转到流量限额配置页面
            val intent = Intent(this, MobileConfigActivity::class.java)
            startActivity(intent)
        } else if (v.id == R.id.iv_refresh) { // 点击了转圈刷新图标
            mDay = mNowDay
            // 立即启动今天的流量刷新任务
            mHandler.post(mDayRefresh)
        }
    }

    override fun onDateSet(year: Int, month: Int, day: Int) {
        val date = String.format("%d年%d月%d日", year, month, day)
        tv_day!!.text = date
        mDay = year * 10000 + month * 100 + day
        // 选择完日期，立即启动流量刷新任务
        mHandler.post(mDayRefresh)
    }

    // 刷新指定日期的流量数据
    private fun refreshTraffic(day: Int) {
        val last_date = DateUtil.getAddDate("" + day, -1)
        // 查询数据库获得截止到昨日的应用流量
        val lastArray: ArrayList<AppInfo>? =
            MainApplication.instance?.mTrafficHelper?.query(
                "day=$last_date"
            )
        // 查询数据库获得截止到今日的应用流量
        val thisArray: ArrayList<AppInfo>? =
            MainApplication.instance?.mTrafficHelper?.query(
                "day=$day"
            )
        val newArray = ArrayList<AppInfo?>()
        traffic_day = 0
        // 截止到今日的应用流量减去截止到昨日的应用流量，二者之差便是今日的流量数据
        if (thisArray != null) {
            for (i in thisArray.indices) {
                val item = thisArray[i]
                if (lastArray != null) {
                    for (j in lastArray.indices) {
                        if (item.uid == lastArray[j].uid) {
                            item.traffic -= lastArray[j].traffic
                            break
                        }
                    }
                }
                traffic_day += item.traffic
                newArray.add(item)
            }
        }
        // 给流量信息队列补充每个应用的图标
        val fullArray = AppUtil.fillAppInfo(this, newArray)
        // 构建一个流量信息的列表适配器
        val adapter = TrafficInfoAdapter(this@MobileAssistantActivity, fullArray)
        // 给nslv_traffic设置流量信息列表适配器
        nslv_traffic!!.adapter = adapter
        showDayAnimation() // 显示日流量动画
        showMonthAnimation() // 显示月流量动画
    }

    // 显示日流量的圆弧动画
    private fun showDayAnimation() {
        rl_day!!.removeAllViews()
        val diameter = Math.min(rl_day!!.width, rl_day!!.height) - line_width * 2
        var desc = "今日已用流量" + StringUtil.formatData(traffic_day)
        // 创建日流量的圆弧动画
        val dayAnimation = CircleAnimation(this@MobileAssistantActivity)
        // 设置日流量动画的四周边界
        dayAnimation.setRect(
            (rl_day!!.width - diameter) / 2 + line_width,
            (rl_day!!.height - diameter) / 2 + line_width,
            (rl_day!!.width + diameter) / 2 - line_width,
            (rl_day!!.height + diameter) / 2 - line_width
        )
        val trafficM = traffic_day / 1024.0f / 1024.0f
        desc = if (trafficM > limit_day * 2) { // 超出两倍限额，则展示红色圆弧进度
            val end_angle =
                (if (trafficM > limit_day * 3) 360 else (trafficM - limit_day * 2) * 360 / limit_day) as Int
            dayAnimation.setAngle(0, end_angle)
            dayAnimation.setFront(Color.RED, line_width.toFloat(), Paint.Style.STROKE)
            String.format(
                "%s\n超出限额%s", desc,
                StringUtil.formatData(traffic_day - limit_day * 1024 * 1024)
            )
        } else if (trafficM > limit_day) { // 超出一倍限额，则展示橙色圆弧进度
            val end_angle =
                (if (trafficM > limit_day * 2) 360 else (trafficM - limit_day) * 360 / limit_day) as Int
            dayAnimation.setAngle(0, end_angle)
            dayAnimation.setFront(-0x6700, line_width.toFloat(), Paint.Style.STROKE)
            String.format(
                "%s\n超出限额%s", desc,
                StringUtil.formatData(traffic_day - limit_day * 1024 * 1024)
            )
        } else { // 未超出限额，则展示绿色圆弧进度
            val end_angle = (trafficM * 360 / limit_day).toInt()
            dayAnimation.setAngle(0, end_angle)
            dayAnimation.setFront(Color.GREEN, line_width.toFloat(), Paint.Style.STROKE)
            String.format(
                "%s\n剩余流量%s", desc,
                StringUtil.formatData(limit_day * 1024 * 1024 - traffic_day)
            )
        }
        rl_day!!.addView(dayAnimation)
        // 渲染日流量的圆弧动画
        dayAnimation.render()
        tv_day_traffic!!.text = desc
    }

    // 显示月流量的圆弧动画。未实现，读者可实践之
    private fun showMonthAnimation() {
        rl_month!!.removeAllViews()
        val diameter = Math.min(rl_month!!.width, rl_month!!.height) - line_width * 2
        tv_month_traffic!!.text = "本月已用流量待统计"
        // 创建月流量的圆弧动画
        val monthAnimation = CircleAnimation(this@MobileAssistantActivity)
        // 设置月流量动画的四周边界
        monthAnimation.setRect(
            (rl_month!!.width - diameter) / 2 + line_width,
            (rl_month!!.height - diameter) / 2 + line_width,
            (rl_month!!.width + diameter) / 2 - line_width,
            (rl_month!!.height + diameter) / 2 - line_width
        )
        monthAnimation.setAngle(0, 0)
        monthAnimation.setFront(Color.GREEN, line_width.toFloat(), Paint.Style.STROKE)
        rl_month!!.addView(monthAnimation)
        // 渲染月流量的圆弧动画
        monthAnimation.render()
    }

    companion object {
        private const val TAG = "MobileAssistantActivity"
    }
}