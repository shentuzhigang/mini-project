package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.*
import android.database.ContentObserver
import android.net.Uri
import android.os.*
import android.telephony.SmsManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.MainApplication
import io.shentuzhigang.demo.traffic.util.SharedUtil

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint(value = ["SetTextI18n", "DefaultLocale", "StaticFieldLeak"])
class MobileConfigActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_config)
        et_config_month = findViewById(R.id.et_config_month)
        et_config_day = findViewById(R.id.et_config_day)
        findViewById<View>(R.id.btn_config_save).setOnClickListener(this)
        findViewById<View>(R.id.btn_auto_adjust).setOnClickListener(this)
        // 从共享参数中获取每月的流量限额
        val limit_month: Int = SharedUtil.Companion.getIntance(this)!!
            .readInt("limit_month", 1024)
        // 从共享参数中获取每日的流量限额
        val limit_day: Int = SharedUtil.Companion.getIntance(this)!!
            .readInt("limit_day", 50)
        et_config_month?.setText("" + limit_month)
        et_config_day?.setText("" + limit_day)
        // 初始化短信的内容观察器
        initSmsObserver()
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_config_save) {
            // 把流量限额保存到共享参数中
            saveFlowConfig(
                et_config_month!!.text.toString().toInt(),
                et_config_day!!.text.toString().toInt()
            )
            Toast.makeText(this, "成功保存配置", Toast.LENGTH_SHORT).show()
            finish()
        } else if (v.id == R.id.btn_auto_adjust) {
            // 无需用户操作，自动发送短信
            sendSmsAuto(mCustomNumber, "18")
        }
    }

    // 短信发送事件
    private val SENT_SMS_ACTION = "com.example.custom.SENT_SMS_ACTION"

    // 短信接收事件
    private val DELIVERED_SMS_ACTION = "com.example.custom.DELIVERED_SMS_ACTION"

    // 无需用户操作，由App自动发送短信
    fun sendSmsAuto(phoneNumber: String?, message: String?) {
        // 以下指定短信发送事件的详细信息
        val sentIntent = Intent(SENT_SMS_ACTION)
        sentIntent.putExtra("phone", phoneNumber)
        sentIntent.putExtra("message", message)
        val sentPI =
            PendingIntent.getBroadcast(this, 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        // 以下指定短信接收事件的详细信息
        val deliverIntent = Intent(DELIVERED_SMS_ACTION)
        deliverIntent.putExtra("phone", phoneNumber)
        deliverIntent.putExtra("message", message)
        val deliverPI =
            PendingIntent.getBroadcast(this, 1, deliverIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        // 获取默认的短信管理器
        val smsManager = SmsManager.getDefault()
        // 开始发送短信内容。要确保打开发送短信的完全权限，不是那种还需提示的不完整权限
        smsManager.sendTextMessage(phoneNumber, null, message, sentPI, deliverPI)
    }

    private val mHandler = Handler() // 声明一个处理器对象
    private var mObserver // 声明一个短信获取的观察器对象
            : SmsGetObserver? = null

    // 初始化短信观察器
    private fun initSmsObserver() {
        //mSmsUri = Uri.parse("content://sms/inbox");
        //Android5.0之后似乎无法单独观察某个信箱，只能监控整个短信
        mSmsUri = Uri.parse("content://sms")
        mSmsColumn = arrayOf("address", "body", "date")
        // 创建一个短信观察器对象
        mObserver = SmsGetObserver(this, mHandler)
        // 给指定Uri注册内容观察器，一旦Uri内部发生数据变化，就触发观察器的onChange方法
        contentResolver.registerContentObserver(mSmsUri!!, true, mObserver!!)
    }

    override fun onDestroy() {
        // 注销内容观察器
        contentResolver.unregisterContentObserver(mObserver!!)
        super.onDestroy()
    }

    // 定义一个短信获取的观察器
    private class SmsGetObserver(context: Context, handler: Handler?) : ContentObserver(handler) {
        private val mContext // 声明一个上下文对象
                : Context

        override fun onChange(selfChange: Boolean) { // 观察到短信的内容提供器发生变化
            var sender = ""
            var content = ""
            // 构建一个查询短信的条件语句，这里使用移动号码测试，故而查找10086发来的短信
            val selection = String.format(
                "address='%s' and date>%d",
                mCustomNumber, System.currentTimeMillis() - 1000 * 60 * 60
            )
            // 通过内容解析器获取符合条件的结果集游标
            val cursor = mContext.contentResolver.query(
                mSmsUri!!, mSmsColumn, selection, null, " date desc"
            )
            // 循环取出游标所指向的所有短信记录
            while (cursor!!.moveToNext()) {
                sender = cursor.getString(0)
                content = cursor.getString(1)
                break
            }
            cursor.close()
            //content = "您办理的套餐内含数据总流量为1GB176MB，已使用310MB，剩余890MB。";
            var totalFlow = "0"
            if (sender == mCustomNumber) {
                // 解析流量校准短信里面的总流量数值
                totalFlow = findFlow(content, "总流量为", "，")
            }
            val flows = totalFlow.split("GB".toRegex()).toTypedArray()
            Log.d(TAG, "totalFlow=" + totalFlow + ", flows.length=" + flows.size)
            var flowData = 0
            if (totalFlow.contains("GB") && TextUtils.isDigitsOnly(flows[0])) {
                flowData += flows[0].toInt() * 1024
            }
            if (flows.size > 1 && TextUtils.isDigitsOnly(flows[1])) {
                flowData += flows[1].toInt()
            }
            if (et_config_month != null && flowData != 0) {
                et_config_month!!.setText("" + flowData)
                et_config_day!!.setText("" + flowData / 30)
                // 把流量限额保存到共享参数中
                saveFlowConfig(flowData, flowData / 30)
                Toast.makeText(
                    MainApplication.instance,
                    "流量校准成功",
                    Toast.LENGTH_SHORT
                ).show()
            }
            super.onChange(selfChange)
        }

        init {
            mContext = context
        }
    }

    companion object {
        private const val TAG = "MobileConfigActivity"
        private var et_config_month: EditText? = null
        private var et_config_day: EditText? = null

        // 把每月和每日的流量限额保存到共享参数中
        private fun saveFlowConfig(limit_month: Int, limit_day: Int) {
            SharedUtil.Companion.getIntance(MainApplication.instance)!!
                .writeInt("limit_month", limit_month)
            SharedUtil.Companion.getIntance(MainApplication.instance)!!
                .writeInt("limit_day", limit_day)
        }

        private const val mCustomNumber = "10086" // 中国移动的客服号码
        private var mSmsUri // 声明一个系统短信提供器的Uri对象
                : Uri? = null
        private lateinit var mSmsColumn // 声明一个短信记录的字段数组
                : Array<String>

        // 解析流量校准短信里面的流量数值
        private fun findFlow(sms: String, begin: String, end: String): String {
            val begin_pos = sms.indexOf(begin)
            if (begin_pos < 0) {
                return "未获取"
            }
            val sub_sms = sms.substring(begin_pos)
            val end_pos = sub_sms.indexOf(end)
            if (end_pos < 0) {
                return "未获取"
            }
            return if (end == "，") {
                sub_sms.substring(begin.length, end_pos)
            } else {
                sub_sms.substring(begin.length, end_pos + end.length)
            }
        }
    }
}