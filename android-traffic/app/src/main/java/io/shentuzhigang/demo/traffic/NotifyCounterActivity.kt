package io.shentuzhigang.demo.traffic

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R

//import androidx.appcompat.app.AppCompatActivity;
/**
 * Created by ouyangshen on 2017/10/14.
 */
class NotifyCounterActivity : AppCompatActivity(), View.OnClickListener {
    private var et_title: EditText? = null
    private var et_message: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notify_counter)
        et_title = findViewById(R.id.et_title)
        et_message = findViewById(R.id.et_message)
        findViewById<View>(R.id.btn_send_counter).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_send_counter) {
            val title = et_title!!.text.toString()
            val message = et_message!!.text.toString()
            sendCounterNotify(title, message)
        }
    }

    // 发送计时的通知消息（通知栏右边自动计时）
    private fun sendCounterNotify(title: String, message: String) {
        // 创建一个跳转到活动页面的意图
        val cancelIntent = Intent(this, MainActivity::class.java)
        // 创建一个用于页面跳转的延迟意图
        val deleteIntent = PendingIntent.getActivity(
            this,
            R.string.app_name, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        // 创建一个通知消息的构造器
        var builder = Notification.Builder(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Android 8.0开始必须给每个通知分配对应的渠道
            builder = Notification.Builder(this, getString(R.string.app_name))
        }
        builder.setDeleteIntent(deleteIntent) // 设置内容的清除意图
            .setAutoCancel(true) // 设置是否允许自动清除
            .setUsesChronometer(true) // 设置是否显示计数器
            .setProgress(100, 60, false) // 设置进度条与当前进度
            .setNumber(99) // 设置通知栏右下方的数字
            .setSmallIcon(R.drawable.ic_app) // 设置状态栏里的小图标
            .setTicker("提示消息来啦") // 设置状态栏里面的提示文本
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_app)) // 设置通知栏里面的大图标
            .setContentTitle(title) // 设置通知栏里面的标题文本
            .setContentText(message) // 设置通知栏里面的内容文本
        // 根据消息构造器构建一个通知对象
        val notify = builder.build()
        // 从系统服务中获取通知管理器
        val notifyMgr = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        // 使用通知管理器推送通知，然后在手机的通知栏就会看到该消息
        notifyMgr.notify(R.string.app_name, notify)
    }
}