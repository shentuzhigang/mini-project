package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R

/**
 * Created by ouyangshen on 2017/10/14.
 */
class NotifyProgressActivity : AppCompatActivity(), View.OnClickListener {
    private var pb_progress // 声明一个进度条对象
            : ProgressBar? = null
    private var et_progress: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notify_progress)
        // 从布局文件中获取名叫pb_progress的进度条
        pb_progress = findViewById(R.id.pb_progress)
        et_progress = findViewById(R.id.et_progress)
        findViewById<View>(R.id.btn_progress).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_progress) {
            val progress = et_progress!!.text.toString().toInt()
            // 设置进度条的当前进度
            pb_progress!!.progress = progress
        }
    }
}