package io.shentuzhigang.demo.traffic

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.service.MusicService

/**
 * Created by ouyangshen on 2017/10/14.
 */
class NotifyServiceActivity : AppCompatActivity(), View.OnClickListener {
    private var et_song: EditText? = null
    private var btn_send_service: Button? = null
    private var isPlaying = true // 是否正在播放
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notify_service)
        et_song = findViewById(R.id.et_song)
        btn_send_service = findViewById(R.id.btn_send_service)
        btn_send_service?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_send_service) {
            // 创建一个通往音乐服务的意图
            val intent = Intent(this, MusicService::class.java)
            intent.putExtra("is_play", isPlaying)
            intent.putExtra("song", et_song!!.text.toString())
            if (isPlaying) { // 正在播放
                // 启动音乐服务，使之开始播放
                startService(intent)
                btn_send_service!!.text = "停止播放音乐"
            } else { // 不在播放
                // 停止音乐服务，使之结束播放
                stopService(intent)
                btn_send_service!!.text = "开始播放音乐"
            }
            isPlaying = !isPlaying
        }
    }
}