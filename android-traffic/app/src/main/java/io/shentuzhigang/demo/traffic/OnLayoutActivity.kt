package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.OnLayoutActivity
import io.shentuzhigang.demo.traffic.util.Utils
import io.shentuzhigang.demo.traffic.widget.OffsetLayout

/**
 * Created by ouyangshen on 2017/10/14.
 */
class OnLayoutActivity : AppCompatActivity() {
    private var ol_content // 声明一个偏移布局对象
            : OffsetLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_layout)
        // 从布局文件中获取名叫ol_content的偏移布局
        ol_content = findViewById(R.id.ol_content)
        initOffsetSpinner()
    }

    // 初始化偏移大小的下拉框
    private fun initOffsetSpinner() {
        val offsetAdapter = ArrayAdapter(
            this,
            R.layout.item_select, descArray
        )
        val sp_offset = findViewById<Spinner>(R.id.sp_offset)
        sp_offset.prompt = "请选择偏移大小"
        sp_offset.adapter = offsetAdapter
        sp_offset.onItemSelectedListener = OffsetSelectedListener()
        sp_offset.setSelection(0)
    }

    private val descArray = arrayOf("无偏移", "向左偏移50", "向右偏移50", "向上偏移50", "向下偏移50")
    private val offsetArray = intArrayOf(0, -50, 50, -50, 50)

    internal inner class OffsetSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            val offset = Utils.dip2px(
                this@OnLayoutActivity, offsetArray[arg2]
                    .toFloat()
            )
            if (arg2 == 0 || arg2 == 1 || arg2 == 2) {
                // 设置偏移布局在水平方向上的偏移量
                ol_content!!.setOffsetHorizontal(offset)
            } else if (arg2 == 3 || arg2 == 4) {
                // 设置偏移布局在垂直方向上的偏移量
                ol_content!!.setOffsetVertical(offset)
            }
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }
}