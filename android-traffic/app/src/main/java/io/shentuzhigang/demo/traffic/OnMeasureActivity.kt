package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.PlanetListAdapter
import io.shentuzhigang.demo.traffic.bean.Planet
import io.shentuzhigang.demo.traffic.widget.NoScrollListView

/**
 * Created by ouyangshen on 2017/9/17.
 */
class OnMeasureActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_measure)
        val adapter1 = PlanetListAdapter(this, Planet.defaultList)
        // 从布局文件中获取名叫lv_planet的列表视图
        // lv_planet是系统自带的ListView，被ScrollView嵌套只能显示一行
        val lv_planet = findViewById<ListView>(R.id.lv_planet)
        lv_planet.adapter = adapter1
        lv_planet.onItemClickListener = adapter1
        lv_planet.onItemLongClickListener = adapter1
        val adapter2 = PlanetListAdapter(this, Planet.defaultList)
        // 从布局文件中获取名叫nslv_planet的不滚动列表视图
        // nslv_planet是自定义控件NoScrollListView，会显示所有行
        val nslv_planet = findViewById<NoScrollListView>(R.id.nslv_planet)
        nslv_planet.adapter = adapter2
        nslv_planet.onItemClickListener = adapter2
        nslv_planet.onItemLongClickListener = adapter2
    }
}