package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.util.MeasureUtil

/**
 * Created by ouyangshen on 2017/10/14.
 */
class PullRefreshActivity : AppCompatActivity(), View.OnClickListener {
    private var ll_header: LinearLayout? = null
    private var btn_pull: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_refresh)
        ll_header = findViewById(R.id.ll_header)
        btn_pull = findViewById(R.id.btn_pull)
        ll_header?.setVisibility(View.GONE)
        btn_pull?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        // 计算获取线性布局的实际高度
        val height = MeasureUtil.getRealHeight(ll_header).toInt()
        if (v.id == R.id.btn_pull) {
            if (!isStarted) { // 不在刷新，则开始下拉刷新
                mOffset = -height
                btn_pull!!.isEnabled = false
                // 立即开始下拉刷新任务
                mHandler.post(mRefresh)
            } else { // 已在刷新，则停止下拉刷新
                btn_pull!!.text = "开始下拉"
                ll_header!!.visibility = View.GONE
            }
            isStarted = !isStarted
        }
    }

    private var isStarted = false // 是否开始刷新
    private val mHandler = Handler() // 声明一个处理器对象
    private var mOffset = 0 // 刷新过程中的下拉偏移

    // 定义一个下拉刷新任务
    private val mRefresh: Runnable = object : Runnable {
        override fun run() {
            if (mOffset <= 0) { // 尚未下拉到位
                // 通过设置视图上方的间隔，达到布局缩进的效果
                ll_header!!.setPadding(0, mOffset, 0, 0)
                ll_header!!.visibility = View.VISIBLE
                mOffset += 8
                // 延迟八十毫秒后重复刷新任务
                mHandler.postDelayed(this, 80)
            } else { // 已经下拉到顶了
                btn_pull!!.text = "恢复页面"
                btn_pull!!.isEnabled = true
            }
        }
    }

    companion object {
        private const val TAG = "PullRefreshActivity"
    }
}