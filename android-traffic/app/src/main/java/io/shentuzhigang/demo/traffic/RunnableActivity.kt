package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R

/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint("SetTextI18n")
class RunnableActivity : AppCompatActivity(), View.OnClickListener {
    private var btn_runnable: Button? = null
    private var tv_result: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_runnable)
        btn_runnable = findViewById(R.id.btn_runnable)
        tv_result = findViewById(R.id.tv_result)
        btn_runnable?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_runnable) {
            if (!isStarted) { // 不在计数，则开始计数
                btn_runnable!!.text = "停止计数"
                // 立即启动计数任务
                mHandler.post(mCounter)
            } else { // 已在计数，则停止计数
                btn_runnable!!.text = "开始计数"
                // 立即取消计数任务
                mHandler.removeCallbacks(mCounter)
            }
            isStarted = !isStarted
        }
    }

    private var isStarted = false // 是否开始计数
    private val mHandler = Handler() // 声明一个处理器对象
    private var mCount = 0 // 计数值

    // 定义一个计数任务
    private val mCounter: Runnable = object : Runnable {
        override fun run() {
            mCount++
            tv_result!!.text = "当前计数值为：$mCount"
            // 延迟一秒后重复计数任务
            mHandler.postDelayed(this, 1000)
        }
    }

    companion object {
        private const val TAG = "RunnableActivity"
    }
}