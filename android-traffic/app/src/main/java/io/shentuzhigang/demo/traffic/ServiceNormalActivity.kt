package io.shentuzhigang.demo.traffic

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.service.NormalService
import io.shentuzhigang.demo.traffic.util.DateUtil

/**
 * Created by ouyangshen on 2017/10/14.
 */
@SuppressLint("StaticFieldLeak")
class ServiceNormalActivity : AppCompatActivity(), View.OnClickListener {
    private var mIntent // 声明一个意图对象
            : Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_normal)
        tv_normal = findViewById(R.id.tv_normal)
        findViewById<View>(R.id.btn_start).setOnClickListener(this)
        findViewById<View>(R.id.btn_stop).setOnClickListener(this)
        // 创建一个通往普通服务的意图
        mIntent = Intent(this, NormalService::class.java)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_start) { // 点击了启动服务按钮
            startService(mIntent) // 启动指定意图的服务
        } else if (v.id == R.id.btn_stop) { // 点击了停止服务按钮
            stopService(mIntent) // 停止指定意图的服务
        }
    }

    companion object {
        private var tv_normal: TextView? = null
        private var mDesc = ""
        fun showText(desc: String?) {
            if (tv_normal != null) {
                mDesc = String.format("%s%s %s\n", mDesc, DateUtil.getNowDateTime("HH:mm:ss"), desc)
                tv_normal!!.text = mDesc
            }
        }
    }
}