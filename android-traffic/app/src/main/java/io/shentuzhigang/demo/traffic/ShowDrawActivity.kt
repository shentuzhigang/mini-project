package io.shentuzhigang.demo.traffic

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.widget.DrawRelativeLayout

/**
 * Created by ouyangshen on 2017/10/14.
 */
class ShowDrawActivity : AppCompatActivity() {
    private var drl_content // 声明一个绘画布局对象
            : DrawRelativeLayout? = null
    private var btn_center: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_draw)
        // 从布局文件中获取名叫drl_content的绘画布局
        drl_content = findViewById(R.id.drl_content)
        btn_center = findViewById(R.id.btn_center)
        initTypeSpinner()
    }

    // 初始化绘图方式的下拉框
    private fun initTypeSpinner() {
        val drawAdapter = ArrayAdapter(
            this,
            R.layout.item_select, descArray
        )
        val sp_draw = findViewById<Spinner>(R.id.sp_draw)
        sp_draw.prompt = "请选择绘图方式"
        sp_draw.adapter = drawAdapter
        sp_draw.onItemSelectedListener = DrawSelectedListener()
        sp_draw.setSelection(0)
    }

    private val descArray = arrayOf(
        "不画图", "画矩形", "画圆角矩形", "画圆圈", "画椭圆",
        "onDraw画叉叉", "dispatchDraw画叉叉"
    )
    private val typeArray = intArrayOf(0, 1, 2, 3, 4, 5, 6)

    internal inner class DrawSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
            val type = typeArray[arg2]
            if (type == 5 || type == 6) {
                btn_center!!.visibility = View.VISIBLE
            } else {
                btn_center!!.visibility = View.GONE
            }
            // 设置绘图布局的绘制类型
            drl_content!!.setDrawType(type)
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }
}