package io.shentuzhigang.demo.traffic

import android.net.TrafficStats
import android.os.*
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.TrafficInfoAdapter
import io.shentuzhigang.demo.traffic.util.AppUtil
import io.shentuzhigang.demo.traffic.util.StringUtil

/**
 * Created by ouyangshen on 2017/10/14.
 */
class TrafficInfoActivity : AppCompatActivity() {
    private var tv_traffic // 声明一个列表视图对象
            : TextView? = null
    private var lv_traffic: ListView? = null
    private val mHandler = Handler() // 声明一个处理器对象
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_traffic_info)
        tv_traffic = findViewById(R.id.tv_traffic)
        // 从布局文件中获取名叫lv_traffic的列表视图
        lv_traffic = findViewById(R.id.lv_traffic)
        // 延迟50毫秒后开始刷新应用流量数据
        mHandler.postDelayed(mRefresh, 50)
    }

    // 定义一个刷新任务
    private val mRefresh = Runnable {
        val desc = String.format(
            """
    当前总共接收流量：%s
    　　其中接收数据流量：%s
    当前总共发送流量：%s
    　　其中发送数据流量：%s
    """.trimIndent(),
            StringUtil.formatData(TrafficStats.getTotalRxBytes()),  // 获取总共接收的流量数据
            StringUtil.formatData(TrafficStats.getMobileRxBytes()),  // 获取数据流量的接收数据
            StringUtil.formatData(TrafficStats.getTotalTxBytes()),  // 获取总共发送的流量数据
            StringUtil.formatData(TrafficStats.getMobileTxBytes())
        ) // 获取数据流量的发送数据
        tv_traffic!!.text = desc
        // 获取已安装的应用信息队列
        val appinfoList = AppUtil.getAppInfo(this@TrafficInfoActivity, 1)
        for (i in appinfoList!!.indices) {
            val item = appinfoList[i]
            // 根据应用编号获取该应用的接收流量数据
            // Android7之后，TrafficStats类的getUidRxBytes和getUidTxBytes只能查自身的流量。只有当前应用为系统应用之时，才能查其他应用的流量
            item!!.traffic = TrafficStats.getUidRxBytes(item.uid)
            appinfoList[i] = item
        }
        // 构建一个流量信息的列表适配器
        val adapter = TrafficInfoAdapter(this@TrafficInfoActivity, appinfoList)
        // 给lv_traffic设置流量信息列表适配器
        lv_traffic!!.adapter = adapter
    }

    companion object {
        private const val TAG = "TrafficInfoActivity"
    }
}