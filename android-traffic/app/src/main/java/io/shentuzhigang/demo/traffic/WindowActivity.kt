package io.shentuzhigang.demo.traffic

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import io.shentuzhigang.demo.traffic.R

/**
 * Created by ouyangshen on 2017/10/14.
 */
class WindowActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 请求窗口的特征。其中Window.FEATURE_NO_TITLE指的是去掉窗口顶部的导航栏
        window.requestFeature(Window.FEATURE_NO_TITLE)
        // 设置窗口的内容视图
        window.setContentView(R.layout.activity_window)
        // 设置窗口的布局参数（如宽度和高度）
//        getWindow().setLayout(400, 400);
        // 设置窗口的背景图片
//        getWindow().setBackgroundDrawableResource(R.drawable.icon_header);
        // 从窗口中获取名叫tv_info的文本视图
        val tv_info = window.findViewById<TextView>(R.id.tv_info)
        tv_info.text = "我在直接操作窗口啦"
    }
}