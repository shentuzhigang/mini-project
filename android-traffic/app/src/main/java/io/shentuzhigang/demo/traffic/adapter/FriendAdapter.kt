package io.shentuzhigang.demo.traffic.adapter

import android.content.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.FriendAdapter
import io.shentuzhigang.demo.traffic.bean.Friend
import io.shentuzhigang.demo.traffic.widget.DialogFriendRelation
import io.shentuzhigang.demo.traffic.widget.DialogFriendRelation.onSelectRelationListener

class FriendAdapter(context: Context, friendList: List<Friend>, listener: OnDeleteListener?) :
    BaseAdapter(), onSelectRelationListener {
    private val mContext // 声明一个上下文对象
            : Context
    private val names // 关系名称数组
            : Array<String>
    val friends // 声明一个朋友队列
            : List<Friend>

    override fun getCount(): Int {
        return friends.size
    }

    override fun getItem(position: Int): Any {
        return friends[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            holder = ViewHolder()
            // 根据布局文件item_friend.xml生成转换视图对象
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_friend, null)
            holder.rl_relation = convertView.findViewById(R.id.rl_relation)
            holder.tv_relation = convertView.findViewById(R.id.tv_relation)
            holder.ib_dropdown = convertView.findViewById(R.id.ib_dropdown)
            holder.tv_phone = convertView.findViewById(R.id.tv_phone)
            holder.rg_admit = convertView.findViewById(R.id.rg_admit)
            holder.rb_true = convertView.findViewById(R.id.rb_true)
            holder.rb_false = convertView.findViewById(R.id.rb_false)
            holder.tv_operation = convertView.findViewById(R.id.tv_operation)
            // 将视图持有者保存到转换视图当中
            convertView.tag = holder
        } else {
            // 从转换视图中获取之前保存的视图持有者
            holder = convertView.tag as ViewHolder
        }
        holder.tv_phone!!.text = friends[position].phone
        holder.tv_relation!!.text = friends[position].relation
        // 设置是否允许访问朋友圈的勾选监听器
        holder.rg_admit!!.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.rb_true) {
                friends[position].admit_circle = true
            } else if (checkedId == R.id.rb_false) {
                friends[position].admit_circle = false
            }
        }
        // 设置删除按钮的点击监听器
        holder.tv_operation!!.setOnClickListener { deleteListener?.onDeleteClick(position) }
        holder.rl_relation!!.setBackgroundResource(R.color.white)
        setRelationChangeListener(holder, position)
        return convertView
    }

    // 设置朋友关系变化的监听器
    private fun setRelationChangeListener(holder: ViewHolder, position: Int) {
        holder.rl_relation!!.setOnClickListener {
            var selected = 0
            for (i in names.indices) {
                if (names[i] == friends[position].relation) {
                    selected = i
                    break
                }
            }
            // 构建一个朋友关系的选择对话框
            val dialog = DialogFriendRelation(mContext, this@FriendAdapter)
            // 显示朋友关系对话框
            dialog.show(count - position, selected)
            holder.rl_relation!!.setBackgroundResource(R.color.grey)
        }
    }

    override fun setRelation(gap: Int, name: String?, value: String?) {
        friends[count - gap].relation = name!!
        friends[count - gap].value = value!!
        // 通知适配器数据发生变更，以便适配器及时刷新列表展示
        notifyDataSetChanged()
    }

    private val deleteListener: OnDeleteListener?

    // 定义一个删除朋友关系的监听器接口
    interface OnDeleteListener {
        fun onDeleteClick(position: Int)
    }

    // 定义一个视图持有者，以便重用列表项的视图资源
    inner class ViewHolder {
        var rl_relation: RelativeLayout? = null
        var tv_relation: TextView? = null
        var ib_dropdown: ImageView? = null
        var tv_phone: TextView? = null
        var rg_admit: RadioGroup? = null
        var rb_true: RadioButton? = null
        var rb_false: RadioButton? = null
        var tv_operation: TextView? = null
    }

    // 朋友信息适配器的构造函数，传入上下文、朋友队列，以及删除监听器
    init {
        mContext = context
        friends = friendList
        // 从资源文件arrays.xml中获取关系名称的字符串数组
        names = mContext.resources.getStringArray(R.array.relation_name)
        deleteListener = listener
    }
}