package io.shentuzhigang.demo.traffic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import io.shentuzhigang.demo.traffic.R
import java.util.*

class FriendRelationAdapter(context: Context, content_list: Array<String?>, selected: Int) :
    BaseAdapter() {
    private val mContext // 声明一个上下文对象
            : Context
    private val mContentList // 声明一个关系名称队列
            : ArrayList<String>
    private val mSelected // 选中记录的序号
            : Int

    override fun getCount(): Int {
        return mContentList.size
    }

    override fun getItem(arg0: Int): Any {
        return mContentList[arg0]
    }

    override fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            holder = ViewHolder()
            // 根据布局文件item_friend_relation.xml生成转换视图对象
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_friend_relation, null)
            holder.tv_friend_relation = convertView.findViewById(R.id.tv_friend_relation)
            // 将视图持有者保存到转换视图当中
            convertView.tag = holder
        } else {
            // 从转换视图中获取之前保存的视图持有者
            holder = convertView.tag as ViewHolder
        }
        holder.tv_friend_relation!!.text = mContentList[position]
        // 如果当前元素正是已选中的记录，则要高亮显示
        if (position == mSelected) {
            holder.tv_friend_relation!!.setBackgroundResource(R.color.blue)
            holder.tv_friend_relation!!.setTextColor(mContext.resources.getColor(R.color.white))
        }
        return convertView
    }

    // 定义一个视图持有者，以便重用列表项的视图资源
    inner class ViewHolder {
        var tv_friend_relation: TextView? = null
    }

    // 朋友关系适配器的构造函数，传入上下文、关系队列，以及选中记录的序号
    init {
        mContext = context
        mContentList = ArrayList()
        Collections.addAll(mContentList as MutableCollection<in String?>, *content_list)
        mSelected = selected
    }
}