package io.shentuzhigang.demo.traffic.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import androidx.viewpager.widget.PagerAdapter
import io.shentuzhigang.demo.traffic.bean.GoodsInfo
import java.util.*

//import android.support.v4.view.PagerAdapter;
class ImagePagerAdapater(context: Context, goodsList: ArrayList<GoodsInfo>?) : PagerAdapter() {
    private val mContext // 声明一个上下文对象
            : Context

    // 声明一个图像视图队列
    private val mViewList = ArrayList<ImageView>()

    // 声明一个商品信息队列
    private var mGoodsList: ArrayList<GoodsInfo>? = ArrayList()

    // 获取页面项的个数
    override fun getCount(): Int {
        return mViewList.size
    }

    override fun isViewFromObject(arg0: View, arg1: Any): Boolean {
        return arg0 === arg1
    }

    // 从容器中销毁指定位置的页面
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(mViewList[position])
    }

    // 实例化指定位置的页面，并将其添加到容器中
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        container.addView(mViewList[position])
        return mViewList[position]
    }

    // 获得指定页面的标题文本
    override fun getPageTitle(position: Int): CharSequence? {
        return mGoodsList!![position].name
    }

    // 图像翻页适配器的构造函数，传入上下文与商品信息队列
    init {
        mContext = context
        mGoodsList = goodsList
        // 给每个商品分配一个专用的图像视图
        for (i in mGoodsList!!.indices) {
            val view = ImageView(mContext)
            view.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
            view.setImageResource(mGoodsList!![i].pic)
            view.scaleType = ScaleType.FIT_CENTER
            // 把该商品的图像视图添加到图像视图队列
            mViewList.add(view)
        }
    }
}