package io.shentuzhigang.demo.traffic.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemLongClickListener
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.bean.Planet
import java.util.*

@SuppressLint("DefaultLocale")
class PlanetListAdapter(context: Context, planet_list: ArrayList<Planet>) : BaseAdapter(),
    OnItemClickListener, OnItemLongClickListener {
    private val mContext // 声明一个上下文对象
            : Context
    private val mPlanetList // 声明一个行星信息队列
            : ArrayList<Planet>

    // 获取列表项的个数
    override fun getCount(): Int {
        return mPlanetList.size
    }

    // 获取列表项的数据
    override fun getItem(arg0: Int): Any {
        return mPlanetList[arg0]
    }

    // 获取列表项的编号
    override fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

    // 获取指定位置的列表项视图
    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) { // 转换视图为空
            holder = ViewHolder() // 创建一个新的视图持有者
            // 根据布局文件item_list.xml生成转换视图对象
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, null)
            holder.iv_icon = convertView.findViewById(R.id.iv_icon)
            holder.tv_name = convertView.findViewById(R.id.tv_name)
            holder.tv_desc = convertView.findViewById(R.id.tv_desc)
            // 将视图持有者保存到转换视图当中
            convertView.tag = holder
        } else { // 转换视图非空
            // 从转换视图中获取之前保存的视图持有者
            holder = convertView.tag as ViewHolder
        }
        val planet = mPlanetList[position]
        holder.iv_icon!!.setImageResource(planet.image) // 显示行星的图片
        holder.tv_name!!.text = planet.name // 显示行星的名称
        holder.tv_desc!!.text = planet.desc // 显示行星的描述
        return convertView
    }

    // 定义一个视图持有者，以便重用列表项的视图资源
    inner class ViewHolder {
        var iv_icon // 声明行星图片的图像视图对象
                : ImageView? = null
        var tv_name // 声明行星名称的文本视图对象
                : TextView? = null
        var tv_desc // 声明行星描述的文本视图对象
                : TextView? = null
    }

    // 处理列表项的点击事件，由接口OnItemClickListener触发
    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        val desc = String.format(
            "您点击了第%d个行星，它的名字是%s", position + 1,
            mPlanetList[position].name
        )
        Toast.makeText(mContext, desc, Toast.LENGTH_LONG).show()
    }

    // 处理列表项的长按事件，由接口OnItemLongClickListener触发
    override fun onItemLongClick(
        parent: AdapterView<*>?,
        view: View,
        position: Int,
        id: Long
    ): Boolean {
        val desc = String.format(
            "您长按了第%d个行星，它的名字是%s", position + 1,
            mPlanetList[position].name
        )
        Toast.makeText(mContext, desc, Toast.LENGTH_LONG).show()
        return true
    }

    // 行星适配器的构造函数，传入上下文与行星队列
    init {
        mContext = context
        mPlanetList = planet_list
    }
}