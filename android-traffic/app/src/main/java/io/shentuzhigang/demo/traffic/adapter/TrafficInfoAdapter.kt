package io.shentuzhigang.demo.traffic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.bean.AppInfo
import io.shentuzhigang.demo.traffic.util.StringUtil
import java.util.*

class TrafficInfoAdapter(context: Context, appinfoList: ArrayList<AppInfo>?) : BaseAdapter() {
    private val mContext // 声明一个上下文对象
            : Context
    private val mAppInfoList // 声明一个应用信息队列
            : ArrayList<AppInfo>?

    // 获取列表项的个数
    override fun getCount(): Int {
        return mAppInfoList!!.size
    }

    // 获取列表项的数据
    override fun getItem(arg0: Int): Any {
        return mAppInfoList!![arg0]
    }

    // 获取列表项的编号
    override fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

    // 获取指定位置的列表项视图
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) { // 转换视图为空
            holder = ViewHolder() // 创建一个新的视图持有者
            // 根据布局文件item_traffic.xml生成转换视图对象
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_traffic, null)
            holder.iv_icon = convertView.findViewById(R.id.iv_icon)
            holder.tv_label = convertView.findViewById(R.id.tv_label)
            holder.tv_package_name = convertView.findViewById(R.id.tv_package_name)
            holder.tv_traffic = convertView.findViewById(R.id.tv_traffic)
            // 将视图持有者保存到转换视图当中
            convertView.tag = holder
        } else { // 转换视图非空
            // 从转换视图中获取之前保存的视图持有者
            holder = convertView.tag as ViewHolder
        }
        val item = mAppInfoList!![position]
        if (item.icon != null) {
            holder.iv_icon!!.setImageDrawable(item.icon) // 显示应用的图标
        }
        holder.tv_label!!.text = item.label // 显示应用的名称
        holder.tv_package_name!!.text = item.package_name // 显示应用的包名
        holder.tv_traffic!!.text = StringUtil.formatData(item.traffic) // 显示应用的流量
        return convertView
    }

    // 定义一个视图持有者，以便重用列表项的视图资源
    inner class ViewHolder {
        var iv_icon // 声明应用图标的图像视图对象
                : ImageView? = null
        var tv_label // 声明应用名称的文本视图对象
                : TextView? = null
        var tv_package_name // 声明应用包名的文本视图对象
                : TextView? = null
        var tv_traffic // 声明应用流量的文本视图对象
                : TextView? = null
    }

    // 流量信息适配器的构造函数，传入上下文与应用队列
    init {
        mContext = context
        mAppInfoList = appinfoList
    }
}