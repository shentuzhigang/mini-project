package io.shentuzhigang.demo.traffic.bean

import android.graphics.drawable.Drawable

class AppInfo {
    var uid = 0
    var label = ""
    var package_name = ""
    var icon: Drawable? = null
    var traffic: Long = 0
    var rowid: Long = 0
    var xuhao = 0
    var month = 0
    var day = 0
    var icon_path = ""
}