package io.shentuzhigang.demo.traffic.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import io.shentuzhigang.demo.traffic.bean.AppInfo
import java.util.*

@SuppressLint("DefaultLocale")
class TrafficDBHelper : SQLiteOpenHelper {
    private var mDB: SQLiteDatabase? = null // 数据库的实例

    private constructor(context: Context) : super(context, DB_NAME, null, DB_VERSION) {}
    private constructor(context: Context, version: Int) : super(context, DB_NAME, null, version) {}

    // 打开数据库的读连接
    fun openReadLink(): SQLiteDatabase? {
        if (mDB == null || !mDB!!.isOpen) {
            mDB = mHelper!!.readableDatabase
        }
        return mDB
    }

    // 打开数据库的写连接
    fun openWriteLink(): SQLiteDatabase? {
        if (mDB == null || !mDB!!.isOpen) {
            mDB = mHelper!!.writableDatabase
        }
        return mDB
    }

    // 关闭数据库连接
    fun closeLink() {
        if (mDB != null && mDB!!.isOpen) {
            mDB!!.close()
            mDB = null
        }
    }

    // 创建数据库，执行建表语句
    override fun onCreate(db: SQLiteDatabase) {
        Log.d(TAG, "onCreate")
        val drop_sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";"
        Log.d(TAG, "drop_sql:$drop_sql")
        db.execSQL(drop_sql)
        val create_sql = ("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "month INTEGER NOT NULL," + "day INTEGER NOT NULL,"
                + "uid INTEGER NOT NULL," + "label VARCHAR NOT NULL,"
                + "package_name VARCHAR NOT NULL," + "icon_path VARCHAR NOT NULL,"
                + "traffic LONG NOT NULL"
                + ");")
        Log.d(TAG, "create_sql:$create_sql")
        db.execSQL(create_sql)
    }

    // 修改数据库，执行表结构变更语句
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    // 根据指定条件删除表记录
    fun delete(condition: String?): Int {
        // 执行删除记录动作，该语句返回删除记录的数目
        return mDB!!.delete(TABLE_NAME, condition, null)
    }

    // 删除该表的所有记录
    fun deleteAll(): Int {
        // 执行删除记录动作，该语句返回删除记录的数目
        return mDB!!.delete(TABLE_NAME, "1=1", null)
    }

    // 往该表添加一条记录
    fun insert(info: AppInfo?): Long {
        val infoArray = ArrayList<AppInfo?>()
        infoArray.add(info)
        return insert(infoArray)
    }

    // 往该表添加多条记录
    fun insert(infoArray: ArrayList<AppInfo?>?): Long {
        var result: Long = -1
        for (info in infoArray!!) {
            // 如果存在相同rowid的记录，则更新记录
            if (info!!.rowid > 0) {
                val condition = String.format("rowid='%d'", info.rowid)
                update(info, condition)
                result = info.rowid
                continue
            }
            // 如果存在同样日期的uid，则更新记录
            if (info.day > 0 && info.uid > 0) {
                val condition = String.format("day=%d and uid=%d", info.day, info.uid)
                var tempArray = ArrayList<AppInfo>()
                tempArray = query(condition)
                if (tempArray.size > 0) {
                    update(info, condition)
                    result = tempArray[0].rowid
                    continue
                }
            }
            // 不存在唯一性重复的记录，则插入新记录
            val cv = ContentValues()
            cv.put("month", info.month)
            cv.put("day", info.day)
            cv.put("uid", info.uid)
            cv.put("label", info.label)
            cv.put("package_name", info.package_name)
            cv.put("icon_path", info.icon_path)
            cv.put("traffic", info.traffic)
            // 执行插入记录动作，该语句返回插入记录的行号
            result = mDB!!.insert(TABLE_NAME, "", cv)
            // 添加成功后返回行号，失败后返回-1
            if (result == -1L) {
                return result
            }
        }
        return result
    }

    // 根据条件更新指定的表记录
    @JvmOverloads
    fun update(info: AppInfo?, condition: String? = "rowid=" + info!!.rowid): Int {
        val cv = ContentValues()
        cv.put("month", info!!.month)
        cv.put("day", info.day)
        cv.put("uid", info.uid)
        cv.put("label", info.label)
        cv.put("package_name", info.package_name)
        cv.put("icon_path", info.icon_path)
        cv.put("traffic", info.traffic)
        // 执行更新记录动作，该语句返回记录更新的数目
        return mDB!!.update(TABLE_NAME, cv, condition, null)
    }

    // 根据指定条件查询记录，并返回结果数据队列
    fun query(condition: String?): ArrayList<AppInfo> {
        val sql = String.format(
            "select rowid,_id,month,day,uid,label,package_name,icon_path,traffic" +
                    " from %s where %s;", TABLE_NAME, condition
        )
        Log.d(TAG, "query sql: $sql")
        val infoArray = ArrayList<AppInfo>()
        // 执行记录查询动作，该语句返回结果集的游标
        val cursor = mDB!!.rawQuery(sql, null)
        // 循环取出游标指向的每条记录
        while (cursor.moveToNext()) {
            val info = AppInfo()
            info.rowid = cursor.getLong(0)
            info.xuhao = cursor.getInt(1)
            info.month = cursor.getInt(2)
            info.day = cursor.getInt(3)
            info.uid = cursor.getInt(4)
            info.label = cursor.getString(5)
            info.package_name = cursor.getString(6)
            info.icon_path = cursor.getString(7)
            info.traffic = cursor.getLong(8)
            infoArray.add(info)
        }
        cursor.close() // 查询完毕，关闭游标
        return infoArray
    }

    // 根据行号查询指定记录
    fun queryById(rowid: Long): AppInfo? {
        var info: AppInfo? = null
        val infoArray = query(String.format("rowid='%d'", rowid))
        if (infoArray.size > 0) {
            info = infoArray[0]
        }
        return info
    }

    companion object {
        private const val TAG = "TrafficDBHelper"
        private const val DB_NAME = "traffic.db" // 数据库的名称
        private const val DB_VERSION = 1 // 数据库的版本号
        private var mHelper: TrafficDBHelper? = null // 数据库帮助器的实例
        private const val TABLE_NAME = "traffic_info" // 表的名称

        // 利用单例模式获取数据库帮助器的唯一实例
        fun getInstance(context: Context, version: Int): TrafficDBHelper? {
            if (version > 0 && mHelper == null) {
                mHelper = TrafficDBHelper(context, version)
            } else if (mHelper == null) {
                mHelper = TrafficDBHelper(context)
            }
            return mHelper
        }
    }
}