package io.shentuzhigang.demo.traffic.service

import android.app.*
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import io.shentuzhigang.demo.traffic.BindDelayActivity

/**
 * Created by ouyangshen on 2017/10/14.
 */
class BindDelayService : Service() {
    // 创建一个粘合剂对象
    private val mBinder: IBinder = LocalBinder()

    // 定义一个当前服务的粘合剂，用于将该服务黏到活动页面的进程中
    inner class LocalBinder : Binder() {
        val service: BindDelayService
            get() = this@BindDelayService
    }

    private fun refresh(text: String) {
        Log.d(TAG, text)
        BindDelayActivity.Companion.showText(text)
    }

    override fun onCreate() { // 创建服务
        refresh("onCreate")
        super.onCreate()
    }

    override fun onStart(intent: Intent, startid: Int) { // 启动服务
        refresh("onStart")
        super.onStart(intent, startid)
    }

    override fun onDestroy() { // 销毁服务
        refresh("onDestroy")
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? { // 绑定服务。返回该服务的粘合剂对象
        Log.d(TAG, "绑定服务开始旅程！")
        refresh("onBind")
        return mBinder
    }

    override fun onRebind(intent: Intent) { // 重新绑定服务
        refresh("onRebind")
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean { // 解绑服务。返回false表示只能绑定一次，返回true表示允许多次绑定
        Log.d(TAG, "绑定服务结束旅程！")
        refresh("onUnbind")
        return true
    }

    companion object {
        private const val TAG = "BindDelayService"
    }
}