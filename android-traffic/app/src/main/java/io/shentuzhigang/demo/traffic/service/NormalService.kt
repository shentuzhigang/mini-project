package io.shentuzhigang.demo.traffic.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import io.shentuzhigang.demo.traffic.ServiceNormalActivity

/**
 * Created by ouyangshen on 2017/10/14.
 */
class NormalService : Service() {
    private fun refresh(text: String) {
        Log.d(TAG, text)
        ServiceNormalActivity.Companion.showText(text)
    }

    override fun onCreate() { // 创建服务
        refresh("onCreate")
        super.onCreate()
    }

    override fun onStart(intent: Intent, startid: Int) { // 启动服务，Android2.0以下使用
        refresh("onStart")
        super.onStart(intent, startid)
    }

    override fun onStartCommand(
        intent: Intent,
        flags: Int,
        startid: Int
    ): Int { // 启动服务，Android2.0以上使用
        Log.d(TAG, "测试服务到此一游！")
        refresh("onStartCommand. flags=$flags")
        return START_STICKY
    }

    override fun onDestroy() { // 销毁服务
        refresh("onDestroy")
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? { // 绑定服务。普通服务不存在绑定和解绑流程
        refresh("onBind")
        return null
    }

    override fun onRebind(intent: Intent) { // 重新绑定服务
        refresh("onRebind")
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean { // 解绑服务
        refresh("onUnbind")
        return true
    }

    companion object {
        private const val TAG = "NormalService"
    }
}