package io.shentuzhigang.demo.traffic.service

import android.app.*
import android.content.Intent
import android.net.TrafficStats
import android.os.*
import android.util.Log
import android.widget.RemoteViews
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.MainActivity
import io.shentuzhigang.demo.traffic.MainApplication
import io.shentuzhigang.demo.traffic.bean.AppInfo
import io.shentuzhigang.demo.traffic.util.AppUtil
import io.shentuzhigang.demo.traffic.util.DateUtil
import io.shentuzhigang.demo.traffic.util.SharedUtil
import io.shentuzhigang.demo.traffic.util.StringUtil

/**
 * Created by ouyangshen on 2017/10/14.
 */
class TrafficService : Service() {
    private var app // 声明一个应用对象
            : MainApplication? = null
    private var limit_day // 日限额
            = 0
    private var mNowDay // 今日日期
            = 0
    private var mNotify // 声明一个通知对象
            : Notification? = null

    override fun onStartCommand(intent: Intent, flags: Int, startid: Int): Int {
        // 获取当前应用的唯一实例
        app = MainApplication.instance
        // 从共享参数中获取日限额数值
        limit_day = SharedUtil.Companion.getIntance(this)!!.readInt("limit_day", 50)
        // 立即启动流量刷新任务
        mHandler.post(mRefresh)
        return START_STICKY
    }

    private val mHandler = Handler() // 声明一个处理器对象

    // 定义一个流量刷新任务
    private val mRefresh: Runnable = object : Runnable {
        override fun run() {
            // 更新流量数据库
            refreshData()
            // 刷新流量通知栏
            refreshNotify()
            // 延迟10秒后再次启动流量刷新任务
            mHandler.postDelayed(this, 10000)
        }
    }

    private fun refreshData() {
        mNowDay = DateUtil.getNowDateTime("yyyyMMdd").toInt()
        // 获取最新的应用信息队列
        val appinfoList = AppUtil.getAppInfo(this, 1)
        for (i in appinfoList.indices) {
            val item = appinfoList[i]
            // 获取该应用最新的流量接收数据
            item.traffic = TrafficStats.getUidRxBytes(item.uid)
            item.month = mNowDay / 100
            item.day = mNowDay
            appinfoList[i] = item
        }
        // 往流量数据库插入最新的应用流量记录
        app!!.mTrafficHelper?.insert(appinfoList as ArrayList<AppInfo?>?)
    }

    private fun refreshNotify() {
        val lastDate = DateUtil.getAddDate("" + mNowDay, -1)
        // 查询数据库获得截止到昨日的应用流量
        val lastArray = app!!.mTrafficHelper!!.query("day=$lastDate")
        // 查询数据库获得截止到今日的应用流量
        val thisArray = app!!.mTrafficHelper!!.query("day=$mNowDay")
        var traffic_day: Long = 0
        // 截止到今日的应用流量减去截止到昨日的应用流量，二者之差便是今日的流量数据
        for (i in thisArray.indices) {
            val item = thisArray[i]
            for (j in lastArray.indices) {
                if (item.uid == lastArray[j].uid) {
                    item.traffic -= lastArray[j].traffic
                    break
                }
            }
            traffic_day += item.traffic
        }
        val desc = "今日已用流量" + StringUtil.formatData(traffic_day)
        val progress: Int
        var layoutId = R.layout.notify_traffic_green
        val trafficM = traffic_day / 1024.0f / 1024.0f
        if (trafficM > limit_day * 2) { // 超出两倍限额，则展示红色进度条
            progress =
                (if (trafficM > limit_day * 3) 100 else (trafficM - limit_day * 2) * 100 / limit_day) as Int
            layoutId = R.layout.notify_traffic_red
        } else if (trafficM > limit_day) { // 超出一倍限额，则展示橙色进度条
            progress =
                (if (trafficM > limit_day * 2) 100 else (trafficM - limit_day) * 100 / limit_day) as Int
            layoutId = R.layout.notify_traffic_yellow
        } else { // 未超出限额，则展示绿色进度条
            progress = (trafficM * 100 / limit_day).toInt()
        }
        Log.d(TAG, "progress=$progress")
        // 显示流量通知
        showFlowNotify(layoutId, desc, progress)
    }

    private fun showFlowNotify(layoutId: Int, desc: String, progress: Int) {
        // 根据布局文件layoutId生成远程视图对象
        val notify_traffic = RemoteViews(this.packageName, layoutId)
        // 设置远程视图内部的流量文字描述
        notify_traffic.setTextViewText(R.id.tv_flow, desc)
        // 设置远程视图内部的进度条属性
        notify_traffic.setProgressBar(R.id.pb_flow, 100, progress, false)
        // 创建一个跳转到活动页面的意图
        val intent = Intent(this, MainActivity::class.java)
        // 创建一个用于页面跳转的延迟意图
        val clickIntent = PendingIntent.getActivity(
            this,
            R.string.app_name, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        // 创建一个通知消息的构造器
        var builder = Notification.Builder(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Android 8.0开始必须给每个通知分配对应的渠道
            builder = Notification.Builder(this, getString(R.string.app_name))
        }
        builder.setContentIntent(clickIntent) // 设置内容的点击意图
            .setContent(notify_traffic) // 设置内容视图
            .setTicker("手机安全助手运行中") // 设置状态栏里面的提示文本
            .setSmallIcon(R.drawable.ic_app) // 设置状态栏里的小图标
        // 根据消息构造器构建一个通知对象
        mNotify = builder.build()
        // 把服务推送到前台的通知栏
        startForeground(9, mNotify)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mNotify != null) {
            // 停止前台展示，也就是清除通知栏的流量消息
            stopForeground(true)
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    companion object {
        private const val TAG = "TrafficService"
    }
}