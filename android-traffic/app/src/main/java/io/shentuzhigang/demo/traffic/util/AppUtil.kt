package io.shentuzhigang.demo.traffic.util

import android.content.Context
import android.content.pm.PackageManager
import android.util.SparseIntArray
import io.shentuzhigang.demo.traffic.bean.AppInfo
import java.util.*

object AppUtil {
    // 获取已安装的应用信息队列
    fun getAppInfo(ctx: Context, type: Int): ArrayList<AppInfo> {
        val appList = ArrayList<AppInfo>()
        val siArray = SparseIntArray()
        // 获得应用包管理器
        val pm = ctx.packageManager
        // 获取系统中已经安装的应用列表
        val installList = pm.getInstalledApplications(
            PackageManager.PERMISSION_GRANTED
        )
        for (i in installList.indices) {
            val item = installList[i]
            // 去掉重复的应用信息
            if (siArray.indexOfKey(item.uid) >= 0) {
                continue
            }
            // 往siArray中添加一个应用编号，以便后续的去重校验
            siArray.put(item.uid, 1)
            try {
                // 获取该应用的权限列表
                val permissions = pm.getPackageInfo(
                    item.packageName,
                    PackageManager.GET_PERMISSIONS
                ).requestedPermissions ?: continue
                var isQueryNetwork = false
                for (permission in permissions) {
                    // 过滤那些具备上网权限的应用
                    if (permission == "android.permission.INTERNET") {
                        isQueryNetwork = true
                        break
                    }
                }
                // 类型为0表示所有应用，为1表示只要联网应用
                if (type == 0 || type == 1 && isQueryNetwork) {
                    val app = AppInfo()
                    app.uid = item.uid // 获取应用的编号
                    app.label = item.loadLabel(pm).toString() // 获取应用的名称
                    app.package_name = item.packageName // 获取应用的包名
                    app.icon = item.loadIcon(pm) // 获取应用的图标
                    appList.add(app)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                continue
            }
        }
        return appList // 返回去重后的应用包队列
    }

    // 填充应用的完整信息。主要做两个事情：其一是补充应用的图标字段，其二是将列表按照流量排序
    fun fillAppInfo(ctx: Context, originArray: ArrayList<AppInfo?>): ArrayList<AppInfo> {
        val fullArray = originArray.clone() as ArrayList<AppInfo>
        val pm = ctx.packageManager
        // 获取系统中已经安装的应用列表
        val installList = pm.getInstalledApplications(PackageManager.PERMISSION_GRANTED)
        for (i in fullArray.indices) {
            val app = fullArray[i]
            for (item in installList) {
                if (app.uid == item.uid) {
                    // 填充应用的图标信息。因为数据库没保存图标的位图，所以取出数据库记录之后还要补上图标数据
                    app.icon = item.loadIcon(pm)
                    break
                }
            }
            fullArray[i] = app
        }
        // 各应用按照流量大小降序排列
        Collections.sort(fullArray) { o1, o2 -> if (o1.traffic < o2.traffic) 1 else -1 }
        return fullArray
    }
}