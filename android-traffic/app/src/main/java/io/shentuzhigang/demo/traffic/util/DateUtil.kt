package io.shentuzhigang.demo.traffic.util

import android.annotation.SuppressLint
import android.text.TextUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ouyangshen on 2017/9/24.
 */
@SuppressLint("SimpleDateFormat")
object DateUtil {
    fun getNowDateTime(formatStr: String?): String {
        var format = formatStr
        if (TextUtils.isEmpty(format)) {
            format = "yyyyMMddHHmmss"
        }
        val sdf = SimpleDateFormat(format)
        return sdf.format(Date())
    }

    val nowTime: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss")
            return sdf.format(Date())
        }
    val nowTimeDetail: String
        get() {
            val sdf = SimpleDateFormat("HH:mm:ss.SSS")
            return sdf.format(Date())
        }

    fun getAddDate(str: String?, day_num: Long): String {
        val sdf = SimpleDateFormat("yyyyMMdd")
        val old_date: Date
        old_date = try {
            sdf.parse(str)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
        var time = old_date.time
        val diff_time = day_num * 24 * 60 * 60 * 1000
        //		LogUtil.debug(TAG, "day_num="+day_num+", diff_time="+diff_time);
        time += diff_time
        val new_date = Date(time)
        return sdf.format(new_date)
    }
}