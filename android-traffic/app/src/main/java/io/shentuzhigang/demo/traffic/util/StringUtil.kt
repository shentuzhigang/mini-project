package io.shentuzhigang.demo.traffic.util

import android.annotation.SuppressLint

@SuppressLint("DefaultLocale")
object StringUtil {
    // 保留小数点后面多少位
    fun formatWithString(value: Double, digit: Int): String {
        val format = String.format("%%.%df", digit)
        return String.format(format, value)
    }

    // 格式化流量数据/文件大小。输入以字节为单位的长整数，输出带具体单位的字符串
    fun formatData(data: Long): String {
        var result = ""
        result = if (data > 1024 * 1024) {
            String.format("%sM", formatWithString(data / 1024.0 / 1024.0, 1))
        } else if (data > 1024) {
            String.format("%sK", formatWithString(data / 1024.0, 1))
        } else {
            String.format("%sB", "" + data)
        }
        return result
    }
}