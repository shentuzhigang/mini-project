package io.shentuzhigang.demo.traffic.util

import android.content.Context

/**
 * Created by ouyangshen on 2017/9/11.
 */
object Utils {
    // 根据手机的分辨率从 dp 的单位 转成为 px(像素)
    fun dip2px(context: Context, dpValue: Float): Int {
        // 获取当前手机的像素密度
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt() // 四舍五入取整
    }

    // 根据手机的分辨率从 px(像素) 的单位 转成为 dp
    fun px2dip(context: Context, pxValue: Float): Int {
        // 获取当前手机的像素密度
        val scale = context.resources.displayMetrics.density
        return (pxValue / scale + 0.5f).toInt() // 四舍五入取整
    }

    // 根据手机的分辨率从 sp 的单位 转成为 px(像素)
    fun sp2px(context: Context, spValue: Float): Int {
        // 获取当前手机的像素密度
        val scale = context.resources.displayMetrics.density
        return (spValue * scale + 0.5f).toInt() // 四舍五入取整
    }

    // 根据手机的分辨率从 px(像素) 的单位 转成为 sp
    fun px2sp(context: Context, pxValue: Float): Int {
        // 获取当前手机的像素密度
        val scale = context.resources.displayMetrics.density
        return (pxValue / scale + 0.5f).toInt() // 四舍五入取整
    }
}