package io.shentuzhigang.demo.traffic.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import io.shentuzhigang.demo.traffic.util.Utils

class CircleAnimation(  // 声明一个上下文对象
    private var mContext: Context
) : RelativeLayout(mContext) {
    private var mRect // 矩形边界
            : RectF
    private var mBeginAngle = 0 // 起始角度
    private var mEndAngle = 270 // 终止角度
    private var mFrontColor = -0x10000 // 前景颜色
    private var mFrontLine = 10f // 前景线宽
    private var mFrontStyle = Paint.Style.STROKE // 前景风格。STROK表示空心，FILL表示实心
    private var mFrontView // 前景视图
            : FrontView? = null
    private var mShadeColor = -0x111112 // 阴影颜色
    private var mShadeLine = 10f // 阴影线宽
    private var mShadeStyle = Paint.Style.STROKE // 阴影风格。STROK表示空心，FILL表示实心
    private var mShadeView // 阴影视图
            : ShadeView? = null
    private var mRate = 2 // 每次绘制的递增角度
    private var mDrawTimes // 总共要绘制的次数
            = 0
    private var mInterval = 70 // 每次绘制之间的间隔时间，单位毫秒
    private var mFactor // 加速因子，让圆弧动画展现加速效果
            = 0
    private var mSeq = 0 // 已绘制的次数
    private var mDrawingAngle = 0 // 已绘制的角度

    // 渲染圆弧动画。渲染操作包括初始化与播放两个动作
    fun render() {
        removeAllViews() // 移除所有下级视图
        mShadeView = ShadeView(mContext) // 创建新的阴影视图
        addView(mShadeView) // 添加阴影视图
        mFrontView = FrontView(mContext) // 创建新的前景视图
        addView(mFrontView) // 添加前景视图
        play() // 播放圆弧动画
    }

    // 播放圆弧动画
    fun play() {
        mSeq = 0
        mDrawingAngle = 0
        mDrawTimes = mEndAngle / mRate
        mFactor = mDrawTimes / mInterval + 1
        Log.d(TAG, "mDrawTimes=$mDrawTimes,mInterval=$mInterval,mFactor=$mFactor")
        mFrontView!!.invalidateView() // 立即刷新前景视图
    }

    // 设置圆弧动画的矩形边界
    fun setRect(left: Int, top: Int, right: Int, bottom: Int) {
        mRect = RectF(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat())
    }

    // 设置圆弧动画的起始角度和终止角度
    fun setAngle(begin_angle: Int, end_angle: Int) {
        mBeginAngle = begin_angle
        mEndAngle = end_angle
    }

    // 设置动画播放参数。speed表示每次移动几个度数，frames表示每秒移动几帧
    fun setRate(speed: Int, frames: Int) {
        mRate = speed
        mInterval = 1000 / frames
    }

    // 设置圆弧前景的颜色、线宽和风格
    fun setFront(color: Int, line: Float, style: Paint.Style) {
        mFrontColor = color
        mFrontLine = line
        mFrontStyle = style
    }

    // 设置圆弧阴影的颜色、线宽和风格
    fun setShade(color: Int, line: Float, style: Paint.Style) {
        mShadeColor = color
        mShadeLine = line
        mShadeStyle = style
    }

    // 定义一个阴影视图
    private inner class ShadeView(context: Context?) : View(context) {
        private val paint // 声明一个画笔对象
                : Paint

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            // 绘制360度的阴影圆弧。第四个参数为true表示绘制扇形，为false表示绘制圆弧
            canvas.drawArc(mRect, mBeginAngle.toFloat(), 360f, false, paint)
        }

        init {
            paint = Paint() // 创建新画笔
            paint.isAntiAlias = true //设置画笔为无锯齿
            paint.isDither = true // 设置画笔为防抖动
            paint.color = mShadeColor // 设置画笔的颜色
            paint.strokeWidth = mShadeLine // 设置画笔的线宽
            paint.style = mShadeStyle // 设置画笔的类型。STROK表示空心，FILL表示实心
        }
    }

    // 定义一个前景视图
    private inner class FrontView(context: Context?) : View(context) {
        private val paint // 声明一个画笔对象
                : Paint
        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            // 绘制指定角度的阴影圆弧。第四个参数为true表示绘制扇形，为false表示绘制圆弧
            canvas.drawArc(mRect, mBeginAngle.toFloat(), mDrawingAngle.toFloat(), false, paint)
        }

        fun invalidateView() {
            // 立即启动绘制任务
            handler.post(drawRunnable)
        }

        // 定义一个绘制任务
        private val drawRunnable: Runnable = object : Runnable {
            override fun run() {
                if (mDrawingAngle >= mEndAngle) { // 绘制已完成
                    mDrawingAngle = mEndAngle
                    invalidate() // 立即触发视图的onDraw方法
                    handler.removeCallbacks(this) // 清除绘制任务
                } else { // 绘制未完成
                    mDrawingAngle = mSeq * mRate
                    mSeq++
                    // 间隔若干时间后，再次启动绘制任务
                    handler.postDelayed(this, (mInterval - mSeq / mFactor).toLong())
                    invalidate() // 立即触发视图的onDraw方法
                }
            }
        }

        init {
            paint = Paint() // 创建新画笔
            paint.isAntiAlias = true // 设置画笔为无锯齿
            paint.isDither = true // 设置画笔为防抖动
            paint.color = mFrontColor // 设置画笔的颜色
            paint.strokeWidth = mFrontLine // 设置画笔的线宽
            paint.style = mFrontStyle // 设置画笔的类型。STROK表示空心，FILL表示实心
            //paint.setStrokeJoin(Paint.Join.ROUND); // 设置画笔的接洽点类型。影响矩形直角的外轮廓
            paint.strokeCap = Paint.Cap.ROUND // 设置画笔的笔刷类型，影响画笔的始末端。ROUND表示圆角
        }
    }

    companion object {
        private const val TAG = "CicleAnimation"
    }

    init {
        mContext = mContext
        mRect = RectF(
            Utils.dip2px(mContext, 30f).toFloat(),
            Utils.dip2px(mContext, 10f).toFloat(),
            Utils.dip2px(
                mContext, 330f
            ).toFloat(),
            Utils.dip2px(mContext, 310f).toFloat()
        )
    }
}