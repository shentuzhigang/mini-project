package io.shentuzhigang.demo.traffic.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import androidx.viewpager.widget.PagerTabStrip
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.util.Utils

//import android.support.v4.view.PagerTabStrip;
class CustomPagerTab : PagerTabStrip {
    private var textColor = Color.BLACK // 文本颜色
    private var textSize = 15 // 文本大小

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (attrs != null) {
            // 根据CustomPagerTab的属性定义，从布局文件中获取属性数组描述
            val attrArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomPagerTab)
            // 根据属性描述定义，获取布局文件中的文本颜色
            textColor = attrArray.getColor(R.styleable.CustomPagerTab_textColor, textColor)
            // 根据属性描述定义，获取布局文件中的文本大小
            // getDimension得到的是px值，需要转换为sp值
            textSize = Utils.px2sp(
                context,
                attrArray.getDimension(R.styleable.CustomPagerTab_textSize, textSize.toFloat())
            )
            val customBackground =
                attrArray.getResourceId(R.styleable.CustomPagerTab_customBackground, 0)
            val customOrientation =
                attrArray.getInt(R.styleable.CustomPagerTab_customOrientation, 0)
            val customGravity = attrArray.getInt(R.styleable.CustomPagerTab_customGravity, 0)
            Log.d(TAG, "textColor=$textColor, textSize=$textSize")
            Log.d(
                TAG,
                "customBackground=$customBackground, customOrientation=$customOrientation, customGravity=$customGravity"
            )
            // 回收属性数组描述
            attrArray.recycle()
        }
    }

    //    //PagerTabStrip没有三个参数的构造函数
    //    public CustomPagerTab(Context context, AttributeSet attrs, int defStyleAttr) {
    //    }
    override fun onDraw(canvas: Canvas) { // 绘制函数
        setTextColor(textColor) // 设置标题文字的文本颜色
        setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize.toFloat()) // 设置标题文字的文本大小
        super.onDraw(canvas)
    }

    companion object {
        private const val TAG = "CustomPagerTab"
    }
}