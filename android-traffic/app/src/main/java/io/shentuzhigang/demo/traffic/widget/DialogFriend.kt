package io.shentuzhigang.demo.traffic.widget

import android.app.Dialog
import android.content.*
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.FriendAdapter
import io.shentuzhigang.demo.traffic.adapter.FriendAdapter.OnDeleteListener
import io.shentuzhigang.demo.traffic.bean.Friend

class DialogFriend(// 声明一个上下文对象
    private val mContext: Context, friendList: List<Friend>, listener: onAddFriendListener?
) : View.OnClickListener, OnDeleteListener {
    private val dialog // 声明一个对话框对象
            : Dialog?
    private val view // 声明一个视图对象
            : View
    private val tv_title: TextView
    private val lv_friend // 声明一个列表视图
            : ListView
    private val mFriendList: ArrayList<Friend>
    private val friendAdapter // 声明一个朋友信息适配器
            : FriendAdapter

    // 显示对话框
    fun show() {
        // 设置对话框窗口的内容视图
        dialog!!.window!!.setContentView(view)
        // 设置对话框窗口的布局参数
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        // 设置对话框为靠下对齐
        dialog.window!!.setGravity(Gravity.BOTTOM)
        // 允许取消对话框
        dialog.setCancelable(true)
        dialog.show() // 显示对话框
    }

    // 关闭对话框
    fun dismiss() {
        // 如果对话框显示出来了，就关闭它
        if (dialog != null && dialog.isShowing) {
            dialog.dismiss()
        }
    }

    // 判断对话框是否显示
    val isShowing: Boolean
        get() = dialog?.isShowing ?: false

    // 设置对话框的标题
    fun setTitle(title: String?) {
        tv_title.text = title
    }

    // 声明一个添加完成的监听器对象
    private val mOnAddFriendListener: onAddFriendListener?

    // 定义一个添加完成的监听器接口
    interface onAddFriendListener {
        fun addFriend(friendList: List<Friend?>?)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tv_ok) { // 点击了确定按钮
            dialog!!.dismiss() // 关闭对话框
            mOnAddFriendListener?.addFriend(friendAdapter.friends)
        }
    }

    // 点击了列表中的删除按钮，就触发这里的onDeleteClick方法
    override fun onDeleteClick(position: Int) {
        // 移除队列中的该记录
        mFriendList.removeAt(position)
        // 通知适配器发生了数据变更
        friendAdapter.notifyDataSetChanged()
    }

    // 对话框的构造函数，传入上下文、朋友队列，以及完成添加监听器
    init {
        // 根据布局文件dialog_friend.xml生成视图对象
        view = LayoutInflater.from(mContext).inflate(R.layout.dialog_friend, null)
        // 创建一个指定风格的对话框对象
        dialog = Dialog(mContext, R.style.dialog_layout_bottom)
        tv_title = view.findViewById(R.id.tv_title)
        // 从布局文件中获取名叫lv_friend的列表视图
        lv_friend = view.findViewById(R.id.lv_friend)
        view.findViewById<View>(R.id.tv_ok).setOnClickListener(this)
        mOnAddFriendListener = listener
        mFriendList = friendList as ArrayList<Friend>
        // 构建一个朋友信息适配器
        friendAdapter = FriendAdapter(mContext, mFriendList, this)
        // 给lv_friend设置朋友适配器
        lv_friend.adapter = friendAdapter
    }
}