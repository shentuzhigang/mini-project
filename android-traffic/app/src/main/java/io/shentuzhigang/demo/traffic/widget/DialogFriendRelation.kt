package io.shentuzhigang.demo.traffic.widget

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.GridView
import android.widget.LinearLayout
import io.shentuzhigang.demo.traffic.R
import io.shentuzhigang.demo.traffic.adapter.FriendRelationAdapter
import io.shentuzhigang.demo.traffic.util.Utils

class DialogFriendRelation(context: Context, listener: onSelectRelationListener?) :
    OnItemClickListener, DialogInterface.OnDismissListener {
    private val mContext // 声明一个上下文对象
            : Context
    private val dialog // 声明一个对话框对象
            : Dialog?
    private val view // 声明一个视图对象
            : View
    private val gv_relation // 声明一个网格视图
            : GridView
    private val ll_relation_gap: LinearLayout
    private var friendRelationAdapter // 声明一个朋友关系适配器
            : FriendRelationAdapter? = null
    private val relation_name_array // 关系名称数组
            : Array<String?>
    private val relation_value_array // 关系值数组
            : Array<String>
    private var mGap // 空白行。关系选择对话框紧贴着朋友列表上方，所以要计算下面空出多少行
            = 0
    private var mSelected // 当前选中的记录序号
            = 0

    // 显示对话框
    fun show(gap: Int, selected: Int) {
        mGap = gap
        mSelected = selected
        val dip_48 = Utils.dip2px(mContext, 48f)
        val dip_2 = Utils.dip2px(mContext, 2f)
        val params = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, dip_48 * (gap + 1) - dip_2 + gap
        )
        ll_relation_gap.layoutParams = params
        // 设置空白处的点击事件。一旦点击了空白处，就自动关闭对话框
        ll_relation_gap.setOnClickListener { dismiss() }
        // 构建一个朋友关系适配器
        friendRelationAdapter = FriendRelationAdapter(mContext, relation_name_array, selected)
        // 给gv_relation设置朋友关系适配器
        gv_relation.adapter = friendRelationAdapter
        gv_relation.onItemClickListener = this
        // 设置对话框窗口的内容视图
        dialog!!.window!!.setContentView(view)
        // 设置对话框窗口的布局参数
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        // 设置对话框为靠下对齐
        dialog.window!!.setGravity(Gravity.BOTTOM)
        dialog.show() // 显示对话框
        // 设置对话框的消失监听器，在关闭对话框时触发监听器的onDismiss方法
        dialog.setOnDismissListener(this)
    }

    // 关闭对话框
    fun dismiss() {
        // 如果对话框显示出来了，就关闭它
        if (dialog != null && dialog.isShowing) {
            dialog.dismiss()
        }
    }

    // 判断对话框是否显示
    val isShowing: Boolean
        get() = dialog?.isShowing ?: false

    // 点击某个网格项的时候，便触发onItemClick方法
    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        mSelected = position
        dismiss() // 关闭对话框
    }

    // 声明一个关系选择的监听器对象
    private val mOnSelectRelationListener: onSelectRelationListener?

    // 定义一个关系选择的监听器接口
    interface onSelectRelationListener {
        fun setRelation(gap: Int, name: String?, value: String?)
    }

    // 关闭对话框的时候，便触发onDismiss方法
    override fun onDismiss(dialog: DialogInterface) {
        mOnSelectRelationListener?.setRelation(
            mGap,
            relation_name_array[mSelected], relation_value_array[mSelected]
        )
    }

    // 对话框的构造函数，传入上下文，以及关系选择监听器
    init {
        mContext = context
        // 根据布局文件dialog_friend_relation.xml生成视图对象
        view = LayoutInflater.from(context).inflate(R.layout.dialog_friend_relation, null)
        // 创建一个指定风格的对话框对象
        dialog = Dialog(context, R.style.dialog_layout_bottom_transparent)
        // 从布局文件中获取名叫gv_relation的网格视图
        gv_relation = view.findViewById(R.id.gv_relation)
        ll_relation_gap = view.findViewById(R.id.ll_relation_gap)
        // 从资源文件arrays.xml中获取关系名称的字符串数组
        relation_name_array = context.resources.getStringArray(R.array.relation_name)
        // 从资源文件arrays.xml中获取关系值的字符串数组
        relation_value_array = context.resources.getStringArray(R.array.relation_value)
        mOnSelectRelationListener = listener
    }
}