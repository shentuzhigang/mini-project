package io.shentuzhigang.demo.traffic.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.RelativeLayout

class DrawRelativeLayout @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null) :
    RelativeLayout(context, attrs) {
    private var mDrawType = 0 // 绘制类型
    private val mPaint = Paint() // 创建一个画笔对象

    // onDraw方法在绘制下级视图之前调用
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val width = measuredWidth // 获得布局的实际宽度
        val height = measuredHeight // 获得布局的实际高度
        if (width > 0 && height > 0) {
            if (mDrawType == 1) { // 绘制矩形
                val rect = Rect(0, 0, width, height)
                canvas.drawRect(rect, mPaint)
            } else if (mDrawType == 2) { // 绘制圆角矩形
                val rectF = RectF(0F, 0F, width.toFloat(), height.toFloat())
                canvas.drawRoundRect(rectF, 30f, 30f, mPaint)
            } else if (mDrawType == 3) { // 绘制圆圈
                val radius = Math.min(width, height) / 2
                canvas.drawCircle(
                    (width / 2).toFloat(),
                    (height / 2).toFloat(),
                    radius.toFloat(),
                    mPaint
                )
            } else if (mDrawType == 4) { // 绘制椭圆
                val oval = RectF(0F, 0F, width.toFloat(), height.toFloat())
                canvas.drawOval(oval, mPaint)
            } else if (mDrawType == 5) { // 绘制矩形及其对角线
                val rect = Rect(0, 0, width, height)
                canvas.drawRect(rect, mPaint)
                canvas.drawLine(0f, 0f, width.toFloat(), height.toFloat(), mPaint)
                canvas.drawLine(0f, height.toFloat(), width.toFloat(), 0f, mPaint)
            }
        }
    }

    // dispatchDraw方法在绘制下级视图之前调用
    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        val width = measuredWidth // 获得布局的实际宽度
        val height = measuredHeight // 获得布局的实际高度
        if (width > 0 && height > 0) {
            if (mDrawType == 6) { // 绘制矩形及其对角线
                val rect = Rect(0, 0, width, height)
                canvas.drawRect(rect, mPaint)
                canvas.drawLine(0f, 0f, width.toFloat(), height.toFloat(), mPaint)
                canvas.drawLine(0f, height.toFloat(), width.toFloat(), 0f, mPaint)
            }
        }
    }

    // 设置绘制类型
    fun setDrawType(type: Int) {
        // 背景置为白色，目的是把画布擦干净
        setBackgroundColor(Color.WHITE)
        mDrawType = type
        // 立即重新绘图，此时会触发onDraw方法和dispatchDraw方法
        invalidate()
    }

    init {
        mPaint.isAntiAlias = true // 设置画笔为无锯齿
        mPaint.isDither = true // 设置画笔为防抖动
        mPaint.color = Color.BLACK // 设置画笔的颜色
        mPaint.strokeWidth = 3f // 设置画笔的线宽
        mPaint.style = Paint.Style.STROKE // 设置画笔的类型。STROKE表示空心，FILL表示实心
    }
}