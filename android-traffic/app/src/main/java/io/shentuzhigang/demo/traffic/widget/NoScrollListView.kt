package io.shentuzhigang.demo.traffic.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.ListView

class NoScrollListView : ListView {
    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
    }

    // 重写onMeasure方法，以便自行设定视图的高度
    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // 将高度设为最大值，即所有项加起来的总高度
        val expandSpec = MeasureSpec.makeMeasureSpec(
            Int.MAX_VALUE shr 2, MeasureSpec.AT_MOST
        )
        super.onMeasure(widthMeasureSpec, expandSpec)
    }
}