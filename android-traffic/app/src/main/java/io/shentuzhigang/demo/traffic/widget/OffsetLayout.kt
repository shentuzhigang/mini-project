package io.shentuzhigang.demo.traffic.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.AbsoluteLayout

class OffsetLayout : AbsoluteLayout {
    private var mOffsetHorizontal = 0 // 水平方向的偏移量
    private var mOffsetVertical = 0 // 垂直方向的偏移量

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}

    // 重写onLayout方法，意在调整下级视图的方位
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        for (i in 0 until childCount) {
            val child = getChildAt(i) // 获得第i个子视图
            if (child.visibility != GONE) {
                // 计算子视图的左边偏移数值
                val new_left = (r - 1) / 2 - child.measuredWidth / 2 + mOffsetHorizontal
                // 计算子视图的上方偏移数值
                val new_top = (b - t) / 2 - child.measuredHeight / 2 + mOffsetVertical
                // 根据最新的上下左右四周边界，重新放置该子视图
                child.layout(
                    new_left, new_top,
                    new_left + child.measuredWidth, new_top + child.measuredHeight
                )
            }
        }
    }

    // 设置水平方向上的偏移量
    fun setOffsetHorizontal(offset: Int) {
        mOffsetHorizontal = offset
        mOffsetVertical = 0
        // 请求重新布局，此时会触发onLayout方法
        requestLayout()
    }

    // 设置垂直方向上的偏移量
    fun setOffsetVertical(offset: Int) {
        mOffsetHorizontal = 0
        mOffsetVertical = offset
        // 请求重新布局，此时会触发onLayout方法
        requestLayout()
    }
}