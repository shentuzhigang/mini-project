# 书城项目
本项目源自尚硅谷书城项目。

项目采用`Go`原生`http`编程实现。

## 安装教程
```bash
go mod tidy
go install main.go
```

## 学习资源
- [Go基础](https://www.bilibili.com/video/BV1nJ411D7P4)
- [Go Web](https://www.bilibili.com/video/BV1ME411Y71o)
  
## 参考项目
- https://github.com/L1ng14/bookstore
## 参考文章