package controller

import (
	"bookstore/dao"
	"bookstore/model"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

// GetBooks 获取数据库中所有图书
func GetBooks(w http.ResponseWriter, r *http.Request) {
	books, err := dao.GetBooks()
	if err != nil {
		return
	}
	t := template.Must(template.ParseFiles("views/pages/manager/book_manager.html"))
	t.Execute(w, books)
}

// EditBook 编辑图书
func EditBook(w http.ResponseWriter, r *http.Request) {
	idStr := r.FormValue("bookId")

	t := template.Must(template.ParseFiles("views/pages/manager/book_edit.html"))

	if idStr == "" {
		//新增图书
		t.Execute(w, "")
		return
	}
	//更新图书处理
	id, errGetId := strconv.Atoi(idStr)
	if errGetId != nil {
		fmt.Println(w, "获取删除图书的ID出现异常。")
	} else {
		book, _ := dao.GetBookById(id)
		t.Execute(w, book)
	}
}

// UpdateOrSaveBook 更新或添加图书
func UpdateOrSaveBook(w http.ResponseWriter, r *http.Request) {
	//获取添加图书的参数
	title := r.PostFormValue("title")
	price, _ := strconv.ParseFloat(r.PostFormValue("price"), 64)
	author := r.PostFormValue("author")
	sales, _ := strconv.ParseInt(r.PostFormValue("sales"), 10, 0)
	stock, _ := strconv.ParseInt(r.PostFormValue("stock"), 10, 0)

	book := &model.Book{
		Title:  title,
		Author: author,
		Price:  price,
		Sales:  int(sales),
		Stock:  int(stock),
	}
	err := dao.AddBook(book)
	if err != nil {
		fmt.Println("添加出现异常，err:", err)
	}
	GetBooks(w, r)
}

// DeleteBook 根据图书 ID 删除一个图书
func DeleteBook(w http.ResponseWriter, r *http.Request) {
	id, errGetId := strconv.Atoi(r.FormValue("bookId"))
	if errGetId != nil {
		fmt.Println(w, "获取删除图书的ID出现异常。")
	}

	affect, errDelete := dao.DeleteBookById(id)
	if errDelete != nil {
		fmt.Fprintln(w, "删除图书出现异常,err:", errDelete)
		return
	}
	fmt.Println("受影响行数：", affect)
	//执行查找所有图书的处理函数，跳转到图书管理页面
	GetBooks(w, r)
}
