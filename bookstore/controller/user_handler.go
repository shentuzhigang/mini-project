package controller

import (
	"bookstore/dao"
	"bookstore/model"
	"bookstore/utils"
	"net/http"
	"text/template"
)

// Login 登录
func Login(w http.ResponseWriter, r *http.Request) {
	username := r.PostFormValue("username")
	password := r.PostFormValue("password")
	user, _ := dao.FindUserByUsername(username)
	if user != nil && user.ID > 0 && user.Password == utils.Md5(password) {
		t := template.Must(template.ParseFiles("views/pages/user/login_success.html"))
		t.Execute(w, "")
	} else {
		t := template.Must(template.ParseFiles("views/pages/user/login.html"))
		t.Execute(w, "用户名或密码不正确")
	}
}

// Regist 注册
func Regist(w http.ResponseWriter, r *http.Request) {
	username := r.PostFormValue("username")
	password := r.PostFormValue("password")
	email := r.PostFormValue("email")
	user, _ := dao.FindUserByUsername(username)
	if user != nil && user.ID > 0 {
		t := template.Must(template.ParseFiles("views/pages/user/regist.html"))
		t.Execute(w, "用户名已存在")
	} else {
		user = &model.User{
			Username: username,
			Password: password,
			Email:    email,
		}
		err := dao.AddUser(user)
		if err != nil {
			t := template.Must(template.ParseFiles("views/pages/user/regist.html"))
			t.Execute(w, "注册失败")
			return
		}
		t := template.Must(template.ParseFiles("views/pages/user/regist_success.html"))
		t.Execute(w, "")
	}
}

// CheckUsername 用户名检查
func CheckUsername(w http.ResponseWriter, r *http.Request) {
	username := r.PostFormValue("username")
	user, _ := dao.FindUserByUsername(username)
	if user != nil && user.ID > 0 {
		w.Write([]byte("用户名已存在"))
	} else {
		w.Write([]byte("<font style='color:blue'>用户名可用</font>"))
	}
}
