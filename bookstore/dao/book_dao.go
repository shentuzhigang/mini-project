package dao

import (
	"bookstore/model"
	"bookstore/utils"
	"fmt"
)

// GetBooks 获取数据库中所有图书
func GetBooks() ([]*model.Book, error) {
	sqlStr := "select id, title, author, price, sales, stock, img_path from books"
	rows, errQuery := utils.DB.Query(sqlStr)
	if errQuery != nil {
		fmt.Println("查询多行数据出错，err:", errQuery)
		return nil, errQuery
	}
	var books []*model.Book
	for rows.Next() {
		book := &model.Book{}
		errNotFound := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Price, &book.Sales, &book.Stock, &book.ImagePath)
		if errNotFound != nil {
			fmt.Println("赋值结果时出现异常，err :", errNotFound)
			return nil, errNotFound
		}
		books = append(books, book)
	}
	return books, nil
}

// GetBookByTitle 根据图书名查询一条记录
func GetBookByTitle(title string) (book *model.Book, err error) {
	sqlStr := "select id , title,author ,price ,sales ,stock ,img_path  from books where title = ?  "
	row := utils.DB.QueryRow(sqlStr, title)

	book = &model.Book{}
	errNotFound := row.Scan(&book.ID, &book.Title, &book.Author, &book.Price, &book.Sales, &book.Stock, &book.ImagePath)
	if errNotFound != nil {
		return nil, errNotFound
	}
	return book, nil
}

// GetBookById 根据图书ID查询一条记录
func GetBookById(id int) (book *model.Book, err error) {
	sqlStr := "select id , title,author ,price ,sales ,stock ,img_path  from books where id = ?  "
	row := utils.DB.QueryRow(sqlStr, id)

	book = &model.Book{}
	errNotFound := row.Scan(&book.ID, &book.Title, &book.Author, &book.Price, &book.Sales, &book.Stock, &book.ImagePath)
	if errNotFound != nil {
		return nil, errNotFound
	}
	return book, nil
}

// AddBook 新增一个图书
func AddBook(book *model.Book) error {
	//事务性，预编译
	sqlStr := "insert into books(title,author ,price ,sales ,stock ,img_path) values (?,?,?,?,?,?);"

	//Prepare创建一个准备好的状态用于之后的查询和命令。
	//返回值可以同时执行多个查询和命令。
	stmt, err := utils.DB.Prepare(sqlStr)
	if err != nil {
		fmt.Println("预编译 Prepare 出错，err :", err)
		return err
	}
	_, errExec := stmt.Exec(&book.Title, &book.Author, &book.Price, &book.Sales, &book.Stock, &book.ImagePath)
	if errExec != nil {
		fmt.Println("执行出错,err:", errExec)
		return errExec
	}
	//执行成功！
	return nil
}

// DeleteBookById 根据图书 ID 删除一个图书
func DeleteBookById(BookId int) (int64, error) {
	sqlStr := "delete from books where id =?"
	stmt, err := utils.DB.Prepare(sqlStr)
	if err != nil {
		fmt.Println("预编译 Prepare 出错，err :", err)
		return 0, err
	}
	res, errExec := stmt.Exec(BookId)
	if errExec != nil {
		fmt.Println("执行出错,err:", errExec)
		return 0, errExec
	}
	affect, errRes := res.RowsAffected()
	if errRes != nil {
		fmt.Println("取出受影响行数时出现异常，err:", errRes)
		return 0, errRes
	}
	return affect, nil
}
