package dao

import (
	"bookstore/model"
	"fmt"
	"testing"
)

func TestBook(t *testing.T) {
	t.Run("测试获取所有图书", TestGetBooks)
	t.Run("测试获取所有图书", TestAddBook)
}

func TestGetBooks(t *testing.T) {
	books, err := GetBooks()
	if err != nil {
		t.Fail()
		return
	}
	for k, v := range books {
		fmt.Printf("第%v本图书的信息是：%v\n", k+1, v)
	}
}

func TestAddBook(t *testing.T) {
	book := &model.Book{
		Title:     "三公子",
		Author:    "S",
		Price:     1,
		Sales:     100,
		ImagePath: "/static/img/default.jpg",
	}
	AddBook(book)
}

func TestGetBookById(t *testing.T) {
	GetBookById(1)
}

func TestGetBookByTitle(t *testing.T) {
	GetBookByTitle("三国演艺")
}

func TestDeleteBookById(t *testing.T) {
	DeleteBookById(1)
}
