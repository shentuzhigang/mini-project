package dao

import (
	"bookstore/model"
	"bookstore/utils"
	"fmt"
)

// FindUserByName 根据用户名查询一条记录
func FindUserByUsername(username string) (info *model.User, err error) {
	sqlStr := "select id ,username ,password ,email from users where username = ?  "
	row := utils.DB.QueryRow(sqlStr, username)
	info = &model.User{}
	errNotFound := row.Scan(&info.ID, &info.Username, &info.Password, &info.Email)
	if errNotFound != nil {
		return nil, errNotFound
	}
	return info, nil
}

// CheckUsername 判断用户是否存在
func CheckUsername(username string) (bool, error) {
	sqlStr := "select id  from users where username = ? "
	row := utils.DB.QueryRow(sqlStr, username)
	var id *int
	err := row.Scan(id)
	if err != nil {
		return false, err
	}
	return true, nil
}

// AddUser 新增一个用户
func AddUser(info *model.User) error {
	//事务性，预编译
	sqlStr := "insert into users(username,password,email) values (?,?,?);"

	//Prepare创建一个准备好的状态用于之后的查询和命令。
	//返回值可以同时执行多个查询和命令。
	stmt, err := utils.DB.Prepare(sqlStr)
	if err != nil {
		fmt.Println("预编译 Prepare 出错，err :", err)
		return err
	}
	//MD5 加密密码
	info.Password = utils.Md5(info.Password)
	_, errExec := stmt.Exec(info.Username, info.Password, info.Email)
	if errExec != nil {
		fmt.Println("执行出错,err:", errExec)
		return errExec
	}
	//执行成功！
	return nil
}

// DeleteUserById 根据用户 ID 删除一个用户
func DeleteUserById(userId int) (int64, error) {
	sqlStr := "delete from users where id =?"
	stmt, err := utils.DB.Prepare(sqlStr)
	if err != nil {
		fmt.Println("预编译 Prepare 出错，err :", err)
		return 0, err
	}
	res, errExec := stmt.Exec(userId)
	if errExec != nil {
		fmt.Println("执行出错,err:", errExec)
		return 0, errExec
	}
	affect, errRes := res.RowsAffected()
	if errRes != nil {
		fmt.Println("取出受影响行数时出现异常，err:", errRes)
		return 0, errRes
	}
	return affect, nil
}

// UpdatePwdById 根据用户 ID 修改用户的密码
func UpdatePwdById(id int, password string) (int64, error) {
	str := "update users set password =? where id = ?;"
	stmt, err := utils.DB.Prepare(str)
	if err != nil {
		fmt.Println("预编译 Prepare 出错，err :", err)
		return 0, err
	}
	//MD5 加密密码
	password = utils.Md5(password)
	res, errExec := stmt.Exec(password, id)
	if errExec != nil {
		fmt.Println("执行出错,err:", errExec)
		return 0, errExec
	}
	affect, errRes := res.RowsAffected()
	if errRes != nil {
		fmt.Println("取出受影响行数时出现异常，err:", errRes)
		return 0, errRes
	}
	return affect, nil
}
