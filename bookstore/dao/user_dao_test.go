package dao

import (
	"bookstore/model"
	"fmt"
	"testing"
)

func TestUser(t *testing.T) {
	fmt.Println("test user_dao")
	if t.Run("add", testAddUser) {
		t.Run("find", testFindUserByUsername)
	}
}

func testAddUser(t *testing.T) {
	user := &model.User{
		ID:       1,
		Username: "admin",
		Password: "123456",
		Email:    "admin@bookstore.com",
	}
	err := AddUser(user)
	if err != nil {
		t.Fail()
	}
}

func testFindUserByUsername(t *testing.T) {
	user, err := FindUserByUsername("admin")
	if err != nil || user == nil {
		t.Fail()
	}
}
