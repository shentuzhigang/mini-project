package main

import (
	"bookstore/controller"
	"net/http"
	"text/template"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	// 解析模板
	t := template.Must(template.ParseFiles("./views/index.html"))
	// 执行
	t.Execute(w, "")
}

func main() {
	// 设置处理静态资源
	http.Handle("/static/",
		http.StripPrefix("/static/",
			http.FileServer(
				http.Dir("./views/static"))))
	http.Handle("/pages/",
		http.StripPrefix("/pages/",
			http.FileServer(
				http.Dir("./views/pages"))))

	http.HandleFunc("/main", IndexHandler)

	// 登录
	http.HandleFunc("/login", controller.Login)

	// 注册
	http.HandleFunc("/regist", controller.Regist)

	// 用户名检查
	http.HandleFunc("/checkUsername", controller.CheckUsername)

	// 所有图书
	http.HandleFunc("/books", controller.GetBooks)

	// 编辑图书
	http.HandleFunc("/editBook", controller.EditBook)

	// 添加或者更新图书
	http.HandleFunc("/updateOrAddBook", controller.UpdateOrSaveBook)

	// 删除图书
	http.HandleFunc("/deleteBook", controller.DeleteBook)

	http.ListenAndServe(":8080", nil)
}
