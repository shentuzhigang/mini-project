import re
import requests
import openpyxl

cookie = 'HWWAFSESID=784aa7396e96bc89ca; HWWAFSESTIME=1648201531102; ARK_STARTUP=eyJTVEFSVFVQIjp0cnVlLCJTVEFSVFVQVElNRSI6IjIwMjItMDMtMjUgMTc6NDU6MjkuMjg1In0%3D; ARK_ID=JS753b770ca387a7e47d8fb34fdb6e0d1e753b; _dx_uzZo5y=929b077ee76d7b5fe8982f33f2c2bffe8be7aa9ef9cde82774959a3a72576f539cefc56d; xid=WWDLOwYqYqr; ALIASCACHE=WbrHKiiyPFu; mstoken=84872536fa5b9812c695b98e862370a5; msOpenId=ESGa5V; msUserCode=WbrHKiiyPFu; shortCode=; FZ_STROAGE.aikucun.com=eyJBUktTVVBFUiI6eyJzZXJ2ZXJSb2xlIjoiMiIsImNsaWVudCI6Img1IiwiY29tbW9uX2lkIjoiMTY0ODQzMzA2NDQ5OSIsInVzZXJfaWRfc291cmNlIjoiMSIsInNob3BfaWQiOiJXV0RMT3dZcVlxciIsInVzZXJfaWQiOiJXYnJIS2lpeVBGdSJ9LCJTRUVTSU9OSUQiOiIyYmIzYmRhNTliMmJmMzY0IiwiU0VFU0lPTkRBVEUiOjE2NDg0MzMwODU5ODUsIkFOU0FQUElEIjoiNDlkYjVkNTJjMTQ4MDIwMSIsIkFOUyRERUJVRyI6MCwiQU5TVVBMT0FEVVJMIjoiaHR0cHM6Ly9hcmstY29sbGVjdG9yLmFpa3VjdW4uY29tLyIsIkZSSVNUREFZIjoiMjAyMjAzMjUiLCJGUklTVElNRSI6ZmFsc2UsIkFSS19JRCI6IkpTNzUzYjc3MGNhMzg3YTdlNDdkOGZiMzRmZGI2ZTBkMWU3NTNiIiwiQVJLRlJJU1RQUk9GSUxFIjoiMjAyMi0wMy0yNSAxNzo0NToyOS4yODkiLCJBTlNTRVJWRVJUSU1FIjowLCJBUktfTE9HSU5JRCI6IldickhLaWl5UEZ1In0%3D'
headers = {
    "cookie": cookie,
    # 模仿火狐浏览器
    "user-agent": "Mozilla/5.0"
}
response = requests.get(
    'https://h5-shop.aikucun.com/api/commodity/subscribe/v2/queryActivityTagV2/WWDLOwYqYqr?shopBizCode=WWDLOwYqYqr',
    headers=headers)
tags = response.json()
tagNos = []
allBrandList = []
for tag in tags['data']:
    tagNos.append(tag['activityTagNo'])
    for status in range(1, 3):
        print('tag:' + tag['activityTagNo'] + ',status:' + str(status))
        res = requests.get(
            'https://h5-shop.aikucun.com/api/commodity/subscribe/v2/h5/querybrandList?shopBizCode=WWDLOwYqYqr',
            params={
                'tagNo': tag['activityTagNo'],
                'status': status
            },
            headers=headers)
        json1 = res.json()
        if 'data' in json1:
            data = json1['data']
            brandLists = data['brandList']
            for brandList in brandLists:
                blist = brandList['brandList']
                for b in blist:
                    print(dict(b, **b['brandExtend']))
                    if 'pcodelen' in b and b['pcodelen'] != '':
                        str0 = r'u"\u{0}'.format(r'\u'.join(re.findall(r'.{4}', str(b['pcodelen'])))) + '"'
                        print(str0)
                        str1 = str(eval(str0))
                        b['pinpaiming0'] = str1 + str(b['pinpaiming'])[len(str1):]
                        print(b['pinpaiming0'])

                    allBrandList.append(b)
                    print(sorted(dict(b, **b['brandExtend']).items(), key=lambda d: d[0]))
# f = open('aikucun.json', 'w', encoding='utf-8')
# f.write(json.dumps(allBrandList).encode('utf-8').decode('utf-8'))
keys = dict()
i = 1
for jkey in range(len(allBrandList)):
    for key, value in allBrandList[jkey].items():
        if key in keys:
            continue
        keys[key] = i
        i += 1
f = openpyxl.Workbook()
sheet1 = f.create_sheet('aikucun')
for jkey in range(len(allBrandList)):
    jk = jkey + 1
    cT = 0
    for key, value in allBrandList[jkey].items():
        cT += 1
        if cT == 0:
            sheet1.cell(row=jk, column=keys[key]).value = key
        else:
            sheet1.cell(row=jk, column=keys[key]).value = str(value)
f.save('aikucun.xlsx')
