import requests
from bs4 import BeautifulSoup
import openpyxl
from openpyxl.drawing.image import Image
from PIL import Image as PILImage
from io import BytesIO
from concurrent.futures import ThreadPoolExecutor
import threading
import time

f = openpyxl.Workbook()
sheet1 = f.create_sheet('chinasspp')
headers = ['品牌名称', '行业类别', '公司名称', '联系电话', '公司传真', '官方网站', '联系地址', '在线客服']
for index, name in enumerate(headers):
    sheet1.cell(row=1, column=index + 1).value = name

count = 1

def parseDetail(no, link):
    response = requests.get(link)
    response.encoding = "gbk"
    soup = BeautifulSoup(response.text, 'lxml')
    print('no' + str(no))
    for item in soup.select_one("#brand_info_ctl00_blink").select('li'):
        key = item.text.split('：')[0]
        value = item.text.split('：')[1]
        # print(item)
        # print(key + ':' + value)
        # print(headers.index(key))
        sheet1.cell(row=no, column=headers.index(key) + 1).value = value.encode('utf-8').decode('utf-8')
        if key == '联系电话':
            url1 = 'http://www.chinasspp.com' + item.select_one('img').attrs.get('src')
            img1 = PILImage.open(BytesIO(requests.get(url1).content))
            sheet1.add_image(Image(img1), chr(ord("A") + headers.index(key)) + str(no))
        if key == '公司传真':
            url2 = 'http://www.chinasspp.com' + item.select_one('img').attrs.get('src')
            img2 = PILImage.open(BytesIO(requests.get(url2).content))
            sheet1.add_image(Image(img2), chr(ord("A") + headers.index(key)) + str(no))


with ThreadPoolExecutor(max_workers=16) as pool:
    for i in range(1, 516):
        print('Page ' + str(i))
        response = requests.get("http://www.chinasspp.com/brand/%E5%A5%B3%E8%A3%85%E5%93%81%E7%89%8C/" + str(i) + "/")
        soup = BeautifulSoup(response.text, 'lxml')
        soup.select(".brand")
        for brand in soup.select(".brand"):
            link = brand.select_one('.logo').attrs.get('href')
            count += 1
            th = pool.submit(parseDetail, count, link)
    pool.shutdown(wait=True)
    f.save('chinasspp.xlsx')
