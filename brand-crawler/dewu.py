import json

import openpyxl

load_dict = ''
with open("dewu.json", 'r') as load_f:
    load_dict = json.load(load_f)
series = load_dict['data']['list']
allBrandList = []
for l in series:
    dc = dict()
    for d in l:
        dc = dict(dc, **l[d])
    print(dc)
    allBrandList.append(dc)

keys = dict()
i = 1
for jkey in range(len(allBrandList)):
    for key, value in allBrandList[jkey].items():
        if key in keys:
            continue
        keys[key] = i
        i += 1
f = openpyxl.Workbook()
sheet1 = f.create_sheet('dewu')
for jkey in range(len(allBrandList)):
    jk = jkey + 1
    cT = 0
    for key, value in allBrandList[jkey].items():
        cT += 1
        if cT == 0:
            sheet1.cell(row=jk, column=keys[key]).value = key
        else:
            sheet1.cell(row=jk, column=keys[key]).value = str(value)
f.save('dewu.xlsx')
