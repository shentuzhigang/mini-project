import json

import openpyxl

import requests

allBrandList = []
r = requests.get(
    'https://wxapi.appvipshop.com/vips-mobile/rest/shopping/category/index/get_tab/v1?api_key=ce29a51aa5c94a318755b2529dcb8e0b&_xcxid=1648525360552&app_name=shop_weixin_mina&client=wechat_mini_program&source_app=shop_weixin_mina&app_version=4.0&client_type=wap&format=json&mobile_platform=2&ver=2.0&standby_id=native&mobile_channel=nature&mars_cid=1648202533707_3e155b4df020055b73d22e80fb75afa6&warehouse=VIP_SH&fdc_area_id=103103101&province_id=103103&wap_consumer=B&t=1648525360&net=wifi&width=414&height=622&hierarchy_id=107&category_id=&category_filter=&sale_for=&client_from=wxsmall')
json1 = r.json()
data1 = json1['data']['data']['tabs']
for tab in data1:
    print(tab['categoryId'])
    r2 = requests.get(
        'https://wxapi.appvipshop.com/vips-mobile/rest/shopping/category/index/get_tab_data/v1?api_key=ce29a51aa5c94a318755b2529dcb8e0b&_xcxid=1648525360675&app_name=shop_weixin_mina&client=wechat_mini_program&source_app=shop_weixin_mina&app_version=4.0&client_type=wap&format=json&mobile_platform=2&ver=2.0&standby_id=native&mobile_channel=nature&mars_cid=1648202533707_3e155b4df020055b73d22e80fb75afa6&warehouse=VIP_SH&fdc_area_id=103103101&province_id=103103&wap_consumer=B&t=1648525360&net=WIFI&width=750&height=500&pcmpWidth=510&hierarchy_id=107&category_id=' +
        tab['categoryId'] + '&sale_for=')
    json2 = r2.json()
    data2 = json2['data']['data']
    sectionList = data2['sectionList']
    for section in sectionList:
        if section['sectionType'] == 'category' and ('品牌' in str(section['category']['name'])):
            for brand in section['category']['children']:
                B = dict(brand)
                for b in brand:
                    if isinstance(brand[b], dict):
                        B = dict(B, **brand[b])
                print(B)
                allBrandList.append(B)
f = openpyxl.Workbook()
sheet1 = f.create_sheet('vip')
keys = dict()
i = 1
for jkey in range(len(allBrandList)):
    for key, value in allBrandList[jkey].items():
        if key in keys:
            continue
        sheet1.cell(row=1, column=i).value = key
        keys[key] = i
        i += 1

for jkey in range(len(allBrandList)):
    jk = jkey + 2
    cT = 0
    for key, value in allBrandList[jkey].items():
        cT += 1
        sheet1.cell(row=jk, column=keys[key]).value = str(value)
f.save('vip.xlsx')
