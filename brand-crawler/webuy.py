import requests
import json
headers = {
    # 模仿火狐浏览器
    "user-agent": "Mozilla/5.0"
}

allBrandList = []
for i in range(-300, 600):
    for ty in [1, 5]:
        response = requests.post('https://www.webuy.ai/sesame/hyk/shopCategory/brand/detail',
                                 headers=headers,
                                 json={
                                     "exhibitionParkType": ty,
                                     "categoryId": i,
                                     "shopId": 3572,
                                     "pageSize": 1000,
                                     "pageNo": 1,
                                     "isPageQuery": False
                                 })
        print(response.json())
        json1 = response.json()
        entry = json1['entry']
        for b in entry:
            print(b)
            allBrandList.append(b)
f = open('webuy.json', 'w', encoding='utf-8')
f.write(json.dumps(allBrandList).encode('utf-8').decode('utf-8'))