package io.shentuzhigang.crawler.cet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-07 18:10
 */
@SpringBootApplication
public class CETCrawlerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CETCrawlerApplication.class, args);
    }

}
