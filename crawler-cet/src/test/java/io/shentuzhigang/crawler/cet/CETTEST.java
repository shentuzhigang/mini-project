package io.shentuzhigang.crawler.cet;


import io.shentuzhigang.crawler.cet.service.ICETService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-21 11:37
 */
@SpringBootTest
public class CETTEST {
    @Autowired
    private ICETService icetService;
    @Test
    public void query(){
        String res = icetService.query("330410192105229","申屠志刚");
        try {
            JSONObject jsonObject = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }
}
