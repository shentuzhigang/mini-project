package cron_demo

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"testing"
	"time"
)

func TestCron(t *testing.T) {
	c := cron.New()
	c.Start()
	entryID, err := c.AddFunc("*/1 * * * *", func() {
		fmt.Println(time.Now())
	})
	if err != nil {
		fmt.Println(entryID)
		return
	}
	select {}
}
