package cron_demo

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"testing"
	"time"
)

func TestParser(t *testing.T) {
	parser := cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow)
	parse, err := parser.Parse("15 12 20 1-19 8 *")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(parse.Next(time.Now()))
}

func TestParserDow(t *testing.T) {
	parser := cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow)
	parse, err := parser.Parse("15 12 20 * 7 0")
	if err != nil {
		fmt.Println(err)
		return
	}
	cron.New()
	fmt.Println(parse.Next(time.Now()))
}
