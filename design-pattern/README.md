# 设计模式（Design Pattern）
## 基本概念
在GoF(Gang of Four)的书籍《Design Patterns - Elements of Reusable Object-Oriented Software(设计模式-可复用面向对象软件的基础)》中是这样定义设计模式的：Christopher Alexander说过：“每一个模式描述了一个在我们周围不断重复发生的问题以及该问题的解决方案的核心。这样，你就能一次又一次地使用该方案而不必做重复劳动” [AIS+77，第10页]。尽管Alexander所指的是城市和建筑模式，但他的思想也同样适用于于面向对象设计模式，只是在面向对象的解决方案里， 我们哟偶那个对象和接口代替了墙壁和门窗。两类模式的核心都在于提供了相关问题的解决方案。一般而言，设计模式有四个基本要素：

1. 模式名称(pattern name)：一个助记名，它用一两个词来描述模式的问题、解决方案和效果。
2. 问题(problem)：描述了应该在何时使用模式。
3. 解决方案(solution)：描述了设计的组成成分，它们之间的相关关系以及各自的职责和协作方案。
4. 效果(consequences)：描述了模式应用的效果以及使用模式应该权衡的问题。
设计模式的创始人很明确地指出了设计模式的基本要素，但是由于现实中浮躁、偏向过度设计等因素的干扰，开发者很多时候会重点关注第1和第3点要素(过度关注设计模式和设计模式的实现)，忽略第2和第4点要素(忽视使用设计模式的场景和目标)，导致设计出来的编码逻辑可能过于复杂或者达不到预期的效果。

总的来说，设计模式(Design Pattern)是一套被反复使用、多数人知晓的、经过分类编目的、代码设计经验的总结。也就是本来并不存在所谓设计模式，用的人多了，也便成了设计模式。

## 设计模式的七大原则
面向对象的设计模式有七大基本原则：

- 开闭原则（Open Closed Principle，OCP）
- 单一职责原则（Single Responsibility Principle, SRP）
- 里氏代换原则（Liskov Substitution Principle，LSP）
- 依赖倒转原则（Dependency Inversion Principle，DIP）
- 接口隔离原则（Interface Segregation Principle，ISP）
- 合成/聚合复用原则（Composite/Aggregate Reuse Principle，CARP）
- 最少知识原则（Least Knowledge Principle，LKP）或者迪米特法则（Law of Demeter，LOD）

|设计模式原则名称|	简单定义|
| ---- | ---- |
|开闭原则|	对扩展开放，对修改关闭|
|单一职责原则|	一个类只负责一个功能领域中的相应职责|
|里氏代换原则|	所有引用基类的地方必须能透明地使用其子类的对象|
|依赖倒转原则|	依赖于抽象，不能依赖于具体实现|
|接口隔离原则|	类之间的依赖关系应该建立在最小的接口上|
|合成/聚合复用原则|	尽量使用合成/聚合，而不是通过继承达到复用的目的|
|迪米特法则|	一个软件实体应当尽可能少的与其他实体发生相互作用|

## 设计模式类型

### 结构型设计模式

### 创建型设计模式

### 行为型设计模式

## 23种基本设计模式

## 其他设计模式

## 参考文章
[设计模式概念和七大原则](https://www.cnblogs.com/throwable/p/9315318.html)
