package io.shentuzhigang.demo.design.decorator.game;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-19 14:22
 */
public abstract class Decorator implements Character {
    String attack;
    String defence;
    private Character character;

    public Decorator(Character character) {
        this.character = character;
    }

    @Override
    public void weapen() {
        this.character.weapen();
    }

    @Override
    public void armour() {
        this.character.armour();
    }
}
