package io.shentuzhigang.demo.design.decorator.score;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:14
 */
public abstract class Decorator implements Reporter {
    private Reporter reporter;

    public Decorator(Reporter reporter) {
        this.reporter = reporter;
    }

    @Override
    public void report() {
        reporter.report();
    }
}
