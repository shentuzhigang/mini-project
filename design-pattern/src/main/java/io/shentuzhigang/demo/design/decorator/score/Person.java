package io.shentuzhigang.demo.design.decorator.score;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:14
 */
public class Person implements Reporter {
    @Override
    public void report() {
        System.out.println("65");
    }
}
