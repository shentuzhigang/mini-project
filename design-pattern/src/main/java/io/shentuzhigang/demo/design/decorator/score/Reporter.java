package io.shentuzhigang.demo.design.decorator.score;


/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-19 18:40
 */
public interface Reporter {
    /**
     * report
     */
    void report();
}
