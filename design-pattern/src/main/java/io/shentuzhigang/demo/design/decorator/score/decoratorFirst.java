package io.shentuzhigang.demo.design.decorator.score;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:14
 */
public class decoratorFirst extends Decorator {

    public decoratorFirst(Reporter reporter) {
        super(reporter);
    }

    @Override
    public void report() {
        System.out.println("这次考试最高成绩比我只高一点");
        super.report();
    }
}
