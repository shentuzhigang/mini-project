package io.shentuzhigang.demo.design.decorator.score;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:14
 */
public class decoratorZero extends Decorator {

    public decoratorZero(Reporter reporter) {
        super(reporter);
    }

    @Override
    public void report() {
        System.out.println("这次考试比上次增加了几分");
        super.report();
    }
}
