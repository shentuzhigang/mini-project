package io.shentuzhigang.demo.design.simplefactory.pizza;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 19:09
 */
public class PepperPizza extends Pizza {
    @Override
    public void prepare() {

    }
}
