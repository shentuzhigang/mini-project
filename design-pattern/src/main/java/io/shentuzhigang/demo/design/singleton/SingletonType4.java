package io.shentuzhigang.demo.design.singleton;

/**
 * 懒汉式（线程安全）
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:14
 */
public class SingletonType4 {
    /**
     * 1、内部静态变量创建实例对象
     */
    private static SingletonType4 INSTANCE;

    /**
     * 2、构造器私有化，不能通过new创建对象
     */
    private SingletonType4() {

    }

    /**
     * 3. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static synchronized SingletonType4 getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SingletonType4();
        }
        return INSTANCE;
    }
}
