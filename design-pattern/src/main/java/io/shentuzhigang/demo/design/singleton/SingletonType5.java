package io.shentuzhigang.demo.design.singleton;

/**
 * 懒汉式（线程安全，同步代码块）
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:14
 */
public class SingletonType5 {
    /**
     * 1、内部静态变量创建实例对象
     */
    private static SingletonType5 INSTANCE;

    /**
     * 2、构造器私有化，不能通过new创建对象
     */
    private SingletonType5() {

    }

    /**
     * 3. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static SingletonType5 getInstance() {
        if (INSTANCE == null) {
            synchronized (SingletonType5.class) {
                INSTANCE = new SingletonType5();
            }
        }
        return INSTANCE;
    }
}
