package io.shentuzhigang.demo.design.singleton;

/**
 * 懒汉式（双重检查）
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:14
 */
public class SingletonType6 {
    /**
     * 1、内部静态变量创建实例对象
     * volatile 内存可见性和指令重排
     */
    private static volatile SingletonType6 INSTANCE;

    /**
     * 2、构造器私有化，不能通过new创建对象
     */
    private SingletonType6() {

    }

    /**
     * 3. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static SingletonType6 getInstance() {
        if (INSTANCE == null) {
            // 保证一个线程进入
            synchronized (SingletonType6.class) {
                // 防止重复创建实例
                if (INSTANCE == null) {
                    INSTANCE = new SingletonType6();
                }
            }
        }
        return INSTANCE;
    }
}
