package io.shentuzhigang.demo.design.singleton;

/**
 * 静态内部类
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:15
 */
public class SingletonType7 {
    /**
     * 1、构造器私有化，不能通过new创建对象
     */
    private SingletonType7() {

    }

    /**
     * 2. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static SingletonType7 getInstance() {
        return SingletonInstance.INSTANCE;
    }

    private static class SingletonInstance {
        private static final SingletonType7 INSTANCE = new SingletonType7();
    }
}
