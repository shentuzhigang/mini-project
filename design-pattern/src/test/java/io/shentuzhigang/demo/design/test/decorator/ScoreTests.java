package io.shentuzhigang.demo.design.test.decorator;

import io.shentuzhigang.demo.design.decorator.score.*;
import org.junit.jupiter.api.Test;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:12
 */
public class ScoreTests {
    @Test
    public void t() {
        Person person = new Person();
        Decorator decorator = new decoratorZero(new decoratorFirst(
                new decoratorTwo(person)));
        decorator.report();
    }
}
