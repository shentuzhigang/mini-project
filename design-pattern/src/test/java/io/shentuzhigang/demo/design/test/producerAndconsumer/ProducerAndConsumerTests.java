package io.shentuzhigang.demo.design.test.producerAndconsumer;

import io.shentuzhigang.demo.design.producerAndconsumer.Consumer;
import io.shentuzhigang.demo.design.producerAndconsumer.Producer;
import io.shentuzhigang.demo.design.producerAndconsumer.Storage;
import org.junit.jupiter.api.Test;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:41
 */
public class ProducerAndConsumerTests {
    @Test
    public void test() {
        // 仓库对象
        Storage storage = new Storage();
        // 生产者对象
        Producer[] p = new Producer[7];
        for (int i = 0; i < p.length; i++) {
            p[i] = new Producer(storage);
            // 设置生产者产品生产数量
            p[i].setNum(100);
        }
        // 消费者对象
        Consumer[] c = new Consumer[3];
        for (int i = 0; i < c.length; i++) {
            c[i] = new Consumer(storage);
            c[i].setNum(50);
        }
        // 线程开始执行
        for (int i = 0; i < c.length; i++) {
            c[i].start();
        }
        for (int i = 0; i < p.length; i++) {
            p[i].start();
        }
    }
}
