package io.shentuzhigang.demo.design.test.singleton;

import io.shentuzhigang.demo.design.singleton.SingletonType1;
import io.shentuzhigang.demo.design.singleton.SingletonType2;
import io.shentuzhigang.demo.design.singleton.SingletonType3;
import io.shentuzhigang.demo.design.singleton.SingletonType4;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 15:57
 */
public class SingletonTests {
    @Test
    public void testType1() {
        SingletonType1 a = SingletonType1.getInstance();
        SingletonType1 b = SingletonType1.getInstance();
        Assertions.assertSame(a, b, "不是同一个对象实例");
    }

    @Test
    public void testType2() {
        SingletonType2 a = SingletonType2.getInstance();
        SingletonType2 b = SingletonType2.getInstance();
        Assertions.assertSame(a, b, "不是同一个对象实例");
    }

    @Test
    public void testType3() {
        SingletonType3 a = SingletonType3.getInstance();
        SingletonType3 b = SingletonType3.getInstance();
        Assertions.assertSame(a, b, "不是同一个对象实例");
    }

    @Test
    public void testType4() {
        SingletonType4 a = SingletonType4.getInstance();
        SingletonType4 b = SingletonType4.getInstance();
        Assertions.assertSame(a, b, "不是同一个对象实例");
    }
}
