package go_gavc

import (
	"github.com/dlclark/regexp2"
	"strings"
)

type GAVC struct {
	GroupId    string
	ArtifactId string
	Veriosn    string
	Classifier string
}

var gavcRe = regexp2.MustCompile("(?<orgPath>.+?)/(?<module>[^/]+)/(?<baseRev>[^/]+?)(?:-(?<folderItegRev>SNAPSHOT))?/(?<module>\\2)-(?<baseRev>\\3)(?:-(?<fileItegRev>SNAPSHOT|(?:(?:[0-9]{8}.[0-9]{6})-(?:[0-9]+))))?(?:-(?<classifier>[^/]+?))?\\.(?<ext>(?:(?!\\d))[^\\-/]+|7z)", regexp2.RE2)
var jarRe = regexp2.MustCompile("(?<module>[^/]+)-(?<baseRev>[^/]+?)(?:-(?<fileItegRev>SNAPSHOT|(?:(?:[0-9]{8}.[0-9]{6})-(?:[0-9]+))))?(?:-(?<classifier>[^/]+?))?\\.(?<ext>(?:(?!\\d))[^\\-/]+|7z)", regexp2.RE2)

func ResoleGAVC(path string) *GAVC {
	gavc := &GAVC{}
	ps := strings.Split(path, "/")

	if len(ps) >= 4 {
		match, err := gavcRe.FindStringMatch(path)
		if err == nil && match != nil {
			orgPath := match.GroupByName("orgPath")
			if orgPath != nil {
				gavc.GroupId = strings.ReplaceAll(orgPath.String(), "/", ".")
			}
			module := match.GroupByName("module")
			if module != nil {
				gavc.ArtifactId = module.String()
			}
			version := match.GroupByName("baseRev")
			if version != nil {
				gavc.Veriosn = version.String()
			}
			classifier := match.GroupByName("classifier")
			if classifier != nil {
				gavc.Classifier = classifier.String()
			}
			return gavc
		}
	}
	index := strings.LastIndex(path, "/")
	var name string
	if index == -1 {
		name = path
		path = ""
	} else {
		name = path[index+1:]
		path = path[:index]
	}

	match, err := jarRe.FindStringMatch(name)
	if err == nil && match != nil {
		suffix := ""
		version := match.GroupByName("baseRev")
		if version != nil {
			gavc.Veriosn = version.String()
			if len(gavc.Veriosn) > 0 {
				suffix += "/" + version.String()
			}
		}
		classifier := match.GroupByName("classifier")
		if classifier != nil {
			gavc.Classifier = classifier.String()
			if len(gavc.Classifier) > 0 {
				suffix += "-" + classifier.String()
			}
		}
		gavc.GroupId = strings.ReplaceAll(strings.TrimSuffix(path, suffix), "/", ".")

		module := match.GroupByName("module")
		if module != nil {
			gavc.ArtifactId = module.String()
			if len(gavc.ArtifactId) > 0 {
				if strings.HasSuffix(path, "/"+module.String()) {
					gavc.GroupId = strings.ReplaceAll(strings.TrimSuffix(path, "/"+module.String()), "/", ".")
				} else if strings.HasSuffix(path, "/"+module.String()+suffix) {
					gavc.GroupId = strings.ReplaceAll(strings.TrimSuffix(path, "/"+module.String()+suffix), "/", ".")
				}
			}
		}
		if len(gavc.GroupId) == 0 {
			gavc.GroupId = gavc.ArtifactId
		}
		return gavc
	}

	ps = strings.Split(path, "/")
	if len(ps) == 1 {
		path = path + "//"
	} else if len(ps) == 2 {
		path = path + "/"
	}

	index1 := strings.LastIndex(path, "/")
	gavc.Veriosn = path[index1+1:]
	index2 := strings.LastIndex(path[:index1], "/")
	gavc.ArtifactId = path[index2+1 : index1]
	gavc.GroupId = strings.ReplaceAll(path[:index2], "/", ".")

	if len(gavc.ArtifactId) == 0 {
		index3 := strings.LastIndex(name, ".")
		if index3 != -1 {
			gavc.ArtifactId = name[:index3]
		}
	}

	return gavc
}
