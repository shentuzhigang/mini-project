package main

import (
	"fmt"
	"time"
)

func main() {
	tick := time.Tick(100 * time.Millisecond)
	boom := time.After(500 * time.Millisecond)
	count := 0
	for {
		select {
		case i := <-tick:
			fmt.Println("tick.", i)
			fmt.Println("count = ", count)
		case j := <-boom:
			fmt.Println("BOOM!", j)
			return
		default:
			count++
			//fmt.Println("    .")
			//time.Sleep(50 * time.Millisecond)
		}
	}
}
