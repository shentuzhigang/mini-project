package main

import (
	"fmt"
	"time"
)

func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func main() {
	//c := make(chan int)
	//quit := make(chan int)
	x := 1
	go func(d *int) {
		for i := 0; i < 10; i++ {
			//fmt.Println(<-c)
			*d += 1
			//fmt.Println("d =", d)
		}
		//quit <- 0
	}(&x)
	//go func() {
	//	for i := 0; i < 10; i++ {
	//		//fmt.Println(<-c)
	//	}
	//	//quit <- 0
	//}()
	time.Sleep(5 * time.Second)
	fmt.Println("x =", x)
	//fibonacci(c, quit)
}
