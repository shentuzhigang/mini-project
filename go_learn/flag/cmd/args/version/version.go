package version

import (
	"flag"
	"fmt"
	"os"
)

var (
	Version string
)

var show = flag.Bool("version", false, "展示版本号")

func init() {
	flag.Parse()
	if *show {
		fmt.Println(*show)
		os.Exit(0)
	}
}
