package main

import (
	"fmt"
	"github.com/shirou/gopsutil/net"
)

func main() {
	// Network
	netInfo, _ := net.IOCounters(true)
	for index, v := range netInfo {
		fmt.Printf("%v:%v send:%v recv:%v\n", index, v, v.BytesSent, v.BytesRecv)
	}

	// IP
	infers, err := net.Interfaces()
	if err != nil {
		return
	}
	for _, infer := range infers {
		addrs := infer.Addrs
		for _, addr := range addrs {
			fmt.Println(addr.Addr)
		}
	}
}
