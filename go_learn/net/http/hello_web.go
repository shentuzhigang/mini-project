package main

import "net/http"

func SayHello(w http.ResponseWriter, req *http.Request) {
	_, err := w.Write([]byte("Hello"))
	if err != nil {
		return
	}
}

func main() {
	http.HandleFunc("/hello", SayHello)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		return
	}
}
