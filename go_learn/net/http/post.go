package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	url := "https://www.shirdon.com/comment/add"
	body := "{\"userId\":1,\"articleId\":1,\"comment\":\"这是一条评论\"}"
	response, err := http.Post(url, "applicatin/x-www-form-urlencoded", bytes.NewReader([]byte(body)))
	if err != nil {
		fmt.Println("err", err)
	}
	bytes, err := ioutil.ReadAll(response.Body)
	fmt.Printf(string(bytes))
}
