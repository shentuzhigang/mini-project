package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func scan(path string, f os.FileInfo, err error) error {
	fmt.Printf("Scaned: %s\n", path)
	return nil
}

func main() {
	root := `.`
	err := filepath.Walk(root, scan)
	fmt.Printf("filepath.Walk() returned %v\n", err)
}
