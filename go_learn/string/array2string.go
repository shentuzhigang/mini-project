package main

import (
	"fmt"
	"strings"
)

func main() {
	proString := strings.ReplaceAll(strings.Trim(fmt.Sprint([]int{1, 2, 3}), "[]"), " ", ",")
	fmt.Println(proString)
}
