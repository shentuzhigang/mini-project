package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"os"
)

func main() {
	f, _ := os.Open("test.xml")
	doc, err := goquery.NewDocumentFromReader(f)
	if err != nil {
		return
	}
	selection := doc.Find("dependency")
	node := selection.Get(0)
	attr := node.Attr
	for i := 0; i < len(attr); i++ {
		fmt.Println(attr[i])
	}
}
