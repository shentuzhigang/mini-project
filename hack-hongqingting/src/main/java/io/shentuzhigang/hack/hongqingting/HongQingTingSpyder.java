package io.shentuzhigang.hack.hongqingting;


import io.shentuzhigang.hack.hongqingting.bean.RunData;
import io.shentuzhigang.hack.hongqingting.http.HttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.io.IOException;

import static io.shentuzhigang.hack.hongqingting.util.GzipUtil.compress;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-10-26 15:18
 */
public class HongQingTingSpyder {
    private String host = "http://tygl.zstu.edu.cn:8029";

    public String downloadRunData(RunData runData) throws IOException {
        Request.Builder builder = new Request.Builder();
        Request build = builder.url("http://10.11.246.182:8029/DragonFlyServ/Api/webserver/DownloadRunData")
                .header("Charset", "UTF-8")
                .header("Connection", "Keep-Alive")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(RequestBody.create(compress(runData.toString(), "UTF-8")))
                .build();
        return new HttpClient().execute(build).body().string();
    }

    public String uploadRunData(RunData runData) throws IOException {
        Request.Builder builder = new Request.Builder();
        Request build = builder.url("http://10.11.246.182:8029/DragonFlyServ/Api/webserver/uploadRunData")
                .header("Charset", "UTF-8")
                .header("Connection", "Keep-Alive")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(RequestBody.create(compress(runData.toString(), "UTF-8")))
                .build();
        return new HttpClient().execute(build).body().string();
    }

    public String getRunDataSummary(String studentno, String uid) throws IOException {
        String s1 = "{'studentno':'2018329621200'," +
                "'uid':'a26baf4e-a27a-424c-9d58-f04b700114a1faf4352521394a859275687a47b786721602508814$9d7370a8eb0c1286ce3c949c0c53469e'}";
        Request.Builder builder1 = new Request.Builder();
        Request build1 = builder1.url("http://10.11.246.182:8029/DragonFlyServ/Api/webserver/getRunDataSummary")
                .header("Charset", "UTF-8")
                .header("Connection", "Keep-Alive")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(RequestBody.create(compress(s1, "UTF-8")))
                .build();
        return new HttpClient().execute(build1).body().string();

    }
}
