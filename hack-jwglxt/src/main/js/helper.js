// ==UserScript==
// @name         正方教务管理系统助手
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       You
// @match        http://jwglxt.zstu.edu.cn/jwglxt/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    const setting = {
        id:"1",
        baseURL:"https://www.shentuzhigang.top:8443/MyZSTU",
        jwglxtURL:"https://www.yooc.me",
        model:"info"
    };
    let url;
    const log={
        LEVEL:{info:0,debug:1},
        logMessage:(msg,level)=> {
            if (log.LEVEL[setting.model]>=log.LEVEL[level]){
                console.log(msg)
            }
        },
        info:(msg)=> log.logMessage(msg, "info"),
        debug: (msg)=> log.logMessage(msg, "debug")
    };
    let start=()=>{
        if(url!=location.pathname){
            url=location.pathname;
            log.info(url);
            if(!url.match(/^\/mobile/)){
                if(url.match(/\/jwglxt\/xtgl\/index_initMenu.html.*/)){
                    log.info('a');
                    let d=document.getElementsByClassName('navbar-nav')[2]
                    let a=document.createElement('li')
                    d.append(a)
                    a.outerHTML=`<li class="dropdown"><a id="drop1" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">扩展功能(MyZSTU)<b class="caret"></b> </a><ul class="dropdown-menu" role="menu" aria-labelledby="drop1"> <li><a tabindex="-1" onclick="clickMenu('N305005','/cjcx/cjcx_cxDgXsxmcj.html','学生成绩明细查询','null'); return false;" href="javascript:void(0);" target="_blank">学生成绩明细查询</a></li></ul></li>`
                }
                else if(url.match(/\/jwglxt\/cjcx\/cjcx_cxDgXscj.html.*/)){
                    //查询参数
                    function paramMap(){
                        return {
                            "xnm"	: $("#xnm").val(),
                            "xqm"	: $("#xqm").val()
                        };
                    }
                    log.info('b');
                    let d=document.getElementById('but_ancd')
                    let a=document.createElement('button')
                    d.append(a)
                    a.outerHTML=`<button type="button" class="btn btn-default btn_dcmx" href="javascript:void(0);" id="btn_dcmx"><i class="bigger-100 glyphicon glyphicon-export"></i> 导出成绩明细(MyZSTU)</button> `
                    $("#btn_dcmx").click(function () {
                        $.exportDialog(_path + '/cjcx/cjcx_dcXsKccjList.html',"JW_N305005_XSCXXMCJ");
                    });
                    ckCjxqLink=(key)=>{
                        var rowData = jQuery('#tabGrid').jqGrid('getRowData',key);
                        $.showDialog(_path +"/cjcx/cjcx_cxCjxqGjh.html",$.i18n.get("ckcjxq")/*'查看成绩详情'*/,$.extend(true,{},viewConfig,{width:'1086px',
                            data:{
                                "jxb_id": rowData.jxb_id,
                                "xnm"	: rowData.xnm,
                                "xqm"	: rowData.xqm,
                                "kcmc"	: rowData.kcmc
                            }
                        }));
                        var CjGrid = $.extend({},BaseJqGrid,{
                            multiselect : false,
                            postData	:{
                                "jxb_id": rowData.jxb_id,
                                "xnm"	: rowData.xnm,
                                "xqm"	: rowData.xqm,
                                //xh_id 	: row.xh_id,
                            },
                            url			: _path + '/cjcx/cjcx_cxXsXmcjList.html', //这是Action的请求地址
                            colModel:[
                                {label:'成绩分项',name:'xmblmc', index: 'xmblmc',align:'left',width:'200px'},
                                {label:'成绩',name:'xmcj', index: 'xmcj',align:'center',width:'200px'},
                                {label:'服务提供者',name:'', index: '',align:'center',width:'200px',
                                    formatter:	function(cellvalue, options, rowObject) {
                                        return "<a href='https://www.shentuzhigang.top:8443/MyZSTU' class='clj red2'>MyZSTU</a>";/*查看*/
                                    }},
                            ]
                        });
                        $('#subtab').loadJqGrid(CjGrid);
                    };
                }
            }
        }
    };
    let int=self.setInterval(()=>{
        start()
    },1000);

})();