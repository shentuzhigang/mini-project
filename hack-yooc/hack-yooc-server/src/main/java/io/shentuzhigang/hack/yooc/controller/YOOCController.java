package io.shentuzhigang.hack.yooc.controller;

import io.shentuzhigang.hack.yooc.dto.ApiResponse;
import io.shentuzhigang.hack.yooc.dto.ExamDTO;
import io.shentuzhigang.hack.yooc.dto.QuestionDTO;
import io.shentuzhigang.hack.yooc.model.Question;
import io.shentuzhigang.hack.yooc.service.IYOOCExamQuestionService;
import io.shentuzhigang.hack.yooc.utils.ApiResponseUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 08:26
 */
@Controller
@CrossOrigin
@RequestMapping("/yooc")
public class YOOCController {
    @Autowired
    IYOOCExamQuestionService iyoocExamQuestionService;
    @ResponseBody
    @RequestMapping(value = "/exam",method = RequestMethod.POST)
    public Object updateExams(@RequestBody ExamDTO examDTO, HttpServletResponse response){
        response.setHeader("Content-Security-Policy","upgrade-insecure-requests");
        List<Question> questions  = new ArrayList<>();
        for (QuestionDTO questionDTO: examDTO.getQuestions()) {
            Question question =new Question(examDTO.getGroup(),
                    examDTO.getExam(),
                    questionDTO.getId().substring(9),
                    questionDTO.getQuestion());
            questions.add(question);
            //System.out.println(question);

        }
        iyoocExamQuestionService.saveOrUpdateBatch(questions);
        return examDTO;
    }
    @ResponseBody
    @RequestMapping(value = "/group/{groupId}/exam/{examId}/upload",method = RequestMethod.POST)
    public Object uploadExam(@RequestHeader("JS-ID")String JSID,
                             @PathVariable("groupId")String groupId,
                             @PathVariable("examId")String examId,
                             @RequestBody List<QuestionDTO> questionDTOS){

        List<Question> questions  = new ArrayList<>();
        for (QuestionDTO questionDTO: questionDTOS) {
            Question question =new Question(groupId,
                    examId,
                    questionDTO.getId().substring(9),
                    questionDTO.getQuestion());
            questions.add(question);
            //System.out.println(question);

        }
        iyoocExamQuestionService.saveOrUpdateBatch(questions);
        return questions;
    }
    @ResponseBody
    @RequestMapping(value = "/group/{groupId}/exam/{examId}/answer",method = RequestMethod.GET)
    public Object getAnswerByQuestionId(@RequestHeader("JS-ID")String JSID,
                                        @PathVariable("groupId")String groupId,
                                        @PathVariable("examId")String examId,
                                        @RequestParam("question") List<String> questions){
        List<QuestionDTO> questionDTOS  = new ArrayList<>();
        for (String question:questions) {
            Question questiona=iyoocExamQuestionService.getById(question);
            if(questiona==null){
                questionDTOS.add(QuestionDTO.getNullQuestionDTO(question));
            }else{
                questionDTOS.add(new QuestionDTO(questiona.getId(),questiona.getQuestion()));
            }
        }
        return questionDTOS;
    }
    @ResponseBody
    @RequestMapping(value = "/group/{groupId}/exam/{examId}/answer/search",method = {RequestMethod.GET})
    public Object getUserByPage(@RequestHeader("JS-ID")String JSID,
                                @PathVariable("groupId")String groupId,
                                @PathVariable("examId")String examId,
                                @RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "10") Integer size,
                                @RequestParam(defaultValue = "false")Boolean onlynow,
                                String key){
        QueryWrapper<Question> queryWrapper = new QueryWrapper<>();
        if(onlynow){
            if(!StringUtils.isEmpty(groupId)){
                queryWrapper.eq("group",groupId);
            }
            if(!StringUtils.isEmpty(examId)){
                queryWrapper.eq("exam",examId);
            }
        }
        queryWrapper.like("question", key);
        return iyoocExamQuestionService.page(new Page<>(page,size), queryWrapper);
    }
    @ResponseBody
    @RequestMapping(value = "/group/{groupId}/exam/{examId}/answer/total",method = RequestMethod.GET)
    public Object getAnswerTotalByQuestionId(@PathVariable("groupId")String groupId,
                                        @PathVariable("examId")String examId){
        int total = iyoocExamQuestionService.count(new QueryWrapper<>(new Question(groupId, examId)));
        ApiResponse retTemp = ApiResponseUtil.getRetTemp();
        retTemp.setData(total);
        return retTemp;
    }

    @RequestMapping(value = "/yooc_helper.js",method = RequestMethod.GET)
    public String updateJS(Model model){
        model.addAttribute("ID", UUID.randomUUID().toString());
        return "yooc_helper.js";
    }
}
