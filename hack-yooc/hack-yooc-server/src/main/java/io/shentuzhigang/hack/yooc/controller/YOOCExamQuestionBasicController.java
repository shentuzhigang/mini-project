package io.shentuzhigang.hack.yooc.controller;

import io.shentuzhigang.hack.yooc.model.Question;
import io.shentuzhigang.hack.yooc.service.IYOOCExamQuestionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 16:55
 */
@Controller
@CrossOrigin
@RequestMapping("/yooc/group/exam/question")
public class YOOCExamQuestionBasicController {
    @Autowired
    IYOOCExamQuestionService iyoocExamQuestionService;
    @ResponseBody
    @RequestMapping(value = "/",method = {RequestMethod.GET})
    public Object getUserByPage(@RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "10") Integer size,
                                Question question){
        return iyoocExamQuestionService.page(new Page<>(page,size), new QueryWrapper<>(question));
    }
}
