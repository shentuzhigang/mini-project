package io.shentuzhigang.hack.yooc.dto;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 08:30
 */
public class QuestionDTO {
    private String id;

    private String question;

    public QuestionDTO() {

    }

    public QuestionDTO(String id, String question) {
        this.id = id;
        this.question = question;
    }



    public static QuestionDTO getNullQuestionDTO(String id){
        QuestionDTO question = new QuestionDTO();
        question.setId(id);
        question.setQuestion("<div class=\"question-board\" id=\"question-"+id+"\">\n" +
                "               <div class=\"the-ans fls\">\n" +
                "                   <p>这一题还没有正确的答案，快点上传吧！</p>\n" +
                "               </div>\n" +
                "             </div>");
        return question;
    }
    @Override
    public String toString() {
        return "QuestionDTO{" +
                "id='" + id + '\'' +
                ", question='" + question + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

}
