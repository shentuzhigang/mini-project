package io.shentuzhigang.hack.yooc.mapper;

import io.shentuzhigang.hack.yooc.model.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 09:46
 */
@Mapper
@Component
public interface QuestionMapper extends BaseMapper<Question> {
}
