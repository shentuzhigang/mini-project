package io.shentuzhigang.hack.yooc.service.impl;

import io.shentuzhigang.hack.yooc.model.Question;
import io.shentuzhigang.hack.yooc.mapper.QuestionMapper;
import io.shentuzhigang.hack.yooc.service.IYOOCExamQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.shentuzhigang.hack.yooc.service.IYOOCExamQuestionService;
import org.springframework.stereotype.Service;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 09:44
 */
@Service
public class YOOCExamQuestionServiceImpl extends ServiceImpl<QuestionMapper, Question>  implements IYOOCExamQuestionService {
}
