package io.shentuzhigang.demo.hadoop.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-19 16:09
 */
public class HDFSDemo {

    //
    private final static String HDFS_URL = "hdfs://192.168.30.130:9000/";

    /**
     *
     */
    @Test
    public FileSystem testConnect() throws URISyntaxException, IOException {
        // 1、声明配置
        Configuration conf = new Configuration();
        // 2、文件系统
        FileSystem fs = null;
        URI uri = null;
        uri = new URI(HDFS_URL);
        fs = FileSystem.get(uri,conf);
        System.out.println(fs);
        return fs;
    }

    @Test
    public void getFile() throws IOException, URISyntaxException {
        String file = HDFS_URL + "README.html";
        FileSystem fs = testConnect();
        InputStream in = null;
        FileOutputStream out = null;
        in = fs.open(new Path(file));
        out = new FileOutputStream(new File("test.txt"));
        IOUtils.copyBytes(in,out,2048,true);
        System.out.println("ok");
        fs.close();
    }

    @Test
    public void putFIle()throws Exception {
        FileSystem fs = testConnect();
        FileInputStream in = new FileInputStream("test.txt");
        OutputStream out = fs.create(new Path(HDFS_URL + "test.txt"));
        IOUtils.copyBytes(in,out,2048,true);
        in.close();
        out.close();
        fs.close();
    }
}
