package io.shentuzhigang.demo.hadoop.mapreduce.flow;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-20 14:07
 */
public class FlowReducer extends Reducer<Text,FlowWritable,Text, IntWritable> {
    @Override
    protected void reduce(Text key, Iterable<FlowWritable> values, Context context) throws IOException, InterruptedException {
        int sum=0;
        for (FlowWritable val : values) {
            sum += val.getFlow();
        }
        context.write(key,new IntWritable(sum));
    }
}
