package io.shentuzhigang.demo.hadoop.mapreduce.flow.part;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-20 14:58
 */
public class CustomFlowPartitioner extends Partitioner<Text, FlowWritable> {
    @Override
    public int getPartition(Text text, FlowWritable flowWritable, int i) {
        String address = flowWritable.getAddress();
        if("sh".equals(address)){
            return 1;
        }else if("bj".equals(address)){
            return 2;
        }else if("hz".equals(address)){
            return 3;
        }else if("fj".equals(address)) {
            return 4;
        }
        return 0;
    }
}
