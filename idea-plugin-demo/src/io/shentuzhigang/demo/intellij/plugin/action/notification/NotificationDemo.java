package io.shentuzhigang.demo.intellij.plugin.action.notification;

import com.intellij.notification.NotificationGroupManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.MessageType;
import org.jetbrains.annotations.NotNull;

public class NotificationDemo extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        NotificationGroupManager
                .getInstance()
                .getNotificationGroup("Custom Notification Group")
                .createNotification("测试通知", MessageType.INFO)
                .notify(anActionEvent.getProject());
    }
}
