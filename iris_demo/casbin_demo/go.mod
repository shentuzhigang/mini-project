module casbin_demo

go 1.16

require (
	github.com/CloudyKit/jet/v6 v6.1.0 // indirect
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/Shopify/goreferrer v0.0.0-20210630161223-536fa16abd6f // indirect
	github.com/casbin/casbin/v2 v2.34.0
	github.com/casbin/gorm-adapter/v3 v3.3.2
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/iris-contrib/middleware/casbin v0.0.0-20210110101738-6d0a4d799b5d
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210705170737-afb15b860124
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/microcosm-cc/bluemonday v1.0.15 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/tdewolff/minify/v2 v2.9.19 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.4 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/net v0.0.0-20210716203947-853a461950ff // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/time v0.0.0-20210611083556-38a9dc6acbc6 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.12
)
