module custom_context

go 1.16

require (
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/kataras/iris/v12 v12.1.8
	github.com/klauspost/compress v1.11.12 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
)
