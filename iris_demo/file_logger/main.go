package main

import (
	"github.com/kataras/iris/v12"
	"os"
)

func main() {
	app := iris.Default()
	// Logging to a file.
	// Colors are automatically disabled when writing to a file.
	f, _ := os.Create("iris.log")
	app.Logger().SetOutput(f)

	// Use the following code if you need to write the logs
	// to file and console at the same time.
	app.Logger().AddOutput(os.Stdout)

	app.Get("/ping", func(ctx iris.Context) {
		ctx.WriteString("pong")
	})

	app.Listen(":8080")
}
