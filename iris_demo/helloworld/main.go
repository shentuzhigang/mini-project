package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

type (
	request struct {
		Firstname string `json:"firstname"`
		Lastname  string `json:"lastname"`
	}

	response struct {
		ID      uint64 `json:"id"`
		Message string `json:"message"`
	}
)

func main() {
	app := iris.New()
	app.Logger().SetLevel("debug")
	mvc.Configure(app.Party("/users"), configureMVC)
	err := app.Listen(":8080")
	if err != nil {
		return
	}
}

func configureMVC(app *mvc.Application) {
	app.Handle(new(userController))
}

type userController struct {
	// [...dependencies]
}

func (c *userController) PutBy(id uint64, req request) response {
	return response{
		ID:      id,
		Message: req.Firstname + " updated successfully",
	}
}
