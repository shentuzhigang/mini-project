module file_logger

go 1.16

require (
	github.com/CloudyKit/jet/v3 v3.0.0 // indirect
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/ajg/form v1.5.1 // indirect
	github.com/dgraph-io/badger v1.6.0 // indirect
	github.com/etcd-io/bbolt v1.3.3 // indirect
	github.com/gavv/httpexpect v2.0.0+incompatible // indirect
	github.com/gomodule/redigo v1.7.1-0.20190724094224-574c33c3df38 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/iris-contrib/blackfriday v2.0.0+incompatible // indirect
	github.com/iris-contrib/pongo2 v0.0.1 // indirect
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210705170737-afb15b860124
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/ryanuber/columnize v2.1.0+incompatible // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/valyala/fasthttp v1.28.0 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
)
