package main

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"net/http"
	"reflect"
	"runtime"
)

func main() {
	app := iris.New()

	app.WrapRouter(routerWrapper)
	app.AddRouterWrapper(func(w http.ResponseWriter, r *http.Request, router http.HandlerFunc) {
		w.Write([]byte("#1 .WrapRouter2 in\n"))
		fmt.Println(runtime.FuncForPC(reflect.ValueOf(router).Pointer()).Name())
		router(w, r)
		w.Write([]byte("#1 .WrapRouter2 out\n"))
	})
	app.AddRouterWrapper(func(w http.ResponseWriter, r *http.Request, router http.HandlerFunc) {
		w.Write([]byte("#1 .WrapRouter3 in\n"))
		fmt.Println(runtime.FuncForPC(reflect.ValueOf(router).Pointer()).Name())
		router(w, r)
		w.Write([]byte("#1 .WrapRouter3 out\n"))
	})
	app.UseRouter(routerMiddleware)
	app.UseGlobal(globalMiddleware)
	app.Use(useMiddleware)
	app.UseError(errorMiddleware)
	app.Done(doneMiddleware)
	app.DoneGlobal(doneGlobalMiddleware)

	// Adding a OnErrorCode(iris.StatusNotFound) causes `.UseGlobal`
	// to be fired on 404 pages without this,
	// only `UseError` will be called, and thus should
	// be used for error pages.
	app.OnErrorCode(iris.StatusNotFound, notFoundHandler)

	app.Get("/", before, mainHandler, after)
	party := app.Party("/party")

	party.Use(func(ctx iris.Context) {
		ctx.WriteString("Party.Use in\n")
		ctx.Next()
		ctx.WriteString("Party.Use out\n")
	})
	party.UseRouter(func(ctx iris.Context) {
		ctx.WriteString("Party.UseRouter in\n")
		ctx.Next()
		ctx.WriteString("Party.UseRouter out\n")
	})
	party.UseError(func(ctx iris.Context) {
		ctx.WriteString("Party.UseError in\n")
		ctx.Next()
		ctx.WriteString("Party.UseError out\n")
	})
	party.UseFunc(func(ctx iris.Context) {
		ctx.WriteString("Party.UseFunc in\n")
		ctx.Next()
		ctx.WriteString("Party.UseFunc out\n")
	})
	party.Done(func(ctx iris.Context) {
		ctx.WriteString("Party.Done in\n")
		ctx.Next()
		ctx.WriteString("Party.Done out\n")
	})
	party.Get("", before, mainHandler, after)
	app.Listen(":8080")
}

func mainHandler(ctx iris.Context) {
	ctx.WriteString("Main Handler in\n")
	ctx.Next()
	ctx.WriteString("Main Handler out\n")
}

func routerWrapper(w http.ResponseWriter, r *http.Request, router http.HandlerFunc) {
	w.Write([]byte("#1 .WrapRouter in\n"))
	fmt.Println(runtime.FuncForPC(reflect.ValueOf(router).Pointer()).Name())
	router(w, r)
	w.Write([]byte("#1 .WrapRouter out\n"))
}

func before(ctx iris.Context) {
	ctx.WriteString("#5 .before in\n")
	ctx.Next() //继续执行下一个handler，这本例中是mainHandler
	ctx.WriteString("#5 .before out\n")
}

func after(ctx iris.Context) {
	ctx.WriteString("#6 .after in\n")
	ctx.Next()
	ctx.WriteString("#6 .after out\n")
}

func routerMiddleware(ctx iris.Context) {
	ctx.WriteString("#2 .UseRouter in\n")
	ctx.Next()
	ctx.WriteString("#2 .UseRouter out\n")
}

func globalMiddleware(ctx iris.Context) {
	ctx.WriteString("#3 .UseGlobal in\n")
	ctx.Next()
	ctx.WriteString("#3 .UseGlobal out\n")
}

func useMiddleware(ctx iris.Context) {
	ctx.WriteString("#4 .Use in\n")
	ctx.Next()
	ctx.WriteString("#4 .Use out\n")
}

func errorMiddleware(ctx iris.Context) {
	ctx.WriteString("#3 .UseError in\n")
	ctx.Next()
	ctx.WriteString("#3 .UseError out\n")
}

func doneGlobalMiddleware(ctx iris.Context) {
	ctx.WriteString("#7 .DoneGlobal in\n")
	ctx.Next()
	ctx.WriteString("#7 .DoneGlobal out\n")
}

func doneMiddleware(ctx iris.Context) {
	ctx.WriteString("#8 .Done in\n")
	ctx.Next()
	ctx.WriteString("#8 .Done out\n")
}

func notFoundHandler(ctx iris.Context) {
	ctx.WriteString("404 Error Handler")
	ctx.Next()
}
