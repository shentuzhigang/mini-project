package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/logger"
	"github.com/kataras/iris/v12/middleware/recover"
	"github.com/kataras/iris/v12/mvc"
	"movie/services"
	"movie/web/controllers"
	"movie/web/middleware"
)

func main() {
	app := iris.New()
	app.Logger().SetLevel("debug")
	app.Use(recover.New())
	app.Use(logger.New())

	index := mvc.New(app.Party("/"))
	index.Router.Use(middleware.BasicAuth)

	movieService := services.NewMovieService()
	index.Register(movieService)
	index.Handle(new(controllers.IndexController))

	app.RegisterView(iris.HTML("./web/views", ".html").Reload(true))
	app.HandleDir("/public", "./web/public")

	err := app.Run(iris.Addr(":8080"))
	if err != nil {
		return
	}
}
