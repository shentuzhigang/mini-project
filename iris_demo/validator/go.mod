module validator

go 1.16

require (
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/go-playground/validator/v10 v10.6.1
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210616105239-6d3884b0ceba
	github.com/smartystreets/goconvey v1.6.4 // indirect
)
