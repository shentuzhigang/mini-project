package main

import (
	"github.com/go-playground/validator/v10"
	"github.com/kataras/iris/v12"
)

func main() {
	app := iris.New()
	app.Validator = validator.New()
	app.Post("/user", profile)
	err := app.Listen(":8080")
	if err != nil {
		return
	}
}

//{"name" : "Marx", "age" : 202}
func profile(ctx iris.Context) {
	var user User
	err := ctx.ReadJSON(&user)
	if err != nil {
		_, err := ctx.JSON("error")
		if err != nil {
			return
		}
		return
	}

	_, err = ctx.JSON("ok")
	if err != nil {
		return
	}
}

type User struct {
	Name string `json:"name" validate:"required"`
	Age  uint8  `json:"age" validate:"required,gte=0,lte=130"`
}
