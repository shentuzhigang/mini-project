package artifactory

import (
	"encoding/json"
	"fmt"
	"github.com/jfrog/jfrog-client-go/artifactory"
	"github.com/jfrog/jfrog-client-go/artifactory/auth"
	"github.com/jfrog/jfrog-client-go/config"
	"github.com/jfrog/jfrog-client-go/utils/log"
	"io/ioutil"
	"os"
	"testing"
	"time"
)

type Range struct {
	StartPos int `json:"start_pos"`
	EndPos   int `json:"end_pos"`
	Total    int `json:"total"`
}

type Result struct {
	Repo string `json:"repo"`
	Name string `json:"name"`
}

type AqlResponse struct {
	Results []Result `json:"results"`
	Range   Range    `json:"range"`
}

func TestAQLAndJSON(t *testing.T) {
	var file *os.File
	log.SetLogger(log.NewLogger(log.INFO, file))
	rtDetails := auth.NewArtifactoryDetails()
	rtDetails.SetUrl("http://192.168.0.112:8081/artifactory/")
	rtDetails.SetUser("admin")
	rtDetails.SetPassword("123456")

	serviceConfig, err := config.NewConfigBuilder().
		SetServiceDetails(rtDetails).
		// Optionally overwrite the default HTTP timeout, which is set to 30 seconds.
		SetHttpTimeout(180 * time.Second).
		// Optionally overwrite the default HTTP retries, which is set to 3.
		SetHttpRetries(8).
		Build()
	if err != nil {
		fmt.Println(err)
	}
	rtManager, err := artifactory.New(serviceConfig)

	reader, err := rtManager.Aql("items.find()")
	if err != nil {
		t.Fail()
		return
	}

	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Fail()
		return
	}
	res := &AqlResponse{}
	err = json.Unmarshal(bytes, res)
	if err != nil {
		t.Fail()
		return
	}

	fmt.Println(res)
}
