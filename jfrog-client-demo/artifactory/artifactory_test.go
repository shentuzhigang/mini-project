package artifactory

import (
	"fmt"
	"github.com/jfrog/jfrog-client-go/artifactory"
	"github.com/jfrog/jfrog-client-go/artifactory/auth"
	"github.com/jfrog/jfrog-client-go/config"
	"github.com/jfrog/jfrog-client-go/utils/log"
	"os"
	"testing"
	"time"
)

func TestConnect(t *testing.T) {
	var file *os.File
	log.SetLogger(log.NewLogger(log.INFO, file))
	rtDetails := auth.NewArtifactoryDetails()
	rtDetails.SetUrl("http://192.168.0.112:8081/artifactory/")
	rtDetails.SetUser("admin")
	rtDetails.SetPassword("123456")

	serviceConfig, err := config.NewConfigBuilder().
		SetServiceDetails(rtDetails).
		// Optionally overwrite the default HTTP timeout, which is set to 30 seconds.
		SetHttpTimeout(180 * time.Second).
		// Optionally overwrite the default HTTP retries, which is set to 3.
		SetHttpRetries(8).
		Build()
	if err != nil {
		fmt.Println(err)
	}
	rtManager, err := artifactory.New(serviceConfig)

	ping, err := rtManager.Ping()
	if err != nil {
		return
	}
	fmt.Printf("artifactory connect is %s\n", string(ping))
}
