package io.shentuzhigang.demo.jni;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-04-03 20:57
 */
public class JNIHelloWorld {
    public native String sayHello();

    public static void main(String[] args) {
        new JNIHelloWorld().sayHello();
    }
}
