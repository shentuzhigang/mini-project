package io.shentuzhigang.demo.simbot;

import com.forte.component.forcoolqhttpapi.CoolQHttpApp;
import com.forte.component.forcoolqhttpapi.CoolQHttpConfiguration;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-06-14 15:49
 */
public class CoolQHttpAppImpl implements CoolQHttpApp {
    @Override
    public void before(CoolQHttpConfiguration configuration) {
        configuration.setIp("127.0.0.1");
        configuration.setJavaPort(15514);
        configuration.setServerPort(5700);
// 默认为一个斜杠"/"
        configuration.setServerPath("coolq");
    }

    @Override
    public void after(CQCodeUtil cqCodeUtil, MsgSender sender) {

    }
}
