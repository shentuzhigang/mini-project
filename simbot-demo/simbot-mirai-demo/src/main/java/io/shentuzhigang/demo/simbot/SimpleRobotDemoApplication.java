package io.shentuzhigang.demo.simbot;

import com.forte.qqrobot.BaseApplication;
import com.forte.qqrobot.SimpleRobotApplication;

import java.io.IOException;

/**
 * @author ShenTuZhiGang
 */
@SimpleRobotApplication
public class SimpleRobotDemoApplication {

    public static void main(String[] args) throws IOException {
        BaseApplication.runAuto(SimpleRobotDemoApplication.class,args);
    }

}
