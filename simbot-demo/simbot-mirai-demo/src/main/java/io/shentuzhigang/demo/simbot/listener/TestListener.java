package io.shentuzhigang.demo.simbot.listener;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.anno.depend.Beans;
import com.forte.qqrobot.beans.messages.msgget.PrivateMsg;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.sender.MsgSender;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-06-14 15:53
 */
@Beans
public class TestListener {

    @Listen(MsgGetTypes.privateMsg)
    @Filter("hello.*")
    public void testListen1(PrivateMsg msg, MsgSender sender) {
        System.out.println(msg);
        // 以下三种方法均可，效果相同
        System.out.println(sender.SENDER.sendPrivateMsg(msg, msg.getMsg()));
//        sender.SENDER.sendPrivateMsg(msg.getQQ(), msg.getMsg());
//        sender.SENDER.sendPrivateMsg(msg.getQQCode(), msg.getMsg());
    }

}
