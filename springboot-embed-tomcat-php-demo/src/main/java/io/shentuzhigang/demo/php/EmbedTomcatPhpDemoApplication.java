package io.shentuzhigang.demo.php;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-06 20:01
 */
@SpringBootApplication
public class EmbedTomcatPhpDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmbedTomcatPhpDemoApplication.class, args);
    }

}
