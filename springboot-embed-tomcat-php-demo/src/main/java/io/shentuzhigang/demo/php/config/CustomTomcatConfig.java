package io.shentuzhigang.demo.php.config;

import org.apache.catalina.Wrapper;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.Jsp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ShenTuZhiGang
 * @version 1.1.0
 * @date 2020-11-02 22:42
 */
@Configuration
public class CustomTomcatConfig {
    @Bean
    public WebServerFactoryCustomizer webServerFactoryCustomizer() {
        return (WebServerFactoryCustomizer<TomcatServletWebServerFactory>) factory ->
                factory.addContextCustomizers(context -> {
                    Jsp jsp = new Jsp();
                    jsp.setClassName("org.apache.catalina.servlets.DefaultServlet");
                    Wrapper jspServlet = context.createWrapper();
                    jspServlet.setName("jsp");
                    jspServlet.setServletClass(jsp.getClassName());
                    jspServlet.addInitParameter("fork", "false");
                    jsp.getInitParameters().forEach(jspServlet::addInitParameter);
                    jspServlet.setLoadOnStartup(3);
                    context.addChild(jspServlet);
                    context.addServletMappingDecoded("*.jsp", "jsp");
                    context.addServletMappingDecoded("*.jspx", "jsp");
                    context.setPrivileged(true);

                    Php php = new Php();
                    php.setClassName("org.apache.catalina.servlets.CGIServlet");
                    Wrapper phpServlet = context.createWrapper();
                    phpServlet.setName("php");

                    phpServlet.setServletClass(php.getClassName());
                    phpServlet.addInitParameter("fork", "false");
                    php.getInitParameters().forEach(phpServlet::addInitParameter);
                    phpServlet.setLoadOnStartup(4);
                    context.addChild(phpServlet);
                    context.addServletMappingDecoded("*.php", "php");
                    context.addServletMappingDecoded("*.phpx", "php");
                });
    }
    public class Php {

        /**
         * Class name of the servlet to use for JSPs. If registered is true and this class is
         * on the classpath then it will be registered.
         */
        private String className = "org.apache.catalina.servlets.CGIServlet";

        private Map<String, String> initParameters = new HashMap<>();

        /**
         * Whether the JSP servlet is registered.
         */
        private boolean registered = true;

        public Php() {
            this.initParameters.put("development", "false");
            this.initParameters.put("clientInputTimeout","200");
            this.initParameters.put("debug","0");
            this.initParameters.put("executable","D:\\ext\\php\\php-cgi.exe");
            this.initParameters.put("passShellEnvironment","true");
            this.initParameters.put("cgiPathPrefix","");
        }

        /**
         * Return the class name of the servlet to use for JSPs. If {@link #getRegistered()
         * registered} is {@code true} and this class is on the classpath then it will be
         * registered.
         * @return the class name of the servlet to use for JSPs
         */
        public String getClassName() {
            return this.className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        /**
         * Return the init parameters used to configure the JSP servlet.
         * @return the init parameters
         */
        public Map<String, String> getInitParameters() {
            return this.initParameters;
        }

        public void setInitParameters(Map<String, String> initParameters) {
            this.initParameters = initParameters;
        }

        /**
         * Return whether the JSP servlet is registered.
         * @return {@code true} to register the JSP servlet
         */
        public boolean getRegistered() {
            return this.registered;
        }

        public void setRegistered(boolean registered) {
            this.registered = registered;
        }

    }

}
