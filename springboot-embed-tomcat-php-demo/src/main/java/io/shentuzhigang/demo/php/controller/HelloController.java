package io.shentuzhigang.demo.php.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-06 20:36
 */
@Controller
public class HelloController {
    @RequestMapping("/hello/jsp")
    public String jsp(){
        return "/index.jsp";
    }

    @RequestMapping("/hello/php")
    public String php(){
        return "/index.php";
    }
}
