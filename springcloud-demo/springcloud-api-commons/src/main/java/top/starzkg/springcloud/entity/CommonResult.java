package top.starzkg.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 12:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult {
    private Integer code;
    private String message;
    private Object data;
    public  CommonResult(Integer code, String message){
        this(code,message,null);
    }
}
