package top.starzkg.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-16 19:21
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableEurekaClient
@EnableFeignClients
public class OrderFeignMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderFeignMainApplication.class,args);
    }
}
