package top.starzkg.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 01:21
 */
@SpringBootApplication
@EnableEurekaClient
public class PaymentMain8001Application {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain8001Application.class,args);
    }
}
