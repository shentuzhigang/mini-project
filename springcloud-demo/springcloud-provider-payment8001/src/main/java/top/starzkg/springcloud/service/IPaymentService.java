package top.starzkg.springcloud.service;

import top.starzkg.springcloud.entity.Payment;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 13:08
 */
public interface IPaymentService {
    int create(Payment payment);
    Payment getPaymentById(Long id);
}
