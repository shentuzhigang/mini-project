package top.starzkg.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.starzkg.springcloud.entity.CommonResult;
import top.starzkg.springcloud.entity.Payment;
import top.starzkg.springcloud.service.IPaymentService;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 13:12
 */
@Slf4j
@RestController
public class PaymentController {
    @Autowired
    private IPaymentService iPaymentService;
    @Autowired
    private DiscoveryClient discoveryClient;

    @Value("${server.port}")
    private Integer port;
    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment){
        log.info("参数："+payment);
        int result = iPaymentService.create(payment);
        log.info("结果"+result);
        if(result > 0){
            return new CommonResult(200,"成功:"+port,result);
        }else {
            return new CommonResult(500,"失败",null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable(name = "id") Long id){
        Payment result = iPaymentService.getPaymentById(id);
        log.info("结果"+result);
        if(result != null){
            return new CommonResult(200,"成功:"+port,result);
        }else {
            return new CommonResult(500,"失败",null);
        }
    }


    @GetMapping(value = "/payment/dicovery")
    public Object discovery(){
        List<String> servies = discoveryClient.getServices();
        for (String element:servies) {
            log.info("****element:"+element);
        }
        List<ServiceInstance> instances = discoveryClient.getInstances("SPRINGCLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance:instances){
            log.info(instance.getServiceId() + "\t"
                    + instance.getHost() + "\t"
                    + instance.getPort() + "\t"
                    + instance.getUri());
        }
        return discoveryClient;
    }

}
