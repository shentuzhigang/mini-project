package io.shentuzhigang.test.blackbox;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-13 14:59
 */
@RunWith(Parameterized.class)
public class DateDecisionTests {
    private int input1;
    private int input2;
    private int input3;
    private String expected;

    @Parameterized.Parameters
    public static Collection<?> prepareData(){
        Object [][] object = {
                {2019, 9, 16, "20190917"},
                {2019, 9, 30, "20191001"},
                {2019, 9, 31, "日的值不在指定范围内"},
                {2019, 1, 16, "20190117"},
                {2019, 1, 31, "20190201"},
                {2019, 12, 16, "20191217"},
                {2019, 12, 31, "20200101"},
                {2019, 2, 16, "20190217"},   //7
                {2019, 2, 28, "20190301"},    //8
                {2020, 2, 28, "20200229"},
                {2020, 2, 29, "20200301"},
                {2019, 2, 29, "日的值不在指定范围内"},   //11
                {2019, 2, 30, "日的值不在指定范围内"},
        };
        return Arrays.asList(object);
    }
    public DateDecisionTests(int input1,int input2,int input3,String expected){
        this.input1 = input1;
        this.input2 = input2;
        this.input3 = input3;
        this.expected = expected;

    }
    @Test
    public void testDate(){
        String result = Exp2.nextDate(input1,input2,input3);
        Assert.assertEquals(expected, result);
    }
}
