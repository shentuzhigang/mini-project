package io.shentuzhigang.test.phone;

import java.util.regex.Pattern;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-12-24 16:24
 */
public class PhoneNumber {
    private static Pattern pattern = Pattern.compile("^[\\d]*$");

    public static boolean check(String s) {

        if (pattern.matcher(s).matches()) {
            if (s.length() == 3) {
                return s.startsWith("1");
            } else if (s.length() == 8) {
                return s.startsWith("0");
            } else if (s.length() == 11) {
                return s.startsWith("1") || s.startsWith("0");
            } else if (s.length() == 12) {
                return s.startsWith("0");
            }
        }
        return false;
    }
}
