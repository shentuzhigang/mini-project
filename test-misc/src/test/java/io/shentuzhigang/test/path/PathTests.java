package io.shentuzhigang.test.path;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-12-24 19:00
 */
public class PathTests {
    @Test
    public void t() {
        Assertions.assertEquals(Path.Coverage(9, 8, 7), new Point(12, 18));
        Assertions.assertEquals(Path.Coverage(9, 7, 8), new Point(6, 7));
        Assertions.assertEquals(Path.Coverage(10, 5, 5), new Point(6, 7));
        Assertions.assertEquals(Path.Coverage(4, 3, 20), new Point(5, 7));
        Assertions.assertEquals(Path.Coverage(23, 21, 20), new Point(12, 15));
        Assertions.assertEquals(Path.Coverage(10, 9, 8), new Point(12, 18));
        Assertions.assertEquals(Path.Coverage(6, 8, 10), new Point(6, 7));
        Assertions.assertEquals(Path.Coverage(1, 5, 9), new Point(6, 7));
        Assertions.assertEquals(Path.Coverage(1, 5, 10), new Point(5, 7));
    }
}
