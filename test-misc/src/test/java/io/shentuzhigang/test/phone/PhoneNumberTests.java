package io.shentuzhigang.test.phone;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-12-24 17:06
 */
@Testable
public class PhoneNumberTests {
    @Test
    public void testCheckPhoneNumber1() {
        Assertions.assertTrue(PhoneNumber.check("110"), "电话号码合法");
    }

    @Test
    public void testCheckPhoneNumber2() {
        Assertions.assertFalse(PhoneNumber.check("222"), "电话号码不合法");
    }

    @Test
    public void testCheckPhoneNumber3() {
        Assertions.assertTrue(PhoneNumber.check("01111111"), "电话号码合法");
    }

    @Test
    public void testCheckPhoneNumber4() {
        Assertions.assertFalse(PhoneNumber.check("11111111"), "电话号码不合法");
    }

    @Test
    public void testCheckPhoneNumber5() {
        Assertions.assertTrue(PhoneNumber.check("11111111111"), "电话号码合法");
    }

    @Test
    public void testCheckPhoneNumber6() {
        Assertions.assertTrue(PhoneNumber.check("01111111111"), "电话号码合法");
    }

    @Test
    public void testCheckPhoneNumber7() {
        Assertions.assertFalse(PhoneNumber.check("1111111a110"), "电话号码不合法");
    }

    @Test
    public void testCheckPhoneNumber8() {
        Assertions.assertFalse(PhoneNumber.check("01111111-110"), "电话号码不合法");
    }

    @Test
    public void testCheckPhoneNumber9() {
        Assertions.assertTrue(PhoneNumber.check("011111111111"), "电话号码合法");
    }

    @Test
    public void testCheckPhoneNumber10() {
        Assertions.assertTrue(PhoneNumber.check("011111111111"), "电话号码合法");
    }

    @Test
    public void testCheckPhoneNumber11() {
        Assertions.assertFalse(PhoneNumber.check("1"), "电话号码不合法");
    }

    @Test
    public void testCheckPhoneNumber12() {
        Assertions.assertFalse(PhoneNumber.check("1111111"), "电话号码不合法");
    }

    @Test
    public void testCheckPhoneNumber13() {
        Assertions.assertFalse(PhoneNumber.check("101011a0"), "电话号码不合法");
    }
}
