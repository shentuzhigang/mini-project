package io.shentuzhigang.test.shape;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-12-24 13:51
 */
public class RectTests {
    @Test
    public void testCreate() {
        Rect r = new Rect();
        assertNotEquals(r, null);
        Point2d p = r.getTopLeftPoint();
        assertEquals(r.getWidth(), 1.0, 1.0e-6);
        assertEquals(r.getHeight(), 1.0, 1.0e-6);
    }

}
