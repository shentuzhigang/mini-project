package io.shentuzhigang.test.whitebox;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-04-06 17:03
 */
public class Exp1 {

    public Exp1() {
    }

    public int doWork(int x, int y, int z) {
        int k = 0, j = 0;
        if ((x > 3) && (z < 10)) {
            k = x * y - 1;
            j = (int) Math.sqrt(k);
        }
        if ((x == 4) || (y > 5)) {
            j = x * y + 10;
        }
        j = j % 3;
        return j;
    }

    public int div(int x, int y) {
        return x / y;
    }

}