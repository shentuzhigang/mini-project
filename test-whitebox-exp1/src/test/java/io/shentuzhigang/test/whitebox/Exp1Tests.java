package io.shentuzhigang.test.whitebox;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-04-06 17:02
 */
public class Exp1Tests {

    private Exp1 exp1;

    public Exp1Tests() {
        exp1 = new Exp1();
    }

    @Test
    public void testDoWork() {
        // 语句覆盖测试
        assertThat(exp1.doWork(4, 6, 9), is(1));
        // 判定覆盖测试
        assertThat(exp1.doWork(5, 4, 9), is(1));
        assertThat(exp1.doWork(4, 4, 11), is(2));
        // 条件覆盖测试
        assertThat(exp1.doWork(4, 6, 11), is(1));
        assertThat(exp1.doWork(2, 4, 9), is(0));
        // 路径覆盖测试
        assertThat(exp1.doWork(4, 6, 9), is(1));
        assertThat(exp1.doWork(5, 4, 9), is(1));
        assertThat(exp1.doWork(4, 4, 11), is(2));
        assertThat(exp1.doWork(2, 4, 9), is(0));
    }

}

