package io.shentuzhigang.test.whitebox;

/**
 * 程序要求：10个铅球中有一个假球（比其他铅球的重量要轻），用天平三次称出假球。
 * 程序设计思路：
 * 第一次使用天平分别称5个球，判断轻的一边有假球；拿出轻的5个球，
 * 取出其中4个第二次称，两边分别放2个球：
 * 如果两边同重，则剩下的球为假球；
 * 若两边不同重，拿出轻的两个球称第三次，轻的为假球。
 */
public class SearchBall {
    private static int x[]=new int[10];
    public SearchBall(){}
    public void setBWeight(int w[]){
        for(int i=0;i<w.length;i++){
            x[i]=w[i];
        }
    }
    public String BeginSearch(){
        if(x[0]+x[1]+x[2]+x[3]+x[4]<x[5]+x[6]+x[7]+x[8]+x[9]){
            if(x[1]+x[2]==x[3]+x[4]){
                return "1号是假球";
            }
            if(x[1]+x[2]<x[3]+x[4]){
                if (x[1]<x[2]) {
                    return "2号是假球";
                }else {
                    return "3号是假球";
                }
            }else {
                if (x[3]<x[4]){
                    return "4号是假球";
                }
                else{
                    return "5号是假球";
                }
            }
        }else {
            if(x[6]+x[7]==x[8]+x[9]){
                return "6号是假球";
            }
            if(x[6]+x[7]<x[8]+x[9]) {
                if (x[6]<x[7]) {
                    return "7号是假球";
                }else {
                    return "8号是假球";
                }
            }else {
                if (x[8]<x[9]) {
                    return "9号是假球";
                }else {
                    return "10号是假球";
                }
            }
        }
    }

}
