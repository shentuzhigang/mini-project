package io.shentuzhigang.test.whitebox;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-05-29 23:38
 */
public class SearchBallTests {
    @Test
    public void test1() {
        SearchBall obj = new SearchBall();
        int[] input;
        int ballIndex;
        // 遍历测试各个基本路径
        for (int i = 0; i < 10; ++i) {
            // 生成用例输入
            input = new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
            input[i] = 1;
            obj.setBWeight(input);
            // 测试用例输出
            ballIndex = i + 1;
            assertEquals(ballIndex + "号是假球", obj.BeginSearch());
        }
    }
}
