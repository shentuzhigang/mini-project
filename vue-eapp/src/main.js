import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import '@/stylus/index.styl'
import '@/styles/index.scss' // global css


const app = createApp(App)
app.config.globalProperties.$http = axios;
app.use(store).use(router).mount('#app')
