import {createStore} from 'vuex'

export default createStore({
  state: {
    foods: [],
    ballEle: {}
  },
  mutations: {
    changeFoods(state, payload) {
      state.foods = payload.foodList;
    },
    initBallEle(state, payload) {
      state.ballEle = payload.el;
    }
  },
  actions: {},
  modules: {}
})
