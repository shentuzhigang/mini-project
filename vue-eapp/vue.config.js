'use strict'
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  devServer:{ /* 设置为0.0.0.0则所有的地址均能访问 */
    host: '0.0.0.0',
    port: 8080,
    open: true,
    before: require('./mock/mock-server')
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  css: {
    loaderOptions: {
      stylus: {
        import: "~@/stylus/mixin.styl"
        // import: ["~@/assets/variable.styl", "~@/assets/variable2.styl"]
      }
    }
  }
}
