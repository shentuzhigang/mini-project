package dao

import (
	"fmt"
	"xorm.io/xorm/schemas"
	"xorm_demo/datasource"
	"xorm_demo/models"
)

func getDBMetas() []*schemas.Table {
	metas, err := datasource.Instance().DBMetas()
	if err != nil {
		return nil
	}
	return metas
}

func getTableInfo() *schemas.Table {
	info, err := datasource.Instance().TableInfo(models.Movie{})
	if err != nil {
		return nil
	}
	return info
}

func InsertMovie(movie *models.Movie) bool {
	num, err := datasource.Instance().InsertOne(movie)
	if err != nil {
		fmt.Println(err)
		return false
	}
	b := num == 0
	return b
}

func UpdateMovie(movie *models.Movie) bool {
	num, err := datasource.Instance().ID(movie.Id).Cols("name").Update(movie)
	if err != nil {
		fmt.Println(err)
		return false
	}
	b := num == 0
	return b
}

func DeleteMovie(movie *models.Movie) bool {
	num, err := datasource.Instance().Delete(movie)
	if err != nil {
		fmt.Println(err)
		return false
	}
	b := num == 0
	return b
}
