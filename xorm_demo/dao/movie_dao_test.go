package dao

import (
	"encoding/json"
	"fmt"
	"testing"
	"xorm_demo/models"
)

func TestGetDBMetas(t *testing.T) {
	bytes, err := json.Marshal(getDBMetas())
	if err != nil {
		return
	}
	fmt.Println(string(bytes))
}

func TestGetTableInfo(t *testing.T) {
	fmt.Println(getTableInfo())
}

func TestInsertMovie(t *testing.T) {
	movie := &models.Movie{
		Id:     5,
		Name:   "5",
		Poster: "5",
		Year:   5,
	}
	InsertMovie(movie)
}

func TestUpdateMovie(t *testing.T) {
	movie := &models.Movie{
		Id:     5,
		Name:   "5",
		Poster: "5",
		Year:   5,
	}
	UpdateMovie(movie)
}

func TestVersion(t *testing.T) {
	movie := &models.Movie{
		Id:     6,
		Name:   "5",
		Poster: "5",
		Year:   5,
	}
	InsertMovie(movie)
	movie1 := &models.Movie{
		Id:     6,
		Name:   "6",
		Poster: "5",
		Year:   5,
	}
	UpdateMovie(movie1)
}

func TestDeleteMovie(t *testing.T) {
	movie := &models.Movie{
		Id: 6,
	}
	DeleteMovie(movie)
}
