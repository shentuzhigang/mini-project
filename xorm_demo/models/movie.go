package models

import "time"

type Movie struct {
	Id        int       `json:"id" xorm:"not null pk INT"`
	Name      string    `json:"name" xorm:"VARCHAR(255)"`
	Year      int       `json:"year" xorm:"INT"`
	Poster    string    `json:"poster" xorm:"VARCHAR(255)"`
	CreatedAt time.Time `xorm:"created"`
	UpdatedAt time.Time `xorm:"updated"`
	Version   int       `xorm:"version"`
	DeletedAt time.Time `xorm:"deleted"`
}
